

using Backend.Tenants;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class UpdateTenantFunctionalTests
    {

        private ITenantService Service = new TenantServiceImpl();
        
        [Fact]
        public async void UpdateWithValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            request.id = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "Armis";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithEmptyRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            request.id = "Nope";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            request.id = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "1";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithValidRequestButUnknownShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            request.id = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "Armis";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithValidRequestButNoCommitShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            request.id = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "Armis";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
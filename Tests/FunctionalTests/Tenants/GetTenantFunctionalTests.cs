

using Backend.Tenants;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class GetTenantFunctionalTests
    {

        private ITenantService Service = new TenantServiceImpl();
        
        [Fact]
        public async void GetWithValidRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new TenantDTOFactoryImpl();
            var accountFactory = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultTenantServices services = new ConsultTenantServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetTenantRequest request = new GetTenantRequest();
            request.id = "075ea146-94bb-4317-85f7-b056002b012d";
            GetOneResults<TenantDTO> results = await Service.GetOne(services, request);
            Assert.True(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void GetWithEmptyRequestShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new TenantDTOFactoryImpl();
            var accountFactory = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultTenantServices services = new ConsultTenantServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetTenantRequest request = new GetTenantRequest();
            GetOneResults<TenantDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetWithInvalidRequestShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new TenantDTOFactoryImpl();
            var accountFactory = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultTenantServices services = new ConsultTenantServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetTenantRequest request = new GetTenantRequest();
            request.id = "Nope";
            GetOneResults<TenantDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetWithValidRequestUnknownTenantShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new TenantDTOFactoryImpl();
            var accountFactory = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "F", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultTenantServices services = new ConsultTenantServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetTenantRequest request = new GetTenantRequest();
            request.id = "075ea146-94bb-4317-85f7-b056002b012d";
            GetOneResults<TenantDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetPageWithValidRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new TenantDTOFactoryImpl();
            var accountFactory = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultTenantServices services = new ConsultTenantServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultTenantsParameters request = new ConsultTenantsParameters();
            request.page = 1;
            request.size = 24;
            request.sort = "date";
            request.order = "asc";
            request.filters = new string[] {"dateBiggerThan2020"};
            GetPageResults<TenantDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void GetPageWithEmptyRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new TenantDTOFactoryImpl();
            var accountFactory = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultTenantServices services = new ConsultTenantServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultTenantsParameters request = new ConsultTenantsParameters();
            GetPageResults<TenantDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void GetPageWithErrorShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new TenantDTOFactoryImpl();
            var accountFactory = new TenantFactoryImpl();
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultTenantServices services = new ConsultTenantServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultTenantsParameters request = new ConsultTenantsParameters();
            GetPageResults<TenantDTO> results = await Service.GetPage(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
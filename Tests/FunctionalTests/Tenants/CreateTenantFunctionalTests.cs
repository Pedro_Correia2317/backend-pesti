
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Tenants;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class CreateTenantFunctionalTests
    {

        private ITenantService Service = new TenantServiceImpl();

        [Fact]
        public async void FullValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "Armis";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MinimunValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "Armis";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.name = "Armis";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void EmptyRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(2, results.notifications.Length);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "Nope";
            request.name = "Armis";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "1";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "Armis";
            request.entryDescription = "";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void NullDataRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = null;
            request.name = "Armis";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void NullOptionalDataRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "Armis";
            request.entryDescription = null;
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void ValidDataRequestButWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "Armis";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithoutSavingTenantShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "Armis";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithoutSavingEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "Armis";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithUnknownAccountShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new TenantDTOFactoryImpl();
            var licenseFactoryMock = new TenantFactoryImpl();
            var licenseRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, entryFactoryMock, licenseRepoMock, accountRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            request.owner = "075ea146-94bb-4317-85f7-b056002b012d";
            request.name = "Armis";
            request.entryDescription = "Basic Description";
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
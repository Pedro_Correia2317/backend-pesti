

using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class CreateLicensePackFunctionalTests
    {

        private ILicenseService Service = new LicenseServiceImpl();

        [Fact]
        public async void FullValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.duration = "P4Y";
            license1.licenseType = "Basic";
            license1.numberMeetings = 3000;
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.duration = "P3Y";
            license2.licenseType = "Professional";
            license2.numberMeetings = 2000;
            license2.quantity = 3;
            var license3 = new LicenseInCreatePack();
            license3.duration = "P2Y4M";
            license3.licenseType = "Normal";
            license3.numberMeetings = 2500;
            license3.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2, license3 };
            request.entryDescription = "Basic Description";
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.True(results.success);
            Assert.Equal(308, results.result.Length);
        }

        [Fact]
        public async void MinimumValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            request.licenses = new LicenseInCreatePack[] { license1 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.True(results.success);
            Assert.Equal(300, results.result.Length);
        }

        [Fact]
        public async void MixedValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.True(results.success);
            Assert.Equal(305, results.result.Length);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            //request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            //request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            //license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail4()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            //license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail5()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            //license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void NullFieldRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = null;
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void NullFieldRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = null;
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidRequestWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidRequestWithoutSavingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "F", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidRequestWithoutSavingEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidRequestWithUnknownTenantEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryImpl();
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "F", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            var requestAnalyser = new RequestAnalyserImpl();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalyser);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            request.tenant = "075ea146-94bb-4317-85f7-b056002b012d";
            var license1 = new LicenseInCreatePack();
            license1.licenseType = "Trial";
            license1.quantity = 300;
            var license2 = new LicenseInCreatePack();
            license2.licenseType = "Professional";
            license2.numberMeetings = 2500;
            license2.quantity = 5;
            request.licenses = new LicenseInCreatePack[] { license1, license2 };
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
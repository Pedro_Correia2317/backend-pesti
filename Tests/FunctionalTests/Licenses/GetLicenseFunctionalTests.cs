

using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class GetLicenseFunctionalTests
    {

        private ILicenseService Service = new LicenseServiceImpl();
        
        [Fact]
        public async void GetWithValidRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new LicenseDTOFactoryImpl();
            var accountFactory = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetLicenseRequest request = new GetLicenseRequest();
            request.id = "075ea146-94bb-4317-85f7-b056002b012d";
            GetOneResults<LicenseDTO> results = await Service.GetOne(services, request);
            Assert.True(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void GetWithEmptyRequestShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new LicenseDTOFactoryImpl();
            var accountFactory = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetLicenseRequest request = new GetLicenseRequest();
            GetOneResults<LicenseDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetWithInvalidRequestShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new LicenseDTOFactoryImpl();
            var accountFactory = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetLicenseRequest request = new GetLicenseRequest();
            request.id = "Nope";
            GetOneResults<LicenseDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetWithValidRequestUnknownLicenseShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new LicenseDTOFactoryImpl();
            var accountFactory = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetLicenseRequest request = new GetLicenseRequest();
            request.id = "075ea146-94bb-4317-85f7-b056002b012d";
            GetOneResults<LicenseDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetPageWithValidRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new LicenseDTOFactoryImpl();
            var accountFactory = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultLicensesParameters request = new ConsultLicensesParameters();
            request.page = 1;
            request.size = 24;
            request.sort = "date";
            request.order = "asc";
            request.filters = new string[] {"dateBiggerThan2020"};
            GetPageResults<LicenseDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void GetPageWithEmptyRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new LicenseDTOFactoryImpl();
            var accountFactory = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultLicensesParameters request = new ConsultLicensesParameters();
            GetPageResults<LicenseDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void GetPageWithErrorShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new LicenseDTOFactoryImpl();
            var accountFactory = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "F", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultLicensesParameters request = new ConsultLicensesParameters();
            GetPageResults<LicenseDTO> results = await Service.GetPage(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
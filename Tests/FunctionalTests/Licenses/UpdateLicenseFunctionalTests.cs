

using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class UpdateLicenseFunctionalTests
    {

        private ILicenseService Service = new LicenseServiceImpl();
        
        [Fact]
        public async void UpdateWithValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            request.remainingMeetings = 2499;
            request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            //request.remainingMeetings = 2499;
            //request.duration = "P5Y";
            //request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            //request.numberMeetings = 2500;
            request.remainingMeetings = 5;
            //request.duration = "P5Y";
            //request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            //request.numberMeetings = 2500;
            //request.remainingMeetings = 2499;
            request.duration = "P5Y";
            //request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass4()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            //request.numberMeetings = 2500;
            //request.remainingMeetings = 2499;
            //request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass5()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            request.remainingMeetings = 2499;
            //request.duration = "P5Y";
            //request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass6()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            //request.remainingMeetings = 2499;
            request.duration = "P5Y";
            //request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass7()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            //request.remainingMeetings = 2499;
            //request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass8()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            //request.numberMeetings = 2500;
            request.remainingMeetings = 5;
            request.duration = "P5Y";
            //request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass9()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            //request.numberMeetings = 2500;
            request.remainingMeetings = 5;
            //request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass10()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            //request.numberMeetings = 2500;
            //request.remainingMeetings = 2499;
            request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass11()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            request.remainingMeetings = 2499;
            request.duration = "P5Y";
            //request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass12()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            request.remainingMeetings = 2499;
            //request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass13()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            //request.numberMeetings = 2500;
            request.remainingMeetings = 5;
            request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithEmptyRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "Nope";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = -1;
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.remainingMeetings = -1;
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail4()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.licenseType = "Nope";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail5()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.duration = "Nope";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail6()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.numberMeetings = 2000;
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail7()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.remainingMeetings = 2000;
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail8()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.licenseType = "Professional";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail9()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.duration = "P3YT4M";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithValidRequestButUnknownShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            request.remainingMeetings = 2499;
            request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithValidRequestButNoCommitShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            request.remainingMeetings = 2499;
            request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRemainingMeetingsShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            request.remainingMeetings = 3000;
            request.duration = "P5Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidDurationShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryImpl();
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.numberMeetings = 2500;
            request.remainingMeetings = 3000;
            request.duration = "P10Y";
            request.licenseType = "Basic";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

    }
}
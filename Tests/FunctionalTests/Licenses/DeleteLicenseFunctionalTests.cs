
using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class DeleteLicenseFunctionalTests
    {

        private ILicenseService Service = new LicenseServiceImpl();

        [Fact]
        public async void DeleteValidRequestShoulPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            request.code = "075ea146-94bb-4317-85f7-b056002b012d";
            DeleteResults results = await Service.Delete(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void DeleteEmptyRequestShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void DeleteValidRequestWithoutCommitingShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            request.code = "075ea146-94bb-4317-85f7-b056002b012d";
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.True(results.itExists);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void DeleteValidRequestWithUnknownLicenseShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            request.code = "075ea146-94bb-4317-85f7-b056002b012d";
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void DeleteValidRequestWithErrorDeletingShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "F", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            request.code = "075ea146-94bb-4317-85f7-b056002b012d";
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.True(results.itExists);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void DeleteInvalidRequestShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "F", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            request.code = "Nope";
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Single(results.notifications);
        }
    }
}
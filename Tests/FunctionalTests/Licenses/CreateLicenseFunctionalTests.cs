
using System;
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class CreateLicenseFunctionalTests
    {

        private ILicenseService Service = new LicenseServiceImpl();

        [Fact]
        public async void FullValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MinimunValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.licenseType = "Professional";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            //request.date = "01-01-2021";
            request.duration = "P4Y";
            //request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            //request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            //request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass4()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            //request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass5()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            //request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass6()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            //request.date = "01-01-2021";
            //request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass7()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            //request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            //request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass8()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            //request.duration = "P4Y";
            //request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass9()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            //request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            //request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass10()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            //request.numberMeetings = 2000;
            request.licenseType = "Basic";
            //request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass11()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            //request.date = "01-01-2021";
            //request.duration = "P4Y";
            //request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass12()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            //request.date = "01-01-2021";
            //request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            //request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass13()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            //request.date = "01-01-2021";
            request.duration = "P4Y";
            //request.numberMeetings = 2000;
            request.licenseType = "Basic";
            //request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MixedValidRequestShouldPass14()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            //request.duration = "P4Y";
            //request.numberMeetings = 2000;
            request.licenseType = "Basic";
            //request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            //request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            //request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void EmptyRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(2, results.notifications.Length);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "Nope";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "Nope";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01.01.2021";
            request.duration = "Nope";
            request.numberMeetings = 2000;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail4()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "2021/01/01";
            request.duration = "P4Y";
            request.numberMeetings = -1;
            request.licenseType = "Basic";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail5()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = @"01\01\2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Nope";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void NullDataRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = null;
            request.date = @"01\01\2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(2, results.notifications.Length);
        }

        [Fact]
        public async void NullOptionalDataRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = null;
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void NullOptionalDataRequestShouldPass2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = null;
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void NullOptionalDataRequestShouldPass3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = null;
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void ValidDataRequestButWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithoutSavingLicenseShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "F", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithoutSavingEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithUnknownAccountShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithAccountAlreadyLicensedShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryImpl();
            var licenseFactoryMock = new LicenseFactoryImpl();
            var licenseRepoMock = new LicenseRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            request.account = "075ea146-94bb-4317-85f7-b056002b012d";
            request.date = "01-01-2021";
            request.duration = "P4Y";
            request.numberMeetings = 2000;
            request.licenseType = "Professional";
            request.entryDescription = "Basic Description";
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
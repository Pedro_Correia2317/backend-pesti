

using Backend.Accounts;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class GetAccountFunctionalTests
    {

        private IAccountService Service = new AccountServiceImpl();
        
        [Fact]
        public async void GetWithValidRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new AccountDTOFactoryImpl();
            var accountFactory = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultAccountServices services = new ConsultAccountServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetAccountRequest request = new GetAccountRequest();
            request.id = "075ea146-94bb-4317-85f7-b056002b012d";
            GetOneResults<AccountDTO> results = await Service.GetOne(services, request);
            Assert.True(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void GetWithEmptyRequestShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new AccountDTOFactoryImpl();
            var accountFactory = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultAccountServices services = new ConsultAccountServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetAccountRequest request = new GetAccountRequest();
            GetOneResults<AccountDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetWithInvalidRequestShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new AccountDTOFactoryImpl();
            var accountFactory = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultAccountServices services = new ConsultAccountServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetAccountRequest request = new GetAccountRequest();
            request.id = "Nope";
            GetOneResults<AccountDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetWithValidRequestUnknownAccountShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new AccountDTOFactoryImpl();
            var accountFactory = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultAccountServices services = new ConsultAccountServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            GetAccountRequest request = new GetAccountRequest();
            request.id = "075ea146-94bb-4317-85f7-b056002b012d";
            GetOneResults<AccountDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void GetPageWithValidRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new AccountDTOFactoryImpl();
            var accountFactory = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultAccountServices services = new ConsultAccountServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultAccountsParameters request = new ConsultAccountsParameters();
            request.page = 1;
            request.size = 24;
            request.sort = "date";
            request.order = "asc";
            request.filters = new string[] {"dateBiggerThan2020"};
            GetPageResults<AccountDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void GetPageWithEmptyRequestShouldPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new AccountDTOFactoryImpl();
            var accountFactory = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultAccountServices services = new ConsultAccountServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultAccountsParameters request = new ConsultAccountsParameters();
            GetPageResults<AccountDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void GetPageWithErrorShouldFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactory = new AccountDTOFactoryImpl();
            var accountFactory = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "F", "S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            ConsultAccountServices services = new ConsultAccountServices(errorCollector, accountDTOFactory,
                accountFactory, accountRepoMock, translatorMock, logger, utilsDTO);
            ConsultAccountsParameters request = new ConsultAccountsParameters();
            GetPageResults<AccountDTO> results = await Service.GetPage(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
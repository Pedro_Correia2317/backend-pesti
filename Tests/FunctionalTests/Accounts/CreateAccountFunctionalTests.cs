
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class CreateAccountFunctionalTests
    {

        private IAccountService Service = new AccountServiceImpl();

        [Fact]
        public async void FullValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Client";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MinimunValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Client";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            //request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Client";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            //request.email = "pedro.correia@armis.pt";
            request.type = "Client";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void MissingFieldRequestShouldFail3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            //request.type = "Client";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void EmptyRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(3, results.notifications.Length);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "1";
            request.email = "pedro.correia@armis.pt";
            request.type = "Client";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "Nope";
            request.type = "Client";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Nope";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void InvalidDataRequestShouldFail4()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Client";
            request.entryDescription = "1";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void NullDataRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = null;
            request.email = "pedro.correia@armis.pt";
            request.type = "Nope";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(2, results.notifications.Length);
        }

        [Fact]
        public async void NullDataRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = null;
            request.type = "Nope";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(2, results.notifications.Length);
        }

        [Fact]
        public async void NullDataRequestShouldFail3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = null;
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void NullOptionalDataRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Client";
            request.entryDescription = null;
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void ValidDataRequestButWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Nope";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithoutSavingAccountShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "F", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Nope";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void ValidDataRequestButWithoutSavingEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryImpl();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryImpl();
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorServiceImpl();
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock, translatorMock,
                unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            request.name = "Pedro Correia";
            request.email = "pedro.correia@armis.pt";
            request.type = "Nope";
            request.entryDescription = "Basic Description";
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
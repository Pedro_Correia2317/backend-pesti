

using Backend.Accounts;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class UpdateAccountFunctionalTests
    {

        private IAccountService Service = new AccountServiceImpl();
        
        [Fact]
        public async void UpdateWithValidRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "Pedro Correia";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "Pedro Correia";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithMixedRequestShouldPass2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "Banned";
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdateWithEmptyRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            request.code = "Nope";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail2()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "N";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithInvalidRequestShouldFail3()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "1";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithValidRequestButUnknownAccountShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "Pedro Correia";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
        
        [Fact]
        public async void UpdateWithValidRequestButNoCommitShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorImpl();
            var loggerMock = new LoggerServiceImpl();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryImpl();
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryImpl();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            request.code = "1728295f-6c68-4a59-b602-8587a10b5a66";
            request.name = "Pedro Correia";
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.Single(results.notifications);
        }
    }
}
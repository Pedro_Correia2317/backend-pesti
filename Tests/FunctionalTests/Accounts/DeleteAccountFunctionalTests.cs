
using Backend.Accounts;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class DeleteAccountFunctionalTests
    {

        private IAccountService Service = new AccountServiceImpl();

        [Fact]
        public async void DeleteValidRequestShoulPass()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new AccountFactoryImpl();
            var licenseRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteAccountServices services = new DeleteAccountServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteAccountRequest request = new DeleteAccountRequest();
            request.code = "075ea146-94bb-4317-85f7-b056002b012d";
            DeleteResults results = await Service.Delete(services, request);
            Assert.True(results.success);
        }

        [Fact]
        public async void DeleteEmptyRequestShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new AccountFactoryImpl();
            var licenseRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteAccountServices services = new DeleteAccountServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteAccountRequest request = new DeleteAccountRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void DeleteValidRequestWithoutCommitingShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new AccountFactoryImpl();
            var licenseRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteAccountServices services = new DeleteAccountServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteAccountRequest request = new DeleteAccountRequest();
            request.code = "075ea146-94bb-4317-85f7-b056002b012d";
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.True(results.itExists);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void DeleteValidRequestWithUnknownAccountShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new AccountFactoryImpl();
            var licenseRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteAccountServices services = new DeleteAccountServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteAccountRequest request = new DeleteAccountRequest();
            request.code = "075ea146-94bb-4317-85f7-b056002b012d";
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void DeleteValidRequestWithErrorDeletingShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new AccountFactoryImpl();
            var licenseRepoMock = new AccountRepositoryMock("S", "S", "S", "F", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteAccountServices services = new DeleteAccountServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteAccountRequest request = new DeleteAccountRequest();
            request.code = "075ea146-94bb-4317-85f7-b056002b012d";
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.True(results.itExists);
            Assert.Single(results.notifications);
        }

        [Fact]
        public async void DeleteInvalidRequestShoulFail()
        {
            var errorCollector = new ErrorCollectorImpl();
            var logger = new LoggerServiceImpl();
            var translator = new TranslatorMock();
            var licenseFactory = new AccountFactoryImpl();
            var licenseRepoMock = new AccountRepositoryMock("S", "S", "S", "F", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTO = new UtilsDTOFactoryImpl();
            DeleteAccountServices services = new DeleteAccountServices(errorCollector, 
                licenseFactory, licenseRepoMock, translator, logger, unitMock, utilsDTO);
            DeleteAccountRequest request = new DeleteAccountRequest();
            request.code = "Nope";
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Single(results.notifications);
        }
    }
}

using Backend.Accounts;
using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class LicenseTests
    {

        [Fact]
        public void CreateValidLicenseShouldPass()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            License license = new License(id, date, dur, number, rem, type);
            Assert.NotNull(license);
        }

        [Fact]
        public void CreateValidLicenseShouldPass2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y11M30D");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId account = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            License license = new License(id, date, dur, number, rem, type, account);
            Assert.NotNull(license);
        }

        [Fact]
        public void CreateInvalidRemainingMeetingsShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2001);
            LicenseType type = LicenseTypes.Basic;
            AccountId account = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                new License(id, date, dur, number, rem, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.REMAINING_MEETINGS_EXCEEDS_ALLOWED, ex.Error);
            }
        }

        [Fact]
        public void CreateTooShortDurationShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1D");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId account = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                new License(id, date, dur, number, rem, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.DURATION_TOO_SHORT, ex.Error);
            }
        }

        [Fact]
        public void CreateTooLongDurationShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P5YT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId account = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                new License(id, date, dur, number, rem, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.DURATION_TOO_LONG, ex.Error);
            }
        }

        [Fact]
        public void CreateTooLongDurationShouldFail2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y12MT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId account = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                new License(id, date, dur, number, rem, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.DURATION_TOO_LONG, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullIdShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            try
            {
                License license = new License(null, date, dur, number, rem, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullIdShouldFail2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                License license = new License(null, date, dur, number, rem, type, id2);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullDateShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            try
            {
                License license = new License(id, null, dur, number, rem, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullDateShouldFail2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                License license = new License(id, null, dur, number, rem, type, id2);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullDurationShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            try
            {
                License license = new License(id, date, null, number, rem, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullDurationShouldFail2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                License license = new License(id, date, null, number, rem, type, id2);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullNumberShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            try
            {
                License license = new License(id, date, dur, null, rem, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullNumberShouldFail2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                License license = new License(id, date, dur, null, rem, type, id2);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullRemainingShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            try
            {
                License license = new License(id, date, dur, number, null, type);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullRemainingShouldFail2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = null;
            try
            {
                License license = new License(id, date, dur, number, null, type, id2);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullTypeShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            try
            {
                License license = new License(id, date, dur, number, rem, null);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullTypeShouldFail2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = null;
            try
            {
                License license = new License(id, date, dur, number, rem, null, id2);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void CreateLicenseWithNullAccountIdShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            try
            {
                License license = new License(id, date, dur, number, rem, type, null);
                Assert.True(false, "The license should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(LicenseErrors.NULL_DATA_IN_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void SameLicenseShouldPass()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            License license = new License(id, date, dur, number, rem, type);
            Assert.Equal(license, license);
        }

        [Fact]
        public void SameLicenseShouldPass2()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            License license = new License(id, date, dur, number, rem, type, id2);
            Assert.Equal(license, license);
        }

        [Fact]
        public void TwoEqualLicenseShouldPass()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            License license = new License(id, date, dur, number, rem, type, id2);
            License license2 = new License(id, date, dur, number, rem, type, id2);
            Assert.Equal(license, license2);
        }

        [Fact]
        public void TwoDifferentEntriesShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseId id2 = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            License license = new License(id, date, dur, number, rem, type, aId);
            License license2 = new License(id2, date, dur, number, rem, type, aId);
            Assert.NotEqual(license, license2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            License license = new License(id, date, dur, number, rem, type, id2);
            Assert.False(license.Equals(id2), "An account and an id should not be equal");
        }

        [Fact]
        public void OnlyIdShouldMatter()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDate date2 = new LicenseDate("02-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseDuration dur2 = new LicenseDuration("P3Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseNumberMeetings number2 = new LicenseNumberMeetings(2005);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseRemainingMeetings rem2 = new LicenseRemainingMeetings(2005);
            LicenseType type = LicenseTypes.Basic;
            LicenseType type2 = LicenseTypes.Normal;
            License license = new License(id, date, dur, number, rem, type);
            License license2 = new License(id, date2, dur2, number2, rem2, type2);
            Assert.Equal(license, license2);
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P4Y");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            License license = new License(id, date, dur, number, rem, type, id2);
            Assert.False(license.Equals(null), "An license and null should not be equal");
        }
    }
}
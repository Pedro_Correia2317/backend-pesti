


using System;
using Backend.Licenses;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class LicenseDurationTests
    {

        [Fact]
        public void CreateValidDurationShouldPass()
        {
            LicenseDuration duration = new LicenseDuration("P3Y");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass2()
        {
            LicenseDuration duration = new LicenseDuration("P3Y4M");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass3()
        {
            LicenseDuration duration = new LicenseDuration("P3Y4M31D");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass4()
        {
            LicenseDuration duration = new LicenseDuration("P3Y4M31DT4H");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass5()
        {
            LicenseDuration duration = new LicenseDuration("P3Y4M31DT4H30M");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass6()
        {
            LicenseDuration duration = new LicenseDuration("P3Y4M31DT4H30M45S");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass7()
        {
            LicenseDuration duration = new LicenseDuration("PT40000000S");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass8()
        {
            LicenseDuration duration = new LicenseDuration("P1DT1S");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass9()
        {
            LicenseDuration duration = new LicenseDuration("PT24H1S");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass10()
        {
            LicenseDuration duration = new LicenseDuration("P4Y364D");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass11()
        {
            LicenseDuration duration = new LicenseDuration("P1825D");
            Assert.NotNull(duration);
        }

        [Fact]
        public void CreateValidDurationShouldPass12()
        {
            LicenseDuration duration = new LicenseDuration("P4Y11M30DT23H59M59S");
            Assert.NotNull(duration);
        }

        [Fact]
        public void ValidurationNullDurationShouldFail()
        {
            try {
                new LicenseDuration(null);
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.NULL_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationEmptyDurationShouldFail()
        {
            try {
                new LicenseDuration("");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationLettersDurationShouldFail()
        {
            try {
                new LicenseDuration("OL-OQ-OP");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationLettersDurationShouldFail2()
        {
            try {
                new LicenseDuration("01-01-202E");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationInvalidSeparatorDurationShouldFail()
        {
            try {
                new LicenseDuration(@"01\01\21");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationInvalidSeparatorDurationShouldFail2()
        {
            try {
                new LicenseDuration("01-01-2021 14.30");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationInvalidSeparatorDurationShouldFail3()
        {
            try {
                new LicenseDuration("01-01-2021 14h30");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationInvalidSeparatorDurationShouldFail4()
        {
            try {
                new LicenseDuration("01-01-2021 14h30m");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationInvalidSeparatorDurationShouldFail5()
        {
            try {
                new LicenseDuration("01-01-2021 14h30min");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationInvalidSeparatorDurationShouldFail6()
        {
            try {
                new LicenseDuration("01-01-2021T14:30");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationMissingDataDurationShouldFail()
        {
            try {
                new LicenseDuration("PY4M");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationMissingDataDurationShouldFail2()
        {
            try {
                new LicenseDuration("P");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationMissingDataDurationShouldFail3()
        {
            try {
                new LicenseDuration("PT");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationMissingDataDurationShouldFail4()
        {
            try {
                new LicenseDuration("PTS");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

        [Fact]
        public void ValidurationMissingDataDurationShouldFail5()
        {
            try {
                new LicenseDuration("PTMS");
                Assert.True(false, "The duration should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.INVALID_DURATION, ex.Error);
            }
        }

    }
}
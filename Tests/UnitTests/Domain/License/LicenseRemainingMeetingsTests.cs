
using Backend.Licenses;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class LicenseRemainingMeetingsTests
    {

        [Fact]
        public void CreateValidRemainingMeetingsShouldReturnValidObject()
        {
            LicenseRemainingMeetings number = new LicenseRemainingMeetings(2000);
            Assert.NotNull(number);
        }

        [Fact]
        public void CreateValidRemainingMeetingsShouldReturnValidObject2()
        {
            LicenseRemainingMeetings number = new LicenseRemainingMeetings(100000);
            Assert.NotNull(number);
        }

        [Fact]
        public void CreateValidRemainingMeetingsShouldReturnValidObject3()
        {
            LicenseRemainingMeetings number = new LicenseRemainingMeetings(1);
            Assert.NotNull(number);
        }

        [Fact]
        public void CreateValidRemainingMeetingsShouldReturnValidObject4()
        {
            LicenseRemainingMeetings number = new LicenseRemainingMeetings(0);
            Assert.NotNull(number);
        }

        [Fact]
        public void ValidateNegativeMeetingsShouldFail()
        {
            try {
                new LicenseRemainingMeetings(-1);
                Assert.True(false, "The number should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.SMALL_REMAINING_MEETINGS, ex.Error);
            }
        }

        [Fact]
        public void ValidateHugeMeetingsShouldFail()
        {
            try {
                new LicenseRemainingMeetings(100001);
                Assert.True(false, "The number should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.HUGE_REMAINING_MEETINGS, ex.Error);
            }
        }

        [Fact]
        public void SameNameShouldPass()
        {
            LicenseRemainingMeetings id1 = new LicenseRemainingMeetings(2000);
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualNameShouldPass()
        {
            LicenseRemainingMeetings id1 = new LicenseRemainingMeetings(2000);
            LicenseRemainingMeetings id2 = new LicenseRemainingMeetings(2000);
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDifferentNameShouldFail()
        {
            LicenseRemainingMeetings id1 = new LicenseRemainingMeetings(2000);
            LicenseRemainingMeetings id2 = new LicenseRemainingMeetings(2001);
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            LicenseRemainingMeetings id1 = new LicenseRemainingMeetings(2000);
            LicenseNumberMeetings id2 = new LicenseNumberMeetings(2000);
            Assert.False(id1.Equals(id2), "An email and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            LicenseRemainingMeetings id1 = new LicenseRemainingMeetings(2000);
            Assert.False(id1.Equals(null), "An email and null should not be equal");
        }
    }
}
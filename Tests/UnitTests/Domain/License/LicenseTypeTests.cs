

using Backend.Accounts;
using Backend.Licenses;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class LicenseTypeTests
    {

        [Fact]
        public void SameTypeShouldPass()
        {
            LicenseType id1 = LicenseTypes.Basic;
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualTypeShouldPass()
        {
            LicenseType id1 = LicenseTypes.Basic;
            LicenseType id2 = LicenseTypes.Basic;
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentTypeShouldFail()
        {
            LicenseType id1 = LicenseTypes.Basic;
            LicenseType id2 = LicenseTypes.Professional;
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            LicenseType id1 = LicenseTypes.Basic;
            AccountName id2 = new AccountName("Basic");
            Assert.False(id1.Equals(id2), "A status and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            LicenseType id1 = LicenseTypes.Basic;
            Assert.False(id1.Equals(null), "A status and null should not be equal");
        }

        [Fact]
        public void GetValidTypeShouldPass()
        {
            LicenseType status = LicenseTypes.Basic;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass2()
        {
            LicenseType status = LicenseTypes.Normal;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass3()
        {
            LicenseType status = LicenseTypes.Professional;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass4()
        {
            LicenseType status = LicenseTypes.Trial;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByNameShouldPass()
        {
            LicenseType status = LicenseTypes.GetByName("Basic");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByNameShouldPass2()
        {
            LicenseType status = LicenseTypes.GetByName("Normal");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByNameShouldPass3()
        {
            LicenseType status = LicenseTypes.GetByName("Professional");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByNameShouldPass4()
        {
            LicenseType status = LicenseTypes.GetByName("Trial");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetNullTypeByNameShouldFail()
        {
            try {
                LicenseTypes.GetByName(null);
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.NULL_LICENSE_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetEmptyTypeByNameShouldFail()
        {
            try {
                LicenseTypes.GetByName("");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.UNKNOWN_LICENSE_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetInvalidTypeByNameShouldFail()
        {
            try {
                LicenseTypes.GetByName("Invalid");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.UNKNOWN_LICENSE_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetAllTypeShouldPass()
        {
            LicenseType[] statuses = LicenseTypes.GetAll();
            Assert.Equal(4, statuses.Length);
            Assert.DoesNotContain(null, statuses);
        }
    }
}
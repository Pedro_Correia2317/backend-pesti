
using Backend.Licenses;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class LicenseNumberMeetingsTests
    {

        [Fact]
        public void CreateValidNumberMeetingsShouldReturnValidObject()
        {
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            Assert.NotNull(number);
        }

        [Fact]
        public void CreateValidNumberMeetingsShouldReturnValidObject2()
        {
            LicenseNumberMeetings number = new LicenseNumberMeetings(100000);
            Assert.NotNull(number);
        }

        [Fact]
        public void CreateValidNumberMeetingsShouldReturnValidObject3()
        {
            LicenseNumberMeetings number = new LicenseNumberMeetings(1);
            Assert.NotNull(number);
        }

        [Fact]
        public void ValidateZeroMeetingsShouldFail()
        {
            try {
                new LicenseNumberMeetings(0);
                Assert.True(false, "The number should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.SMALL_NUMBER_MEETINGS, ex.Error);
            }
        }

        [Fact]
        public void ValidateNegativeMeetingsShouldFail()
        {
            try {
                new LicenseNumberMeetings(-1);
                Assert.True(false, "The number should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.SMALL_NUMBER_MEETINGS, ex.Error);
            }
        }

        [Fact]
        public void ValidateHugeMeetingsShouldFail()
        {
            try {
                new LicenseNumberMeetings(100001);
                Assert.True(false, "The number should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(LicenseErrors.HUGE_NUMBER_MEETINGS, ex.Error);
            }
        }

        [Fact]
        public void SameNameShouldPass()
        {
            LicenseNumberMeetings id1 = new LicenseNumberMeetings(2000);
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualNameShouldPass()
        {
            LicenseNumberMeetings id1 = new LicenseNumberMeetings(2000);
            LicenseNumberMeetings id2 = new LicenseNumberMeetings(2000);
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDifferentNameShouldFail()
        {
            LicenseNumberMeetings id1 = new LicenseNumberMeetings(2000);
            LicenseNumberMeetings id2 = new LicenseNumberMeetings(2001);
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            LicenseNumberMeetings id1 = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings id2 = new LicenseRemainingMeetings(2000);
            Assert.False(id1.Equals(id2), "An email and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            LicenseNumberMeetings id1 = new LicenseNumberMeetings(2000);
            Assert.False(id1.Equals(null), "An email and null should not be equal");
        }
    }
}


using Backend.Licenses;
using Xunit;

namespace Tests.Domain
{
    public class LicenseDTOFactoryTests
    {
        private ILicenseDTOFactory Factory = new LicenseDTOFactoryImpl();

        [Fact]
        public void CreateDTOWithValidLicenseShouldPass()
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            License license = new License(id, date, dur, number, rem, type);
            LicenseDTO dto = Factory.CreateBasicDTO(license);
            Assert.NotNull(dto);
        }

        [Fact]
        public void CreateDTOWithNullLicenseShouldFail()
        {
            LicenseDTO dto = Factory.CreateBasicDTO(null);
            Assert.NotNull(dto);
            Assert.NotNull(dto.id);
            Assert.NotNull(dto.date);
            Assert.NotNull(dto.duration);
            Assert.NotNull(dto.licenseType);
            Assert.NotNull(dto.account);
        }

        [Fact]
        public void CreateDTOWithNullListShouldFail()
        {
            LicenseDTO[] dto = Factory.CreateListOfBasicDTO(null);
            Assert.NotNull(dto);
            Assert.Empty(dto);
        }

        [Fact]
        public void CreateDTOWithNullListShouldFail3()
        {
            LicenseTypeDTO[] dto = Factory.CreateListTypesDTO(null);
            Assert.NotNull(dto);
            Assert.Empty(dto);
        }

        [Fact]
        public void CreateDTOWithValidTypeShouldFail()
        {
            LicenseTypeDTO dto = Factory.CreateTypeDTO(LicenseTypes.Basic);
            Assert.NotNull(dto);
        }

        [Fact]
        public void CreateDTOWithNullTypeShouldFail()
        {
            LicenseTypeDTO dto = Factory.CreateTypeDTO(null);
            Assert.NotNull(dto);
            Assert.NotNull(dto.description);
        }
    }
}
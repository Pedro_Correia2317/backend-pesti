
using Backend.Licenses;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class LicenseFactoryTests
    {

        private ILicenseFactory Factory = new LicenseFactoryImpl();

        [Fact]
        public void CreatingContainerShouldReturnValidObject()
        {
            LicenseContainer container = Factory.NewContainer();
            Assert.NotNull(container);
        }

        [Fact]
        public void CreatingValidIdShouldReturnValidObject()
        {
            Results<LicenseId> results = Factory.NewId("542a5fe9-843b-4766-846c-303d80df5ec8");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidIdShouldReturnInvalidObject()
        {
            Results<LicenseId> results = Factory.NewId(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidDateShouldReturnValidObject()
        {
            Results<LicenseDate> results = Factory.NewDate("01-01-2021");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidDateShouldReturnInvalidObject()
        {
            Results<LicenseDate> results = Factory.NewDate(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidDurationShouldReturnValidObject()
        {
            Results<LicenseDuration> results = Factory.NewDuration("P4Y");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidDurationShouldReturnInvalidObject()
        {
            Results<LicenseDuration> results = Factory.NewDuration(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidMeetingshouldReturnValidObject()
        {
            Results<LicenseNumberMeetings> results = Factory.NewMeetings(2000);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidMeetingsShouldReturnInvalidObject()
        {
            Results<LicenseNumberMeetings> results = Factory.NewMeetings(-1);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidTypeShouldReturnValidObject()
        {
            Results<LicenseType> results = Factory.NewType("Basic");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidTypeShouldReturnInvalidObject()
        {
            Results<LicenseType> results = Factory.NewType("Invalid!!");
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidRemainingShouldReturnValidObject()
        {
            Results<LicenseRemainingMeetings> results = Factory.NewRemainingMeetings(2000);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidRemainingShouldReturnInvalidObject()
        {
            Results<LicenseRemainingMeetings> results = Factory.NewRemainingMeetings(-1);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidLicenseShouldReturnValidObject()
        {
            LicenseContainer container = Factory.NewContainer();
            container.Id = Factory.NewId("cced7c93-7b83-4b7b-9e0b-eea1f2896619").Result;
            container.Date = Factory.NewDate("01-01-2021").Result;
            container.Duration = Factory.NewDuration("P4Y").Result;
            container.NumberMeetings = Factory.NewMeetings(2000).Result;
            container.RemainingMeetings = Factory.NewRemainingMeetings(2000).Result;
            container.Type = Factory.NewType("Basic").Result;
            Results<License> results = Factory.NewLicense(container);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidLicenseShouldReturnInvalidObject()
        {
            Results<License> results = Factory.NewLicense(null);
            Assert.False(results.wasExecutedSuccessfully());
        }
    }
}
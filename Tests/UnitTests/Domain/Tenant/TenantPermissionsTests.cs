

using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class TenantPermissionsTests
    {

        [Fact]
        public void SamePermissionsShouldPass()
        {
            TenantPermissions id1 = TenantPermissionList.Client;
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualPermissionsShouldPass()
        {
            TenantPermissions id1 = TenantPermissionList.Client;
            TenantPermissions id2 = TenantPermissionList.Client;
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentPermissionsShouldFail()
        {
            TenantPermissions id1 = TenantPermissionList.Client;
            TenantPermissions id2 = TenantPermissionList.Owner;
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            TenantPermissions id1 = TenantPermissionList.Client;
            TenantName id2 = new TenantName("Client");
            Assert.False(id1.Equals(id2), "A status and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            TenantPermissions id1 = TenantPermissionList.Client;
            Assert.False(id1.Equals(null), "A status and null should not be equal");
        }

        [Fact]
        public void GetValidPermissionsShouldPass()
        {
            TenantPermissions status = TenantPermissionList.Client;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidPermissionsShouldPass2()
        {
            TenantPermissions status = TenantPermissionList.Administrator;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidPermissionsShouldPass3()
        {
            TenantPermissions status = TenantPermissionList.Owner;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidPermissionsByNameShouldPass()
        {
            TenantPermissions status = TenantPermissionList.GetByName("Client");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidPermissionsByNameShouldPass2()
        {
            TenantPermissions status = TenantPermissionList.GetByName("Administrator");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidPermissionsByNameShouldPass3()
        {
            TenantPermissions status = TenantPermissionList.GetByName("Owner");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetNullPermissionsByNameShouldFail()
        {
            try {
                TenantPermissionList.GetByName(null);
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NULL_PERMISSIONS, ex.Error);
            }
        }

        [Fact]
        public void GetEmptyPermissionsByNameShouldFail()
        {
            try {
                TenantPermissionList.GetByName("");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.UNKNOWN_PERMISSIONS, ex.Error);
            }
        }

        [Fact]
        public void GetInvalidPermissionsByNameShouldFail()
        {
            try {
                TenantPermissionList.GetByName("Invalid");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.UNKNOWN_PERMISSIONS, ex.Error);
            }
        }

        [Fact]
        public void GetAllPermissionsShouldPass()
        {
            TenantPermissions[] statuses = TenantPermissionList.GetAll();
            Assert.Equal(3, statuses.Length);
            Assert.DoesNotContain(null, statuses);
        }
    }
}
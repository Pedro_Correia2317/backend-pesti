

using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class TenantNameTests
    {

        [Fact]
        public void CreateValidNameShouldPass()
        {
            TenantName name = new TenantName("Pedro Correia");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass2()
        {
            TenantName name = new TenantName("  Pedro Correia");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass3()
        {
            TenantName name = new TenantName("   Pedro Correia  ");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass4()
        {
            TenantName name = new TenantName("Pedro1Correia");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass5()
        {
            TenantName name = new TenantName("pedrocorreia");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass6()
        {
            TenantName name = new TenantName("323522");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass7()
        {
            TenantName name = new TenantName("Pedro Correia.,[]()!?áéíóú+-@");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass8()
        {
            TenantName name = new TenantName("pc");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass9()
        {
            TenantName name = new TenantName("PEDRO CORREIA");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass10()
        {
            TenantName name = new TenantName("Pedro");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass11()
        {
            TenantName name = new TenantName("pedro.correia4567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901MiguelWow");
            // 200 chars
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass12()
        {
            TenantName name = new TenantName("§╬Pedro");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass13()
        {
            TenantName name = new TenantName("§╬,.|()[]'\"?!*ªºPedro");
            Assert.NotNull(name);
        }

        [Fact]
        public void ValidateNullNameShouldFail()
        {
            try {
                new TenantName(null);
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NULL_NAME, ex.Error);
            }
        }

        [Fact]
        public void ValidateEmptyNameShouldFail()
        {
            try {
                new TenantName("");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.EMPTY_NAME, ex.Error);
            }
        }

        [Fact]
        public void ValidateOnlySpacesNameShouldFail()
        {
            try {
                new TenantName("\n\t     \v");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.EMPTY_NAME, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooShortNameShouldFail()
        {
            try {
                new TenantName("p");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NAME_TOO_SMALL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooLongNameShouldFail()
        {
            try {
                string s = "pedro Correia3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890Wowowowowo";
                new TenantName(s);
                // 201 chars
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NAME_TOO_LARGE, ex.Error);
            }
        }

        [Fact]
        public void SameNameShouldPass()
        {
            TenantName id1 = new TenantName("Pedro Correia");
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualNameShouldPass()
        {
            TenantName id1 = new TenantName("Pedro Correia");
            TenantName id2 = new TenantName("Pedro Correia");
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentNameShouldFail()
        {
            TenantName id1 = new TenantName("Pedro Correia");
            TenantName id2 = new TenantName("Pedro Correia 2");
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            TenantName id1 = new TenantName("pedro.correia@armis.pt");
            AccountEmail id2 = new AccountEmail("pedro.correia@armis.pt");
            Assert.False(id1.Equals(id2), "An email and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            TenantName id1 = new TenantName("Pedro Correia");
            Assert.False(id1.Equals(null), "An email and null should not be equal");
        }
    }
}
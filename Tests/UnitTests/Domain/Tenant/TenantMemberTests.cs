
using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class TenantMemberTests
    {

        [Fact]
        public void CreateValidMemberShouldReturnValidObject()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Client);
            Assert.NotNull(member);
        }

        [Fact]
        public void CreateValidMemberShouldReturnValidObject2()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Administrator);
            Assert.NotNull(member);
        }

        [Fact]
        public void CreateValidMemberShouldReturnValidObject3()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Owner);
            Assert.NotNull(member);
        }

        [Fact]
        public void ValidateNullAccountShouldFail()
        {
            try {
                new TenantMember(null, TenantPermissionList.Owner);
                Assert.True(false, "The member should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NULL_ACCOUNT, ex.Error);
            }
        }

        [Fact]
        public void ValidateNullPermissionsShouldFail()
        {
            try {
                AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
                new TenantMember(id, null);
                Assert.True(false, "The member should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NULL_PERMISSIONS, ex.Error);
            }
        }

        [Fact]
        public void SameMemberShouldPass()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember id1 = new TenantMember(id, TenantPermissionList.Client);
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualAccountsShouldPass()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember id1 = new TenantMember(id, TenantPermissionList.Client);
            TenantMember id2 = new TenantMember(id, TenantPermissionList.Client);
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoEqualAccountsShouldPass2()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember id1 = new TenantMember(id, TenantPermissionList.Client);
            TenantMember id2 = new TenantMember(id, TenantPermissionList.Owner);
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDifferentAccountsShouldFail()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            AccountId id2 = new AccountId("97804b27-da08-49bc-82bc-653eef6bb2ca");
            TenantMember member1 = new TenantMember(id, TenantPermissionList.Client);
            TenantMember member2 = new TenantMember(id2, TenantPermissionList.Client);
            Assert.NotEqual(member1, member2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member1 = new TenantMember(id, TenantPermissionList.Client);
            AccountName name = new AccountName("542a5fe9-843b-4766-846c-303d80df5ec8");
            Assert.False(member1.Equals(name), "A member and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Client);
            Assert.False(member.Equals(null), "A member and null should not be equal");
        }
    }
}
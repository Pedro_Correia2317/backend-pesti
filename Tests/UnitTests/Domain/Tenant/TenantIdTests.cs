
using System;
using Backend.HistoryEntries;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class TenantIdTests
    {

        [Fact]
        public void CreateValidIdShouldReturnValidObject()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.NotNull(id);
        }

        [Fact]
        public void ValidateNullIdShouldFail()
        {
            try {
                new TenantId(null);
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NULL_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateEmptyIdShouldFail()
        {
            try {
                new TenantId("");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidIdShouldFail()
        {
            try {
                new TenantId("I'm invalid");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateSpacesOnlyIdShouldFail()
        {
            try {
                new TenantId("\n\t        ");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateNonGuidShouldFail()
        {
            try {
                new TenantId("fien2-d2nk3d-2d9sjwnd");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateGuidWithInvalidCharactersShouldFail()
        {
            try {
                new TenantId("cced7c93-7b83-4%7b-9Ç0÷-eea1f2896619");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateLongIdShouldFail()
        {
            try {
                new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619-3fewwe");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void SameIdShouldPass()
        {
            TenantId id1 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualIdShouldPass()
        {
            TenantId id1 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentIdShouldFail()
        {
            TenantId id1 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantId id2 = new TenantId("d8fb91bb-2ff4-470a-b9a1-cef42dea8291");
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            TenantId id1 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription id2 = new EntryDescription("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.False(id1.Equals(id2), "An id and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            TenantId id1 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.False(id1.Equals(null), "An id and null should not be equal");
        }
    }
}

using System.Collections.Generic;
using Backend.Accounts;
using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class TenantLicensesTests
    {

        [Fact]
        public void CreateValidLicensesShouldReturnValidObject()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses = new TenantLicenses(id);
            Assert.NotNull(licenses);
        }

        [Fact]
        public void ValidateNullLicenseShouldFail()
        {
            try {
                new TenantLicenses(null);
                Assert.True(false, "The licenses should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NULL_LICENSE, ex.Error);
            }
        }

        [Fact]
        public void SameLicensesShouldPass()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses = new TenantLicenses(id);
            Assert.Equal(licenses, licenses);
        }

        [Fact]
        public void TwoEqualLicensesShouldPass()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses1 = new TenantLicenses(id);
            TenantLicenses licenses2 = new TenantLicenses(id);
            Assert.Equal(licenses1, licenses2);
        }

        [Fact]
        public void TwoDifferentLicensesShouldFail()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            LicenseId id2 = new LicenseId("97804b27-da08-49bc-82bc-653eef6bb2ca");
            TenantLicenses licenses1 = new TenantLicenses(id);
            TenantLicenses licenses2 = new TenantLicenses(id2);
            Assert.NotEqual(licenses1, licenses2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses = new TenantLicenses(id);
            AccountName name = new AccountName("542a5fe9-843b-4766-846c-303d80df5ec8");
            Assert.False(licenses.Equals(name), "A license and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses = new TenantLicenses(id);
            Assert.False(licenses.Equals(null), "A member and null should not be equal");
        }

        [Fact]
        public void AssociateNullLicensesShouldFail()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses = new TenantLicenses(id);
            TenantLicenses results = licenses.AssociateLicenses(null);
            Assert.Null(results);
        }

        [Fact]
        public void AssociateOldLicenseShouldFail()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses = new TenantLicenses(id);
            ICollection<License> set = new HashSet<License>();
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            set.Add(new License(id, date, dur, number, rem, type));
            TenantLicenses results = licenses.AssociateLicenses(set);
            Assert.Null(results);
        }

        [Fact]
        public void AssociateNewLicenseShouldPass()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses = new TenantLicenses(id);
            ICollection<License> set = new HashSet<License>();
            LicenseId id2 = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            set.Add(new License(id2, date, dur, number, rem, type));
            TenantLicenses results = licenses.AssociateLicenses(set);
            Assert.NotNull(results);
            Assert.Equal(2, results.Licenses.Count);
        }

        [Fact]
        public void AssociateOneInvalidLicenseShouldFail()
        {
            LicenseId id = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantLicenses licenses = new TenantLicenses(id);
            ICollection<License> set = new HashSet<License>();
            LicenseId id2 = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            set.Add(new License(id, date, dur, number, rem, type));
            set.Add(new License(id2, date, dur, number, rem, type));
            TenantLicenses results = licenses.AssociateLicenses(set);
            Assert.Null(results);
        }
    }
}
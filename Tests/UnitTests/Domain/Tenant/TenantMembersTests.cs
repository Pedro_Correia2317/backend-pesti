
using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class TenantMembersTests
    {

        [Fact]
        public void CreateValidMembersShouldReturnValidObject()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Client);
            TenantMembers members = new TenantMembers(member);
            Assert.NotNull(members);
        }

        [Fact]
        public void ValidateNullAccountShouldFail()
        {
            try {
                new TenantMembers(null);
                Assert.True(false, "The member should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(TenantErrors.NULL_MEMBER, ex.Error);
            }
        }

        [Fact]
        public void SameMembersShouldPass()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Client);
            TenantMembers id1 = new TenantMembers(member);
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualAccountsShouldPass()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Client);
            TenantMembers members1 = new TenantMembers(member);
            TenantMembers members2 = new TenantMembers(member);
            Assert.Equal(members1, members2);
        }

        [Fact]
        public void TwoDifferentAccountsShouldFail()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            AccountId id2 = new AccountId("97804b27-da08-49bc-82bc-653eef6bb2ca");
            TenantMember member = new TenantMember(id, TenantPermissionList.Client);
            TenantMember member2 = new TenantMember(id2, TenantPermissionList.Client);
            TenantMembers members1 = new TenantMembers(member);
            TenantMembers members2 = new TenantMembers(member2);
            Assert.NotEqual(members1, members2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Client);
            TenantMembers members = new TenantMembers(member);
            AccountName name = new AccountName("542a5fe9-843b-4766-846c-303d80df5ec8");
            Assert.False(members.Equals(name), "A member and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantMember member = new TenantMember(id, TenantPermissionList.Client);
            TenantMembers members1 = new TenantMembers(member);
            Assert.False(members1.Equals(null), "A member and null should not be equal");
        }
    }
}
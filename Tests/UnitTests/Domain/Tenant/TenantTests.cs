
using System.Collections.Generic;
using Backend.Accounts;
using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class TenantTests
    {

        [Fact]
        public void CreateValidTenantShouldPass()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant = new Tenant(id, name, member);
            Assert.NotNull(tenant);
        }

        [Fact]
        public void CreateTenantWithNullIdShouldFail()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            try
            {
                new Tenant(null, name, member);
                Assert.True(false, "The tenant should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(TenantErrors.NULL_DATA_IN_TENANT, ex.Error);
            }
        }

        [Fact]
        public void CreateTenantWithNullNameShouldFail()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            try
            {
                new Tenant(id, null, member);
                Assert.True(false, "The tenant should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(TenantErrors.NULL_DATA_IN_TENANT, ex.Error);
            }
        }

        [Fact]
        public void CreateTenantWithNullMemberShouldFail()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            try
            {
                new Tenant(id, name, null);
                Assert.True(false, "The tenant should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(TenantErrors.NULL_DATA_IN_TENANT, ex.Error);
            }
        }

        [Fact]
        public void CreateTenantWithoutOwnerShouldFail()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Client);
            try
            {
                new Tenant(id, name, member);
                Assert.True(false, "The tenant should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(TenantErrors.NO_OWNER_IN_TENANT, ex.Error);
            }
        }

        [Fact]
        public void SameTenantShouldPass()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant license = new Tenant(id, name, member);
            Assert.Equal(license, license);
        }

        [Fact]
        public void TwoEqualTenantShouldPass()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name, member);
            Tenant tenant2 = new Tenant(id, name, member);
            Assert.Equal(tenant1, tenant2);
        }

        [Fact]
        public void TwoDifferentEntriesShouldFail()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantId id2 = new TenantId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name, member);
            Tenant tenant2 = new Tenant(id2, name, member);
            Assert.NotEqual(tenant1, tenant2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name, member);
            Assert.False(tenant1.Equals(member), "A tenant and a member should not be equal");
        }

        [Fact]
        public void OnlyIdShouldMatter()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            TenantName name2 = new TenantName("Pedro Correia 2");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountId aId2 = new AccountId("65aa78c7-513c-4d7d-860e-13d0bf3c255e");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            TenantMember member2 = new TenantMember(aId2, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name2, member2);
            Tenant tenant2 = new Tenant(id, name2, member2);
            Assert.Equal(tenant1, tenant2);
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name, member);
            Assert.False(tenant1.Equals(null), "A tenant and null should not be equal");
        }

        [Fact]
        public void AssociateNullLicensesShouldFail()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name, member);
            Assert.False(tenant1.AssociateLicenses(null));
        }

        [Fact]
        public void AssociateOldLicenseShouldFail()
        {
            LicenseId lId = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name, member);
            ICollection<License> set = new HashSet<License>();
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            set.Add(new License(lId, date, dur, number, rem, type));
            Assert.True(tenant1.AssociateLicenses(set));
            Assert.False(tenant1.AssociateLicenses(set));
        }

        [Fact]
        public void AssociateNewLicenseShouldPass()
        {
            LicenseId lId = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            LicenseId lId2 = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name, member);
            ICollection<License> set = new HashSet<License>();
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            set.Add(new License(lId, date, dur, number, rem, type));
            Assert.True(tenant1.AssociateLicenses(set));
            set.Clear();
            set.Add(new License(lId2, date, dur, number, rem, type));
            Assert.True(tenant1.AssociateLicenses(set));
            Assert.Equal(2, tenant1.Licenses.Licenses.Count);
        }

        [Fact]
        public void AssociateOneInvalidLicenseShouldFail()
        {
            LicenseId lId = new LicenseId("542a5fe9-843b-4766-846c-303d80df5ec8");
            LicenseId lId2 = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant1 = new Tenant(id, name, member);
            ICollection<License> set = new HashSet<License>();
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            set.Add(new License(lId, date, dur, number, rem, type));
            Assert.True(tenant1.AssociateLicenses(set));
            set.Add(new License(lId2, date, dur, number, rem, type));
            Assert.False(tenant1.AssociateLicenses(set));
        }
    }
}


using Backend.Accounts;
using Backend.Tenants;
using Xunit;

namespace Tests.Domain
{
    public class TenantDTOFactoryTests
    {
        private ITenantDTOFactory Factory = new TenantDTOFactoryImpl();

        [Fact]
        public void CreateDTOWithValidTenantShouldPass()
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant license = new Tenant(id, name, member);
            TenantDTO dto = Factory.CreateBasicDTO(license);
            Assert.NotNull(dto);
        }

        [Fact]
        public void CreateDTOWithNullTenantShouldFail()
        {
            TenantDTO dto = Factory.CreateBasicDTO(null);
            Assert.NotNull(dto);
            Assert.NotNull(dto.id);
            Assert.NotNull(dto.name);
        }

        [Fact]
        public void CreateDTOWithNullListShouldFail()
        {
            TenantDTO[] dto = Factory.CreateListOfBasicDTO(null);
            Assert.NotNull(dto);
            Assert.Empty(dto);
        }
    }
}

using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class TenantFactoryTests
    {

        private ITenantFactory Factory = new TenantFactoryImpl();

        [Fact]
        public void CreatingContainerShouldReturnValidObject()
        {
            TenantContainer container = Factory.NewContainer();
            Assert.NotNull(container);
        }

        [Fact]
        public void CreatingValidIdShouldReturnValidObject()
        {
            Results<TenantId> results = Factory.NewId("542a5fe9-843b-4766-846c-303d80df5ec8");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidIdShouldReturnInvalidObject()
        {
            Results<TenantId> results = Factory.NewId(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidNameShouldReturnValidObject()
        {
            Results<TenantName> results = Factory.NewName("Pedro Correia");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidNameShouldReturnInvalidObject()
        {
            Results<TenantName> results = Factory.NewName(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidPermissionsShouldReturnValidObject()
        {
            Results<TenantPermissions> results = Factory.NewPermissions("Client");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidPermissionsShouldReturnInvalidObject()
        {
            Results<TenantPermissions> results = Factory.NewPermissions(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidMemberShouldReturnValidObject()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            Results<TenantMember> results = Factory.NewMember(id, TenantPermissionList.Client);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidMemeberShouldReturnInvalidObject()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            Results<TenantMember> results = Factory.NewMember(null, TenantPermissionList.Client);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidMemeberShouldReturnInvalidObject2()
        {
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            Results<TenantMember> results = Factory.NewMember(id, null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidTypeShouldReturnValidObject()
        {
            TenantPermissions results = Factory.NewOwnerPermissions();
            Assert.True(results != null);
        }

        [Fact]
        public void CreatingValidTenantShouldReturnValidObject()
        {
            TenantContainer container = Factory.NewContainer();
            container.Id = Factory.NewId("cced7c93-7b83-4b7b-9e0b-eea1f2896619").Result;
            container.Name = Factory.NewName("Pedro Correia").Result;
            AccountId id = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            container.Owner = Factory.NewMember(id, TenantPermissionList.Owner).Result;
            Results<Tenant> results = Factory.NewTenant(container);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidTenantShouldReturnInvalidObject()
        {
            Results<Tenant> results = Factory.NewTenant(null);
            Assert.False(results.wasExecutedSuccessfully());
        }
    }
}
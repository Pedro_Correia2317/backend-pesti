
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class HistoryEntryTests
    {

        [Fact]
        public void CreateValidEntryShouldPass()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
            Assert.NotNull(entry);
        }

        [Fact]
        public void CreateValidEntryShouldPass2()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
            Assert.NotNull(entry);
        }

        [Fact]
        public void CreateEntryWithNullIdShouldFail()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                HistoryEntry entry = new HistoryEntry(null, desc, date, type, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullIdShouldFail2()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                HistoryEntry entry = new HistoryEntry(null, desc, date, type, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullDescriptionShouldFail()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                HistoryEntry entry = new HistoryEntry(id, null, date, type, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullDescriptionShouldFail2()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                HistoryEntry entry = new HistoryEntry(id, null, date, type, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullDateShouldFail()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                HistoryEntry entry = new HistoryEntry(id, desc, null, type, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullDateShouldFail2()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                HistoryEntry entry = new HistoryEntry(id, desc, null, type, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullTypeShouldFail()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                HistoryEntry entry = new HistoryEntry(id, desc, date, null, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullTypeShouldFail2()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            try
            {
                HistoryEntry entry = new HistoryEntry(id, desc, date, null, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullTenantIdShouldFail()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            TenantId id2 = null;
            try
            {
                HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void CreateEntryWithNullAccountIdShouldFail2()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id2 = null;
            try
            {
                HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
                Assert.True(false, "The entry should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(EntryErrors.NULL_DATA_IN_ENTRY, ex.Error);
            }
        }

        [Fact]
        public void SameEntryShouldPass()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
            Assert.Equal(entry, entry);
        }

        [Fact]
        public void TwoEqualEntryShouldPass()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
            HistoryEntry entry2 = new HistoryEntry(id, desc, date, type, id2);
            Assert.Equal(entry, entry2);
        }

        [Fact]
        public void TwoDifferentEntriesShouldFail()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryId id2 = new EntryId("542a5fe9-843b-4766-846c-303d80df5ec8");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, aId);
            HistoryEntry entry2 = new HistoryEntry(id2, desc, date, type, aId);
            Assert.NotEqual(entry, entry2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, aId);
            Assert.False(entry.Equals(aId), "An account and an id should not be equal");
        }

        [Fact]
        public void OnlyIdShouldMatter()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDescription desc2 = new EntryDescription("Descrição Básica 2");
            EntryDate date = new EntryDate("01-01-2021");
            EntryDate date2 = new EntryDate("02-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            EntryType type2 = EntryTypes.TenantCreation;
            TenantId tId = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, aId);
            HistoryEntry entry2 = new HistoryEntry(id, desc2, date2, type2, tId);
            Assert.Equal(entry, entry2);
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.AccountCreation;
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, aId);
            Assert.False(entry.Equals(null), "An entry and null should not be equal");
        }
    }
}
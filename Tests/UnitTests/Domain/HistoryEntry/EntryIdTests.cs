
using System;
using Backend.HistoryEntries;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class EntryIdTests
    {

        [Fact]
        public void CreateValidIdShouldReturnValidObject()
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.NotNull(id);
        }

        [Fact]
        public void ValidateNullIdShouldFail()
        {
            try {
                new EntryId(null);
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.NULL_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateEmptyIdShouldFail()
        {
            try {
                new EntryId("");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidIdShouldFail()
        {
            try {
                new EntryId("I'm invalid");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateSpacesOnlyIdShouldFail()
        {
            try {
                new EntryId("\n\t        ");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateNonGuidShouldFail()
        {
            try {
                new EntryId("fien2-d2nk3d-2d9sjwnd");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateGuidWithInvalidCharactersShouldFail()
        {
            try {
                new EntryId("cced7c93-7b83-4%7b-9Ç0÷-eea1f2896619");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateLongIdShouldFail()
        {
            try {
                new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619-3fewwe");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void SameIdShouldPass()
        {
            EntryId id1 = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualIdShouldPass()
        {
            EntryId id1 = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryId id2 = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentIdShouldFail()
        {
            EntryId id1 = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryId id2 = new EntryId("d8fb91bb-2ff4-470a-b9a1-cef42dea8291");
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            EntryId id1 = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription id2 = new EntryDescription("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.False(id1.Equals(id2), "An id and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            EntryId id1 = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.False(id1.Equals(null), "An id and null should not be equal");
        }
    }
}

using System;
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Tenants;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class HistoryEntryFactoryTests
    {

        private IHistoryEntryFactory Factory = new HistoryEntryFactoryImpl();

        [Fact]
        public void CreatingValidIdShouldReturnValidObject()
        {
            Results<EntryId> results = Factory.NewId("542a5fe9-843b-4766-846c-303d80df5ec8");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidIdShouldReturnInvalidObject()
        {
            Results<EntryId> results = Factory.NewId(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidDescriptionShouldReturnValidObject()
        {
            Results<EntryDescription> results = Factory.NewDescription("Descrição Básica");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidDescriptionShouldReturnInvalidObject()
        {
            Results<EntryDescription> results = Factory.NewDescription(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidDateShouldReturnValidObject()
        {
            Results<EntryDate> results = Factory.NewDate("01-01-2021");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidDateShouldReturnValidObject2()
        {
            Results<EntryDate> results = Factory.NewDate(DateTime.Now);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidDateShouldReturnInvalidObject2()
        {
            Results<EntryDate> results = Factory.NewDate(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidStatusShouldReturnValidObject()
        {
            Results<EntryType> results = Factory.NewType("AccountCreation");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidStatusShouldReturnInvalidObject()
        {
            Results<EntryType> results = Factory.NewType(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEntryShouldReturnValidObject()
        {
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEntryShouldReturnValidObject2()
        {
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEntryShouldReturnValidObject3()
        {
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, id);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEntryShouldReturnValidObject4()
        {
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, id);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEntryShouldReturnValidObject5()
        {
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, id, "Wow", "Lol");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEntryShouldReturnValidObject6()
        {
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, id, "Wow", "Lol");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEntryShouldReturnValidObject7()
        {
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, "Wow", id);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEntryShouldReturnValidObject8()
        {
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, "Wow", id);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject()
        {
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(null, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject2()
        {
            EntryDescription desc = null;
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject3()
        {
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject4()
        {
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(null, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject5()
        {
            EntryDescription desc = null;
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject6()
        {
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject7()
        {
            EntryType type = EntryTypes.AccountCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(null, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject8()
        {
            EntryType type = EntryTypes.AccountCreation;
            TenantId id = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject9()
        {
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(null, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject10()
        {
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject11()
        {
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(null, id, "Wow", "Lol");
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject12()
        {
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(null, id, "Wow", "Lol");
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject13()
        {
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, id, "Wow", "Lol");
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject14()
        {
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, id, "Wow", "Lol");
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject15()
        {
            EntryType type = EntryTypes.TenantCreation;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            object[] args = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, id, args);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject16()
        {
            EntryType type = EntryTypes.AccountCreation;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            object[] args = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, id, args);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject17()
        {
            EntryType type = EntryTypes.TenantCreation;
            string desc = "Descrição Básica";
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(null, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject18()
        {
            EntryType type = EntryTypes.AccountCreation;
            string desc = "Descrição Básica";
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(null, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject19()
        {
            EntryType type = EntryTypes.TenantCreation;
            string desc = null;
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject20()
        {
            EntryType type = EntryTypes.AccountCreation;
            string desc = null;
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject21()
        {
            EntryType type = EntryTypes.TenantCreation;
            string desc = "Descrição Básica";
            TenantId id = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEntryShouldReturnInvalidObject22()
        {
            EntryType type = EntryTypes.AccountCreation;
            string desc = "Descrição Básica";
            AccountId id = null;
            Results<HistoryEntry> results = Factory.NewEntry(type, desc, id);
            Assert.False(results.wasExecutedSuccessfully());
        }
    }
}


using System;
using Backend.HistoryEntries;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class EntryDateTests
    {

        [Fact]
        public void CreateValidDateShouldPass()
        {
            EntryDate date = new EntryDate("01-01-2021");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass2()
        {
            EntryDate date = new EntryDate("2021-01-01");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass3()
        {
            EntryDate date = new EntryDate("01/01/2021");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass4()
        {
            EntryDate date = new EntryDate("2021/01/01");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass5()
        {
            EntryDate date = new EntryDate("01/01/01");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass6()
        {
            EntryDate date = new EntryDate("01-01-01");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass7()
        {
            EntryDate date = new EntryDate("01-01-2021 14:30");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass8()
        {
            EntryDate date = new EntryDate("01/01/2021 14:30:35");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass9()
        {
            EntryDate date = new EntryDate("2021.01.01");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass10()
        {
            EntryDate date = new EntryDate("2021.01-01");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass11()
        {
            EntryDate date = new EntryDate("2021-01/01");
            Assert.NotNull(date);
        }

        [Fact]
        public void CreateValidDateShouldPass12()
        {
            EntryDate date = new EntryDate(DateTime.Now);
            Assert.NotNull(date);
        }

        [Fact]
        public void ValidateNullDateShouldFail()
        {
            try {
                new EntryDate(null);
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.NULL_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateEmptyDateShouldFail()
        {
            try {
                new EntryDate("");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateLettersDateShouldFail()
        {
            try {
                new EntryDate("OL-OQ-OP");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateLettersDateShouldFail2()
        {
            try {
                new EntryDate("01-01-202E");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidSeparatorDateShouldFail()
        {
            try {
                new EntryDate(@"01\01\21");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidSeparatorDateShouldFail2()
        {
            try {
                new EntryDate("01-01-2021 14.30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidSeparatorDateShouldFail3()
        {
            try {
                new EntryDate("01-01-2021 14h30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidSeparatorDateShouldFail4()
        {
            try {
                new EntryDate("01-01-2021 14h30m");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidSeparatorDateShouldFail5()
        {
            try {
                new EntryDate("01-01-2021 14h30min");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidSeparatorDateShouldFail6()
        {
            try {
                new EntryDate("01-01-2021T14:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateMissingDataDateShouldFail()
        {
            try {
                new EntryDate("01-01-2021 14");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateMissingDataDateShouldFail2()
        {
            try {
                new EntryDate("01-01-2021 :30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateMissingDataDateShouldFail3()
        {
            try {
                new EntryDate("-01-2021 14:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateMissingDataDateShouldFail4()
        {
            try {
                new EntryDate("01--2021 14:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateMissingDataDateShouldFail5()
        {
            try {
                new EntryDate("01-01- 14:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidDayDateShouldFail()
        {
            try {
                new EntryDate("32-01-2021 14:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidMonthDateShouldFail()
        {
            try {
                new EntryDate("01-13-2021 14:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidYearDateShouldFail()
        {
            try {
                new EntryDate("01-13-20121 14:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidHourDateShouldFail()
        {
            try {
                new EntryDate("01-13-2021 24:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidMinuteDateShouldFail()
        {
            try {
                new EntryDate("01-13-2021 14:60");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidSecondDateShouldFail()
        {
            try {
                new EntryDate("01-13-2021 14:30:60");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidMonthDateShouldFail2()
        {
            try {
                new EntryDate("2021/13/01 14:30:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidMonthDateShouldFail3()
        {
            try {
                new EntryDate("14-14-2021 14:30:30");
                Assert.True(false, "The date should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.INVALID_DATE, ex.Error);
            }
        }

    }
}
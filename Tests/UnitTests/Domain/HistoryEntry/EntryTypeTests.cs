

using Backend.HistoryEntries;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class EntryTypeTests
    {

        [Fact]
        public void SameTypeShouldPass()
        {
            EntryType id1 = EntryTypes.AccountCreation;
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualTypeShouldPass()
        {
            EntryType id1 = EntryTypes.AccountCreation;
            EntryType id2 = EntryTypes.AccountCreation;
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentTypeShouldFail()
        {
            EntryType id1 = EntryTypes.AccountCreation;
            EntryType id2 = EntryTypes.TenantCreation;
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            EntryType id1 = EntryTypes.AccountCreation;
            EntryDescription id2 = new EntryDescription("AccountCreation");
            Assert.False(id1.Equals(id2), "A status and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            EntryType id1 = EntryTypes.AccountCreation;
            Assert.False(id1.Equals(null), "A type and null should not be equal");
        }

        [Fact]
        public void GetValidTypeShouldPass()
        {
            EntryType status = EntryTypes.AccountCreation;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass2()
        {
            EntryType status = EntryTypes.AccountDeletion;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass3()
        {
            EntryType status = EntryTypes.TenantCreation;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass4()
        {
            EntryType status = EntryTypes.AccountStatusChange;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass5()
        {
            EntryType status = EntryTypes.AccountBoughtLicense;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass6()
        {
            EntryType status = EntryTypes.AccountUpdate;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass8()
        {
            EntryType status = EntryTypes.AssociateLicenseToAccount;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass9()
        {
            EntryType status = EntryTypes.DisassociateLicenseToAccount;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass10()
        {
            EntryType status = EntryTypes.TenantBoughtLicenses;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass()
        {
            EntryType status = EntryTypes.GetByName("AccountCreation");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass2()
        {
            EntryType status = EntryTypes.GetByName("AccountActivation");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass3()
        {
            EntryType status = EntryTypes.GetByName("TenantCreation");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass4()
        {
            EntryType status = EntryTypes.GetByName("AccountStatusChange");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass5()
        {
            EntryType status = EntryTypes.GetByName("AccountBoughtLicense");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass6()
        {
            EntryType status = EntryTypes.GetByName("AccountReactivation");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass8()
        {
            EntryType status = EntryTypes.GetByName("AssociateLicenseToAccount");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass9()
        {
            EntryType status = EntryTypes.GetByName("DisassociateLicenseToAccount");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByDescriptionShouldPass10()
        {
            EntryType status = EntryTypes.GetByName("TenantBoughtLicenses");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetNullTypeByDescriptionShouldFail()
        {
            try {
                EntryTypes.GetByName(null);
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.NULL_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetEmptyTypeByDescriptionShouldFail()
        {
            try {
                EntryTypes.GetByName("");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.UNKNOWN_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetInvalidTypeByDescriptionShouldFail()
        {
            try {
                EntryTypes.GetByName("Invalid");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.UNKNOWN_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetAllTypeShouldPass()
        {
            EntryType[] statuses = EntryTypes.GetAll();
            Assert.Equal(9, statuses.Length);
            Assert.DoesNotContain(null, statuses);
        }
    }
}
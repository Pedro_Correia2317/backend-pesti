

using Backend.HistoryEntries;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class EntryDescriptionTests
    {

        [Fact]
        public void CreateValidDescriptionShouldPass()
        {
            EntryDescription name = new EntryDescription("Descrição Básica");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidDescriptionShouldPass2()
        {
            EntryDescription name = new EntryDescription("Descricao Basica");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidDescriptionShouldPass3()
        {
            EntryDescription name = new EntryDescription("Descricao Basica 33245");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidDescriptionShouldPass4()
        {
            EntryDescription name = new EntryDescription("Descricao Basica #33245 (Wow!!!!)");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidDescriptionShouldPass5()
        {
            EntryDescription name = new EntryDescription("Descrição.BÀsica,?'[]}{{€§£@$%&/ºº+*´´``~~^^#33245-+-*//;:<>€(Wow!!!!)");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidDescriptionShouldPass6()
        {
            EntryDescription name = new EntryDescription("  Descricao Basica  ");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidDescriptionShouldPass7()
        {
            EntryDescription name = new EntryDescription("  34556  344    ");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidDescriptionShouldPass8()
        {
            EntryDescription name = new EntryDescription("DB");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidDescriptionShouldPass9()
        {
            EntryDescription name = new EntryDescription("DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO");
            //1000 chars
            Assert.NotNull(name);
        }

        [Fact]
        public void ValidateNullDescriptionShouldFail()
        {
            try {
                new EntryDescription(null);
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.NULL_DESCRIPTION, ex.Error);
            }
        }

        [Fact]
        public void ValidateEmptyDescriptionShouldFail()
        {
            try {
                new EntryDescription("");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.EMPTY_DESCRIPTION, ex.Error);
            }
        }

        [Fact]
        public void ValidateSpacesOnlyDescriptionShouldFail()
        {
            try {
                new EntryDescription("\t\v\n    ");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.EMPTY_DESCRIPTION, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooShortDescriptionShouldFail()
        {
            try {
                new EntryDescription("a");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.DESCRIPTION_TOO_SMALL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooLargeDescriptionShouldFail()
        {
            try {
                new EntryDescription("DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO"
                + "DBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELODBKEMAKELO1");
                //1001 chars
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(EntryErrors.DESCRIPTION_TOO_LARGE, ex.Error);
            }
        }

        [Fact]
        public void SameDescriptionShouldPass()
        {
            EntryDescription id1 = new EntryDescription("Descrição Básica");
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualDescriptionShouldPass()
        {
            EntryDescription id1 = new EntryDescription("Descrição Básica");
            EntryDescription id2 = new EntryDescription("Descrição Básica");
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDifferentDescriptionShouldFail()
        {
            EntryDescription id1 = new EntryDescription("Descrição Básica");
            EntryDescription id2 = new EntryDescription("Descrição Básica 2");
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            EntryDescription id1 = new EntryDescription("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryId id2 = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.False(id1.Equals(id2), "A description and an id should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            EntryDescription id1 = new EntryDescription("Descrição Básica");
            Assert.False(id1.Equals(null), "A description and null should not be equal");
        }
    }
}
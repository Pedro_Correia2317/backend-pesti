

using Backend.Accounts;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class AccountTypeTests
    {

        [Fact]
        public void SameTypeShouldPass()
        {
            AccountType id1 = AccountTypes.Client;
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualTypeShouldPass()
        {
            AccountType id1 = AccountTypes.Client;
            AccountType id2 = AccountTypes.Client;
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentTypeShouldFail()
        {
            AccountType id1 = AccountTypes.Client;
            AccountType id2 = AccountTypes.SuperAdmin;
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            AccountType id1 = AccountTypes.Client;
            AccountName id2 = new AccountName("Client");
            Assert.False(id1.Equals(id2), "A status and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            AccountType id1 = AccountTypes.Client;
            Assert.False(id1.Equals(null), "A status and null should not be equal");
        }

        [Fact]
        public void GetValidTypeShouldPass()
        {
            AccountType status = AccountTypes.Client;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass2()
        {
            AccountType status = AccountTypes.Admin;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeShouldPass3()
        {
            AccountType status = AccountTypes.SuperAdmin;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByNameShouldPass()
        {
            AccountType status = AccountTypes.GetByName("Client");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByNameShouldPass2()
        {
            AccountType status = AccountTypes.GetByName("Admin");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidTypeByNameShouldPass3()
        {
            AccountType status = AccountTypes.GetByName("SuperAdmin");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetNullTypeByNameShouldFail()
        {
            try {
                AccountTypes.GetByName(null);
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.NULL_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetEmptyTypeByNameShouldFail()
        {
            try {
                AccountTypes.GetByName("");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.UNKNOWN_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetInvalidTypeByNameShouldFail()
        {
            try {
                AccountTypes.GetByName("Invalid");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.UNKNOWN_TYPE, ex.Error);
            }
        }

        [Fact]
        public void GetAllTypeShouldPass()
        {
            AccountType[] statuses = AccountTypes.GetAll();
            Assert.Equal(3, statuses.Length);
            Assert.DoesNotContain(null, statuses);
        }
    }
}
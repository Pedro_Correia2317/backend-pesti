

using Backend.Accounts;
using Xunit;

namespace Tests.Domain
{
    public class AccountDTOFactoryTests
    {
        private IAccountDTOFactory Factory = new AccountDTOFactoryImpl();

        [Fact]
        public void CreateDTOWithValidAccountShouldPass()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account = new Account(id, name, email, status, type);
            AccountDTO dto = Factory.CreateBasicDTO(account);
            Assert.NotNull(dto);
        }

        [Fact]
        public void CreateDTOWithNullAccountShouldFail()
        {
            AccountDTO dto = Factory.CreateBasicDTO(null);
            Assert.NotNull(dto);
            Assert.NotNull(dto.name);
            Assert.NotNull(dto.email);
            Assert.NotNull(dto.id);
            Assert.NotNull(dto.type);
            Assert.NotNull(dto.status);
            Assert.NotNull(dto.type.name);
            Assert.NotNull(dto.status.name);
        }

        [Fact]
        public void CreateDTOWithNullListShouldFail()
        {
            AccountDTO[] dto = Factory.CreateListOfBasicDTO(null);
            Assert.NotNull(dto);
            Assert.Empty(dto);
        }

        [Fact]
        public void CreateDTOWithNullListShouldFail2()
        {
            AccountStatusDTO[] dto = Factory.CreateListStatusDTO(null);
            Assert.NotNull(dto);
            Assert.Empty(dto);
        }

        [Fact]
        public void CreateDTOWithNullListShouldFail3()
        {
            AccountTypeDTO[] dto = Factory.CreateListTypesDTO(null);
            Assert.NotNull(dto);
            Assert.Empty(dto);
        }

        [Fact]
        public void CreateDTOWithValidStatusShouldFail()
        {
            AccountStatusDTO dto = Factory.CreateStatusDTO(AccountStatuses.Active);
            Assert.NotNull(dto);
        }

        [Fact]
        public void CreateDTOWithNullStatusShouldFail()
        {
            AccountStatusDTO dto = Factory.CreateStatusDTO(null);
            Assert.NotNull(dto);
            Assert.NotNull(dto.name);
        }

        [Fact]
        public void CreateDTOWithValidTypeShouldFail()
        {
            AccountTypeDTO dto = Factory.CreateTypeDTO(AccountTypes.Admin);
            Assert.NotNull(dto);
        }

        [Fact]
        public void CreateDTOWithNullTypeShouldFail()
        {
            AccountTypeDTO dto = Factory.CreateTypeDTO(null);
            Assert.NotNull(dto);
            Assert.NotNull(dto.name);
        }
    }
}
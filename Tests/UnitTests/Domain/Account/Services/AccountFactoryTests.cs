
using Backend.Accounts;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class AccountFactoryTests
    {

        private IAccountFactory Factory = new AccountFactoryImpl();

        [Fact]
        public void CreatingContainerShouldReturnValidObject()
        {
            AccountContainer container = Factory.NewContainer();
            Assert.NotNull(container);
        }

        [Fact]
        public void CreatingValidIdShouldReturnValidObject()
        {
            Results<AccountId> results = Factory.NewId("542a5fe9-843b-4766-846c-303d80df5ec8");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidIdShouldReturnInvalidObject()
        {
            Results<AccountId> results = Factory.NewId(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidNameShouldReturnValidObject()
        {
            Results<AccountName> results = Factory.NewName("Pedro Correia");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidNameShouldReturnInvalidObject()
        {
            Results<AccountName> results = Factory.NewName(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidEmailShouldReturnValidObject()
        {
            Results<AccountEmail> results = Factory.NewEmail("pedro.correia@armis.pt");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidEmailShouldReturnInvalidObject()
        {
            Results<AccountEmail> results = Factory.NewEmail(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidStatusShouldReturnValidObject()
        {
            Results<AccountStatus> results = Factory.NewStatus("Active");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidStatusShouldReturnInvalidObject()
        {
            Results<AccountStatus> results = Factory.NewStatus(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingDefaultStatusShouldReturnValidObject()
        {
            Results<AccountStatus> results = Factory.NewDefaultStatus();
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidTypeShouldReturnValidObject()
        {
            Results<AccountType> results = Factory.NewType("Client");
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidTypeShouldReturnInvalidObject()
        {
            Results<AccountType> results = Factory.NewType(null);
            Assert.False(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingValidAccountShouldReturnValidObject()
        {
            AccountContainer container = Factory.NewContainer();
            container.Id = Factory.NewId("cced7c93-7b83-4b7b-9e0b-eea1f2896619").Result;
            container.Email = Factory.NewEmail("pedro.correia@armis.pt").Result;
            container.Name = Factory.NewName("Pedro Correia").Result;
            container.Status = Factory.NewStatus("Active").Result;
            container.Type = Factory.NewType("Client").Result;
            Results<Account> results = Factory.NewAccount(container);
            Assert.True(results.wasExecutedSuccessfully());
        }

        [Fact]
        public void CreatingInvalidAccountShouldReturnInvalidObject()
        {
            Results<Account> results = Factory.NewAccount(null);
            Assert.False(results.wasExecutedSuccessfully());
        }
    }
}

using System;
using Backend.Accounts;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class AccountEmailTests
    {

        [Fact]
        public void CreateValidEmailShouldPass()
        {
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass2()
        {
            AccountEmail email = new AccountEmail("Pedro.Correia@armis.pt");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass3()
        {
            AccountEmail email = new AccountEmail("pedro.correia@ARmis.pt");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass4()
        {
            AccountEmail email = new AccountEmail("pedro.correia@armis.PT");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass5()
        {
            AccountEmail email = new AccountEmail("  pedro.correia@armis.pt  ");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass6()
        {
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt  ");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass7()
        {
            AccountEmail email = new AccountEmail("\n  pedro.correia@armis.pt");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass8()
        {
            AccountEmail email = new AccountEmail("pedroCorreia2462@armis.pt");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass9()
        {
            AccountEmail email = new AccountEmail("pedroCorreia@armis.pt");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass10()
        {
            AccountEmail email = new AccountEmail("3992937@armis.pt");
            Assert.NotNull(email);
        }

        [Fact]
        public void CreateValidEmailShouldPass11()
        {
            AccountEmail email = new AccountEmail("pedro.correia4567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901@armis.pt");
            // 200 chars
            Assert.NotNull(email);
        }

        [Fact]
        public void ValidateNullEmailShouldFail()
        {
            try {
                new AccountEmail(null);
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.NULL_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateEmptyEmailShouldFail()
        {
            try {
                new AccountEmail("");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.EMPTY_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidEmailShouldFail()
        {
            try {
                new AccountEmail("I'm invalid");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateOnlySpacesEmailShouldFail()
        {
            try {
                new AccountEmail("\n\t     \v");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.EMPTY_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateOnlyNameEmailShouldFail()
        {
            try {
                new AccountEmail("pedro.correia");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateOnlyDomainEmailShouldFail()
        {
            try {
                new AccountEmail("@armis.pt");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateNoFullDomainEmailShouldFail()
        {
            try {
                new AccountEmail("pedro.correia@armis");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidCharactersEmailShouldFail()
        {
            try {
                new AccountEmail("pedro.╬orreia@armis.pt");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidCharactersEmailShouldFail2()
        {
            try {
                new AccountEmail("pedro.correia@armi☻.pt");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidCharactersEmailShouldFail3()
        {
            try {
                new AccountEmail("pedro.correia@armis.p▒");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidCharactersEmailShouldFail4()
        {
            try {
                new AccountEmail("pedro@correia@armis.pt");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidDomainEmailShouldFail()
        {
            try {
                new AccountEmail("pedro.correia@37942.24");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooShortEmailShouldFail()
        {
            try {
                new AccountEmail("p");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.EMAIL_TOO_SMALL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooShortEmailShouldFail2()
        {
            try {
                new AccountEmail("p@s");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooShortEmailShouldFail3()
        {
            try {
                new AccountEmail("@");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.EMAIL_TOO_SMALL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooShortEmailShouldFail4()
        {
            try {
                new AccountEmail(".");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.EMAIL_TOO_SMALL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooShortEmailShouldFail5()
        {
            try {
                new AccountEmail("p@s.r");
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_EMAIL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooLongEmailShouldFail()
        {
            try {
                string s = "pedro.correia34567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901@armis.pt";
                new AccountEmail(s);
                // 201 chars
                Assert.True(false, "The email should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.EMAIL_TOO_LARGE, ex.Error);
            }
        }

        [Fact]
        public void SameEmailShouldPass()
        {
            AccountEmail id1 = new AccountEmail("pedro.correia@armis.pt");
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualEmailShouldPass()
        {
            AccountEmail id1 = new AccountEmail("pedro.correia@armis.pt");
            AccountEmail id2 = new AccountEmail("pedro.correia@armis.pt");
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentEmailShouldFail()
        {
            AccountEmail id1 = new AccountEmail("pedro.correia@armis.pt");
            AccountEmail id2 = new AccountEmail("pedro.correia@gmail.pt");
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            AccountEmail id1 = new AccountEmail("pedro.correia@armis.pt");
            AccountName id2 = new AccountName("pedro.correia@armis.pt");
            Assert.False(id1.Equals(id2), "An email and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            AccountEmail id1 = new AccountEmail("pedro.correia@armis.pt");
            Assert.False(id1.Equals(null), "An email and null should not be equal");
        }
    }
}
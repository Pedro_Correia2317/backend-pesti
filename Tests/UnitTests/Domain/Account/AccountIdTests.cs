
using System;
using Backend.Accounts;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class AccountIdTests
    {

        [Fact]
        public void CreateValidIdShouldReturnValidObject()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.NotNull(id);
        }

        [Fact]
        public void ValidateNullIdShouldFail()
        {
            try {
                new AccountId(null);
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.NULL_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateEmptyIdShouldFail()
        {
            try {
                new AccountId("");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateInvalidIdShouldFail()
        {
            try {
                new AccountId("I'm invalid");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateSpacesOnlyIdShouldFail()
        {
            try {
                new AccountId("\n\t        ");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateNonGuidShouldFail()
        {
            try {
                new AccountId("fien2-d2nk3d-2d9sjwnd");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateGuidWithInvalidCharactersShouldFail()
        {
            try {
                new AccountId("cced7c93-7b83-4%7b-9Ç0÷-eea1f2896619");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void ValidateLongIdShouldFail()
        {
            try {
                new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619-3fewwe");
                Assert.True(false, "The id should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.INVALID_ID, ex.Error);
            }
        }

        [Fact]
        public void SameIdShouldPass()
        {
            AccountId id1 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualIdShouldPass()
        {
            AccountId id1 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountId id2 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentIdShouldFail()
        {
            AccountId id1 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountId id2 = new AccountId("d8fb91bb-2ff4-470a-b9a1-cef42dea8291");
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            AccountId id1 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountName id2 = new AccountName("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.False(id1.Equals(id2), "An id and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            AccountId id1 = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            Assert.False(id1.Equals(null), "An id and null should not be equal");
        }
    }
}
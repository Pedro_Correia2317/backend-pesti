

using Backend.Accounts;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class AccountNameTests
    {

        [Fact]
        public void CreateValidNameShouldPass()
        {
            AccountName name = new AccountName("Pedro Correia");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass2()
        {
            AccountName name = new AccountName("  Pedro Correia");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass3()
        {
            AccountName name = new AccountName("   Pedro Correia  ");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass4()
        {
            AccountName name = new AccountName("Pedro1Correia");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass5()
        {
            AccountName name = new AccountName("pedrocorreia");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass6()
        {
            AccountName name = new AccountName("323522");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass7()
        {
            AccountName name = new AccountName("Pedro Correia.,[]()!?áéíóú+-@");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass8()
        {
            AccountName name = new AccountName("pc");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass9()
        {
            AccountName name = new AccountName("PEDRO CORREIA");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass10()
        {
            AccountName name = new AccountName("Pedro");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass11()
        {
            AccountName name = new AccountName("pedro.correia4567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901MiguelWow");
            // 200 chars
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass12()
        {
            AccountName name = new AccountName("§╬Pedro");
            Assert.NotNull(name);
        }

        [Fact]
        public void CreateValidNameShouldPass13()
        {
            AccountName name = new AccountName("§╬,.|()[]'\"?!*ªºPedro");
            Assert.NotNull(name);
        }

        [Fact]
        public void ValidateNullNameShouldFail()
        {
            try {
                new AccountName(null);
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.NULL_NAME, ex.Error);
            }
        }

        [Fact]
        public void ValidateEmptyNameShouldFail()
        {
            try {
                new AccountName("");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.EMPTY_NAME, ex.Error);
            }
        }

        [Fact]
        public void ValidateOnlySpacesNameShouldFail()
        {
            try {
                new AccountName("\n\t     \v");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.EMPTY_NAME, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooShortNameShouldFail()
        {
            try {
                new AccountName("p");
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.NAME_TOO_SMALL, ex.Error);
            }
        }

        [Fact]
        public void ValidateTooLongNameShouldFail()
        {
            try {
                string s = "pedro Correia3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890Wowowowowo";
                new AccountName(s);
                // 201 chars
                Assert.True(false, "The name should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.NAME_TOO_LARGE, ex.Error);
            }
        }

        [Fact]
        public void SameNameShouldPass()
        {
            AccountName id1 = new AccountName("Pedro Correia");
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualNameShouldPass()
        {
            AccountName id1 = new AccountName("Pedro Correia");
            AccountName id2 = new AccountName("Pedro Correia");
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentNameShouldFail()
        {
            AccountName id1 = new AccountName("Pedro Correia");
            AccountName id2 = new AccountName("Pedro Correia 2");
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            AccountName id1 = new AccountName("pedro.correia@armis.pt");
            AccountEmail id2 = new AccountEmail("pedro.correia@armis.pt");
            Assert.False(id1.Equals(id2), "An email and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            AccountName id1 = new AccountName("Pedro Correia");
            Assert.False(id1.Equals(null), "An email and null should not be equal");
        }
    }
}
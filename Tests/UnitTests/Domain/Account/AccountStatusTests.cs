

using Backend.Accounts;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class AccountStatusTests
    {

        [Fact]
        public void SameStatusShouldPass()
        {
            AccountStatus id1 = AccountStatuses.Active;
            Assert.Equal(id1, id1);
        }

        [Fact]
        public void TwoEqualStatusShouldPass()
        {
            AccountStatus id1 = AccountStatuses.Active;
            AccountStatus id2 = AccountStatuses.Active;
            Assert.Equal(id1, id2);
        }

        [Fact]
        public void TwoDiferentStatusShouldFail()
        {
            AccountStatus id1 = AccountStatuses.Active;
            AccountStatus id2 = AccountStatuses.Suspended;
            Assert.NotEqual(id1, id2);
        }

        [Fact]
        public void TwoDiferentClassesShouldFail()
        {
            AccountStatus id1 = AccountStatuses.Active;
            AccountName id2 = new AccountName("Active");
            Assert.False(id1.Equals(id2), "A status and a name should not be equal");
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            AccountStatus id1 = AccountStatuses.Active;
            Assert.False(id1.Equals(null), "A status and null should not be equal");
        }

        [Fact]
        public void GetValidStatusShouldPass()
        {
            AccountStatus status = AccountStatuses.Active;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidStatusShouldPass2()
        {
            AccountStatus status = AccountStatuses.Banned;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidStatusShouldPass3()
        {
            AccountStatus status = AccountStatuses.Suspended;
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidStatusByNameShouldPass()
        {
            AccountStatus status = AccountStatuses.GetByName("Active");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidStatusByNameShouldPass2()
        {
            AccountStatus status = AccountStatuses.GetByName("Banned");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetValidStatusByNameShouldPass3()
        {
            AccountStatus status = AccountStatuses.GetByName("Suspended");
            Assert.NotNull(status);
        }

        [Fact]
        public void GetNullStatusByNameShouldFail()
        {
            try {
                AccountStatuses.GetByName(null);
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.NULL_STATUS, ex.Error);
            }
        }

        [Fact]
        public void GetEmptyStatusByNameShouldFail()
        {
            try {
                AccountStatuses.GetByName("");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.UNKNOWN_STATUS, ex.Error);
            }
        }

        [Fact]
        public void GetInvalidStatusByNameShouldFail()
        {
            try {
                AccountStatuses.GetByName("Invalid");
                Assert.True(false, "The status should not be valid");
            } catch (BusinessRuleFailureException ex){
                Assert.Equal(AccountErrors.UNKNOWN_STATUS, ex.Error);
            }
        }

        [Fact]
        public void GetAllStatusShouldPass()
        {
            AccountStatus[] statuses = AccountStatuses.GetAll();
            Assert.Equal(3, statuses.Length);
            Assert.DoesNotContain(null, statuses);
        }
    }
}

using Backend.Accounts;
using Backend.Utils;
using Xunit;

namespace Tests.Domain
{
    public class AccountTests
    {

        [Fact]
        public void CreateValidAccountShouldPass()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account = new Account(id, name, email, status, type);
            Assert.NotNull(account);
        }

        [Fact]
        public void CreateAccountWithNullIdShouldFail()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            try
            {
                Account account = new Account(null, name, email, status, type);
                Assert.True(false, "The account should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(AccountErrors.NULL_DATA_IN_ACCOUNT, ex.Error);
            }
        }

        [Fact]
        public void CreateAccountWithNullNameShouldFail()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            try
            {
                Account account = new Account(id, null, email, status, type);
                Assert.True(false, "The account should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(AccountErrors.NULL_DATA_IN_ACCOUNT, ex.Error);
            }
        }

        [Fact]
        public void CreateAccountWithNullEmailShouldFail()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            try
            {
                Account account = new Account(id, name, null, status, type);
                Assert.True(false, "The account should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(AccountErrors.NULL_DATA_IN_ACCOUNT, ex.Error);
            }
        }

        [Fact]
        public void CreateAccountWithNullStatusShouldFail()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            try
            {
                Account account = new Account(id, name, email, null, type);
                Assert.True(false, "The account should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(AccountErrors.NULL_DATA_IN_ACCOUNT, ex.Error);
            }
        }

        [Fact]
        public void CreateAccountWithNullTypeShouldFail()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            try
            {
                Account account = new Account(id, name, email, status, null);
                Assert.True(false, "The account should not be valid");
            }
            catch (BusinessRuleFailureException ex)
            {
                Assert.Equal(AccountErrors.NULL_DATA_IN_ACCOUNT, ex.Error);
            }
        }

        [Fact]
        public void SameAccountShouldPass()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account = new Account(id, name, email, status, type);
            Assert.Equal(account, account);
        }

        [Fact]
        public void TwoEqualAccountShouldPass()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account1 = new Account(id, name, email, status, type);
            Account account2 = new Account(id, name, email, status, type);
            Assert.Equal(account1, account2);
        }

        [Fact]
        public void TwoDifferentAccountsShouldFail()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountId id2 = new AccountId("542a5fe9-843b-4766-846c-303d80df5ec8");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account1 = new Account(id, name, email, status, type);
            Account account2 = new Account(id2, name, email, status, type);
            Assert.NotEqual(account1, account2);
        }

        [Fact]
        public void TwoDifferentClassesShouldFail()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account = new Account(id, name, email, status, type);
            AccountEmail id2 = new AccountEmail("pedro.correia@armis.pt");
            Assert.False(account.Equals(id2), "An account and an email should not be equal");
        }

        [Fact]
        public void OnlyIdShouldMatter()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountEmail email2 = new AccountEmail("pedro.correia@gmail.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountName name2 = new AccountName("Pedro Correia 2");
            AccountStatus status = AccountStatuses.Active;
            AccountStatus status2 = AccountStatuses.Banned;
            AccountType type = AccountTypes.Client;
            AccountType type2 = AccountTypes.Admin;
            Account account1 = new Account(id, name, email, status, type);
            Account account2 = new Account(id, name2, email2, status2, type2);
            Assert.Equal(account1, account2);
        }

        [Fact]
        public void EqualsWithNullShouldFail()
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account = new Account(id, name, email, status, type);
            Assert.False(account.Equals(null), "An account and null should not be equal");
        }
    }
}


using System.Threading.Tasks;
using Backend.Tenants;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class GetTenantServiceTests
    {

        private ITenantService Service = new TenantServiceImpl();
        
        [Fact]
        public async void ConsultingTenantSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new TenantDTOFactoryMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultTenantServices services = new ConsultTenantServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetTenantRequest request = new GetTenantRequest();
            GetOneResults<TenantDTO> results = await Service.GetOne(services, request);
            Assert.True(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingUnknownTenantShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new TenantDTOFactoryMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "F", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultTenantServices services = new ConsultTenantServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetTenantRequest request = new GetTenantRequest();
            GetOneResults<TenantDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingTenantWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new TenantDTOFactoryMock();
            var accountFactoryMock = new TenantFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultTenantServices services = new ConsultTenantServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetTenantRequest request = new GetTenantRequest();
            GetOneResults<TenantDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingTenantsPageSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new TenantDTOFactoryMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultTenantServices services = new ConsultTenantServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            ConsultTenantsParameters request = new ConsultTenantsParameters();
            GetPageResults<TenantDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void ConsultingTenantsPageWithErrorShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new TenantDTOFactoryMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultTenantServices services = new ConsultTenantServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            ConsultTenantsParameters request = new ConsultTenantsParameters();
            GetPageResults<TenantDTO> results = await Service.GetPage(services, request);
            Assert.False(results.success);
        }
    }
}
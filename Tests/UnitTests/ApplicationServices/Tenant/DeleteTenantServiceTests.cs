

using System.Threading.Tasks;
using Backend.Tenants;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class DeleteTenantServiceTests
    {

        private ITenantService Service = new TenantServiceImpl();
        
        [Fact]
        public async void DeletingTenantSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteTenantServices services = new DeleteTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteTenantRequest request = new DeleteTenantRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void DeletingTenantWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteTenantServices services = new DeleteTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteTenantRequest request = new DeleteTenantRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.True(results.itExists);
        }
        
        [Fact]
        public async void DeletingTenantWithoutDeletingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "F", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteTenantServices services = new DeleteTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteTenantRequest request = new DeleteTenantRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.True(results.itExists);
        }
        
        [Fact]
        public async void DeletingTenantWithoutValidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteTenantServices services = new DeleteTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteTenantRequest request = new DeleteTenantRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void DeletingUnknownTenantShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteTenantServices services = new DeleteTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteTenantRequest request = new DeleteTenantRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void DeletingTenantWithAllFailureShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new TenantRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteTenantServices services = new DeleteTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteTenantRequest request = new DeleteTenantRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
    }
}


using System.Threading.Tasks;
using Backend.Tenants;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class UpdateTenantServiceTests
    {

        private ITenantService Service = new TenantServiceImpl();
        
        [Fact]
        public async void UpdatingTenantSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdatingTenantWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.True(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingUnknownTenantShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingTenantWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingTenantWithInvalidDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void UpdatingTenantWithEverythingFailingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new TenantFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new TenantRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateTenantServices services = new UpdateTenantServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateTenantRequest request = new UpdateTenantRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
        }
    }
}
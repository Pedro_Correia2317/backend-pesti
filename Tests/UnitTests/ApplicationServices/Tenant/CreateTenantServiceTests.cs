

using System.Threading.Tasks;
using Backend.Tenants;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class CreateTenantServiceTests
    {

        private ITenantService Service = new TenantServiceImpl();
        
        [Fact]
        public async void CreatingTenantSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithoutSavingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithFailingCodeShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("F", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithInvalidNameShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "F", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithInvalidMemberShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "F", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithInvalidDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "F");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithoutValidEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "F");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithFailureSavingEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithInvalidAccountIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithUnknownAccountIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingTenantWithEverythingFailingDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var tenantDTOFactoryMock = new TenantDTOFactoryMock();
            var tenantFactoryMock = new TenantFactoryMock("F", "F", "F", "F", "F");
            var tenantRepoMock = new TenantRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F");
            var accountFactoryMock = new AccountFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new AccountRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var entryFactoryMock = new HistoryEntryFactoryMock("F", "F", "F", "F", "F");
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorMock("F", "F", "F", "F", "F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateTenantServices services = new CreateTenantServices(errorCollectorMock, tenantDTOFactoryMock, 
                tenantFactoryMock, accountFactoryMock, entryFactoryMock, tenantRepoMock, accountRepoMock,
                entryRepoMock, translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateTenantRequest request = new CreateTenantRequest();
            CreateResults<TenantDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
    }
}
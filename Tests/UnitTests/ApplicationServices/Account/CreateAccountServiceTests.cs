

using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class CreateAccountServiceTests
    {

        private IAccountService Service = new AccountServiceImpl();
        
        [Fact]
        public async void CreatingAccountSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void CreatingAccountWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithoutSavingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "F", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithRepeatedEmailShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithFailingCodeShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("F", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithInvalidNameShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "F", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithInvalidEmailShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "F", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithInvalidTypeShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "F", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithInvalidDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "F");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithInvalidEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "F");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithoutSavingEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("E", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void CreatingAccountWithoutValidDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingAccountWithEverythingFailingDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new AccountRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var entryFactoryMock = new HistoryEntryFactoryMock("F", "F", "F", "F", "F");
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorMock("F", "F", "F", "F", "F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateAccountServices services = new CreateAccountServices(errorCollectorMock, accountDTOFactoryMock, 
                accountFactoryMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, unitMock, codeMock, loggerMock, utilsDTOMock);
            CreateAccountRequest request = new CreateAccountRequest();
            CreateResults<AccountDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
    }
}
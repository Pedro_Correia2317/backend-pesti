

using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class DeleteAccountServiceTests
    {

        private IAccountService Service = new AccountServiceImpl();
        
        [Fact]
        public async void DeletingAccountSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteAccountServices services = new DeleteAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteAccountRequest request = new DeleteAccountRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void DeletingAccountWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteAccountServices services = new DeleteAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteAccountRequest request = new DeleteAccountRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.True(results.itExists);
        }
        
        [Fact]
        public async void DeletingAccountWithoutDeletingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "F", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteAccountServices services = new DeleteAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteAccountRequest request = new DeleteAccountRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.True(results.itExists);
        }
        
        [Fact]
        public async void DeletingAccountWithoutValidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteAccountServices services = new DeleteAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteAccountRequest request = new DeleteAccountRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void DeletingUnknownAccountShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteAccountServices services = new DeleteAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteAccountRequest request = new DeleteAccountRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void DeletingAccountWithAllFailureShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new AccountRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteAccountServices services = new DeleteAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteAccountRequest request = new DeleteAccountRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
    }
}
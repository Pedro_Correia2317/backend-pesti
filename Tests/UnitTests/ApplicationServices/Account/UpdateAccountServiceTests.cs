

using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class UpdateAccountServiceTests
    {

        private IAccountService Service = new AccountServiceImpl();
        
        [Fact]
        public async void UpdatingAccountSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdatingAccountWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.True(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingUnknownAccountShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingAccountWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingAccountWithInvalidDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void UpdatingAccountWithEverythingFailingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new AccountFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new AccountRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateAccountServices services = new UpdateAccountServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateAccountRequest request = new UpdateAccountRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
        }
    }
}
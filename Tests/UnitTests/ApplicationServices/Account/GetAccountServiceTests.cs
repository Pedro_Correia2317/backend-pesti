

using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class GetAccountServiceTests
    {

        private IAccountService Service = new AccountServiceImpl();
        
        [Fact]
        public async void ConsultingAccountSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultAccountServices services = new ConsultAccountServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetAccountRequest request = new GetAccountRequest();
            GetOneResults<AccountDTO> results = await Service.GetOne(services, request);
            Assert.True(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingUnknownAccountShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultAccountServices services = new ConsultAccountServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetAccountRequest request = new GetAccountRequest();
            GetOneResults<AccountDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingAccountWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultAccountServices services = new ConsultAccountServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetAccountRequest request = new GetAccountRequest();
            GetOneResults<AccountDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingAccountsPageSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultAccountServices services = new ConsultAccountServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            ConsultAccountsParameters request = new ConsultAccountsParameters();
            GetPageResults<AccountDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void ConsultingAccountsPageWithErrorShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new AccountDTOFactoryMock();
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "F", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultAccountServices services = new ConsultAccountServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            ConsultAccountsParameters request = new ConsultAccountsParameters();
            GetPageResults<AccountDTO> results = await Service.GetPage(services, request);
            Assert.False(results.success);
        }
    }
}
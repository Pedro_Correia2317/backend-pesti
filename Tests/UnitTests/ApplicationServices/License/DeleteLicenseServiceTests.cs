

using System.Threading.Tasks;
using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class DeleteLicenseServiceTests
    {

        private ILicenseService Service = new LicenseServiceImpl();
        
        [Fact]
        public async void DeletingLicenseSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void DeletingLicenseWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.True(results.itExists);
        }
        
        [Fact]
        public async void DeletingLicenseWithoutDeletingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "F", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.True(results.itExists);
        }
        
        [Fact]
        public async void DeletingLicenseWithoutValidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void DeletingUnknownLicenseShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void DeletingLicenseWithAllFailureShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new LicenseRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            DeleteLicenseServices services = new DeleteLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            DeleteLicenseRequest request = new DeleteLicenseRequest();
            DeleteResults results = await Service.Delete(services, request);
            Assert.False(results.success);
            Assert.Equal(1, errorCollectorMock.NumErrors);
            Assert.False(results.itExists);
        }
    }
}
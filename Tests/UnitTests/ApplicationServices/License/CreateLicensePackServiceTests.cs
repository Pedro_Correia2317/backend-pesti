

using System.Threading.Tasks;
using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class CreateLicensePackServiceTests
    {

        private ILicenseService Service = new LicenseServiceImpl();
        
        [Fact]
        public async void CreatingLicenseSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithoutSavingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "F", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithoutValidRequestShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("F");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithFailingCodeShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "F", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("F", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidDateShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "F", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidDurationShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "F", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidNumberMeetingsShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "F", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidRemainingMeetingsShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "F", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidTypeShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "F", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "F");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithoutValidEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "F");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithFailureSavingEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidTenantIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("F", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithUnknownTenantIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var tenantFactoryMock = new TenantFactoryMock("S", "S", "S", "S", "S");
            var tenantRepoMock = new TenantRepositoryMock("S", "S", "S", "S", "S", "S", "F", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("S", "S");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithEverythingFailingDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("F", "F", "F", "F", "F", "F", "F");
            var licenseRepoMock = new LicenseRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var tenantFactoryMock = new TenantFactoryMock("F", "F", "F", "F", "F");
            var tenantRepoMock = new TenantRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F");
            var entryFactoryMock = new HistoryEntryFactoryMock("F", "F", "F", "F", "F");
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorMock("F", "F", "F", "F", "F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            var requestAnalMock = new RequestAnalyserMock("F");
            CreateLicensePackServices services = new CreateLicensePackServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, tenantFactoryMock, entryFactoryMock, licenseRepoMock, tenantRepoMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock, requestAnalMock);
            CreateLicensePackRequest request = new CreateLicensePackRequest();
            CreatePackResults<LicenseDTO> results = await Service.CreatePack(services, request);
            Assert.False(results.success);
        }
    }
}


using System.Threading.Tasks;
using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class GetLicenseServiceTests
    {

        private ILicenseService Service = new LicenseServiceImpl();
        
        [Fact]
        public async void ConsultingLicenseSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new LicenseDTOFactoryMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetLicenseRequest request = new GetLicenseRequest();
            GetOneResults<LicenseDTO> results = await Service.GetOne(services, request);
            Assert.True(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingUnknownLicenseShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new LicenseDTOFactoryMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetLicenseRequest request = new GetLicenseRequest();
            GetOneResults<LicenseDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingLicenseWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new LicenseDTOFactoryMock();
            var accountFactoryMock = new LicenseFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            GetLicenseRequest request = new GetLicenseRequest();
            GetOneResults<LicenseDTO> results = await Service.GetOne(services, request);
            Assert.False(results.finishedSuccesfully);
        }
        
        [Fact]
        public async void ConsultingLicensesPageSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new LicenseDTOFactoryMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            ConsultLicensesParameters request = new ConsultLicensesParameters();
            GetPageResults<LicenseDTO> results = await Service.GetPage(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void ConsultingLicensesPageWithErrorShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountDTOFactoryMock = new LicenseDTOFactoryMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "F", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            ConsultLicenseServices services = new ConsultLicenseServices(errorCollectorMock, accountDTOFactoryMock,
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, utilsDTOMock);
            ConsultLicensesParameters request = new ConsultLicensesParameters();
            GetPageResults<LicenseDTO> results = await Service.GetPage(services, request);
            Assert.False(results.success);
        }
    }
}
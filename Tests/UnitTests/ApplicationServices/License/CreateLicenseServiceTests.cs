

using System.Threading.Tasks;
using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class CreateLicenseServiceTests
    {

        private ILicenseService Service = new LicenseServiceImpl();
        
        [Fact]
        public async void CreatingLicenseSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithoutSavingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "F", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithoutNewAccountShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithFailingCodeShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "F", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("F", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidDateShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "F", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidDurationShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "F", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidNumberMeetingsShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "F", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidRemainingMeetingsShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "F", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidTypeShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "F", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "F");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithoutValidEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "F");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithFailureSavingEntryShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithInvalidAccountIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithUnknownAccountIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S", "S");
            var licenseRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var accountFactoryMock = new AccountFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new AccountRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var entryFactoryMock = new HistoryEntryFactoryMock("S", "S", "S", "S", "S");
            var entryRepoMock = new HistoryEntryRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var codeMock = new CodeGeneratorMock("S", "S", "S", "S", "S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
        
        [Fact]
        public async void CreatingLicenseWithEverythingFailingDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var licenseDTOFactoryMock = new LicenseDTOFactoryMock();
            var licenseFactoryMock = new LicenseFactoryMock("F", "F", "F", "F", "F", "F", "F");
            var licenseRepoMock = new LicenseRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var accountFactoryMock = new AccountFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new AccountRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var entryFactoryMock = new HistoryEntryFactoryMock("F", "F", "F", "F", "F");
            var entryRepoMock = new HistoryEntryRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var codeMock = new CodeGeneratorMock("F", "F", "F", "F", "F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            CreateLicenseServices services = new CreateLicenseServices(errorCollectorMock, licenseDTOFactoryMock, 
                licenseFactoryMock, accountFactoryMock, licenseRepoMock, accountRepoMock, entryFactoryMock, entryRepoMock,
                translatorMock, codeMock, loggerMock, unitMock, utilsDTOMock);
            CreateLicenseRequest request = new CreateLicenseRequest();
            CreateResults<LicenseDTO> results = await Service.Create(services, request);
            Assert.False(results.success);
        }
    }
}
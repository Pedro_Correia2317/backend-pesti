

using System.Threading.Tasks;
using Backend.Licenses;
using Backend.Utils;
using Tests.Mocks;
using Xunit;

namespace Tests.Services
{
    public class UpdateLicenseServiceTests
    {

        private ILicenseService Service = new LicenseServiceImpl();
        
        [Fact]
        public async void UpdatingLicenseSuccessfullyShouldPass()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.True(results.success);
        }
        
        [Fact]
        public async void UpdatingLicenseWithoutCommitingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.True(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingUnknownLicenseShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("S", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "F", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingLicenseWithInvalidIdShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("F", "S", "S", "S", "S", "S");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
            Assert.Equal(1, errorCollectorMock.NumErrors);
        }
        
        [Fact]
        public async void UpdatingLicenseWithInvalidDataShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new LicenseRepositoryMock("S", "S", "S", "S", "S", "S", "S", "S", "S", "S");
            var unitMock = new UnitOfWorkMock("S");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
        }
        
        [Fact]
        public async void UpdatingLicenseWithEverythingFailingShouldFail()
        {
            var errorCollectorMock = new ErrorCollectorMock();
            var loggerMock = new LoggerMock();
            var translatorMock = new TranslatorMock();
            var accountFactoryMock = new LicenseFactoryMock("F", "F", "F", "F", "F", "F");
            var accountRepoMock = new LicenseRepositoryMock("F", "F", "F", "F", "F", "F", "F", "F", "F", "F");
            var unitMock = new UnitOfWorkMock("F");
            var utilsDTOMock = new UtilsDTOFactoryMock();
            UpdateLicenseServices services = new UpdateLicenseServices(errorCollectorMock, 
                accountFactoryMock, accountRepoMock, translatorMock, loggerMock, unitMock, utilsDTOMock);
            UpdateLicenseRequest request = new UpdateLicenseRequest();
            UpdateResults results = await Service.Update(services, request);
            Assert.False(results.success);
            Assert.False(results.itExists);
        }
    }
}


using System.Collections.Generic;
using Backend.Tenants;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class TenantDTOFactoryMock : ITenantDTOFactory
    {

        public TenantDTO CreateBasicDTO(Tenant account)
        {
            return new TenantDTO();
        }

        public TenantDTO[] CreateListOfBasicDTO(Tenant[] accounts)
        {
            return new TenantDTO[] {};
        }

        public TenantDTO[] CreateListOfBasicDTO(ICollection<Tenant> types)
        {
            return new TenantDTO[] {};
        }

        public QueryParams CreateQueryParamsOf(ConsultTenantsParameters request)
        {
            return new QueryParams();
        }
    }
}
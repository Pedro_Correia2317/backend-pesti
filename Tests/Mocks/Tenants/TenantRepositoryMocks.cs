

using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Tests.Mocks
{
    public class TenantRepositoryMock : ITenantRepository
    {

        private string[] Behaviours;

        public TenantRepositoryMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public Task<QueryResults<bool>> Save(Tenant element)
        {
            switch (Behaviours?[0])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> SaveAll(ICollection<Tenant> element)
        {
            switch (Behaviours?[1])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Delete(Tenant element)
        {
            switch (Behaviours?[2])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Update(Tenant element)
        {
            switch (Behaviours?[3])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> isIdUnique(TenantId Id)
        {
            switch (Behaviours?[4])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> AreIdsValid(List<TenantId> Id)
        {
            switch (Behaviours?[5])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<Tenant>> FindById(TenantId Id)
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant = new Tenant(id, name, member);
            switch (Behaviours?[6])
            {
                case "S":
                    return Task.Run(() => new QueryResults<Tenant>().withReturnedObject(tenant));
                default:
                    return Task.Run(() => new QueryResults<Tenant>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<PageQueryResults<Tenant>> FindByPage(QueryParams Params)
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant = new Tenant(id, name, member);
            var list = new List<Tenant>();
            list.Add(tenant);
            switch (Behaviours?[7])
            {
                case "S":
                    return Task.Run(() => new PageQueryResults<Tenant>().withReturnedObject(list, 1));
                default:
                    return Task.Run(() => new PageQueryResults<Tenant>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<int>> CountElements(QueryParams Params)
        {
            switch (Behaviours?[8])
            {
                case "S":
                    return Task.Run(() => new QueryResults<int>().withReturnedObject(1));
                default:
                    return Task.Run(() => new QueryResults<int>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<Tenant>> FindByMember(AccountId adminId)
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant = new Tenant(id, name, member);
            switch (Behaviours?[9])
            {
                case "S":
                    return Task.Run(() => new QueryResults<Tenant>().withReturnedObject(tenant));
                default:
                    return Task.Run(() => new QueryResults<Tenant>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }
    }
}
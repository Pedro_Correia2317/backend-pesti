

using System;
using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class TenantFactoryMock : ITenantFactory
    {

        private string[] Behaviours;

        public TenantFactoryMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public TenantContainer NewContainer()
        {
            return new TenantContainer();
        }

        public Results<TenantId> NewId(string id)
        {
            switch(Behaviours?[0])
            {
                case "S":
                    return new Results<TenantId>().withReturnedObject(new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619"));
                default:
                    return new Results<TenantId>().withFailure(TenantErrors.NULL_ID.Copy());
            }
        }

        public Results<TenantName> NewName(string name)
        {
            switch(Behaviours?[1])
            {
                case "S":
                    return new Results<TenantName>().withReturnedObject(new TenantName("Pedro Correia"));
                default:
                    return new Results<TenantName>().withFailure(TenantErrors.NULL_ID.Copy());
            }
        }

        public Results<TenantMember> NewMember(AccountId account, TenantPermissions permissions)
        {
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            switch(Behaviours?[2])
            {
                case "S":
                    return new Results<TenantMember>().withReturnedObject(new TenantMember(aId, TenantPermissionList.Owner));
                default:
                    return new Results<TenantMember>().withFailure(TenantErrors.NULL_ID.Copy());
            }
        }

        public TenantPermissions NewOwnerPermissions()
        {
            return TenantPermissionList.Owner;
        }

        public Results<TenantPermissions> NewPermissions(string permissions)
        {
            switch(Behaviours?[3])
            {
                case "S":
                    return new Results<TenantPermissions>().withReturnedObject(TenantPermissionList.Owner);
                default:
                    return new Results<TenantPermissions>().withFailure(TenantErrors.NULL_ID.Copy());
            }
        }

        public Results<Tenant> NewTenant(TenantContainer container)
        {
            TenantId id = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantName name = new TenantName("Pedro Correia");
            AccountId aId = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            TenantMember member = new TenantMember(aId, TenantPermissionList.Owner);
            Tenant tenant = new Tenant(id, name, member);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<Tenant>().withReturnedObject(tenant);
                default:
                    return new Results<Tenant>().withFailure(TenantErrors.NULL_ID.Copy());
            }
        }
    }
}
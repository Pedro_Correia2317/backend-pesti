

using System.Collections.Generic;
using Backend.Accounts;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class AccountDTOFactoryMock : IAccountDTOFactory
    {

        public AccountDTO CreateBasicDTO(Account account)
        {
            return new AccountDTO();
        }

        public AccountDTO[] CreateListOfBasicDTO(Account[] accounts)
        {
            return new AccountDTO[] {};
        }

        public AccountStatusDTO[] CreateListStatusDTO(AccountStatus[] types)
        {
            return new AccountStatusDTO[] {};
        }

        public AccountTypeDTO[] CreateListTypesDTO(AccountType[] types)
        {
            return new AccountTypeDTO[] {};
        }

        public QueryParams CreateQueryParamsOf(ConsultAccountsParameters request)
        {
            return new QueryParams();
        }

        public AccountStatusDTO CreateStatusDTO(AccountStatus type)
        {
            return new AccountStatusDTO();
        }

        public AccountTypeDTO CreateTypeDTO(AccountType type)
        {
            return new AccountTypeDTO();
        }
    }
}
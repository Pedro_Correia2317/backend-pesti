

using Backend.Accounts;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class AccountFactoryMock : IAccountFactory
    {

        private string[] Behaviours;

        public AccountFactoryMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public AccountContainer NewContainer()
        {
            return new AccountContainer();
        }

        public Results<AccountId> NewId(string id)
        {
            switch(Behaviours?[0])
            {
                case "S":
                    return new Results<AccountId>().withReturnedObject(new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619"));
                default:
                    return new Results<AccountId>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<AccountName> NewName(string name)
        {
            switch(Behaviours?[1])
            {
                case "S":
                    return new Results<AccountName>().withReturnedObject(new AccountName("Pedro Correia"));
                default:
                    return new Results<AccountName>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<AccountEmail> NewEmail(string email)
        {
            switch(Behaviours?[2])
            {
                case "S":
                    return new Results<AccountEmail>().withReturnedObject(new AccountEmail("pedro.correia@armis.pt"));
                default:
                    return new Results<AccountEmail>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<AccountStatus> NewStatus(string status)
        {
            switch(Behaviours?[3])
            {
                case "S":
                    return new Results<AccountStatus>().withReturnedObject(AccountStatuses.Active);
                default:
                    return new Results<AccountStatus>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<AccountStatus> NewDefaultStatus()
        {
            return new Results<AccountStatus>().withReturnedObject(AccountStatuses.Active);
        }

        public Results<AccountType> NewType(string type)
        {
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<AccountType>().withReturnedObject(AccountTypes.Client);
                default:
                    return new Results<AccountType>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<Account> NewAccount(AccountContainer container)
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account = new Account(id, name, email, status, type);
            switch(Behaviours?[5])
            {
                case "S":
                    return new Results<Account>().withReturnedObject(account);
                default:
                    return new Results<Account>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }
    }
}
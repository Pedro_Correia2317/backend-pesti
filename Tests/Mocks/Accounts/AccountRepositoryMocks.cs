

using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class AccountRepositoryMock : IAccountRepository
    {

        private string[] Behaviours;

        public AccountRepositoryMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public Task<QueryResults<bool>> IsEmailUnique(AccountEmail email)
        {
            switch (Behaviours?[0])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Save(Account element)
        {
            switch (Behaviours?[1])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> SaveAll(ICollection<Account> element)
        {
            switch (Behaviours?[2])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Delete(Account element)
        {
            switch (Behaviours?[3])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Update(Account element)
        {
            switch (Behaviours?[4])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> isIdUnique(AccountId Id)
        {
            switch (Behaviours?[5])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> AreIdsValid(List<AccountId> Id)
        {
            switch (Behaviours?[6])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<Account>> FindById(AccountId Id)
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account = new Account(id, name, email, status, type);
            switch (Behaviours?[7])
            {
                case "S":
                    return Task.Run(() => new QueryResults<Account>().withReturnedObject(account));
                default:
                    return Task.Run(() => new QueryResults<Account>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<PageQueryResults<Account>> FindByPage(QueryParams Params)
        {
            AccountId id = new AccountId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            AccountEmail email = new AccountEmail("pedro.correia@armis.pt");
            AccountName name = new AccountName("Pedro Correia");
            AccountStatus status = AccountStatuses.Active;
            AccountType type = AccountTypes.Client;
            Account account = new Account(id, name, email, status, type);
            var list = new List<Account>();
            list.Add(account);
            switch (Behaviours?[8])
            {
                case "S":
                    return Task.Run(() => new PageQueryResults<Account>().withReturnedObject(list, 1));
                default:
                    return Task.Run(() => new PageQueryResults<Account>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<int>> CountElements(QueryParams Params)
        {
            switch (Behaviours?[9])
            {
                case "S":
                    return Task.Run(() => new QueryResults<int>().withReturnedObject(1));
                default:
                    return Task.Run(() => new QueryResults<int>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }
    }
}
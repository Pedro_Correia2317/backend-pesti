

using System.Collections.Generic;
using Backend.Licenses;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class LicenseDTOFactoryMock : ILicenseDTOFactory
    {

        public LicenseDTO CreateBasicDTO(License account)
        {
            return new LicenseDTO();
        }

        public LicenseDTO[] CreateListOfBasicDTO(License[] accounts)
        {
            return new LicenseDTO[] {};
        }

        public LicenseDTO[] CreateListOfBasicDTO(ICollection<License> types)
        {
            return new LicenseDTO[] {};
        }

        public LicenseTypeDTO[] CreateListTypesDTO(LicenseType[] types)
        {
            return new LicenseTypeDTO[] {};
        }

        public QueryParams CreateQueryParamsOf(ConsultLicensesParameters request)
        {
            return new QueryParams();
        }

        public LicenseTypeDTO CreateTypeDTO(LicenseType type)
        {
            return new LicenseTypeDTO();
        }
    }
}
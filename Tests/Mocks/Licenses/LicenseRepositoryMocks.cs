

using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Licenses;
using Backend.Utils;

namespace Tests.Mocks
{
    public class LicenseRepositoryMock : ILicenseRepository
    {

        private string[] Behaviours;

        public LicenseRepositoryMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public Task<QueryResults<bool>> HasNoLicense(AccountId account)
        {
            switch (Behaviours?[0])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Save(License element)
        {
            switch (Behaviours?[1])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> SaveAll(ICollection<License> element)
        {
            switch (Behaviours?[2])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Delete(License element)
        {
            switch (Behaviours?[3])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Update(License element)
        {
            switch (Behaviours?[4])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> isIdUnique(LicenseId Id)
        {
            switch (Behaviours?[5])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> AreIdsValid(List<LicenseId> Id)
        {
            switch (Behaviours?[6])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<License>> FindById(LicenseId Id)
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            License license = new License(id, date, dur, number, rem, type);
            switch (Behaviours?[7])
            {
                case "S":
                    return Task.Run(() => new QueryResults<License>().withReturnedObject(license));
                default:
                    return Task.Run(() => new QueryResults<License>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<PageQueryResults<License>> FindByPage(QueryParams Params)
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            License license = new License(id, date, dur, number, rem, type);
            var list = new List<License>();
            list.Add(license);
            switch (Behaviours?[8])
            {
                case "S":
                    return Task.Run(() => new PageQueryResults<License>().withReturnedObject(list, 1));
                default:
                    return Task.Run(() => new PageQueryResults<License>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<int>> CountElements(QueryParams Params)
        {
            switch (Behaviours?[9])
            {
                case "S":
                    return Task.Run(() => new QueryResults<int>().withReturnedObject(1));
                default:
                    return Task.Run(() => new QueryResults<int>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }
    }
}
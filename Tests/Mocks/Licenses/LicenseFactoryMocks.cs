

using System;
using Backend.Licenses;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class LicenseFactoryMock : ILicenseFactory
    {

        private string[] Behaviours;

        public LicenseFactoryMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public LicenseContainer NewContainer()
        {
            return new LicenseContainer();
        }

        public Results<LicenseId> NewId(string id)
        {
            switch(Behaviours?[0])
            {
                case "S":
                    return new Results<LicenseId>().withReturnedObject(new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619"));
                default:
                    return new Results<LicenseId>().withFailure(LicenseErrors.NULL_ID.Copy());
            }
        }

        public Results<LicenseDate> NewDate(string name)
        {
            switch(Behaviours?[1])
            {
                case "S":
                    return new Results<LicenseDate>().withReturnedObject(new LicenseDate("01-01-2021"));
                default:
                    return new Results<LicenseDate>().withFailure(LicenseErrors.NULL_ID.Copy());
            }
        }

        public Results<LicenseDate> NewDate(DateTime date)
        {
            switch(Behaviours?[1])
            {
                case "S":
                    return new Results<LicenseDate>().withReturnedObject(new LicenseDate("01-01-2021"));
                default:
                    return new Results<LicenseDate>().withFailure(LicenseErrors.NULL_ID.Copy());
            }
        }

        public Results<LicenseDuration> NewDuration(string duration)
        {
            switch(Behaviours?[2])
            {
                case "S":
                    return new Results<LicenseDuration>().withReturnedObject(new LicenseDuration("P4Y"));
                default:
                    return new Results<LicenseDuration>().withFailure(LicenseErrors.NULL_ID.Copy());
            }
        }

        public Results<LicenseNumberMeetings> NewMeetings(int? numMeetings)
        {
            switch(Behaviours?[3])
            {
                case "S":
                    return new Results<LicenseNumberMeetings>().withReturnedObject(new LicenseNumberMeetings(2000));
                default:
                    return new Results<LicenseNumberMeetings>().withFailure(LicenseErrors.NULL_ID.Copy());
            }
        }

        public Results<LicenseRemainingMeetings> NewRemainingMeetings(int? remMeetings)
        {
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<LicenseRemainingMeetings>().withReturnedObject(new LicenseRemainingMeetings(2000));
                default:
                    return new Results<LicenseRemainingMeetings>().withFailure(LicenseErrors.NULL_ID.Copy());
            }
        }

        public Results<LicenseType> NewType(string type)
        {
            switch(Behaviours?[5])
            {
                case "S":
                    return new Results<LicenseType>().withReturnedObject(LicenseTypes.Basic);
                default:
                    return new Results<LicenseType>().withFailure(LicenseErrors.NULL_ID.Copy());
            }
        }

        public Results<License> NewLicense(LicenseContainer container)
        {
            LicenseId id = new LicenseId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            LicenseDate date = new LicenseDate("01-01-2021");
            LicenseDuration dur = new LicenseDuration("P1DT1S");
            LicenseNumberMeetings number = new LicenseNumberMeetings(2000);
            LicenseRemainingMeetings rem = new LicenseRemainingMeetings(2000);
            LicenseType type = LicenseTypes.Basic;
            License license = new License(id, date, dur, number, rem, type);
            switch(Behaviours?[6])
            {
                case "S":
                    return new Results<License>().withReturnedObject(license);
                default:
                    return new Results<License>().withFailure(LicenseErrors.NULL_ID.Copy());
            }
        }
    }
}
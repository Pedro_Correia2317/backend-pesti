

using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.HistoryEntries;
using Backend.Tenants;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class HistoryEntryRepositoryMock : IHistoryEntryRepository
    {

        private string[] Behaviours;

        public HistoryEntryRepositoryMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public Task<QueryResults<bool>> Save(HistoryEntry element)
        {
            switch (Behaviours?[0])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> SaveAll(ICollection<HistoryEntry> element)
        {
            switch (Behaviours?[1])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Delete(HistoryEntry element)
        {
            switch (Behaviours?[2])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> Update(HistoryEntry element)
        {
            switch (Behaviours?[3])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> isIdUnique(EntryId Id)
        {
            switch (Behaviours?[4])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<bool>> AreIdsValid(List<EntryId> Id)
        {
            switch (Behaviours?[5])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<HistoryEntry>> FindById(EntryId Id)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
            switch (Behaviours?[6])
            {
                case "S":
                    return Task.Run(() => new QueryResults<HistoryEntry>().withReturnedObject(entry));
                default:
                    return Task.Run(() => new QueryResults<HistoryEntry>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<PageQueryResults<HistoryEntry>> FindByPage(QueryParams Params)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
            var list = new List<HistoryEntry>();
            list.Add(entry);
            switch (Behaviours?[7])
            {
                case "S":
                    return Task.Run(() => new PageQueryResults<HistoryEntry>().withReturnedObject(list, 1));
                default:
                    return Task.Run(() => new PageQueryResults<HistoryEntry>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<QueryResults<int>> CountElements(QueryParams Params)
        {
            switch (Behaviours?[8])
            {
                case "S":
                    return Task.Run(() => new QueryResults<int>().withReturnedObject(1));
                default:
                    return Task.Run(() => new QueryResults<int>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }

        public Task<PageQueryResults<HistoryEntry>> FindByPage(EntryQueryParams args)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType type = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, type, id2);
            var list = new List<HistoryEntry>();
            list.Add(entry);
            switch (Behaviours?[7])
            {
                case "S":
                    return Task.Run(() => new PageQueryResults<HistoryEntry>().withReturnedObject(list, 1));
                default:
                    return Task.Run(() => new PageQueryResults<HistoryEntry>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }
    }
}


using System;
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Tenants;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class HistoryEntryFactoryMock : IHistoryEntryFactory
    {

        private string[] Behaviours;

        public HistoryEntryFactoryMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public Results<EntryId> NewId(string id)
        {
            switch(Behaviours?[0])
            {
                case "S":
                    return new Results<EntryId>().withReturnedObject(new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619"));
                default:
                    return new Results<EntryId>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<EntryDate> NewDate(string date)
        {
            switch(Behaviours?[1])
            {
                case "S":
                    return new Results<EntryDate>().withReturnedObject(new EntryDate("01-01-2021"));
                default:
                    return new Results<EntryDate>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<EntryDate> NewDate(DateTime date)
        {
            switch(Behaviours?[1])
            {
                case "S":
                    return new Results<EntryDate>().withReturnedObject(new EntryDate("01-01-2021"));
                default:
                    return new Results<EntryDate>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<EntryDescription> NewDescription(string description)
        {
            switch(Behaviours?[2])
            {
                case "S":
                    return new Results<EntryDescription>().withReturnedObject(new EntryDescription("Descrição Básica"));
                default:
                    return new Results<EntryDescription>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<EntryDescription> NewDescription(string description, object[] args)
        {
            switch(Behaviours?[2])
            {
                case "S":
                    return new Results<EntryDescription>().withReturnedObject(new EntryDescription("Descrição Básica"));
                default:
                    return new Results<EntryDescription>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<EntryType> NewType(string type)
        {
            switch(Behaviours?[3])
            {
                case "S":
                    return new Results<EntryType>().withReturnedObject(EntryTypes.TenantCreation);
                default:
                    return new Results<EntryType>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<HistoryEntry> NewEntry(EntryType type, EntryDescription description, TenantId tenant)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType eType = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, eType, id2);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<HistoryEntry>().withReturnedObject(entry);
                default:
                    return new Results<HistoryEntry>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string description, TenantId tenant)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType eType = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, eType, id2);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<HistoryEntry>().withReturnedObject(entry);
                default:
                    return new Results<HistoryEntry>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<HistoryEntry> NewEntry(EntryType type, TenantId tenant)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType eType = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, eType, id2);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<HistoryEntry>().withReturnedObject(entry);
                default:
                    return new Results<HistoryEntry>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<HistoryEntry> NewEntry(EntryType type, TenantId tenant, params object[] args)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType eType = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, eType, id2);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<HistoryEntry>().withReturnedObject(entry);
                default:
                    return new Results<HistoryEntry>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<HistoryEntry> NewEntry(EntryType type, EntryDescription description, AccountId account)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType eType = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, eType, id2);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<HistoryEntry>().withReturnedObject(entry);
                default:
                    return new Results<HistoryEntry>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string description, AccountId account)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType eType = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, eType, id2);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<HistoryEntry>().withReturnedObject(entry);
                default:
                    return new Results<HistoryEntry>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<HistoryEntry> NewEntry(EntryType type, AccountId account)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType eType = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, eType, id2);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<HistoryEntry>().withReturnedObject(entry);
                default:
                    return new Results<HistoryEntry>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }

        public Results<HistoryEntry> NewEntry(EntryType type, AccountId account, params object[] args)
        {
            EntryId id = new EntryId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            EntryDescription desc = new EntryDescription("Descrição Básica");
            EntryDate date = new EntryDate("01-01-2021");
            EntryType eType = EntryTypes.TenantCreation;
            TenantId id2 = new TenantId("cced7c93-7b83-4b7b-9e0b-eea1f2896619");
            HistoryEntry entry = new HistoryEntry(id, desc, date, eType, id2);
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<HistoryEntry>().withReturnedObject(entry);
                default:
                    return new Results<HistoryEntry>().withFailure(EntryErrors.NULL_ID.Copy());
            }
        }
    }
}
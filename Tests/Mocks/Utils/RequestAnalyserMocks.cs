

using System;
using System.Collections.Generic;
using Backend.Licenses;
using Backend.Utils;

namespace Tests.Mocks
{
    public class RequestAnalyserMock : IRequestAnalyser
    {
        private string[] Behaviours;

        public RequestAnalyserMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public Error AnalyseRequest(CreateLicensePackRequest request)
        {
            switch(Behaviours?[0])
            {
                case "S":
                    return null;
                default:
                    return LicenseErrors.NO_LICENSES_TO_CREATE.Copy();
            }
        }

        public ICollection<License> CreateLicenses(CreateLicensePackRequest request, Func<LicenseInCreatePack, License> createLicenseFunction)
        {
            createLicenseFunction(new LicenseInCreatePack());
            switch(Behaviours?[0])
            {
                case "S":
                    return new List<License>();
                default:
                    return null;
            }
        }
    } 
}
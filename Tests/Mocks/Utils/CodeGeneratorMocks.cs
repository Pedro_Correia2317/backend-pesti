

using Backend.Accounts;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class CodeGeneratorMock : ICodeGeneratorService
    {

        private string[] Behaviours;

        public CodeGeneratorMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public Results<string> NewAccountCode()
        {
            switch(Behaviours?[0])
            {
                case "S":
                    return new Results<string>().withReturnedObject("9b19c43d-543d-4e3d-8fdd-0a61207ac6ba");
                default:
                    return new Results<string>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<string> NewLicenseCode()
        {
            switch(Behaviours?[1])
            {
                case "S":
                    return new Results<string>().withReturnedObject("9b19c43d-543d-4e3d-8fdd-0a61207ac6ba");
                default:
                    return new Results<string>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<string> NewMeetingCode()
        {
            switch(Behaviours?[2])
            {
                case "S":
                    return new Results<string>().withReturnedObject("9b19c43d-543d-4e3d-8fdd-0a61207ac6ba");
                default:
                    return new Results<string>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<string> NewProjectCode()
        {
            switch(Behaviours?[3])
            {
                case "S":
                    return new Results<string>().withReturnedObject("9b19c43d-543d-4e3d-8fdd-0a61207ac6ba");
                default:
                    return new Results<string>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }

        public Results<string> NewTenantCode()
        {
            switch(Behaviours?[4])
            {
                case "S":
                    return new Results<string>().withReturnedObject("9b19c43d-543d-4e3d-8fdd-0a61207ac6ba");
                default:
                    return new Results<string>().withFailure(AccountErrors.NULL_ID.Copy());
            }
        }
    }
}


using System.Collections.Generic;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class LoggerMock : ILoggerService
    {

        public void logAllNotificationsInfo(List<Error> lists)
        {
        }

        public void logError(string message)
        {
        }

        public void logInfo(string message)
        {
        }

        public void logWarn(string message)
        {
        }
    }
}


using System.Threading.Tasks;
using Backend.Utils;

namespace Tests.Mocks
{
    public class UnitOfWorkMock : IUnitOfWork
    {

        private string[] Behaviours;

        public UnitOfWorkMock(params string[] behaviours)
        {
            this.Behaviours = behaviours;
        }

        public Task<QueryResults<bool>> Commit()
        {
            switch(Behaviours?[0])
            {
                case "S":
                    return Task.Run(() => new QueryResults<bool>().withReturnedObject(true));
                default:
                    return Task.Run(() => new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy()));
            }
        }
    }
}


using System.Collections.Generic;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class TranslatorMock : ITranslatorService
    {

        public List<Error> TranslateAllMessages(List<Error> errors)
        {
            return errors;
        }
    }
}


using System.Collections.Generic;
using Backend.Accounts;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class UtilsDTOFactoryMock : IUtilsDTOFactory
    {

        public DeleteResults CreateDeleteResultsDTO()
        {
            var dto = new DeleteResults();
            dto.success = true;
            return dto;
        }

        public DeleteResults CreateDeleteResultsDTO(List<Error> notifications, bool itExists)
        {
            var dto = new DeleteResults();
            dto.success = false;
            dto.notifications = new ErrorDTO[notifications.Count];
            dto.itExists = itExists;
            return dto;
        }

        public GetOneResults<T> CreateGetOneResultsDTO<T>(List<Error> nots) where T : IDTO
        {
            var dto = new GetOneResults<T>();
            dto.finishedSuccesfully = false;
            dto.notifications = new ErrorDTO[nots.Count];
            return dto;
        }

        public GetOneResults<T> CreateGetOneResultsDTO<T>(T obj) where T : IDTO
        {
            var dto = new GetOneResults<T>();
            dto.finishedSuccesfully = true;
            dto.result = obj;
            return dto;
        }

        public GetPageResults<T> CreateGetPageResultsDTO<T>(List<Error> nots) where T : IDTO
        {
            var dto = new GetPageResults<T>();
            dto.success = false;
            dto.notifications = new ErrorDTO[nots.Count];
            return dto;
        }

        public GetPageResults<T> CreateGetPageResultsDTO<T>(T[] listDTO, int count) where T : IDTO
        {
            var dto = new GetPageResults<T>();
            var dto2 = new QueryDTO<T>();
            dto2.results = listDTO;
            dto2.totalNumber = count;
            dto.success = true;
            dto.results = dto2;
            return dto;
        }

        public CreatePackResults<T> CreatePackResults<T>(T[] correspondantObject) where T : IDTO
        {
            var dto = new CreatePackResults<T>();
            dto.success = true;
            dto.result = correspondantObject;
            return dto;
        }

        public CreatePackResults<T> CreatePackResults<T>(List<Error> errors) where T : IDTO
        {
            var dto = new CreatePackResults<T>();
            dto.success = false;
            dto.notifications = new ErrorDTO[errors.Count];
            return dto;
        }

        public CreateResults<T> CreateResultsDTO<T>(T correspondantObject) where T : IDTO
        {
            var dto = new CreateResults<T>();
            dto.success = true;
            dto.result = correspondantObject;
            return dto;
        }

        public CreateResults<T> CreateResultsDTO<T>(List<Error> nots) where T : IDTO
        {
            var dto = new CreateResults<T>();
            dto.success = false;
            dto.notifications = new ErrorDTO[nots.Count];
            return dto;
        }

        public StandardResults CreateStandardResultsDTO()
        {
            throw new System.NotImplementedException();
        }

        public StandardResults CreateStandardResultsDTO(List<Error> errors)
        {
            throw new System.NotImplementedException();
        }

        public UpdateResults CreateUpdateResultsDTO()
        {
            var dto = new UpdateResults();
            dto.success = true;
            return dto;
        }

        public UpdateResults CreateUpdateResultsDTO(List<Error> notifications, bool itExists)
        {
            var dto = new UpdateResults();
            dto.success = false;
            dto.notifications = new ErrorDTO[notifications.Count];
            dto.itExists = itExists;
            return dto;
        }
    }
}
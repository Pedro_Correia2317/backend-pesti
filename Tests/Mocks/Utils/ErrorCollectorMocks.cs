

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;
using Moq;

namespace Tests.Mocks
{
    public class ErrorCollectorMock : IErrorCollector
    {

        public int NumErrors;

        public T Analyse<T>(Results<T> Results)
        {
            if (!Results.wasExecutedSuccessfully())
            {
                NumErrors++;
            }
            return Results.Result;
        }

        public T Analyse<T>(QueryResults<T> Results)
        {
            if (!Results.WasExecutedSuccessfully)
            {
                NumErrors++;
            }
            return Results.Results;
        }

        public void Analyse<T>(PageQueryResults<T> Results)
        {
            if (!Results.WasExecutedSuccessfully)
            {
                NumErrors++;
            }
        }

        public T[] Analyse<T>(PageQueryResults<T> results, out int totalNumber)
        {
            if (!results.WasExecutedSuccessfully)
            {
                NumErrors++;
                totalNumber = 0;
            } else {
                totalNumber = results.Results.Length;
            }
            return results.Results;
        }

        public void Catch(Error error)
        {
            if (error != null)
            {
                NumErrors++;
            }
        }

        public bool HasErrors()
        {
            return NumErrors != 0;
        }

        public bool HasNoErrors()
        {
            return NumErrors == 0;
        }

        public T IfZeroErrorsDo<T>(Func<Results<T>> function)
        {
            if (HasNoErrors())
            {
                return Analyse(function());
            }
            return default(T);
        }

        public T IfZeroErrorsDo<T>(Func<T> function)
        {
            if (HasNoErrors())
            {
                return function();
            }
            return default(T);
        }

        public async Task<T> IfZeroErrorsDo<T>(Func<Task<QueryResults<T>>> function)
        {
            if(this.HasNoErrors())
            {
                return this.Analyse(await function());
            }
            return default(T);
        }

        public List<Error> ListErrors()
        {
            return new List<Error>(NumErrors);
        }
    }
}
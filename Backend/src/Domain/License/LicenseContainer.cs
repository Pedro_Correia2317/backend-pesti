

using Backend.Accounts;

namespace Backend.Licenses
{
    public class LicenseContainer
    {
        public LicenseId Id;

        public LicenseDate Date;

        public LicenseDuration Duration;

        public LicenseNumberMeetings NumberMeetings;

        public LicenseRemainingMeetings RemainingMeetings;

        public LicenseType Type;

        public AccountId Account;
    }
}
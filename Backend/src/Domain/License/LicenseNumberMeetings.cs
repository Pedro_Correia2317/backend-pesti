

using System;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseNumberMeetings : IValueObject
    {
        public int NumberMeetings { get; private set; }

        protected LicenseNumberMeetings() {}

        public LicenseNumberMeetings(int? num)
        {
            Error err = applyNumber(num);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyNumber(int? num)
        {
            if(num == null)
                return LicenseErrors.NULL_NUMBER_MEETINGS.Copy();
            int actualNum = (int) num;
            if(actualNum < 1)
                return LicenseErrors.SMALL_NUMBER_MEETINGS.Copy().WithParams(actualNum, 1);
            if(actualNum > 100000)
                return LicenseErrors.HUGE_NUMBER_MEETINGS.Copy().WithParams(actualNum, 100000);
            this.NumberMeetings = actualNum;
            return null;
        }

        public int Value()
        {
            return NumberMeetings;
        }

        public override bool Equals(object obj)
        {
            return obj is LicenseNumberMeetings id &&
                   this.NumberMeetings == id.NumberMeetings;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.NumberMeetings);
        }

        public override string ToString()
        {
            return "LicenseNumberMeetings: [" + this.NumberMeetings + "]";
        }

        public Error CheckRemainingMeetings(LicenseRemainingMeetings rem)
        {
            if(rem == null)
            {
                return LicenseErrors.NULL_REMAINING_MEETINGS.Copy();
            }
            if(rem.LargerThan(Value()))
            {
                return LicenseErrors.REMAINING_MEETINGS_EXCEEDS_ALLOWED.Copy().WithParams(rem.Value(), Value());
            }
            return null;
        }
    }
}
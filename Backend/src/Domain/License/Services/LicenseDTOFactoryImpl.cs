
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseDTOFactoryImpl : ILicenseDTOFactory
    {
        public LicenseDTO CreateBasicDTO(License license)
        {
            LicenseDTO dto = new LicenseDTO();
            if (license == null)
            {
                dto.id = "";
                dto.date = "";
                dto.duration = "";
                dto.account = "";
                dto.licenseType = CreateTypeDTO(null);
            }
            else
            {
                dto.id = license.BusinessId.Value;
                dto.date = license.Date.Value();
                dto.duration = license.Duration.Value();
                dto.numberMaxMeetings = license.NumberMeetings.Value();
                dto.numberRemainingMeetings = license.RemainingMeetings.Value();
                dto.account = license.Account?.Value;
                dto.licenseType = CreateTypeDTO(license.Type);
            }
            return dto;
        }

        public LicenseTypeDTO CreateTypeDTO(LicenseType type)
        {
            LicenseTypeDTO dto = new LicenseTypeDTO();
            dto.key = type == null ? "" : type.GetName();
            dto.description = type == null ? "" : type.GetName();
            return dto;
        }

        public LicenseTypeDTO[] CreateListTypesDTO(LicenseType[] types)
        {
            return FactoryUtils.NewArrayDTO(types, this.CreateTypeDTO);
        }

        public QueryParams CreateQueryParamsOf(ConsultLicensesParameters request)
        {
            QueryParams queryParams = new QueryParams();
            queryParams.page = request.page;
            queryParams.size = request.size;
            queryParams.filters = request.filters;
            queryParams.sort = request.sort;
            queryParams.order = request.order;
            queryParams.condition = request.condition;
            return queryParams;
        }

        public LicenseDTO[] CreateListOfBasicDTO(License[] licenses)
        {
            return FactoryUtils.NewArrayDTO(licenses, this.CreateBasicDTO);
        }

        public LicenseDTO[] CreateListOfBasicDTO(ICollection<License> types)
        {
            return FactoryUtils.NewArrayDTO(types, this.CreateBasicDTO);
        }
    }
}
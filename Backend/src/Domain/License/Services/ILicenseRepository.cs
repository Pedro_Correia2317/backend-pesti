

using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Licenses
{
    public interface ILicenseRepository : IRepository<License, LicenseId>
    {
        Task<QueryResults<bool>> HasNoLicense(AccountId account);
        Task<QueryResults<License>> FindByAccount(AccountId account);
    }
}
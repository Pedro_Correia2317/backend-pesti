
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Licenses
{
    public interface ILicenseDTOFactory
    {
        LicenseDTO CreateBasicDTO(License license);
        LicenseTypeDTO CreateTypeDTO(LicenseType type);
        LicenseTypeDTO[] CreateListTypesDTO(LicenseType[] types);
        QueryParams CreateQueryParamsOf(ConsultLicensesParameters request);
        LicenseDTO[] CreateListOfBasicDTO(License[] licenses);
        LicenseDTO[] CreateListOfBasicDTO(ICollection<License> types);
    }
}
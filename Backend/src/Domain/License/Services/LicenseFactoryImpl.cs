

using System;
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseFactoryImpl : ILicenseFactory
    {
        public LicenseContainer NewContainer()
        {
            return new LicenseContainer();
        }

        public Results<LicenseId> NewId(string id)
        {
            return FactoryUtils.NewObject(() => new LicenseId(id));
        }

        public Results<LicenseDate> NewDate(string date)
        {
            return FactoryUtils.NewObject(() => new LicenseDate(date));
        }

        public Results<LicenseDate> NewDate(DateTime date)
        {
            return FactoryUtils.NewObject(() => new LicenseDate(date));
        }

        public Results<LicenseDuration> NewDuration(string duration)
        {
            return FactoryUtils.NewObject(() => new LicenseDuration(duration));
        }

        public Results<LicenseNumberMeetings> NewMeetings(int? numMeetings)
        {
            return FactoryUtils.NewObject(() => new LicenseNumberMeetings(numMeetings));
        }

        public Results<LicenseRemainingMeetings> NewRemainingMeetings(int? remMeetings)
        {
            return FactoryUtils.NewObject(() => new LicenseRemainingMeetings(remMeetings));
        }

        public Results<LicenseType> NewType(string type)
        {
            return FactoryUtils.NewObject(() => LicenseTypes.GetByName(type));
        }

        public Results<License> NewLicense(LicenseContainer container)
        {
            var id = container?.Id;
            var date = container?.Date;
            var dur = container?.Duration;
            var num = container?.NumberMeetings;
            var rem = container?.RemainingMeetings;
            var type = container?.Type;
            Func<License> expr = container?.Account == null?
                () => new License(id, date, dur, num, rem, type) :
                () => new License(id, date, dur, num, rem, type, container?.Account);
            return FactoryUtils.NewObject(expr);
        }
    }
}
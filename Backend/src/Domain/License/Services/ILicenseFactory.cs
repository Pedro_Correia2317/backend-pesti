

using System;
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Licenses
{
    public interface ILicenseFactory
    {

        LicenseContainer NewContainer();
        Results<LicenseId> NewId(string id);
        Results<LicenseDate> NewDate(string date);
        Results<LicenseDate> NewDate(DateTime date);
        Results<LicenseDuration> NewDuration(string duration);
        Results<LicenseNumberMeetings> NewMeetings(int? numMeetings);
        Results<LicenseRemainingMeetings> NewRemainingMeetings(int? remMeetings);
        Results<LicenseType> NewType(string type);
        Results<License> NewLicense(LicenseContainer container);
    }
}
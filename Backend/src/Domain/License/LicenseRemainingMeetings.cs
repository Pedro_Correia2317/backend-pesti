

using System;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseRemainingMeetings : IValueObject
    {
        public int RemainingMeetings { get; private set; }

        protected LicenseRemainingMeetings() {}

        public LicenseRemainingMeetings(int? num)
        {
            Error err = applyNumber(num);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyNumber(int? num)
        {
            if(num == null)
                return LicenseErrors.NULL_REMAINING_MEETINGS.Copy();
            int actualNum = (int) num;
            if(actualNum < 0)
                return LicenseErrors.SMALL_REMAINING_MEETINGS.Copy().WithParams(actualNum, 0);
            if(actualNum > 100000)
                return LicenseErrors.HUGE_REMAINING_MEETINGS.Copy().WithParams(actualNum, 100000);
            this.RemainingMeetings = actualNum;
            return null;
        }

        public bool LargerThan(int numberMeetings)
        {
            return this.RemainingMeetings > numberMeetings;
        }

        public int Value()
        {
            return RemainingMeetings;
        }

        public Results<LicenseRemainingMeetings> DecreaseOneMeeting()
        {
            if(RemainingMeetings == 0)
            {
                return Results<LicenseRemainingMeetings>.Of(LicenseErrors.CANT_CREATE_MORE_MEETINGS.Copy());
            }
            return Results<LicenseRemainingMeetings>.Of(new LicenseRemainingMeetings(RemainingMeetings-1));
        }

        public override bool Equals(object obj)
        {
            return obj is LicenseRemainingMeetings id &&
                   this.RemainingMeetings == id.RemainingMeetings;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.RemainingMeetings);
        }

        public override string ToString()
        {
            return "LicenseRemainingMeetings: [" + this.RemainingMeetings + "]";
        }
    }
}
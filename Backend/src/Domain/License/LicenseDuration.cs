

using System;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseDuration : IValueObject
    {

        public Duration Duration { get; private set; }
        
        protected LicenseDuration() {}

        public LicenseDuration(string duration)
        {
            Error err = applyDuration(duration);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyDuration(string duration)
        {
            if(duration == null)
                return LicenseErrors.NULL_DURATION.Copy();
            string trimmedDuration = duration.Trim();
            Duration outDur;
            if(!Duration.TryCreate(trimmedDuration, out outDur))
                return LicenseErrors.INVALID_DURATION.Copy().WithParams(trimmedDuration);
            this.Duration = outDur;
            return null;
        }

        public Error CheckDuration(LicenseDate date)
        {
            if(this.Duration.SmallerOrEqualThanADay(date.Date))
            {
                return LicenseErrors.DURATION_TOO_SHORT.Copy().WithParams(this.Value(), 1);
            }
            if(this.Duration.LargerOrEqualThanFiveYears(date.Date))
            {
                return LicenseErrors.DURATION_TOO_LONG.Copy().WithParams(this.Value(), 5);
            }
            return null;
        }

        public string Value()
        {
            return this.Duration.GetValue();
        }

        public override bool Equals(object obj)
        {
            return obj is LicenseDuration id &&
                   this.Duration == id.Duration;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.Duration);
        }

        public override string ToString()
        {
            return "LicenseDuration: [" + this.Duration + "]";
        }
    }
}
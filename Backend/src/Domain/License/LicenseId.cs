

using System;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseId : EntityId, IValueObject
    {
        
        protected LicenseId() : base() {}

        public LicenseId(string id)
        {
            Error err = applyId(id);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyId(string id)
        {
            if(id == null)
                return LicenseErrors.NULL_ID.Copy();
            string trimmedId = id.Trim();
            Guid outGuid;
            if(!Guid.TryParse(trimmedId, out outGuid))
                return LicenseErrors.INVALID_ID.Copy().WithParams(trimmedId);
            this.Value = trimmedId;
            return null;
        }

        public override bool Equals(object obj)
        {
            return obj is LicenseId id &&
                   base.Equals(obj) &&
                   Value == id.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Value);
        }

        public override string ToString()
        {
            return "LicenseId: [" + this.Value + "]";
        }
    }
}
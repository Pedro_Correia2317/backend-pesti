

using System;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Licenses
{
    public class License : Entity<LicenseId>, IAggregateRoot
    {

        public LicenseDate Date { get; private set; }

        public LicenseDuration Duration { get; private set; }

        public LicenseNumberMeetings NumberMeetings { get; private set; }

        public LicenseRemainingMeetings RemainingMeetings { get; private set; }

        public LicenseType Type { get; private set; }

        public AccountId Account { get; private set; }

        protected License() { }

        public License(LicenseId id, LicenseDate date, LicenseDuration duration, LicenseNumberMeetings numMeetings,
                LicenseRemainingMeetings remMeetings, LicenseType type)
        {
            if (DomainUtils.HasNullFields(id, date, duration, numMeetings, remMeetings, type))
            {
                throw new BusinessRuleFailureException(LicenseErrors.NULL_DATA_IN_LICENSE.Copy());
            }
            Error err = ValidateLicense(date, duration, numMeetings, remMeetings);
            if (err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
            this.BusinessId = id;
            this.Date = date;
            this.Duration = duration;
            this.NumberMeetings = numMeetings;
            this.RemainingMeetings = remMeetings;
            this.Type = type;
        }

        public License(LicenseId id, LicenseDate date, LicenseDuration duration, LicenseNumberMeetings numMeetings,
                LicenseRemainingMeetings remMeetings, LicenseType type, AccountId account)
        {
            if (DomainUtils.HasNullFields(id, date, duration, numMeetings, remMeetings, type, account))
            {
                throw new BusinessRuleFailureException(LicenseErrors.NULL_DATA_IN_LICENSE.Copy());
            }
            Error err = ValidateLicense(date, duration, numMeetings, remMeetings);
            if (err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
            this.BusinessId = id;
            this.Date = date;
            this.Duration = duration;
            this.NumberMeetings = numMeetings;
            this.RemainingMeetings = remMeetings;
            this.Type = type;
            this.Account = account;
        }

        private Error ValidateLicense(LicenseDate date, LicenseDuration duration, LicenseNumberMeetings numMeetings,
                LicenseRemainingMeetings remMeetings)
        {
            Error err = duration.CheckDuration(date);
            if (err == null)
            {
                err = numMeetings.CheckRemainingMeetings(remMeetings);
            }
            return err;
        }

        public Results<bool> DecreaseOneMeeting()
        {
            Results<LicenseRemainingMeetings> results = this.RemainingMeetings.DecreaseOneMeeting();
            if(results.wasExecutedSuccessfully())
            {
                this.RemainingMeetings = results.Result;
                return Results<bool>.Of(true);
            }
            return Results<bool>.Of(results.Error);
        }

        public Results<bool> ChangeDuration(LicenseDuration newDuration)
        {
            Results<bool> results = new Results<bool>();
            Error err = newDuration.CheckDuration(this.Date);
            if (err == null)
            {
                this.Duration = newDuration;
                results = results.withReturnedObject(true);
            }
            else
            {
                results = results.withFailure(err);
            }
            return results;
        }

        public Results<bool> ChangeNumberMeetings(LicenseNumberMeetings newNumberMeetings, LicenseRemainingMeetings remaining)
        {
            Results<bool> results = new Results<bool>();
            Error err = newNumberMeetings.CheckRemainingMeetings(remaining);
            if (err == null)
            {
                this.RemainingMeetings = remaining;
                this.NumberMeetings = newNumberMeetings;
                results = results.withReturnedObject(true);
            }
            else
            {
                results = results.withFailure(err);
            }
            return results;
        }

        public Results<bool> ChangeRemainingMeetings(LicenseRemainingMeetings newRemainingMeetings)
        {
            Results<bool> results = new Results<bool>();
            Error err = this.NumberMeetings.CheckRemainingMeetings(newRemainingMeetings);
            if (err == null)
            {
                this.RemainingMeetings = newRemainingMeetings;
                results = results.withReturnedObject(true);
            }
            else
            {
                results = results.withFailure(err);
            }
            return results;
        }

        public Results<bool> ChangeType(LicenseType newType)
        {
            Results<bool> results = new Results<bool>();
            if (newType != null)
            {
                this.Type = newType;
                results = results.withReturnedObject(true);
            }
            else
            {
                results = results.withFailure(LicenseErrors.NULL_LICENSE_TYPE.Copy());
            }
            return results;
        }

        public Results<bool> AddAccount(AccountId clientId)
        {
            if(clientId == null)
            {
                return Results<bool>.Of(LicenseErrors.NULL_ACCOUNT.Copy());
            }
            if(this.Account != null)
            {
                return Results<bool>.Of(LicenseErrors.LICENSE_ALREADY_HAS_ACCOUNT.Copy().WithParams(this.Account.Value));
            }
            this.Account = clientId;
            return Results<bool>.Of(true);
        }

        public override bool Equals(object obj)
        {
            return obj is License license && this.BusinessId.Equals(license.BusinessId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(BusinessId);
        }

        public override string ToString()
        {
            return $"License: [{this.BusinessId}, {this.Date}, {this.Duration}, {this.NumberMeetings}, "
                + $"{this.RemainingMeetings}, {this.Type}, {this.Account}]";
        }
    }
}
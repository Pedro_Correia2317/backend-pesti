
using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseType
    {
        public string Name { get; private set; }
        public int NumberMeetings { get; private set; }
        public string Period { get; private set; }
        public int NumberAttachments { get; private set; }

        protected LicenseType() {}

        public LicenseType(string name, int numMeetings, string period, int numAttachments)
        {
            this.Name = name;
            this.NumberMeetings = numMeetings;
            this.Period = period;
            this.NumberAttachments = numAttachments;
        }

        public string GetName()
        {
            return this.Name;
        }

        public int GetNumberOfAttachmentsAllowed()
        {
            return this.NumberAttachments;
        }

        public override bool Equals(object obj)
        {
            return obj is LicenseType type &&
                   Name == type.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public override string ToString()
        {
            return $"LicenseType: [{this.Name}]";
        }
    }

    public static class LicenseTypes
    {
        public static readonly LicenseType Basic = new LicenseType("Basic", 1000, "P3M", 10);
        public static readonly LicenseType Normal = new LicenseType("Normal", 3000, "P6M", 20);
        public static readonly LicenseType Professional = new LicenseType("Professional", 10000, "P1Y", 50);
        public static readonly LicenseType Trial = new LicenseType("Trial", 500, "P1M", 1);
        private static Dictionary<string, LicenseType> AllLicenseTypes = new Dictionary<string, LicenseType>();

        static LicenseTypes()
        {
            AllLicenseTypes.Add("Basic", Basic);
            AllLicenseTypes.Add("Normal", Normal);
            AllLicenseTypes.Add("Professional", Professional);
            AllLicenseTypes.Add("Trial", Trial);
        }

        public static LicenseType GetByName(string type)
        {
            if(type == null)
            {
                Error e = LicenseErrors.NULL_LICENSE_TYPE.Copy();
                throw new BusinessRuleFailureException(e);
            }
            LicenseType licenseType = AllLicenseTypes.GetValueOrDefault(type + "");
            if(licenseType == null)
            {
                Error e = LicenseErrors.UNKNOWN_LICENSE_TYPE.Copy().WithParams(type);
                throw new BusinessRuleFailureException(e);
            }
            return licenseType;
        }

        public static LicenseType[] GetAll()
        {
            return AllLicenseTypes.Values.ToArray();
        }
    }
}


using System;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseDate : IValueObject
    {

        public Date Date { get; private set; }
        
        protected LicenseDate() {}

        public LicenseDate(string date)
        {
            Error err = applyDate(date);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        public LicenseDate(DateTime date)
        {
            this.Date = Date.Create(date);
        }

        private Error applyDate(string date)
        {
            if(date == null)
                return LicenseErrors.NULL_DATE.Copy();
            string trimmedDate = date.Trim();
            Date outDate;
            if(!Date.TryCreate(trimmedDate, out outDate))
                return LicenseErrors.INVALID_DATE.Copy().WithParams(trimmedDate);
            this.Date = outDate;
            return null;
        }

        public string Value()
        {
            return this.Date.GetValue();
        }

        public override bool Equals(object obj)
        {
            return obj is LicenseDate id &&
                   base.Equals(obj) &&
                   this.Date == id.Date;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Date);
        }

        public override string ToString()
        {
            return "LicenseDate: [" + this.Date + "]";
        }
    }
}
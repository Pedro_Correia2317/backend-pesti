

using Backend.Utils;

namespace Backend.Licenses
{
    public static class LicenseErrors
    {
        public static readonly string TYPE = "License";
        public static readonly Error NULL_DATA_IN_LICENSE = new Error("There is null data in the license", "NULL_DATA_IN_LICENSE", TYPE);
        public static readonly Error NULL_ID = new Error("No license id has been inserted", "NULL_ID", TYPE);
        public static readonly Error INVALID_ID = new Error("The id '{{0}}' is not a Guid", "INVALID_ID", TYPE);
        public static readonly Error NULL_DATE = new Error("No license date has been inserted", "NULL_DATE", TYPE);
        public static readonly Error INVALID_DATE = new Error("The date '{{0}}' is not a valid date", "INVALID_DATE", TYPE);
        public static readonly Error INVALID_QUANTITY = new Error("One of the specified quantities is not valid", "INVALID_QUANTITY", TYPE);
        public static readonly Error NULL_DURATION = new Error("No license duration has been inserted", "NULL_DURATION", TYPE);
        public static readonly Error INVALID_DURATION = new Error("The duration '{{0}}' is not valid", "INVALID_DURATION", TYPE);
        public static readonly Error DURATION_TOO_SHORT = new Error("The duration '{{0}}' can not be smaller or equal to {{1}} day(s)", "DURATION_TOO_SHORT", TYPE);
        public static readonly Error DURATION_TOO_LONG = new Error("The duration '{{0}}' can not be longer or equal to {{1}} year(s)", "DURATION_TOO_LONG", TYPE);
        public static readonly Error NULL_NUMBER_MEETINGS = new Error("The number of meetings is null", "NULL_NUMBER_MEETINGS", TYPE);
        public static readonly Error SMALL_NUMBER_MEETINGS = new Error("The number of meetings '{{0}}' can not be smaller than '{{1}}'", "SMALL_NUMBER_MEETINGS", TYPE);
        public static readonly Error HUGE_NUMBER_MEETINGS = new Error("The number of meetings '{{0}}' can not be bigger than '{{1}}'", "HUGE_NUMBER_MEETINGS", TYPE);
        public static readonly Error NULL_REMAINING_MEETINGS = new Error("The number of remaining meetings is null", "NULL_REMAINING_MEETINGS", TYPE);
        public static readonly Error SMALL_REMAINING_MEETINGS = new Error("The number of meetings remaining '{{0}}' can not be smaller than '{{1}}'", "SMALL_REMAINING_MEETINGS", TYPE);
        public static readonly Error HUGE_REMAINING_MEETINGS = new Error("The number of meetings remaining '{{0}}' can not be bigger than '{{1}}'", "HUGE_REMAINING_MEETINGS", TYPE);
        public static readonly Error NULL_LICENSE_TYPE = new Error("No license type has been inserted", "NULL_LICENSE_TYPE", TYPE);
        public static readonly Error UNKNOWN_LICENSE_TYPE = new Error("There is no license type with the name '{{0}}'", "UNKNOWN_LICENSE_TYPE", TYPE);
        public static readonly Error NO_LICENSES_TO_CREATE = new Error("No license type has been inserted", "NO_LICENSES_TO_CREATE", TYPE);
        public static readonly Error TOO_MANY_LICENSES_TO_CREATE = new Error("Can't create the {{0}} licenses, the max is {{1}}", "TOO_MANY_LICENSES_TO_CREATE", TYPE);
        public static readonly Error UNKNOWN_ID = new Error("No license with the id '{{0}}' exists", "UNKNOWN_ID", TYPE);
        public static readonly Error ACCOUNT_ALREADY_HAS_LICENSE = new Error("The account '{{0}}' already has a license associated with it", "ACCOUNT_ALREADY_HAS_LICENSE", TYPE);
        public static readonly Error REMAINING_MEETINGS_EXCEEDS_ALLOWED = new Error("There is {{0}} remaining meetings, larger than the {{1}} meetings allowed", "REMAINING_MEETINGS_EXCEEDS_ALLOWED", TYPE);
        public static readonly Error NULL_ACCOUNT = new Error("The account is null", "NULL_ACCOUNT", TYPE);
        public static readonly Error LICENSE_ALREADY_HAS_ACCOUNT = new Error("The license is associated with the account '{{0}}'", "LICENSE_ALREADY_HAS_ACCOUNT", TYPE);
        public static readonly Error ACCOUNT_DOES_NOT_HAVE_LICENSE = new Error("The account '{{0}}' does not have any license associated", "ACCOUNT_DOES_NOT_HAVE_LICENSE", TYPE);
        public static readonly Error CANT_CREATE_MORE_MEETINGS = new Error("The plafond for creating meetings has been used", "CANT_CREATE_MORE_MEETINGS", TYPE);
    }
}


using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountStatus : IValueObject
    {
        
        public string Name { get; private set; }

        protected AccountStatus() {}

        public AccountStatus(string name)
        {
            this.Name = name;
        }

        public string GetName()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            return obj is AccountStatus status &&
                   Name == status.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public override string ToString()
        {
            return $"AccountStatus: [{this.Name}]";
        }

        public Results<bool> IsActive()
        {
            if(this.Name.Equals("Active"))
            {
                return Results<bool>.Of(true);
            }
            return Results<bool>.Of(AccountErrors.NOT_ACTIVE_ACCOUNT.Copy());
        }
    }

    public static class AccountStatuses
    {
        public static readonly AccountStatus Suspended = new AccountStatus("Suspended");
        public static readonly AccountStatus Active = new AccountStatus("Active");
        public static readonly AccountStatus Banned = new AccountStatus("Banned");
        private static Dictionary<string, AccountStatus> AllAccountStatuses = new Dictionary<string, AccountStatus>();

        static AccountStatuses()
        {
            AllAccountStatuses.Add("Suspended", Suspended);
            AllAccountStatuses.Add("Active", Active);
            AllAccountStatuses.Add("Banned", Banned);
        }

        public static AccountStatus GetByName(string type)
        {
            if(type == null)
            {
                Error e = AccountErrors.NULL_STATUS.Copy();
                throw new BusinessRuleFailureException(e);
            }
            AccountStatus licenseType = AllAccountStatuses.GetValueOrDefault(type + "");
            if(licenseType == null)
            {
                Error e = AccountErrors.UNKNOWN_STATUS.Copy().WithParams(type);
                throw new BusinessRuleFailureException(e);
            }
            return licenseType;
        }

        public static AccountStatus[] GetAll()
        {
            return AllAccountStatuses.Values.ToArray();
        }
    }
}


using System;
using Backend.Utils;

namespace Backend.Accounts
{
    public class Account : Entity<AccountId>, IAggregateRoot
    {

        public AccountName Name { get; set; }

        public AccountEmail Email { get; private set; }

        public AccountStatus Status { get; private set; }

        public AccountType Type { get; private set; }

        protected Account() {}

        public Account(AccountId id, AccountName name, AccountEmail email, AccountStatus status, AccountType type)
        {
            if(DomainUtils.HasNullFields(id, name, email, status, type))
            {
                throw new BusinessRuleFailureException(AccountErrors.NULL_DATA_IN_ACCOUNT.Copy());
            }
            this.BusinessId = id;
            this.Name = name;
            this.Email = email;
            this.Status = status;
            this.Type = type;
        }

        public Results<bool> ChangeStatus(AccountStatus newStatus)
        {
            if(newStatus == null)
            {
                return Results<bool>.Of(AccountErrors.NULL_STATUS.Copy());
            }
            if(this.Status.Equals(newStatus))
            {
                return Results<bool>.Of(AccountErrors.SAME_STATUS.Copy().WithParams(newStatus.GetName()));
            }
            this.Status = newStatus;
            return Results<bool>.Of(true);
        }

        public override bool Equals(object obj)
        {
            return obj is Account account && this.BusinessId.Equals(account.BusinessId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(BusinessId);
        }

        public override string ToString()
        {
            return $"Account: [{this.BusinessId}, {this.Name}, {this.Email}, {this.Status}, {this.Type}]";
        }
    }
}
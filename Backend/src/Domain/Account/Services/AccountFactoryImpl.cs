
using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountFactoryImpl : IAccountFactory
    {

        public AccountContainer NewContainer()
        {
            return new AccountContainer();
        }

        public Results<AccountId> NewId(string id)
        {
            return FactoryUtils.NewObject(() => new AccountId(id));
        }

        public Results<AccountName> NewName(string name)
        {
            return FactoryUtils.NewObject(() => new AccountName(name));
        }

        public Results<AccountEmail> NewEmail(string email)
        {
            return FactoryUtils.NewObject(() => new AccountEmail(email));
        }

        public Results<AccountStatus> NewDefaultStatus()
        {
            return FactoryUtils.NewObject(() => AccountStatuses.Active);
        }

        public Results<AccountStatus> NewStatus(string status)
        {
            return FactoryUtils.NewObject(() => AccountStatuses.GetByName(status));
        }

        public Results<AccountType> NewType(string type)
        {
            return FactoryUtils.NewObject(() => AccountTypes.GetByName(type));
        }
        public Results<Account> NewAccount(AccountContainer x)
        {
            return FactoryUtils.NewObject(() => new Account(x?.Id, x?.Name, x?.Email, x?.Status, x?.Type));
        }
    }
}
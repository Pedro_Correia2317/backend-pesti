
using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.Accounts
{
    public interface IAccountRepository : IRepository<Account, AccountId>
    {

        Task<QueryResults<bool>> IsEmailUnique(AccountEmail email);

        Task<QueryResults<Account>> FindByEmail(AccountEmail email);
        
    }
}


using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountDTOFactoryImpl : IAccountDTOFactory
    {
        public AccountDTO CreateBasicDTO(Account account)
        {
            AccountDTO dto = new AccountDTO();
            if (account != null)
            {
                dto.id = account.BusinessId.Value;
                dto.name = account.Name.Value();
                dto.email = account.Email.Value();
                dto.status = this.CreateStatusDTO(account.Status);
                dto.type = this.CreateTypeDTO(account.Type);
            } else {
                dto.id = "";
                dto.name = "";
                dto.email = "";
                dto.status = this.CreateStatusDTO(null);
                dto.type = this.CreateTypeDTO(null);
            }
            return dto;
        }

        public AccountDetailsDTO CreateDetailsDTO(Account account, License license, Tenant tenant)
        {
            AccountDetailsDTO dto = new AccountDetailsDTO();
            dto.type = this.CreateTypeDTO(account?.Type);
            dto.isActive = account?.Status.IsActive().Result ?? false;
            dto.hasLicense = license != null;
            dto.isOwnerOfTenant = tenant != null && tenant.HasOwner(account.BusinessId).Result;
            dto.itExists = account != null;
            dto.canCreateProjects = tenant?.HasPermissionsToCreateProject(account.BusinessId).Result ?? false;
            dto.canDeleteProjects = tenant?.HasPermissionsToDeleteProject(account.BusinessId).Result ?? false;
            dto.canUpdateProjects = tenant?.HasPermissionsToUpdateProject(account.BusinessId).Result ?? false;
            dto.canCreateMeetings = tenant?.HasPermissionsToCreateMeeting(account.BusinessId).Result ?? false;
            dto.canDeleteMeetings = tenant?.HasPermissionsToDeleteMeeting(account.BusinessId).Result ?? false;
            dto.canUpdateMeetings = tenant?.HasPermissionsToUpdateMeeting(account.BusinessId).Result ?? false;
            return dto;
        }

        public AccountDTO[] CreateListOfBasicDTO(Account[] accounts)
        {
            return FactoryUtils.NewArrayDTO(accounts, this.CreateBasicDTO);
        }

        public AccountStatusDTO[] CreateListStatusDTO(AccountStatus[] statuses)
        {
            return FactoryUtils.NewArrayDTO(statuses, this.CreateStatusDTO);
        }

        public AccountTypeDTO[] CreateListTypesDTO(AccountType[] types)
        {
            return FactoryUtils.NewArrayDTO(types, this.CreateTypeDTO);
        }

        public QueryParams CreateQueryParamsOf(ConsultAccountsParameters request)
        {
            QueryParams queryParams = new QueryParams();
            if (request != null)
            {
                queryParams.page = request.page;
                queryParams.size = request.size;
                queryParams.search = request.search;
                queryParams.filters = request.filters;
                queryParams.sort = request.sort;
                queryParams.order = request.order;
                queryParams.condition = request.condition;
            }
            return queryParams;
        }

        public AccountStatusDTO CreateStatusDTO(AccountStatus status)
        {
            AccountStatusDTO dto = new AccountStatusDTO();
            dto.key = status == null ? "" : status.GetName();
            dto.description = status == null ? "" : status.GetName();
            return dto;
        }

        public AccountTypeDTO CreateTypeDTO(AccountType type)
        {
            AccountTypeDTO dto = new AccountTypeDTO();
            dto.key = type == null ? "" : type.GetName();
            dto.description = type == null ? "" : type.GetName();
            return dto;
        }
    }
}
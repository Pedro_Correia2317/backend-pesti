

using Backend.Utils;

namespace Backend.Accounts
{
    public interface IAccountFactory
    {
        AccountContainer NewContainer();
        Results<AccountId> NewId(string id);
        Results<AccountName> NewName(string name);
        Results<AccountEmail> NewEmail(string email);
        Results<AccountStatus> NewStatus(string status);
        Results<AccountStatus> NewDefaultStatus();
        Results<AccountType> NewType(string type);
        Results<Account> NewAccount(AccountContainer container);
    }
}
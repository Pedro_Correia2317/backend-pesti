
using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Accounts
{
    public interface IAccountDTOFactory
    {
        AccountDTO CreateBasicDTO(Account account);
        AccountTypeDTO CreateTypeDTO(AccountType type);
        AccountTypeDTO[] CreateListTypesDTO(AccountType[] types);
        AccountStatusDTO CreateStatusDTO(AccountStatus type);
        AccountStatusDTO[] CreateListStatusDTO(AccountStatus[] types);
        QueryParams CreateQueryParamsOf(ConsultAccountsParameters request);
        AccountDTO[] CreateListOfBasicDTO(Account[] accounts);
        AccountDetailsDTO CreateDetailsDTO(Account account, License license, Tenant tenant);
    }
}


using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountType : IValueObject
    {
        
        public string Name { get; private set; }

        protected AccountType() {}

        public AccountType(string name)
        {
            this.Name = name;
        }

        public string GetName()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            return obj is AccountType status &&
                   Name == status.Name;
        }

        public Results<bool> IsSuperAdministrator()
        {
            if(this.Name.Equals("SuperAdmin"))
            {
                return Results<bool>.Of(true);
            }
            return Results<bool>.Of(AccountErrors.NO_NECESSARY_PERMISSIONS.Copy());
        }

        public Results<bool> IsSuperAdministratorOrAdministrator()
        {
            if(this.Name.Equals("SuperAdmin") || this.Name.Equals("Admin"))
            {
                return Results<bool>.Of(true);
            }
            return Results<bool>.Of(AccountErrors.NO_NECESSARY_PERMISSIONS.Copy());
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public override string ToString()
        {
            return $"AccountType: [{this.Name}]";
        }
    }

    public static class AccountTypes
    {
        public static readonly AccountType Client = new AccountType("Client");
        public static readonly AccountType Admin = new AccountType("Admin");
        public static readonly AccountType SuperAdmin = new AccountType("SuperAdmin");
        private static Dictionary<string, AccountType> AllAccountTypes = new Dictionary<string, AccountType>();

        static AccountTypes()
        {
            AllAccountTypes.Add("Client", Client);
            AllAccountTypes.Add("Admin", Admin);
            AllAccountTypes.Add("SuperAdmin", SuperAdmin);
        }

        public static AccountType GetByName(string type)
        {
            if(type == null)
            {
                Error e = AccountErrors.NULL_TYPE.Copy();
                throw new BusinessRuleFailureException(e);
            }
            AccountType licenseType = AllAccountTypes.GetValueOrDefault(type + "");
            if(licenseType == null)
            {
                Error e = AccountErrors.UNKNOWN_TYPE.Copy().WithParams(type);
                throw new BusinessRuleFailureException(e);
            }
            return licenseType;
        }

        public static AccountType[] GetAll()
        {
            return AllAccountTypes.Values.ToArray();
        }
    }
}
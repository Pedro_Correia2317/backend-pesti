
namespace Backend.Accounts
{
    public class AccountContainer
    {
        public AccountId Id;

        public AccountName Name;

        public AccountEmail Email;

        public AccountStatus Status;

        public AccountType Type;
    }
}


using Backend.Utils;

namespace Backend.Accounts
{
    public static class AccountErrors
    {

        public static readonly string TYPE = "Account";
        public static readonly Error UNKNOWN_ID = new Error("The account id '{{0}}' does not belong to any account", "UNKNOWN_ID", TYPE);
        public static readonly Error NULL_ID = new Error("No Account id has been inserted", "NULL_ID", TYPE);
        public static readonly Error INVALID_ID = new Error("The account id '{{0}}' is not a Guid", "INVALID_ID", TYPE);
        public static readonly Error NULL_NAME = new Error("The inserted name is null", "NULL_NAME", TYPE);
        public static readonly Error EMPTY_NAME = new Error("The inserted name is empty", "EMPTY_NAME", TYPE);
        public static readonly Error NAME_TOO_SMALL = new Error("The inserted name '{{0}}' is smaller than {{1}} chars", "NAME_TOO_SMALL", TYPE);
        public static readonly Error NAME_TOO_LARGE = new Error("The inserted name '{{0}}' is larger than {{1}} chars", "NAME_TOO_LARGE", TYPE);
        public static readonly Error INVALID_NAME = new Error("The inserted name '{{0}}' does not obey the {{1}} regex", "INVALID_NAME", TYPE);
        public static readonly Error REPEATED_EMAIL = new Error("There is already an account with the email {{0}}", "REPEATED_EMAIL", TYPE);
        public static readonly Error NULL_EMAIL = new Error("The inserted email is null", "NULL_EMAIL", TYPE);
        public static readonly Error EMPTY_EMAIL = new Error("The inserted email is empty", "EMPTY_EMAIL", TYPE);
        public static readonly Error EMAIL_TOO_SMALL = new Error("The inserted email '{{0}}' is smaller than {{1}} chars", "EMAIL_TOO_SMALL", TYPE);
        public static readonly Error EMAIL_TOO_LARGE = new Error("The inserted email '{{0}}' is larger than {{1}} chars", "EMAIL_TOO_LARGE", TYPE);
        public static readonly Error INVALID_EMAIL = new Error("The inserted email '{{0}}' does not obey the {{1}} regex", "INVALID_EMAIL", TYPE);
        public static readonly Error NULL_STATUS = new Error("The inserted account status is null", "NULL_STATUS", TYPE);
        public static readonly Error UNKNOWN_STATUS = new Error("The inserted account status '{{0}}' is unknown", "UNKNOWN_STATUS", TYPE);
        public static readonly Error NULL_TYPE = new Error("The inserted account type is null", "NULL_TYPE", TYPE);
        public static readonly Error UNKNOWN_TYPE = new Error("The inserted account type '{{0}}' is unknown", "UNKNOWN_TYPE", TYPE);
        public static readonly Error NULL_DATA_IN_ACCOUNT = new Error("The field '{{0}}' of account is null", "NULL_DATA_IN_ACCOUNT", TYPE);
        public static readonly Error SAME_STATUS = new Error("The status '{{0}}' was already present on the account", "SAME_STATUS", TYPE);
        public static readonly Error UNKNOWN_EMAIL = new Error("The account email '{{0}}' does not belong to any account", "UNKNOWN_EMAIL", TYPE);
        public static readonly Error NO_NECESSARY_PERMISSIONS = new Error("The account does not have the necessary permissions", "NO_NECESSARY_PERMISSIONS", TYPE);
        public static readonly Error NOT_ACTIVE_ACCOUNT = new Error("The account is not active", "NOT_ACTIVE_ACCOUNT", TYPE);
    }
}


using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountEmail : IValueObject
    {

        public Email Email { get; private set; }
        
        protected AccountEmail() {}

        public AccountEmail(string name)
        {
            Error err = applyEmail(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyEmail(string name)
        {
            if(name == null)
                return AccountErrors.NULL_EMAIL.Copy();
            string trimmedEmail = name.Trim();
            if(trimmedEmail.Length == 0)
                return AccountErrors.EMPTY_EMAIL.Copy();
            if(trimmedEmail.Length < 2)
                return AccountErrors.EMAIL_TOO_SMALL.Copy().WithParams(trimmedEmail, 2);
            if(trimmedEmail.Length > 200)
                return AccountErrors.EMAIL_TOO_LARGE.Copy().WithParams(trimmedEmail, 200);
            Email email;
            if(!Email.TryCreate(trimmedEmail, out email))
                return AccountErrors.INVALID_EMAIL.Copy().WithParams(trimmedEmail, "RFC 802");
            this.Email = email;
            return null;
        }

        public string Value()
        {
            return this.Email.GetValue();
        }

        public override bool Equals(object obj)
        {
            return obj is AccountEmail email &&
                   Email.Equals(email.Email);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Email);
        }

        public override string ToString()
        {
            return "AccountEmail: [" + this.Email + "]";
        }

    }
}


using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Backend.Utils
{
    public class Email : IComparable, IComparable<Email>
    {

        private static string PATTERN = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                   + "@"
                                   + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";
        public MailAddress Value { get; private set; }

        protected Email() { }

        private Email(MailAddress date)
        {
            this.Value = date;
        }

        public string GetValue()
        {
            return this.Value.Address;
        }

        public override bool Equals(object obj)
        {
            return obj is Email date &&
                   Value.Equals(date.Value);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value);
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

        public static Email Create(MailAddress email)
        {
            return new Email(email);
        }

        public static Email Create(string date)
        {
            Email outEmail;
            TryCreate(date, out outEmail);
            return outEmail;
        }

        public static bool TryCreate(string stringEmail, out Email email)
        {
            email = null;
            if(!Regex.IsMatch(stringEmail, PATTERN))
            {
                return false;
            }
            try
            {
                MailAddress m = new MailAddress(stringEmail);
                email = new Email(m);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public int CompareTo(object other)
        {
            Email email = other as Email;
            return this.CompareTo(email);
        }

        public int CompareTo(Email other)
        {
            if(other == null)
            {
                return 1;
            }
            return this.Value.Address.CompareTo(other.Value.Address);
        }
    }
}
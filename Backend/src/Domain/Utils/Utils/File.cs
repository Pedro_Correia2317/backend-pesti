

using System;

namespace Backend.Utils
{
    public class File : IComparable, IComparable<File>
    {

        public byte[] Value { get; private set; }

        protected File() { }

        private File(byte[] file)
        {
            this.Value = file;
        }

        public override bool Equals(object obj)
        {
            return obj is File file &&
                   Value.Equals(file.Value);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value);
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

        public int CompareTo(object obj)
        {
            File file = obj as File;
            return this.CompareTo(file);
        }

        public int CompareTo(File other)
        {
            if(other == null)
            {
                return 1;
            }
            return this.Value.Length - other.Value.Length;
        }

        public static File Create(byte[] fileBytes)
        {
            File outFile;
            TryCreate(fileBytes, out outFile);
            return outFile;
        }

        public static bool TryCreate(byte[] fileBytes, out File outFile)
        {
            outFile = null;
            if(fileBytes != null && fileBytes.Length > 0)
            {
                outFile = new File(fileBytes);
                return true;
            }
            return false;
        }
    }
}
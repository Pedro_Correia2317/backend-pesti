

using System;

namespace Backend.Utils
{
    public class Date
    {
        public DateTime Value { get; private set; }

        protected Date() {}

        private Date(DateTime date)
        {
            this.Value = date;
        }

        public string GetValue()
        {
            return this.Value.ToString();
        }

        public bool BiggerThan(string otherDate)
        {
            return true;
        }

        public Date AddYears(int years)
        {
            return Create(this.Value.AddYears(years));
        }

        public Date AddMonths(int months)
        {
            return Create(this.Value.AddMonths(months));
        }

        public Date AddDays(int days)
        {
            return Create(this.Value.AddDays(days));
        }

        public Date AddHours(long hours)
        {
            return Create(this.Value.AddHours(hours));
        }

        public Date AddMinutes(long minutes)
        {
            return Create(this.Value.AddMinutes(minutes));
        }

        public Date AddSeconds(long seconds)
        {
            return Create(this.Value.AddSeconds(seconds));
        }

        public Date AddMilliseconds(long miliseconds)
        {
            return Create(this.Value.AddMilliseconds(miliseconds));
        }

        public int CompareTo(Date otherDate)
        {
            return this.Value.CompareTo(otherDate.Value);
        }

        public override bool Equals(object obj)
        {
            return obj is Date date &&
                   Value == date.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value);
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

        public static Date Create(DateTime date)
        {
            return new Date(date);
        }

        public static Date Create(string date)
        {
            Date outDate;
            TryCreate(date, out outDate);
            return outDate;
        }

        public static bool TryCreate(string stringDate, out Date date)
        {
            date = null;
            DateTime outDate;
            if(DateTime.TryParse(stringDate, out outDate))
            {
                date = new Date(outDate);
                return true;
            }
            return false;
        }

        public static Date Default()
        {
            return Date.Create(DateTime.MinValue);
        }
    }
}


using System;
using System.Net;

namespace Backend.Utils
{
    public class InternetAddress : IComparable, IComparable<InternetAddress>
    {

        public IPAddress Value { get; private set; }

        protected InternetAddress() {}

        private InternetAddress(IPAddress address)
        {
            this.Value = address;
        }

        public string GetValue()
        {
            return this.Value.ToString();
        }

        public static InternetAddress Create(string address)
        {
            InternetAddress outAddress;
            TryCreate(address, out outAddress);
            return outAddress;
        }

        public static bool TryCreate(string stringUrl, out InternetAddress address)
        {
            address = null;
            IPAddress ip;
            if(IPAddress.TryParse(stringUrl, out ip))
            {
                address = new InternetAddress(ip);
                return true;
            }
            return false;
        }

        public int CompareTo(object obj)
        {
            InternetAddress address = obj as InternetAddress;
            return this.CompareTo(address);
        }

        public int CompareTo(InternetAddress other)
        {
            if(other == null)
            {
                return 1;
            }
            return this.Value.ToString().CompareTo(other.Value.ToString());
        }
    }
}

using System;
using System.Collections.Generic;
using NodaTime;
using NodaTime.Text;

namespace Backend.Utils
{
    public class Duration : IComparable, IComparable<Duration>
    {

        public Period Value { get; private set; }

        protected Duration() {}

        private Duration(Period duration)
        {
            this.Value = duration;
        }

        public bool BiggerThan(string otherDuration)
        {
            return false;
        }

        public string GetValue()
        {
            return this.Value.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is Duration duration &&
                   this.Value.Equals(duration.Value);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value);
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

        public bool SmallerOrEqualThanADay(Date date)
        {
            Date expirationDate = date.AddYears(this.Value.Years)
                .AddMonths(this.Value.Months)
                .AddDays(this.Value.Days)
                .AddHours(this.Value.Hours)
                .AddMinutes(this.Value.Minutes)
                .AddSeconds(this.Value.Seconds)
                .AddMilliseconds(this.Value.Milliseconds);
            Date minExpirationDate = date.AddDays(1);
            return minExpirationDate.CompareTo(expirationDate) >= 0;
        }

        public bool LargerOrEqualThanFiveYears(Date date)
        {
            Date expirationDate = date.AddYears(this.Value.Years)
                .AddMonths(this.Value.Months)
                .AddDays(this.Value.Days)
                .AddHours(this.Value.Hours)
                .AddMinutes(this.Value.Minutes)
                .AddSeconds(this.Value.Seconds)
                .AddMilliseconds(this.Value.Milliseconds);
            Date maxExpirationDate = date.AddYears(5);
            return maxExpirationDate.CompareTo(expirationDate) < 0;
        }

        public static Duration Create(string duration)
        {
            Duration outDur;
            TryCreate(duration, out outDur);
            return outDur;
        }

        public static bool TryCreate(string stringDur, out Duration duration)
        {
            duration = null;
            ParseResult<Period> results = PeriodPattern.NormalizingIso.Parse(stringDur);
            if(results.Success)
            {
                duration = new Duration(results.Value);
                return true;
            }
            return false;
        }

        public int CompareTo(object other)
        {
            Duration dur = other as Duration;
            return this.CompareTo(dur);
        }

        public int CompareTo(Duration other)
        {
            if(other == null)
            {
                return 1;
            }
            return this.CompareDates(this.Value).CompareTo(this.CompareDates(other.Value));
        }

        private Date CompareDates(Period period)
        {
            return Date.Default().AddYears(period.Years)
                .AddMonths(period.Months)
                .AddDays(period.Days)
                .AddHours(period.Hours)
                .AddMinutes(period.Minutes)
                .AddSeconds(period.Seconds)
                .AddMilliseconds(period.Milliseconds);
        }
    }
}
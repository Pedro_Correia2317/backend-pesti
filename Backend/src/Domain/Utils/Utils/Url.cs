

using System;

namespace Backend.Utils
{
    public class Url : IComparable, IComparable<Url>
    {

        public Uri Value { get; private set; }

        protected Url() {}

        private Url(Uri url)
        {
            this.Value = url;
        }

        public string GetValue()
        {
            return this.Value.ToString();
        }

        public static Url Create(string duration)
        {
            Url outUrl;
            TryCreate(duration, out outUrl);
            return outUrl;
        }

        public static bool TryCreate(string stringUrl, out Url url)
        {
            url = null;
            Uri uri;
            if(Uri.TryCreate(stringUrl, UriKind.Absolute, out uri))
            {
                url = new Url(uri);
                return true;
            }
            return false;
        }

        public int CompareTo(object obj)
        {
            Url url = obj as Url;
            return this.CompareTo(url);
        }

        public int CompareTo(Url other)
        {
            if(other == null)
            {
                return 1;
            }
            return this.Value.AbsoluteUri.CompareTo(other.Value.AbsoluteUri);
        }
    }
}
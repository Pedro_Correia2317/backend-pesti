

namespace Backend.Utils
{
    public class QueryResults<T>
    {

        public bool WasExecutedSuccessfully { get; private set; }

        public T Results { get; private set; }

        public Error FailureDescription { get; private set; }

        public QueryResults()
        {
        }

        public QueryResults<T> withReturnedObject(T Result)
        {
            this.WasExecutedSuccessfully = true;
            this.Results = Result;
            this.FailureDescription = null;
            return this;
        }

        public QueryResults<T> withFailure(Error FailureDescription)
        {
            this.WasExecutedSuccessfully = false;
            this.Results = default(T);
            this.FailureDescription = FailureDescription;
            return this;
        }
        
    }
}

namespace Backend.Utils
{
    public class Results<T>
    {

        private bool FinishedSuccesfully;

        public T Result { get; private set; }

        public Error Error { get; private set; }

        public Results()
        {
        }

        public Results<T> withReturnedObject(T Result)
        {
            this.Result = Result;
            this.FinishedSuccesfully = true;
            this.Error = null;
            return this;
        }

        public Results<T> withFailure(Error Error)
        {
            this.Result = default(T);
            this.FinishedSuccesfully = false;
            this.Error = Error;
            return this;
        }

        public bool wasExecutedSuccessfully(){
            return this.FinishedSuccesfully;
        }

        public static Results<T> Of(T of)
        {
            return new Results<T>().withReturnedObject(of);
        }

        public static Results<T> Of(Error of)
        {
            return new Results<T>().withFailure(of);
        }
    }
}

namespace Backend.Utils
{
    public class GeneralErrors
    {
        public static readonly string TYPE = "General";
        public static readonly Error DB_ERROR = new Error("The following problem ocurred accessing the db: {{0}}", "DB_ERROR", TYPE);
        public static readonly Error UNKNOWN_ID = new Error("The id '{{0}}' of class '{{1}}' is unknown", "UNKNOWN_ID", TYPE);
    }
}
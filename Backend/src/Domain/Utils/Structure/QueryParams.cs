
namespace Backend.Utils
{
    public class QueryParams
    {
        public int page;
        public int size;
        public string sort;
        public string search;
        public string[] filters;
        public string order;
        public string condition;

        public QueryParams()
        {
            this.page = 0;
            this.size = 0;
            this.sort = "";
            this.search = "";
            this.filters = new string[] {""};
            this.order = "";
            this.condition = "";
        }
    }

}
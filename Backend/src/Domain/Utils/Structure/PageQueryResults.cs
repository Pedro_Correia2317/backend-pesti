

using System.Collections.Generic;

namespace Backend.Utils
{
    public class PageQueryResults<T>
    {

        public bool WasExecutedSuccessfully { get; private set; }

        public T[] Results { get; private set; }

        public int TotalNumber { get; private set; }

        public Error FailureDescription { get; private set; }

        public PageQueryResults()
        {
        }

        public PageQueryResults<T> withReturnedObject(List<T> Result, int totalNumber)
        {
            this.WasExecutedSuccessfully = true;
            this.Results = Result.ToArray();
            this.TotalNumber = totalNumber;
            this.FailureDescription = null;
            return this;
        }

        public PageQueryResults<T> withFailure(Error FailureDescription)
        {
            this.WasExecutedSuccessfully = false;
            this.Results = default(T[]);
            this.TotalNumber = -1;
            this.FailureDescription = FailureDescription;
            return this;
        }

    }
}

using System;
using System.Collections.Generic;

namespace Backend.Utils
{
    public class Error
    {

        private string LoggerMessage;

        public string UserMessage { get; set; }

        public object[] Parameters { get; private set; }

        public string Type { get; private set; }

        public Error(string loggerMessage, string userMessage, string type)
        {
            this.LoggerMessage = loggerMessage;
            this.UserMessage = userMessage;
            this.Parameters = new object[0];
            this.Type = type;
        }

        private Error(string loggerMessage, string userMessage, object[] parameters, string type)
        {
            this.LoggerMessage = loggerMessage;
            this.UserMessage = userMessage;
            this.Parameters = parameters;
            this.Type = type;
        }

        public Error Copy()
        {
            return new Error(this.LoggerMessage, this.UserMessage, this.Type);
        }

        public Error WithParams(params object[] parameters)
        {
            this.Parameters = parameters;
            return this;
        }

        public string FormattedLoggerMessage()
        {
            string formattedMessage = LoggerMessage;
            for (int i = 0; i < Parameters.Length; i++)
            {
                formattedMessage = formattedMessage.Replace("{{" + i + "}}", Parameters[i]?.ToString());
            }
            return formattedMessage;
        }

        public Error ChangeUserMessage(string newUserMessage)
        {
            return new Error(this.LoggerMessage, newUserMessage, this.Parameters, this.Type);
        }

        public override bool Equals(object obj)
        {
            return obj is Error error &&
                   LoggerMessage == error.LoggerMessage &&
                   UserMessage == error.UserMessage &&
                   Type == error.Type;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(LoggerMessage, UserMessage, Parameters, Type);
        }
    }
}
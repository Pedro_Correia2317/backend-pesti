
using System;
using System.Runtime.Serialization;

namespace Backend.Utils
{
    public class BusinessRuleFailureException : Exception
    {

        public Error Error { get; private set; }

        public BusinessRuleFailureException() { }

        public BusinessRuleFailureException(Error Error) : base(Error?.FormattedLoggerMessage())
        {
            this.Error = Error;
        }

        public BusinessRuleFailureException(string message) : base(message) { }

        public BusinessRuleFailureException(string message, System.Exception inner) : base(message, inner) { }

    }
}
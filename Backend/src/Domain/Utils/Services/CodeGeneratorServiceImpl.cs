

using System;
using System.Linq;

namespace Backend.Utils
{
    public class CodeGeneratorServiceImpl : ICodeGeneratorService
    {
        public Results<string> NewAccountCode()
        {
            return this.NewGuid();
        }

        public Results<string> NewLicenseCode()
        {
            return this.NewGuid();
        }

        public Results<string> NewMeetingCode()
        {
            return this.NewGuid();
        }

        public Results<string> NewProjectCode()
        {
            return this.NewGuid();
        }

        public Results<string> NewTenantCode()
        {
            return this.NewGuid();
        }

        private Results<string> NewGuid()
        {
            return new Results<string>().withReturnedObject(Guid.NewGuid().ToString());
        }
    }
}
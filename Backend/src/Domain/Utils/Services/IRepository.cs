
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Utils
{
    public interface IRepository<Element, Id>
    {
        Task<QueryResults<bool>> Save(Element element);
        Task<QueryResults<bool>> SaveAll(ICollection<Element> element);
        Task<QueryResults<bool>> Delete(Element element);
        Task<QueryResults<bool>> Update(Element element);
        Task<QueryResults<bool>> isIdUnique(Id Id);
        Task<QueryResults<bool>> AreIdsValid(List<Id> Id);
        Task<QueryResults<Element>> FindById(Id Id);
        Task<PageQueryResults<Element>> FindByPage(QueryParams Params);
        Task<QueryResults<int>> CountElements(QueryParams Params);
    }
}
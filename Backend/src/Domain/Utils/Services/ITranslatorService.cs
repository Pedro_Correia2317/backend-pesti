
using System.Collections.Generic;
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Meetings;
using Backend.Tenants;

namespace Backend.Utils
{
    public interface ITranslatorService
    {
        List<Error> TranslateAllMessages(List<Error> errors);
        AccountDetailsDTO Translate(AccountDetailsDTO account);
        AccountDTO Translate(AccountDTO account);
        AccountDTO[] Translate(AccountDTO[] accounts);
        TenantDTO Translate(TenantDTO tenant);
        TenantDTO[] Translate(TenantDTO[] tenants);
        TenantMemberDTO[] Translate(TenantMemberDTO[] members);
        LicenseDTO Translate(LicenseDTO license);
        LicenseDTO[] Translate(LicenseDTO[] licenses);
        EntryDTO Translate(EntryDTO entry);
        EntryDTO[] Translate(EntryDTO[] entries);
        AccountTypeDTO[] Translate(AccountTypeDTO[] types);
        AccountStatusDTO[] Translate(AccountStatusDTO[] statuses);
        TenantPermissionsDTO[] Translate(TenantPermissionsDTO[] permissions);
        LicenseTypeDTO[] Translate(LicenseTypeDTO[] types);
        EntryTypeDTO[] Translate(EntryTypeDTO[] types);
        MeetingRoleDTO[] Translate(MeetingRoleDTO[] roles);
        MeetingParticipantTypeDTO[] Translate(MeetingParticipantTypeDTO[] arrayDTO);
    }
}

using System;
using System.Collections.Generic;

namespace Backend.Utils
{
    public class LoggerServiceImpl : ILoggerService
    {

        public LoggerServiceImpl()
        {
        }

        public void logAllNotificationsInfo(List<Error> lists)
        {
            foreach (Error not in lists)
            {
                logInfo(not.FormattedLoggerMessage());
            }
        }

        public void logError(string message)
        {
            Console.WriteLine("Error: " + message);
        }

        public void logInfo(string message)
        {
            Console.WriteLine("Info: " + message);
        }

        public void logWarn(string message)
        {
            Console.WriteLine("Warn: " + message);
        }
    }
}
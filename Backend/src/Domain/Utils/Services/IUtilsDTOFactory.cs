
using System.Collections.Generic;

namespace Backend.Utils
{
    public interface IUtilsDTOFactory
    {
        CreateResults<T> CreateResultsDTO<T>(T correspondantObject) where T : IDTO;
        CreateResults<T> CreateResultsDTO<T>(List<Error> nots) where T : IDTO;
        CreatePackResults<T> CreatePackResults<T>(T[] correspondantObject) where T : IDTO;
        CreatePackResults<T> CreatePackResults<T>(List<Error> errors) where T : IDTO;
        GetPageResults<T> CreateGetPageResultsDTO<T>(List<Error> nots) where T : IDTO;
        GetPageResults<T> CreateGetPageResultsDTO<T>(T[] listDTO, int count) where T : IDTO;
        GetOneResults<T> CreateGetOneResultsDTO<T>(List<Error> nots) where T : IDTO;
        GetOneResults<T> CreateGetOneResultsDTO<T>(T obj) where T : IDTO;
        GetFileResults CreateGetFileResults(List<Error> nots);
        GetFileResults CreateGetFileResults(File file, string name);
        DeleteResults CreateDeleteResultsDTO();
        DeleteResults CreateDeleteResultsDTO(List<Error> notifications, bool itExists);
        UpdateResults CreateUpdateResultsDTO();
        UpdateResults CreateUpdateResultsDTO(List<Error> notifications, bool itExists);
        StandardResults CreateStandardResultsDTO();
        StandardResults CreateStandardResultsDTO(List<Error> errors);
    }
}


using System;
using System.Collections.Generic;
using System.IO;
using Backend.Licenses;
using Backend.Meetings;
using Microsoft.AspNetCore.Http;

namespace Backend.Utils
{
    public class RequestAnalyserImpl : IRequestAnalyser
    {
        public Error AnalyseRequest(CreateLicensePackRequest request)
        {
            int totalLicenses = 0;
            if (request?.licenses == null)
            {
                return LicenseErrors.NO_LICENSES_TO_CREATE.Copy();
            }
            foreach (var lic in request.licenses)
            {
                if (lic.quantity == null || lic.quantity <= 0)
                {
                    return LicenseErrors.INVALID_QUANTITY.Copy();
                }
                totalLicenses += lic.quantity > 0 ? (int)lic.quantity : 0;
            }
            if (totalLicenses <= 0)
            {
                return LicenseErrors.NO_LICENSES_TO_CREATE.Copy();
            }
            if (totalLicenses > 10000)
            {
                return LicenseErrors.TOO_MANY_LICENSES_TO_CREATE.Copy().WithParams(totalLicenses, 10000);
            }
            return null;
        }

        public ICollection<License> CreateLicenses(CreateLicensePackRequest request, Func<LicenseInCreatePack, License> createLicenseFunction)
        {
            ICollection<License> list = new HashSet<License>();
            foreach (var lic in request.licenses)
            {
                for (int i = 0; i < lic.quantity; i++)
                {
                    License license = createLicenseFunction(lic);
                    if (license == null)
                    {
                        return list;
                    }
                    list.Add(license);
                }
            }
            return list;
        }

        public Results<byte[]> GetBytesFromFile(IFormFile file)
        {
            if(file == null)
            {
                return Results<byte[]>.Of(MeetingErrors.NULL_FILE_BYTES.Copy());
            }
            using (var memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                return Results<byte[]>.Of(memoryStream.ToArray());
            }
        }

        public Results<string> GetNameFromFile(IFormFile file)
        {
            if(file == null)
            {
                return Results<string>.Of(MeetingErrors.NULL_FILE_NAME.Copy());
            }
            return Results<string>.Of(file.FileName);
        }
    }
}
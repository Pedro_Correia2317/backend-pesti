
using System.Collections.Generic;

namespace Backend.Utils
{
    public interface ILoggerService
    {
        void logInfo(string message);

        void logWarn(string message);

        void logError(string message);
        
        void logAllNotificationsInfo(List<Error> lists);
    }
}
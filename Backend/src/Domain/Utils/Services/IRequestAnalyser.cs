

using System;
using System.Collections.Generic;
using Backend.Licenses;
using Microsoft.AspNetCore.Http;

namespace Backend.Utils
{
    public interface IRequestAnalyser
    {
        Error AnalyseRequest(CreateLicensePackRequest request);
        ICollection<License> CreateLicenses(CreateLicensePackRequest request, Func<LicenseInCreatePack, License> createLicenseFunction);
        Results<byte[]> GetBytesFromFile(IFormFile file);
        Results<string> GetNameFromFile(IFormFile file);
    }
}
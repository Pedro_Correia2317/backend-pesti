

namespace Backend.Utils
{
    public interface ICodeGeneratorService
    {
        Results<string> NewLicenseCode();
        Results<string> NewTenantCode();
        Results<string> NewAccountCode();
        Results<string> NewProjectCode();
        Results<string> NewMeetingCode();
    }
}

using System.Threading.Tasks;

namespace Backend.Utils
{
    public interface IUnitOfWork
    {
        Task<QueryResults<bool>> Commit();
    }
}
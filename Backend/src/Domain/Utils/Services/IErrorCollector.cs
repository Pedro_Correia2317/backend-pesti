
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Utils
{
    public interface IErrorCollector
    {
        T Analyse<T>(Results<T> Results);
        
        T Analyse<T>(QueryResults<T> Results);

        void Analyse<T>(PageQueryResults<T> Results);

        T[] Analyse<T>(PageQueryResults<T> results, out int totalNumber);

        T IfZeroErrorsDo<T>(Func<Results<T>> function);

        T IfZeroErrorsDo<T>(Func<T> function);

        Task<T> IfZeroErrorsDo<T>(Func<Task<QueryResults<T>>> function);

        bool HasErrors();

        bool HasNoErrors();

        List<Error> ListErrors();
        
        void Catch(Error error);
    }
}
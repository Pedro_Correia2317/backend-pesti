
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Utils
{
    public class ErrorCollectorImpl : IErrorCollector
    {

        private List<Error> Errors;

        public ErrorCollectorImpl()
        {
            this.Errors = new List<Error>();
        }

        public T Analyse<T>(Results<T> results)
        {
            if(!results.wasExecutedSuccessfully())
            {
                this.Errors.Add(results.Error);
            }
            return results.Result;
        }

        public T Analyse<T>(QueryResults<T> results)
        {
            if(!results.WasExecutedSuccessfully)
            {
                this.Errors.Add(results.FailureDescription);
            }
            return results.Results;
        }

        public T IfZeroErrorsDo<T>(Func<Results<T>> function)
        {
            if(this.HasNoErrors())
            {
                return this.Analyse(function());
            }
            return default(T);
        }

        public T IfZeroErrorsDo<T>(Func<T> function)
        {
            if(this.HasNoErrors())
            {
                return function();
            }
            return default(T);
        }

        public async Task<T> IfZeroErrorsDo<T>(Func<Task<QueryResults<T>>> function)
        {
            if(this.HasNoErrors())
            {
                return this.Analyse(await function());
            }
            return default(T);
        }

        public void Analyse<T>(PageQueryResults<T> results)
        {
            if(!results.WasExecutedSuccessfully)
            {
                this.Errors.Add(results.FailureDescription);
            }
        }

        public T[] Analyse<T>(PageQueryResults<T> results, out int totalNumber)
        {
            if(!results.WasExecutedSuccessfully)
            {
                this.Errors.Add(results.FailureDescription);
            }
            totalNumber = results.TotalNumber;
            return results.Results;
        }

        public bool HasErrors()
        {
            return Errors.Count > 0;
        }

        public bool HasNoErrors()
        {
            return Errors.Count == 0;
        }

        public List<Error> ListErrors()
        {
            return Errors;
        }

        public void Catch(Error error)
        {
            if(error != null)
            {
                this.Errors.Add(error);
            }
        }
    }
}
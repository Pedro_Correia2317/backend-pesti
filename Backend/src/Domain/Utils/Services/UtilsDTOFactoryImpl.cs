
using System.Collections.Generic;
using System.IO;

namespace Backend.Utils
{
    public class UtilsDTOFactoryImpl : IUtilsDTOFactory
    {

        public CreateResults<T> CreateResultsDTO<T>(T correspondantObject) where T : IDTO
        {
            CreateResults<T> dto = new CreateResults<T>();
            dto.success = true;
            dto.notifications = new ErrorDTO[0];
            dto.result = correspondantObject;
            return dto;
        }

        public CreateResults<T> CreateResultsDTO<T>(List<Error> nots) where T : IDTO
        {
            CreateResults<T> dto = new CreateResults<T>();
            dto.success = false;
            dto.notifications = new ErrorDTO[nots == null? 0 : nots.Count];
            for (int i = 0; i < dto.notifications.Length; i++)
            {
                dto.notifications[i] = CreateErrorDTO(nots[i]);
            }
            return dto;
        }

        public CreatePackResults<T> CreatePackResults<T>(T[] correspondantObject) where T : IDTO
        {
            CreatePackResults<T> dto = new CreatePackResults<T>();
            dto.success = true;
            dto.notifications = new ErrorDTO[0];
            dto.result = correspondantObject;
            return dto;
        }

        public CreatePackResults<T> CreatePackResults<T>(List<Error> errors) where T : IDTO
        {
            CreatePackResults<T> dto = new CreatePackResults<T>();
            dto.success = false;
            dto.notifications = new ErrorDTO[errors == null? 0 : errors.Count];
            for (int i = 0; i < dto.notifications.Length; i++)
            {
                dto.notifications[i] = CreateErrorDTO(errors[i]);
            }
            return dto;
        }

        public DeleteResults CreateDeleteResultsDTO()
        {
            DeleteResults dto = new DeleteResults();
            dto.success = true;
            dto.notifications = new ErrorDTO[0];
            dto.itExists = true;
            return dto;
        }

        public DeleteResults CreateDeleteResultsDTO(List<Error> notifications, bool itExists)
        {
            DeleteResults dto = new DeleteResults();
            dto.success = false;
            dto.notifications = new ErrorDTO[notifications == null? 0 : notifications.Count];
            for (int i = 0; i < dto.notifications.Length; i++)
            {
                dto.notifications[i] = CreateErrorDTO(notifications[i]);
            }
            dto.itExists = itExists;
            return dto;
        }

        public UpdateResults CreateUpdateResultsDTO()
        {
            UpdateResults dto = new UpdateResults();
            dto.success = true;
            dto.notifications = new ErrorDTO[0];
            dto.itExists = true;
            return dto;
        }

        public UpdateResults CreateUpdateResultsDTO(List<Error> notifications, bool itExists)
        {
            UpdateResults dto = new UpdateResults();
            dto.success = false;
            dto.notifications = new ErrorDTO[notifications == null? 0 : notifications.Count];
            for (int i = 0; i < dto.notifications.Length; i++)
            {
                dto.notifications[i] = CreateErrorDTO(notifications[i]);
            }
            dto.itExists = itExists;
            return dto;
        }

        private ErrorDTO CreateErrorDTO(Error error)
        {
            ErrorDTO dto = new ErrorDTO();
            dto.description = error.UserMessage;
            return dto;
        }

        public GetPageResults<T> CreateGetPageResultsDTO<T>(List<Error> nots) where T : IDTO
        {
            GetPageResults<T> dto = new GetPageResults<T>();
            dto.success = false;
            dto.notifications = new ErrorDTO[nots == null? 0 : nots.Count];
            for (int i = 0; i < dto.notifications.Length; i++)
            {
                dto.notifications[i] = CreateErrorDTO(nots[i]);
            }
            dto.results = null;
            return dto;
        }

        public GetPageResults<T> CreateGetPageResultsDTO<T>(T[] listDTO, int count) where T : IDTO
        {
            GetPageResults<T> dto = new GetPageResults<T>();
            dto.success = true;
            dto.notifications = new ErrorDTO[0];
            QueryDTO<T> query = new QueryDTO<T>();
            query.totalNumber = count;
            query.results = listDTO;
            dto.results = query;
            return dto;
        }

        public GetOneResults<T> CreateGetOneResultsDTO<T>(List<Error> nots) where T : IDTO
        {
            GetOneResults<T> dto = new GetOneResults<T>();
            dto.finishedSuccesfully = false;
            dto.notifications = new ErrorDTO[nots == null? 0 : nots.Count];
            for (int i = 0; i < dto.notifications.Length; i++)
            {
                dto.notifications[i] = CreateErrorDTO(nots[i]);
            }
            dto.result = default(T);
            return dto;
        }

        public GetOneResults<T> CreateGetOneResultsDTO<T>(T obj) where T : IDTO
        {
            GetOneResults<T> dto = new GetOneResults<T>();
            dto.finishedSuccesfully = true;
            dto.notifications = new ErrorDTO[0];
            dto.result = obj;
            return dto;
        }

        public StandardResults CreateStandardResultsDTO()
        {
            StandardResults dto = new StandardResults();
            dto.success = true;
            dto.notifications = new ErrorDTO[0];
            return dto;
        }

        public StandardResults CreateStandardResultsDTO(List<Error> errors)
        {
            StandardResults dto = new StandardResults();
            dto.success = false;
            dto.notifications = new ErrorDTO[errors == null? 0 : errors.Count];
            for (int i = 0; i < dto.notifications.Length; i++)
            {
                dto.notifications[i] = CreateErrorDTO(errors[i]);
            }
            return dto;
        }

        public GetFileResults CreateGetFileResults(List<Error> errors)
        {
            GetFileResults dto = new GetFileResults();
            dto.WasFinishedSuccessfully = false;
            dto.Notifications = new ErrorDTO[errors == null? 0 : errors.Count];
            for (int i = 0; i < dto.Notifications.Length; i++)
            {
                dto.Notifications[i] = CreateErrorDTO(errors[i]);
            }
            return dto;
        }

        public GetFileResults CreateGetFileResults(File file, string name)
        {
            GetFileResults dto = new GetFileResults();
            dto.WasFinishedSuccessfully = true;
            dto.FileStream = new MemoryStream(file.Value);
            dto.FileName = name;
            dto.Notifications = new ErrorDTO[0];
            return dto;
        }
    }
}


namespace Backend.Utils
{
    public class DomainUtils
    {
        public static bool HasNullFields(params object[] args)
        {
            foreach (object arg in args)
            {
                if(arg == null)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
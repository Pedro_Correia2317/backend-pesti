

using System;
using System.Collections.Generic;
using Backend.Utils;

public static class FactoryUtils
{
    public static Results<T> NewObject<T>(Func<T> function)
    {
        Results<T> results = new Results<T>();
        try
        {
            results.withReturnedObject(function());
        }
        catch (BusinessRuleFailureException ex)
        {
            results.withFailure(ex.Error);
        }
        return results;
    }

    public static T[] NewArrayDTO<T, S>(S[] objs, Func<S, T> function)
    {
        int size = objs == null ? 0 : objs.Length;
        T[] listDTO = new T[size];
        for (int i = 0; i < size; i++)
        {
            listDTO[i] = function(objs[i]);
        }
        return listDTO;
    }

    public static T[] NewArrayDTO<T, S>(ICollection<S> objs, Func<S, T> function)
    {
        int size = objs == null ? 0 : objs.Count;
        T[] listDTO = new T[size];
        int i = 0;
        foreach (S obj in objs)
        {
            listDTO[i] = function(obj);
            i++;
        }
        return listDTO;
    }

    public static T[] NewArrayDTO<T>(T[] objs, Func<T, T> function)
    {
        foreach (var item in objs)
        {
            function(item);
        }
        return objs;
    }
}
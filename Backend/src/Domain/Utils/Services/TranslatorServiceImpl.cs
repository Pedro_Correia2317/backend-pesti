

using System.Linq;
using System.Collections.Generic;
using Backend.Accounts;
using Backend.Licenses;
using Microsoft.Extensions.Localization;
using Backend.Tenants;
using Backend.HistoryEntries;
using Backend.Meetings;
using Backend.Projects;

namespace Backend.Utils
{
    public class TranslatorServiceImpl : ITranslatorService
    {

        private Dictionary<string, IStringLocalizer> AllLocalizers;

        public TranslatorServiceImpl(IStringLocalizer<Error> general, IStringLocalizer<License> licenses,
                IStringLocalizer<Account> accounts, IStringLocalizer<Tenant> tenants, 
                IStringLocalizer<HistoryEntry> entries, IStringLocalizer<Meeting> meetings, 
                IStringLocalizer<Project> projects)
        {
            this.AllLocalizers = new Dictionary<string, IStringLocalizer>();
            this.AllLocalizers.Add(GeneralErrors.TYPE, general);
            this.AllLocalizers.Add(LicenseErrors.TYPE, licenses);
            this.AllLocalizers.Add(AccountErrors.TYPE, accounts);
            this.AllLocalizers.Add(TenantErrors.TYPE, tenants);
            this.AllLocalizers.Add(EntryErrors.TYPE, entries);
            this.AllLocalizers.Add(ProjectErrors.TYPE, projects);
            this.AllLocalizers.Add(MeetingErrors.TYPE, meetings);
        }

        private IStringLocalizer GetLocalizer(string type)
        {
            IStringLocalizer aux = this.AllLocalizers.GetValueOrDefault(type);
            return aux == null? this.AllLocalizers.First().Value : aux;
        }

        public List<Error> TranslateAllMessages(List<Error> errors)
        {
            List<Error> translatedErrors = new List<Error>(errors.Count);
            foreach (Error error in errors)
            {
                IStringLocalizer localizer = this.GetLocalizer(error.Type);
                string translatedMessage = localizer.GetString(error.UserMessage, error.Parameters);
                translatedErrors.Add(error.ChangeUserMessage(translatedMessage));
            }
            return translatedErrors;
        }

        private AccountTypeDTO Translate(AccountTypeDTO type, IStringLocalizer localizer)
        {
            type.description = localizer.GetString(type.key);
            return type;
        }

        private AccountStatusDTO Translate(AccountStatusDTO type, IStringLocalizer localizer)
        {
            type.description = localizer.GetString(type.key);
            return type;
        }

        private TenantPermissionsDTO Translate(TenantPermissionsDTO type, IStringLocalizer localizer)
        {
            type.description = localizer.GetString(type.key);
            return type;
        }

        private LicenseTypeDTO Translate(LicenseTypeDTO type, IStringLocalizer localizer)
        {
            type.description = localizer.GetString(type.key);
            return type;
        }

        private EntryTypeDTO Translate(EntryTypeDTO type, IStringLocalizer localizer)
        {
            type.description = localizer.GetString(type.key);
            return type;
        }

        private MeetingRoleDTO Translate(MeetingRoleDTO type, IStringLocalizer localizer)
        {
            type.description = localizer.GetString(type.key);
            return type;
        }

        private MeetingParticipantTypeDTO Translate(MeetingParticipantTypeDTO type, IStringLocalizer localizer)
        {
            type.description = localizer.GetString(type.key);
            return type;
        }

        public AccountTypeDTO[] Translate(AccountTypeDTO[] arrayDTO)
        {
            IStringLocalizer localizer = this.GetLocalizer(AccountErrors.TYPE);
            foreach (var status in arrayDTO)
            {
                this.Translate(status, localizer);
            }
            return arrayDTO;
        }

        public AccountStatusDTO[] Translate(AccountStatusDTO[] arrayDTO)
        {
            IStringLocalizer localizer = this.GetLocalizer(AccountErrors.TYPE);
            foreach (var status in arrayDTO)
            {
                this.Translate(status, localizer);
            }
            return arrayDTO;
        }

        public TenantPermissionsDTO[] Translate(TenantPermissionsDTO[] arrayDTO)
        {
            IStringLocalizer localizer = this.GetLocalizer(TenantErrors.TYPE);
            foreach (var status in arrayDTO)
            {
                this.Translate(status, localizer);
            }
            return arrayDTO;
        }

        public LicenseTypeDTO[] Translate(LicenseTypeDTO[] arrayDTO)
        {
            IStringLocalizer localizer = this.GetLocalizer(LicenseErrors.TYPE);
            foreach (var status in arrayDTO)
            {
                this.Translate(status, localizer);
            }
            return arrayDTO;
        }

        public EntryTypeDTO[] Translate(EntryTypeDTO[] arrayDTO)
        {
            IStringLocalizer localizer = this.GetLocalizer(EntryErrors.TYPE);
            foreach (var status in arrayDTO)
            {
                this.Translate(status, localizer);
            }
            return arrayDTO;
        }

        public MeetingRoleDTO[] Translate(MeetingRoleDTO[] arrayDTO)
        {
            IStringLocalizer localizer = this.GetLocalizer(MeetingErrors.TYPE);
            foreach (var status in arrayDTO)
            {
                this.Translate(status, localizer);
            }
            return arrayDTO;
        }

        public MeetingParticipantTypeDTO[] Translate(MeetingParticipantTypeDTO[] arrayDTO)
        {
            IStringLocalizer localizer = this.GetLocalizer(MeetingErrors.TYPE);
            foreach (var status in arrayDTO)
            {
                this.Translate(status, localizer);
            }
            return arrayDTO;
        }

        public AccountDetailsDTO Translate(AccountDetailsDTO account)
        {
            IStringLocalizer localizer = this.GetLocalizer(AccountErrors.TYPE);
            account.type = this.Translate(account.type, localizer);
            return account;
        }

        public AccountDTO Translate(AccountDTO account)
        {
            IStringLocalizer localizer = this.GetLocalizer(AccountErrors.TYPE);
            account.type = this.Translate(account.type, localizer);
            account.status = this.Translate(account.status, localizer);
            return account;
        }

        public AccountDTO[] Translate(AccountDTO[] accounts)
        {
            return FactoryUtils.NewArrayDTO<AccountDTO>(accounts, this.Translate);
        }

        public TenantDTO Translate(TenantDTO tenant)
        {
            return tenant;
        }

        public TenantDTO[] Translate(TenantDTO[] tenants)
        {
            return FactoryUtils.NewArrayDTO<TenantDTO>(tenants, this.Translate);
        }

        private TenantMemberDTO Translate(TenantMemberDTO member)
        {
            IStringLocalizer localizer = this.GetLocalizer(TenantErrors.TYPE);
            member.permissions = this.Translate(member.permissions, localizer);
            return member;
        }

        public TenantMemberDTO[] Translate(TenantMemberDTO[] members)
        {
            return FactoryUtils.NewArrayDTO<TenantMemberDTO>(members, this.Translate);
        }

        public LicenseDTO Translate(LicenseDTO license)
        {
            IStringLocalizer localizer = this.GetLocalizer(LicenseErrors.TYPE);
            license.licenseType = this.Translate(license.licenseType, localizer);
            return license;
        }

        public LicenseDTO[] Translate(LicenseDTO[] licenses)
        {
            return FactoryUtils.NewArrayDTO<LicenseDTO>(licenses, this.Translate);
        }

        public EntryDTO Translate(EntryDTO entry)
        {
            IStringLocalizer localizer = this.GetLocalizer(EntryErrors.TYPE);
            entry.type = this.Translate(entry.type, localizer);
            return entry;
        }

        public EntryDTO[] Translate(EntryDTO[] entries)
        {
            return FactoryUtils.NewArrayDTO<EntryDTO>(entries, this.Translate);
        }
    }
}
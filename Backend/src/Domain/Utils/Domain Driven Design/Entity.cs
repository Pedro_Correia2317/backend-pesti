
namespace Backend.Utils
{
    public abstract class Entity<TEntityId> where TEntityId: EntityId
    {
         public TEntityId BusinessId { get; set; }
    }
}
using System;

namespace Backend.Utils
{

    public abstract class EntityId: IEquatable<EntityId>, IComparable, IComparable<EntityId>
    {
        public String Value { get; protected set; }

        protected EntityId() {}

        protected EntityId(String value)
        {
            this.Value = value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is EntityId other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public bool Equals(EntityId other)
        {
            if (other == null)
                return false;
            if (this.GetType() != other.GetType())
                return false;
            return this.Value == other.Value;
        }

        public int CompareTo(object other){
            EntityId id = other as EntityId;
            return this.CompareTo(id);
        }

        public int CompareTo(EntityId other){
            if (other == null)
                return -1;
            return this.Value.CompareTo(other.Value);
        }

        public static bool operator ==(EntityId obj1, EntityId obj2)
        {
            if (object.Equals(obj1, null))
            {
                if (object.Equals(obj2, null))
                {
                    return true;
                }
                return false;
            }
            return obj1.Equals(obj2);
        }
        public static bool operator !=(EntityId x, EntityId y) 
        {
            return !(x == y);
        }
    }

}


using System;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Projects
{
    public class Project : Entity<ProjectId>, IAggregateRoot
    {
        public ProjectName Name { get; private set; }

        public ProjectDescription Description { get; private set; }

        public TenantId Tenant { get; private set; }

        protected Project () {}

        public Project(ProjectId id, ProjectName name, ProjectDescription description, TenantId tenant)
        {
            if (DomainUtils.HasNullFields(id, name, description, tenant))
            {
                throw new BusinessRuleFailureException(ProjectErrors.NULL_DATA_IN_PROJECT.Copy());
            }
            this.BusinessId = id;
            this.Name = name;
            this.Description = description;
            this.Tenant = tenant;
        }

        public Results<bool> ChangeName(ProjectName name)
        {
            if (name == null)
            {
                return Results<bool>.Of(ProjectErrors.NULL_NAME.Copy());
            }
            this.Name = name;
            return Results<bool>.Of(true);
        }

        public Results<bool> ChangeDescription(ProjectDescription description)
        {
            if (description == null)
            {
                return Results<bool>.Of(ProjectErrors.NULL_DESCRIPTION.Copy());
            }
            this.Description = description;
            return Results<bool>.Of(true);
        }

        public Results<bool> HasTenant(TenantId tenant)
        {
            if (this.Tenant.Equals(tenant))
            {
                return Results<bool>.Of(true);
            }
            return Results<bool>.Of(ProjectErrors.WRONG_TENANT.Copy().WithParams(this.BusinessId.Value, tenant?.Value));
        }

        public override bool Equals(object obj)
        {
            return obj is Project project && this.BusinessId.Equals(project.BusinessId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(BusinessId);
        }

        public override string ToString()
        {
            return $"Project: [{this.BusinessId}, {this.Name}, {this.Description}, {this.Tenant}]";
        }
    }
}
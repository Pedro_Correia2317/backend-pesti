
using Backend.Utils;

namespace Backend.Projects
{
    public static class ProjectErrors
    {
        public static readonly string TYPE = "Project";
        public static readonly Error NULL_DATA_IN_PROJECT = new Error("There is null data in the project", "NULL_DATA_IN_PROJECT", TYPE);
        public static readonly Error UNKNOWN_ID = new Error("There is no project with the id '{0}'", "UNKNOWN_ID", TYPE);
        public static readonly Error NULL_ID = new Error("No project id has been inserted", "NULL_ID", TYPE);
        public static readonly Error INVALID_ID = new Error("The id '{{0}}' is not a Guid", "INVALID_ID", TYPE);
        public static readonly Error NULL_NAME = new Error("The inserted name is null", "NULL_NAME", TYPE);
        public static readonly Error EMPTY_NAME = new Error("The inserted name is empty", "EMPTY_NAME", TYPE);
        public static readonly Error NAME_TOO_SMALL = new Error("The inserted name '{{0}}' is smaller than {{1}} chars", "NAME_TOO_SMALL", TYPE);
        public static readonly Error NAME_TOO_LARGE = new Error("The inserted name '{{0}}' is larger than {{1}} chars", "NAME_TOO_LARGE", TYPE);
        public static readonly Error INVALID_NAME = new Error("The inserted name '{{0}}' does not obey the {{1}} regex", "INVALID_NAME", TYPE);
        public static readonly Error NULL_DESCRIPTION = new Error("The description is null", "NULL_DESCRIPTION", TYPE);
        public static readonly Error EMPTY_DESCRIPTION = new Error("The inserted description is empty", "EMPTY_DESCRIPTION", TYPE);
        public static readonly Error DESCRIPTION_TOO_SMALL = new Error("The description '{{0}}' has less than {{1}} chars", "DESCRIPTION_TOO_SMALL", TYPE);
        public static readonly Error DESCRIPTION_TOO_LARGE = new Error("The description '{{0}}' has more than {{1}} chars", "DESCRIPTION_TOO_LARGE", TYPE);
        public static readonly Error INVALID_DESCRIPTION = new Error("The description '{{0}}' does not obey the pattern '{{1}}'", "INVALID_DESCRIPTION", TYPE);
        public static readonly Error WRONG_TENANT = new Error("The project '{{0}}' does not belong to the tenant '{{1}}'", "WRONG_TENANT", TYPE);
        
        
    }
}
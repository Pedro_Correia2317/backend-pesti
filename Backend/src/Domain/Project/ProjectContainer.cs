
using Backend.Tenants;

namespace Backend.Projects
{
    public class ProjectContainer
    {
        public ProjectId Id;

        public ProjectName Name;

        public ProjectDescription Description;

        public TenantId Tenant;
        
    }
}


using System;
using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectId : EntityId, IValueObject
    {
        
        protected ProjectId() : base() {}

        public ProjectId(string id)
        {
            Error err = applyId(id);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyId(string id)
        {
            if(id == null)
                return ProjectErrors.NULL_ID.Copy();
            string trimmedId = id.Trim();
            Guid outGuid;
            if(!Guid.TryParse(trimmedId, out outGuid))
                return ProjectErrors.INVALID_ID.Copy().WithParams(trimmedId);
            this.Value = trimmedId;
            return null;
        }

        public override bool Equals(object obj)
        {
            return obj is ProjectId id &&
                   base.Equals(obj) &&
                   Value == id.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Value);
        }

        public override string ToString()
        {
            return "ProjectId: [" + this.Value + "]";
        }
    }
}
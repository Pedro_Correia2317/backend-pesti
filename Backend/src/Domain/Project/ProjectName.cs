
using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectName : IValueObject
    {
        private static readonly string PATTERN = "[\\p{L}\\d\\s]+";

        public string Name { get; private set; }
        
        protected ProjectName() {}

        public ProjectName(string name)
        {
            Error err = applyName(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyName(string name)
        {
            if(name == null)
                return ProjectErrors.NULL_NAME.Copy();
            string trimmedName = name.Trim();
            if(trimmedName.Length == 0)
                return ProjectErrors.EMPTY_NAME.Copy();
            if(trimmedName.Length < 2)
                return ProjectErrors.NAME_TOO_SMALL.Copy().WithParams(trimmedName, 2);
            if(trimmedName.Length > 200)
                return ProjectErrors.NAME_TOO_LARGE.Copy().WithParams(trimmedName, 200);
            if(!Regex.IsMatch(trimmedName, PATTERN))
                return ProjectErrors.INVALID_NAME.Copy().WithParams(trimmedName, PATTERN);
            this.Name = trimmedName;
            return null;
        }

        public string Value()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            return obj is ProjectName id &&
                   this.Name == id.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.Name);
        }

        public override string ToString()
        {
            return "ProjectName: [" + this.Name + "]";
        }

    }
}
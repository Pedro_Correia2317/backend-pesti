

using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Projects
{
    public interface IProjectDTOFactory
    {
        ProjectDTO CreateBasicDTO(Project project);
        QueryParams CreateQueryParamsOf(ConsultProjectsParameters request);
        ProjectDTO[] CreateListOfBasicDTO(Project[] projects);
        ProjectDTO[] CreateListOfBasicDTO(ICollection<Project> projects);
    }
}


using System;
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Projects
{
    public interface IProjectFactory
    {

        ProjectContainer NewContainer();
        Results<ProjectId> NewId(string id);
        Results<ProjectName> NewName(string name);
        Results<ProjectDescription> NewDescription(string description);
        Results<Project> NewProject(ProjectContainer container);
    }
}
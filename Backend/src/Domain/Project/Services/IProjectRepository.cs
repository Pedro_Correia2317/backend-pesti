

using System.Threading.Tasks;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Projects
{
    public interface IProjectRepository : IRepository<Project, ProjectId>
    {
        Task<PageQueryResults<Project>> FindByPageWithTenant(QueryParams Params, Tenant tenant);
        
    }
}
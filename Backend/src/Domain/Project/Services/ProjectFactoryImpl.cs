

using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectFactoryImpl : IProjectFactory
    {
        public ProjectContainer NewContainer()
        {
            return new ProjectContainer();
        }

        public Results<ProjectId> NewId(string id)
        {
            return FactoryUtils.NewObject(() => new ProjectId(id));
        }

        public Results<ProjectName> NewName(string name)
        {
            return FactoryUtils.NewObject(() => new ProjectName(name));
        }

        public Results<ProjectDescription> NewDescription(string description)
        {
            return FactoryUtils.NewObject(() => new ProjectDescription(description));
        }

        public Results<Project> NewProject(ProjectContainer c)
        {
            return FactoryUtils.NewObject(() => new Project(c.Id, c.Name, c.Description, c.Tenant));
        }
    }
}
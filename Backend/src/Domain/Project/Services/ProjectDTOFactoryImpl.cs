

using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectDTOFactoryImpl : IProjectDTOFactory
    {
        public ProjectDTO CreateBasicDTO(Project project)
        {
            ProjectDTO dto = new ProjectDTO();
            if (project == null)
            {
                dto.id = "";
                dto.name = "";
                dto.description = "";
                dto.tenant = "";
            } else {
                dto.id = project.BusinessId.Value;
                dto.name = project.Name.Value();
                dto.description = project.Description.Value();
                dto.tenant = project.Tenant.Value;
            }
            return dto;
        }

        public ProjectDTO[] CreateListOfBasicDTO(Project[] projects)
        {
            return FactoryUtils.NewArrayDTO(projects, this.CreateBasicDTO);
        }

        public ProjectDTO[] CreateListOfBasicDTO(ICollection<Project> projects)
        {
            return FactoryUtils.NewArrayDTO(projects, this.CreateBasicDTO);
        }

        public QueryParams CreateQueryParamsOf(ConsultProjectsParameters request)
        {
            QueryParams queryParams = new QueryParams();
            queryParams.page = request.page;
            queryParams.size = request.size;
            queryParams.filters = request.filters;
            queryParams.sort = request.sort;
            queryParams.order = request.order;
            queryParams.condition = request.condition;
            queryParams.search = request.search;
            return queryParams;
        }
    }
}
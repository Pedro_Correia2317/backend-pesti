

using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectDescription : IValueObject
    {
        private static readonly string PATTERN = "[\\p{L}\\d\\s]+";

        public string Description { get; private set; }
        
        protected ProjectDescription() {}

        public ProjectDescription(string name)
        {
            Error err = applyDescription(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyDescription(string name)
        {
            if(name == null)
                return ProjectErrors.NULL_DESCRIPTION.Copy();
            string trimmedDescription = name.Trim();
            if(trimmedDescription.Length == 0)
                return ProjectErrors.EMPTY_DESCRIPTION.Copy();
            if(trimmedDescription.Length < 2)
                return ProjectErrors.DESCRIPTION_TOO_SMALL.Copy().WithParams(trimmedDescription, 2);
            if(trimmedDescription.Length > 1000)
                return ProjectErrors.DESCRIPTION_TOO_LARGE.Copy().WithParams(trimmedDescription, 1000);
            if(!Regex.IsMatch(trimmedDescription, PATTERN))
                return ProjectErrors.INVALID_DESCRIPTION.Copy().WithParams(trimmedDescription, PATTERN);
            this.Description = trimmedDescription;
            return null;
        }

        public string Value()
        {
            return this.Description;
        }

        public override string ToString()
        {
            return "ProjectDescription: [" + this.Description + "]";
        }

        public override bool Equals(object obj)
        {
            return obj is ProjectDescription description &&
                   Description == description.Description;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Description);
        }
    }
}
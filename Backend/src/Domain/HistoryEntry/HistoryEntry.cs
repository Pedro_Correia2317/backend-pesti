

using System;
using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class HistoryEntry : Entity<EntryId>, IAggregateRoot
    {
        
        public EntryDescription Description { get; private set; }

        public EntryDate Date { get; private set; }

        public EntryType Type { get; private set; }

        public EntryClientAddress Address { get; private set; }

        public TenantId Tenant { get; private set; }

        public AccountId Account { get; private set; }

        protected HistoryEntry() {}

        public HistoryEntry(EntryId id, EntryDescription desc, EntryDate date, EntryType type, 
            EntryClientAddress address, TenantId tenant)
        {
            if (DomainUtils.HasNullFields(id, desc, date, type, address, tenant))
            {
                throw new BusinessRuleFailureException(EntryErrors.NULL_DATA_IN_ENTRY.Copy());
            }
            if(!type.IsDestinedForTenants())
            {
                throw new BusinessRuleFailureException(EntryErrors.ACCOUNT_TYPE_MIXED_WITH_TENANTS.Copy());
            }
            this.BusinessId = id;
            this.Date = date;
            this.Description = desc;
            this.Type = type;
            this.Address = address;
            this.Tenant = tenant;
        }

        public HistoryEntry(EntryId id, EntryDescription desc, EntryDate date, EntryType type, 
            EntryClientAddress address, AccountId account)
        {
            if (DomainUtils.HasNullFields(id, desc, date, type, address, account))
            {
                throw new BusinessRuleFailureException(EntryErrors.NULL_DATA_IN_ENTRY.Copy());
            }
            if(!type.IsDestinedForAccounts())
            {
                throw new BusinessRuleFailureException(EntryErrors.TENANT_TYPE_MIXED_WITH_ACCOUNTS.Copy());
            }
            this.BusinessId = id;
            this.Date = date;
            this.Description = desc;
            this.Type = type;
            this.Address = address;
            this.Account = account;
        }

        public override bool Equals(object obj)
        {
            return obj is HistoryEntry entry && this.BusinessId.Equals(entry.BusinessId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(BusinessId);
        }

        public override string ToString()
        {
            return $"HistoryEntry: [{this.BusinessId}, {this.Date}, {this.Description}, "
                    + $"{this.Address}, {this.Type}, {this.Tenant}, {this.Account}]";
        }
    }
}


using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class EntryQueryParams : QueryParams
    {

        public string tenant;

        public string account;

        public EntryQueryParams() : base()
        {
            this.tenant = "";
            this.account = "";
        }
    }
}

using System;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class EntryClientAddress : IValueObject
    {

        public InternetAddress Address { get; private set; }
        
        protected EntryClientAddress() {}

        public EntryClientAddress(string address)
        {
            Error err = ApplyAddress(address);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error ApplyAddress(string address)
        {
            if(address == null)
                return EntryErrors.NULL_INTERNET_ADDRESS.Copy();
            string trimmedAddress = address.Trim();
            InternetAddress outAddress;
            if(!InternetAddress.TryCreate(trimmedAddress, out outAddress))
                return EntryErrors.INVALID_INTERNET_ADDRESS.Copy().WithParams(trimmedAddress);
            this.Address = outAddress;
            return null;
        }

        public string Value()
        {
            return this.Address.GetValue();
        }

        public override bool Equals(object obj)
        {
            return obj is EntryClientAddress id &&
                   this.Address == id.Address;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Address);
        }

        public override string ToString()
        {
            return "EntryClientAddress: [" + this.Address + "]";
        }
    }
}
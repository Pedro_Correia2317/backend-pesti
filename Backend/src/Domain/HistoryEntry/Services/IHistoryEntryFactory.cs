

using System;
using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public interface IHistoryEntryFactory
    {
        Results<EntryId> NewId(string id);
        Results<EntryDate> NewDate(string date);
        Results<EntryDate> NewDate(DateTime date);
        Results<EntryDescription> NewDescription(string description);
        Results<EntryDescription> NewDescription(string description, object[] args);
        Results<EntryClientAddress> NewAddress(string address);
        Results<EntryType> NewType(string type);
        Results<HistoryEntry> NewEntry(EntryType type, string address, EntryDescription description, TenantId tenant);
        Results<HistoryEntry> NewEntry(EntryType type, string address, string description, TenantId tenant);
        Results<HistoryEntry> NewEntry(EntryType type, string address, TenantId tenant);
        Results<HistoryEntry> NewEntry(EntryType type, string address, TenantId tenant, params object[] args);
        Results<HistoryEntry> NewEntry(EntryType type, string address, EntryDescription description, AccountId account);
        Results<HistoryEntry> NewEntry(EntryType type, string address, string description, AccountId account);
        Results<HistoryEntry> NewEntry(EntryType type, string address, AccountId account);
        Results<HistoryEntry> NewEntry(EntryType type, string address, AccountId account, params object[] args);
    }
}


using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public interface IHistoryEntryRepository : IRepository<HistoryEntry, EntryId>
    {

        Task<PageQueryResults<HistoryEntry>> FindByPage(EntryQueryParams args);

    }
}


using System;
using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class HistoryEntryFactoryImpl : IHistoryEntryFactory
    {

        public Results<EntryId> NewId(string id)
        {
            return FactoryUtils.NewObject(() => new EntryId(id));
        }

        public Results<EntryType> NewType(string type)
        {
            return FactoryUtils.NewObject(() => EntryTypes.GetByName(type));
        }
        
        public Results<EntryDate> NewDate(string date)
        {
            return FactoryUtils.NewObject(() => new EntryDate(date));
        }

        public Results<EntryDate> NewDate(DateTime date)
        {
            return FactoryUtils.NewObject(() => new EntryDate(date));
        }

        public Results<EntryDescription> NewDescription(string description)
        {
            return FactoryUtils.NewObject(() => new EntryDescription(description));
        }

        public Results<EntryDescription> NewDescription(string description, object[] args)
        {
            return FactoryUtils.NewObject(() => new EntryDescription(description, args));
        }

        public Results<EntryClientAddress> NewAddress(string address)
        {
            return FactoryUtils.NewObject(() => new EntryClientAddress(address));
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string address, EntryDescription description, TenantId tenant)
        {
            EntryId entryId = this.NewId(Guid.NewGuid().ToString()).Result;
            EntryDate date = this.NewDate(DateTime.Now).Result;
            EntryClientAddress add = this.NewAddress(address).Result;
            return FactoryUtils.NewObject(() => new HistoryEntry(entryId, description, date, type, add, tenant));
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string address, TenantId tenant)
        {
            return this.NewEntry(type, address, this.NewDescription(type?.DefaultDescription).Result, tenant);
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string address, EntryDescription description, AccountId account)
        {
            EntryId entryId = this.NewId(Guid.NewGuid().ToString()).Result;
            EntryDate date = this.NewDate(DateTime.Now).Result;
            EntryClientAddress add = this.NewAddress(address).Result;
            return FactoryUtils.NewObject(() => new HistoryEntry(entryId, description, date, type, add, account));
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string address, AccountId account)
        {
            return this.NewEntry(type, address, this.NewDescription(type?.DefaultDescription).Result, account);
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string address, TenantId tenant, params object[] args)
        {
            return this.NewEntry(type, address, this.NewDescription(type?.DefaultDescription, args).Result, tenant);
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string address, AccountId account, params object[] args)
        {
            return this.NewEntry(type, address, this.NewDescription(type?.DefaultDescription, args).Result, account);
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string address, string description, TenantId tenant)
        {
            return this.NewEntry(type, address, this.NewDescription(description).Result, tenant);
        }

        public Results<HistoryEntry> NewEntry(EntryType type, string address, string description, AccountId account)
        {
            return this.NewEntry(type, address, this.NewDescription(description).Result, account);
        }
    }

}
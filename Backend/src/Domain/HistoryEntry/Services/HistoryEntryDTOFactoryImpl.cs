

using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class HistoryEntryDTOFactoryImpl : IHistoryEntryDTOFactory
    {
        public EntryDTO CreateBasicDTO(HistoryEntry entry)
        {
            EntryDTO dto = new EntryDTO();
            if (entry != null)
            {
                dto.id = entry.BusinessId.Value;
                dto.description = entry.Description.Value();
                dto.date = entry.Date.Value();
                dto.tenant = entry.Tenant?.Value;
                dto.address = entry.Address.Value();
                dto.account = entry.Account?.Value;
                dto.type = this.CreateTypeDTO(entry.Type);
            } else {
                dto.id = "";
                dto.description = "";
                dto.date = "";
                dto.type = this.CreateTypeDTO(null);
                dto.address = "";
                dto.tenant = "";
                dto.account = "";
            }
            return dto;
        }

        public EntryDTO[] CreateListOfBasicDTO(HistoryEntry[] entries)
        {
            return FactoryUtils.NewArrayDTO(entries, this.CreateBasicDTO);
        }

        public EntryTypeDTO CreateTypeDTO(EntryType type)
        {
            EntryTypeDTO dto = new EntryTypeDTO();
            dto.key = type != null? type.Name : "";
            dto.description = type != null? type.Name : "";
            return dto;
        }

        public EntryTypeDTO[] CreateListOfTypeDTO(EntryType[] types)
        {
            return FactoryUtils.NewArrayDTO(types, this.CreateTypeDTO);
        }

        public EntryQueryParams CreateQueryParamsOf(ConsultEntriesParameters request)
        {
            EntryQueryParams queryParams = new EntryQueryParams();
            queryParams.page = request.page;
            queryParams.size = request.size;
            queryParams.search = request.search;
            queryParams.filters = request.filters;
            queryParams.sort = request.sort;
            queryParams.order = request.order;
            queryParams.condition = request.condition;
            queryParams.tenant = request.tenant;
            queryParams.account = request.account;
            return queryParams;
        }
    }
}
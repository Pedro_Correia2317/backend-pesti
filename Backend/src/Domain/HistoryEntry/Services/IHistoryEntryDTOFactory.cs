

using Backend.Utils;

namespace Backend.HistoryEntries
{
    public interface IHistoryEntryDTOFactory
    {
        
        EntryDTO CreateBasicDTO(HistoryEntry entry);

        EntryDTO[] CreateListOfBasicDTO(HistoryEntry[] entries);
        
        EntryTypeDTO CreateTypeDTO(EntryType type);

        EntryTypeDTO[] CreateListOfTypeDTO(EntryType[] types);

        EntryQueryParams CreateQueryParamsOf(ConsultEntriesParameters request);
    }
}
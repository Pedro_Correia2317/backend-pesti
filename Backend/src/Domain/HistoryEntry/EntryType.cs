

using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class EntryType : IValueObject
    {
        public string Name { get; private set; }
        public string DefaultDescription { get; private set; }
        public string Destination { get; private set; }

        protected EntryType() {}

        public EntryType(string name, string desc, string dest)
        {
            this.Name = name;
            this.DefaultDescription = desc;
            this.Destination = dest;
        }

        public string GetName()
        {
            return this.Name;
        }

        public bool IsDestinedForAccounts()
        {
            return this.Destination == "Account";
        }

        public bool IsDestinedForTenants()
        {
            return this.Destination == "Tenant";
        }

        public override bool Equals(object obj)
        {
            return obj is EntryType type &&
                   Name == type.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public override string ToString()
        {
            return $"EntryType: [{this.Name}, {this.DefaultDescription}, {this.Destination}]";
        }
    }

    public static class EntryTypes
    {
        public static readonly EntryType AccountCreation = new EntryType("AccountCreation",
            "A conta foi criada com sucesso", "Account");
        public static readonly EntryType AccountStatusChange = new EntryType("AccountStatusChange",
            "O estado da conta foi alterado para {0}", "Account");
        public static readonly EntryType AccountDeletion = new EntryType("AccountDeletion",
            "A conta foi apagada", "Account");
        public static readonly EntryType AccountUpdate = new EntryType("AccountUpdate",
            "A conta foi atualizada com novos dados: {0}", "Account");
        public static readonly EntryType AccountBoughtLicense = new EntryType("AccountBoughtLicense",
            "A conta comprou uma licença individual", "Account");
        public static readonly EntryType AssociateLicenseToAccount = new EntryType("AssociateLicenseToAccount",
            "A conta ficou associada à licença '{0}'", "Account");
        public static readonly EntryType DisassociateLicenseToAccount = new EntryType("DisassociateLicenseToAccount",
            "A conta perdeu acesso à licença '{0}'", "Account");
        public static readonly EntryType TenantCreation = new EntryType("TenantCreation",
            "O tenant foi criado com sucesso", "Tenant");
        public static readonly EntryType TenantDeletion = new EntryType("TenantDeletion",
            "O tenant {0} foi apagado com sucesso", "Tenant");
        public static readonly EntryType TenantUpdate = new EntryType("TenantUpdate",
            "O tenant foi atualizado com novos dados: {0}", "Tenant");
        public static readonly EntryType TenantBoughtLicenses = new EntryType("TenantBoughtLicenses",
            "O tenant comprou {0} licenças", "Tenant");
        public static readonly EntryType TenantAddMember = new EntryType("TenantAddMember",
            "O tenant adicionou o membro {0}", "Tenant");
        public static readonly EntryType TenantRemoveMember = new EntryType("TenantRemoveMember",
            "O tenant removeu o membro {0}", "Tenant");

        private static Dictionary<string, EntryType> AllTypes = new Dictionary<string, EntryType>();

        static EntryTypes()
        {
            AllTypes.Add("AccountCreation", AccountCreation);
            AllTypes.Add("AccountStatusChange", AccountStatusChange);
            AllTypes.Add("AccountDeletion", AccountDeletion);
            AllTypes.Add("AccountUpdate", AccountUpdate);
            AllTypes.Add("AccountBoughtLicense", AccountBoughtLicense);
            AllTypes.Add("AssociateLicenseToAccount", AssociateLicenseToAccount);
            AllTypes.Add("DisassociateLicenseToAccount", DisassociateLicenseToAccount);
            AllTypes.Add("TenantCreation", TenantCreation);
            AllTypes.Add("TenantDeletion", TenantDeletion);
            AllTypes.Add("TenantUpdate", TenantUpdate);
            AllTypes.Add("TenantBoughtLicenses", TenantBoughtLicenses);
            AllTypes.Add("TenantAddMember", TenantAddMember);
            AllTypes.Add("TenantRemoveMember", TenantRemoveMember);
        }

        public static EntryType GetByName(string type)
        {
            if(type == null)
            {
                Error e = EntryErrors.NULL_TYPE.Copy();
                throw new BusinessRuleFailureException(e);
            }
            EntryType permissions = AllTypes.GetValueOrDefault(type);
            if(permissions == null)
            {
                Error e = EntryErrors.UNKNOWN_TYPE.Copy().WithParams(type);
                throw new BusinessRuleFailureException(e);
            }
            return permissions;
        }

        public static EntryType[] GetAll()
        {
            return AllTypes.Values.ToArray();
        }
    }
}
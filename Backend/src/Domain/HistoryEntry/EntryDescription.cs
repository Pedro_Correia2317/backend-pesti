

using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class EntryDescription : IValueObject
    {
        private static readonly string PATTERN = "[\\p{L}\\d\\s]+";

        public string Description { get; private set; }
        
        protected EntryDescription() {}

        public EntryDescription(string name)
        {
            Error err = applyDescription(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        public EntryDescription(string name, object[] args)
        {
            Error err = applyDescription(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
            applyParameters(args);
        }

        private Error applyDescription(string name)
        {
            if(name == null)
                return EntryErrors.NULL_DESCRIPTION.Copy();
            string trimmedDescription = name.Trim();
            if(trimmedDescription.Length == 0)
                return EntryErrors.EMPTY_DESCRIPTION.Copy();
            if(trimmedDescription.Length < 2)
                return EntryErrors.DESCRIPTION_TOO_SMALL.Copy().WithParams(trimmedDescription, 2);
            if(trimmedDescription.Length > 1000)
                return EntryErrors.DESCRIPTION_TOO_LARGE.Copy().WithParams(trimmedDescription, 1000);
            if(!Regex.IsMatch(trimmedDescription, PATTERN))
                return EntryErrors.INVALID_DESCRIPTION.Copy().WithParams(trimmedDescription, PATTERN);
            this.Description = trimmedDescription;
            return null;
        }

        private void applyParameters(object[] args)
        {
            if(args != null)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    this.Description = this.Description.Replace("{" + i + "}", args[i].ToString());
                }
            }
        }

        public string Value()
        {
            return this.Description;
        }

        public override string ToString()
        {
            return "EntryDescription: [" + this.Description + "]";
        }

        public override bool Equals(object obj)
        {
            return obj is EntryDescription description &&
                   Description == description.Description;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Description);
        }
    }
}
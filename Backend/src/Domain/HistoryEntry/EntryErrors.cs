

using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class EntryErrors
    {
        public static readonly string TYPE = "Entry";
        public static readonly Error NULL_ID = new Error("No entry id has been inserted", "NULL_ID", TYPE);
        public static readonly Error UNKNOWN_ID = new Error("No entry has the id '{{0}}'", "UNKNOWN_ID", TYPE);
        public static readonly Error INVALID_ID = new Error("The entry id '{{0}}' is not a Guid", "INVALID_ID", TYPE);
        public static readonly Error NULL_DATE = new Error("No entry date has been inserted", "NULL_DATE", TYPE);
        public static readonly Error INVALID_DATE = new Error("The entry date '{{0}}' is not a valid date", "INVALID_DATE", TYPE);
        public static readonly Error NULL_DESCRIPTION = new Error("The entry description is null", "NULL_DESCRIPTION", TYPE);
        public static readonly Error EMPTY_DESCRIPTION = new Error("The inserted entry description is empty", "EMPTY_DESCRIPTION", TYPE);
        public static readonly Error DESCRIPTION_TOO_SMALL = new Error("The entry description '{{0}}' has less than {{1}} chars", "DESCRIPTION_TOO_SMALL", TYPE);
        public static readonly Error DESCRIPTION_TOO_LARGE = new Error("The entry description '{{0}}' has more than {{1}} chars", "DESCRIPTION_TOO_LARGE", TYPE);
        public static readonly Error INVALID_DESCRIPTION = new Error("The entry description '{{0}}' does not obey the pattern '{{1}}'", "INVALID_DESCRIPTION", TYPE);
        public static readonly Error NULL_TYPE = new Error("The entry type is null", "NULL_TYPE", TYPE);
        public static readonly Error UNKNOWN_TYPE = new Error("The entry type '{{0}}' does not correspond to an entry type", "UNKNOWN_TYPE", TYPE);
        public static readonly Error NULL_DATA_IN_ENTRY = new Error("There is null data passed on to the constructor", "NULL_DATA_IN_ENTRY", TYPE);
        public static readonly Error ACCOUNT_TYPE_MIXED_WITH_TENANTS = new Error("The type '{{0}}' is for use with accounts, but is being used with tenants", "ACCOUNT_TYPE_MIXED_WITH_TENANTS", TYPE);
        public static readonly Error TENANT_TYPE_MIXED_WITH_ACCOUNTS = new Error("The type '{{0}}' is for use with tenants, but is being used with accounts", "TENANT_TYPE_MIXED_WITH_ACCOUNTS", TYPE);
        public static readonly Error NULL_INTERNET_ADDRESS = new Error("The inserted internet address is null", "NULL_INTERNET_ADDRESS", TYPE);
        public static readonly Error INVALID_INTERNET_ADDRESS = new Error("The internet address '{{0}}' is not valid", "INVALID_INTERNET_ADDRESS", TYPE);
    }
}
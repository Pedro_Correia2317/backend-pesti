
using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.Meetings
{
    public class TaskName : IValueObject
    {
        private static readonly string PATTERN = "[\\p{L}\\d\\s]+";

        public string Name { get; private set; }
        
        protected TaskName() {}

        public TaskName(string name)
        {
            Error err = applyName(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyName(string name)
        {
            if(name == null)
                return MeetingErrors.NULL_TASK_NAME.Copy();
            string trimmedName = name.Trim();
            if(trimmedName.Length == 0)
                return MeetingErrors.EMPTY_TASK_NAME.Copy();
            if(trimmedName.Length < 2)
                return MeetingErrors.TASK_NAME_TOO_SMALL.Copy().WithParams(trimmedName, 2);
            if(trimmedName.Length > 200)
                return MeetingErrors.TASK_NAME_TOO_LARGE.Copy().WithParams(trimmedName, 200);
            if(!Regex.IsMatch(trimmedName, PATTERN))
                return MeetingErrors.INVALID_TASK_NAME.Copy().WithParams(trimmedName, PATTERN);
            this.Name = trimmedName;
            return null;
        }

        public string Value()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            return obj is TaskName id &&
                   this.Name == id.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.Name);
        }

        public override string ToString()
        {
            return "TaskName: [" + this.Name + "]";
        }

    }
}
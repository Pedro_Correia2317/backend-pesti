


using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingParticipantList : IValueObject
    {

        public List<MeetingParticipant> ParticipantList { get; private set; }

        protected MeetingParticipantList() {}

        public MeetingParticipantList(MeetingParticipant participant)
        {
            this.ParticipantList = new List<MeetingParticipant>();
            this.AddParticipantToList(participant);
        }

        private void AddParticipantToList(MeetingParticipant participant)
        {
            if(participant == null)
            {
                throw new BusinessRuleFailureException(MeetingErrors.NULL_PARTICIPANT.Copy());
            }
            this.ParticipantList.Add(participant);
        }

        public Results<bool> AddNewParticipant(MeetingParticipant newParticipant)
        {
            if(newParticipant == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_PARTICIPANT.Copy());
            }
            if(this.ParticipantList.Count > 200)
            {
                return Results<bool>.Of(MeetingErrors.MEETING_HAS_MAX_NUMBER_OF_PARTICIPANTS.Copy().WithParams(200));
            }
            if(this.ParticipantList.Contains(newParticipant))
            {
                return Results<bool>.Of(MeetingErrors.PARTICIPANT_ALREADY_EXISTS.Copy().WithParams(newParticipant.User.Value));
            }
            this.ParticipantList.Add(newParticipant);
            return Results<bool>.Of(true);
        }

        public Results<bool> RemoveParticipant(AccountId participant)
        {
            if(participant == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_PARTICIPANT.Copy());
            }
            MeetingParticipant participantToRemove = this.ParticipantList.Find((x) => x.User.Equals(participant));
            if(participantToRemove == null)
            {
                return Results<bool>.Of(MeetingErrors.PARTICIPANT_NOT_AVAILABLE.Copy().WithParams(participant.Value));
            }
            if(!this.ParticipantList.Remove(participantToRemove))
            {
                return Results<bool>.Of(MeetingErrors.COULD_NOT_REMOVE_PARTICIPANT.Copy().WithParams(participantToRemove.User.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasParticipant(AccountId participant)
        {
            if(participant == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_PARTICIPANT.Copy());
            }
            MeetingParticipant aux = this.ParticipantList.Find((x) => x.User.Equals(participant));
            if(aux == null)
            {
                return Results<bool>.Of(MeetingErrors.PARTICIPANT_NOT_AVAILABLE.Copy().WithParams(participant.Value));
            }
            return Results<bool>.Of(true);
        }

        public ICollection<MeetingParticipant> ToCollection()
        {
            ICollection<MeetingParticipant> collection = new List<MeetingParticipant>();
            foreach (var participant in ParticipantList)
            {
                collection.Add(participant);
            }
            return collection;
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingParticipantList participants &&
                ParticipantList.All(participants.ParticipantList.Contains);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ParticipantList);
        }

        public override string ToString()
        {
            return $"MeetingParticipantList [{this.ParticipantList}]";
        }
    }
}
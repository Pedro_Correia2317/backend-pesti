

using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingAttachmentList : IValueObject
    {

        public List<MeetingAttachment> AttachmentList { get; private set; }

        protected MeetingAttachmentList() {}

        public MeetingAttachmentList(MeetingAttachment attachment)
        {
            this.AttachmentList = new List<MeetingAttachment>();
            this.AddAttachmentToList(attachment);
        }

        private void AddAttachmentToList(MeetingAttachment attachment)
        {
            if(attachment == null)
            {
                throw new BusinessRuleFailureException(MeetingErrors.NULL_ATTACHMENT.Copy());
            }
            this.AttachmentList.Add(attachment);
        }

        public Results<bool> AddNewAttachment(MeetingAttachment newAttachment)
        {
            if(newAttachment == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_ATTACHMENT.Copy());
            }
            if(this.AttachmentList.Count > 100)
            {
                return Results<bool>.Of(MeetingErrors.MEETING_HAS_MAX_NUMBER_OF_ATTACHMENTS.Copy().WithParams(100));
            }
            if(this.AttachmentList.Contains(newAttachment))
            {
                return Results<bool>.Of(MeetingErrors.ATTACHMENT_ALREADY_UPLOADED.Copy().WithParams(newAttachment.Name));
            }
            this.AttachmentList.Add(newAttachment);
            return Results<bool>.Of(true);
        }

        public Results<bool> RemoveAttachment(string attachmentName)
        {
            if(attachmentName == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_ATTACHMENT.Copy());
            }
            MeetingAttachment attachmentToRemove = this.AttachmentList.Find((x) => x.Name.Equals(attachmentName));
            if(!this.AttachmentList.Contains(attachmentToRemove))
            {
                return Results<bool>.Of(MeetingErrors.ATTACHMENT_NOT_AVAILABLE.Copy().WithParams(attachmentName));
            }
            if(!this.AttachmentList.Remove(attachmentToRemove))
            {
                return Results<bool>.Of(MeetingErrors.COULD_NOT_REMOVE_ATTACHMENT.Copy().WithParams(attachmentToRemove.Name));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasLessAttachmentsThan(int numAttachments)
        {
            if (this.AttachmentList.Count > numAttachments)
            {
                return Results<bool>.Of(MeetingErrors.MEETING_HAS_MAX_NUMBER_OF_ATTACHMENTS.Copy().WithParams(numAttachments));
            }
            return Results<bool>.Of(true);
        }

        public ICollection<MeetingAttachment> ToCollection()
        {
            ICollection<MeetingAttachment> collection = new List<MeetingAttachment>();
            foreach (var attachment in this.AttachmentList)
            {
                collection.Add(attachment);
            }
            return collection;
        }

        public Results<MeetingAttachment> GetAttachmentWithName(string fileName)
        {
            if(fileName == null)
            {
                return Results<MeetingAttachment>.Of(MeetingErrors.NULL_ATTACHMENT.Copy());
            }
            MeetingAttachment attachment = this.AttachmentList.Find((x) => x.Name.Equals(fileName));
            if(attachment == null)
            {
                return Results<MeetingAttachment>.Of(MeetingErrors.ATTACHMENT_NOT_AVAILABLE.Copy().WithParams(fileName));
            }
            return Results<MeetingAttachment>.Of(attachment);
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingAttachmentList accounts &&
                AttachmentList.All(accounts.AttachmentList.Contains);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(AttachmentList);
        }

        public override string ToString()
        {
            return $"MeetingAttachmentList [{this.AttachmentList}]";
        }
    }
}
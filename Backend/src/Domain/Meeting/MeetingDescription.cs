

using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingDescription : IValueObject
    {
        private static readonly string PATTERN = "[\\p{L}\\d\\s]+";

        public string Description { get; private set; }
        
        protected MeetingDescription() {}

        public MeetingDescription(string name)
        {
            Error err = applyDescription(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyDescription(string name)
        {
            if(name == null)
                return MeetingErrors.NULL_DESCRIPTION.Copy();
            string trimmedDescription = name.Trim();
            if(trimmedDescription.Length == 0)
                return MeetingErrors.EMPTY_DESCRIPTION.Copy();
            if(trimmedDescription.Length < 2)
                return MeetingErrors.DESCRIPTION_TOO_SMALL.Copy().WithParams(trimmedDescription, 2);
            if(trimmedDescription.Length > 1000)
                return MeetingErrors.DESCRIPTION_TOO_LARGE.Copy().WithParams(trimmedDescription, 1000);
            if(!Regex.IsMatch(trimmedDescription, PATTERN))
                return MeetingErrors.INVALID_DESCRIPTION.Copy().WithParams(trimmedDescription, PATTERN);
            this.Description = trimmedDescription;
            return null;
        }

        public string Value()
        {
            return this.Description;
        }

        public override string ToString()
        {
            return "MeetingDescription: [" + this.Description + "]";
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingDescription description &&
                   Description == description.Description;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Description);
        }
    }
}
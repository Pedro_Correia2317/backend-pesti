

using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Meetings
{
    public interface IMeetingDTOFactory
    {
        MeetingDTO CreateBasicDTO(Meeting meeting);
        MeetingParticipantDTO CreateParticipantDTO(MeetingParticipant participant);
        MeetingTaskDTO CreateTaskDTO(MeetingTask task);
        MeetingAttachmentDTO CreateAttachmentDTO(MeetingAttachment attachment);
        MeetingRoleDTO CreateRoleDTO(MeetingRole role);
        MeetingParticipantTypeDTO CreateParticipantTypeDTO(MeetingParticipantType participant);
        QueryParams CreateQueryParamsOf(ConsultMeetingsParameters request);
        MeetingDTO[] CreateListOfBasicDTO(Meeting[] meetings);
        MeetingDTO[] CreateListOfBasicDTO(ICollection<Meeting> meetings);
        MeetingParticipantDTO[] CreateListOfParticipantsDTO(ICollection<MeetingParticipant> participants);
        MeetingTaskDTO[] CreateListOfTasksDTO(ICollection<MeetingTask> tasks);
        MeetingAttachmentDTO[] CreateListOfAttachmentsDTO(ICollection<MeetingAttachment> attachments);
        MeetingRoleDTO[] CreateListOfRolesDTO(MeetingRole[] roles);
        MeetingParticipantTypeDTO[] CreateListOfParticipantTypesDTO(MeetingParticipantType[] types);

    }
}
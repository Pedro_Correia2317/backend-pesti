

using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingDTOFactoryImpl : IMeetingDTOFactory
    {

        public MeetingDTO CreateBasicDTO(Meeting meeting)
        {
            MeetingDTO dto = new MeetingDTO();
            if (meeting == null)
            {
                dto.id = "";
                dto.startDate = "";
                dto.endDate = "";
                dto.description = "";
                dto.recordingLink = "";
                dto.transcriptLink = "";
                dto.dependencies = "";
                dto.nextSteps = "";
                dto.decisions = "";
                dto.project = "";
                dto.nextMeeting = "";
                dto.tenant = "";
            } else {
                dto.id = meeting.BusinessId.Value;
                dto.startDate = meeting.StartDate.Value();
                dto.endDate = meeting.EndDate.Value();
                dto.description = meeting.Description.Value();
                dto.recordingLink = meeting.RecordingLink?.Value();
                dto.transcriptLink = meeting.TranscriptLink?.Value();
                dto.dependencies = meeting.Dependencies?.Value();
                dto.nextSteps = meeting.NextSteps?.Value();
                dto.decisions = meeting.Decisions?.Value();
                dto.project = meeting.Project?.Value;
                dto.nextMeeting = meeting.NextMeeting?.Value;
                dto.tenant = meeting.Tenant?.Value;
            }
            return dto;
        }

        public MeetingAttachmentDTO CreateAttachmentDTO(MeetingAttachment attachment)
        {
            MeetingAttachmentDTO dto = new MeetingAttachmentDTO();
            if (attachment == null)
            {
                dto.name = "";
            } else {
                dto.name = attachment.Value();
            }
            return dto;
        }

        public MeetingParticipantDTO CreateParticipantDTO(MeetingParticipant participant)
        {
            MeetingParticipantDTO dto = new MeetingParticipantDTO();
            if (participant == null)
            {
                dto.account = "";
                dto.meetingRole = this.CreateRoleDTO(null);
                dto.participantType = this.CreateParticipantTypeDTO(null);
            } else {
                dto.account = participant.User.Value;
                dto.meetingRole = this.CreateRoleDTO(participant.Role);
                dto.participantType = this.CreateParticipantTypeDTO(participant.Type);
            }
            return dto;
        }

        public MeetingRoleDTO CreateRoleDTO(MeetingRole role)
        {
            MeetingRoleDTO dto = new MeetingRoleDTO();
            if (role == null)
            {
                dto.key = "";
                dto.description = "";
            } else {
                dto.key = role.GetName();
                dto.description = role.GetName();
            }
            return dto;
        }

        public MeetingParticipantTypeDTO CreateParticipantTypeDTO(MeetingParticipantType type)
        {
            MeetingParticipantTypeDTO dto = new MeetingParticipantTypeDTO();
            if (type == null)
            {
                dto.key = "";
                dto.description = "";
            } else {
                dto.key = type.GetName();
                dto.description = type.GetName();
            }
            return dto;
        }

        public MeetingTaskDTO CreateTaskDTO(MeetingTask task)
        {
            MeetingTaskDTO dto = new MeetingTaskDTO();
            if (task == null)
            {
                dto.name = "";
                dto.startDate = "";
                dto.endDate = "";
                dto.expectedEndDate = "";
                dto.description = "";
                dto.responsible = "";
            } else {
                dto.name = task.Name.Value();
                dto.startDate = task.StartDate.Value();
                dto.endDate = task.EndDate.Value();
                dto.expectedEndDate = task.ExpectedEndDate.Value();
                dto.description = task.Description.Value();
                dto.responsible = task.Responsible.Value;
            }
            return dto;
        }

        public MeetingDTO[] CreateListOfBasicDTO(Meeting[] meetings)
        {
            return FactoryUtils.NewArrayDTO(meetings, this.CreateBasicDTO);
        }

        public MeetingDTO[] CreateListOfBasicDTO(ICollection<Meeting> meetings)
        {
            return FactoryUtils.NewArrayDTO(meetings, this.CreateBasicDTO);
        }

        public MeetingParticipantDTO[] CreateListOfParticipantsDTO(ICollection<MeetingParticipant> participants)
        {
            return FactoryUtils.NewArrayDTO(participants, this.CreateParticipantDTO);
        }

        public MeetingTaskDTO[] CreateListOfTasksDTO(ICollection<MeetingTask> tasks)
        {
            return FactoryUtils.NewArrayDTO(tasks, this.CreateTaskDTO);
        }

        public MeetingAttachmentDTO[] CreateListOfAttachmentsDTO(ICollection<MeetingAttachment> attachments)
        {
            return FactoryUtils.NewArrayDTO(attachments, this.CreateAttachmentDTO);
        }

        public MeetingRoleDTO[] CreateListOfRolesDTO(MeetingRole[] roles)
        {
            return FactoryUtils.NewArrayDTO(roles, this.CreateRoleDTO);
        }

        public MeetingParticipantTypeDTO[] CreateListOfParticipantTypesDTO(MeetingParticipantType[] types)
        {
            return FactoryUtils.NewArrayDTO(types, this.CreateParticipantTypeDTO);
        }

        public QueryParams CreateQueryParamsOf(ConsultMeetingsParameters request)
        {
            QueryParams queryParams = new QueryParams();
            queryParams.page = request.page;
            queryParams.size = request.size;
            queryParams.filters = request.filters;
            queryParams.sort = request.sort;
            queryParams.order = request.order;
            queryParams.condition = request.condition;
            queryParams.search = request.search;
            return queryParams;
        }
    }
}
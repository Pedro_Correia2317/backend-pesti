

using System.Threading.Tasks;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public interface IMeetingRepository : IRepository<Meeting, MeetingId>
    {
        Task<PageQueryResults<Meeting>> FindByPageWithTenant(QueryParams Params, Tenant tenant);
        
    }
}
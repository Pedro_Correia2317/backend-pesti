

using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public interface IMeetingFactory
    {

        MeetingContainer NewContainer();

        Results<MeetingId> NewId(string id);

        Results<MeetingDescription> NewDescription(string description);

        Results<MeetingStartDate> NewStartDate(string startDate);

        Results<MeetingEndDate> NewEndDate(string endDate);

        Results<MeetingRecordingLink> NewRecordingLink(string recordingLink);

        Results<MeetingTranscriptLink> NewTranscriptLink(string transcriptLink);

        Results<MeetingDependencies> NewDependencies(string dependencies);

        Results<MeetingNextSteps> NewNextSteps(string nextSteps);

        Results<MeetingDecisions> NewDecisions(string decisions);

        Results<MeetingRole> NewRole(string role);

        Results<MeetingParticipantType> NewParticipantType(string type);

        Results<MeetingParticipant> NewParticipant(AccountId user, MeetingParticipantType type, MeetingRole role);

        Results<MeetingAttachment> NewAttachment(byte[] file, string name);

        Results<TaskName> NewTaskName(string taskName);

        Results<TaskDescription> NewTaskDescription(string taskDescription);

        Results<TaskStartDate> NewTaskStartDate(string taskStartDate);

        Results<TaskEndDate> NewTaskEndDate(string taskEndDate);

        Results<TaskExpectedEndDate> NewTaskExpectedEndDate(string taskExpectedEndDate);

        Results<MeetingTask> NewTask(TaskName name, TaskDescription description, TaskStartDate startDate,
                TaskEndDate endDate, TaskExpectedEndDate expectedEndDate, AccountId account);

        Results<Meeting> NewMeeting(MeetingContainer container);
        
    }
}


using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingFactoryImpl : IMeetingFactory
    {
        public MeetingContainer NewContainer()
        {
            return new MeetingContainer();
        }

        public Results<MeetingId> NewId(string id)
        {
            return FactoryUtils.NewObject(() => new MeetingId(id));
        }

        public Results<MeetingDescription> NewDescription(string description)
        {
            return FactoryUtils.NewObject(() => new MeetingDescription(description));
        }

        public Results<MeetingStartDate> NewStartDate(string startDate)
        {
            return FactoryUtils.NewObject(() => new MeetingStartDate(startDate));
        }

        public Results<MeetingEndDate> NewEndDate(string endDate)
        {
            return FactoryUtils.NewObject(() => new MeetingEndDate(endDate));
        }

        public Results<Meeting> NewMeeting(MeetingContainer c)
        {
            return FactoryUtils.NewObject(() => new Meeting(c.Id, c.StartDate, c.EndDate, c.Description, c.Tenant, c.Creator));
        }

        public Results<MeetingRecordingLink> NewRecordingLink(string recordingLink)
        {
            return FactoryUtils.NewObject(() => new MeetingRecordingLink(recordingLink));
        }

        public Results<MeetingTranscriptLink> NewTranscriptLink(string transcriptLink)
        {
            return FactoryUtils.NewObject(() => new MeetingTranscriptLink(transcriptLink));
        }

        public Results<MeetingDependencies> NewDependencies(string dependencies)
        {
            return FactoryUtils.NewObject(() => new MeetingDependencies(dependencies));
        }

        public Results<MeetingNextSteps> NewNextSteps(string nextSteps)
        {
            return FactoryUtils.NewObject(() => new MeetingNextSteps(nextSteps));
        }

        public Results<MeetingDecisions> NewDecisions(string decisions)
        {
            return FactoryUtils.NewObject(() => new MeetingDecisions(decisions));
        }

        public Results<MeetingRole> NewRole(string role)
        {
            return FactoryUtils.NewObject(() => new MeetingRole(role));
        }

        public Results<MeetingParticipantType> NewParticipantType(string type)
        {
            return FactoryUtils.NewObject(() => new MeetingParticipantType(type));
        }

        public Results<MeetingParticipant> NewParticipant(AccountId user, MeetingParticipantType type, MeetingRole role)
        {
            return FactoryUtils.NewObject(() => new MeetingParticipant(user, type, role));
        }

        public Results<MeetingAttachment> NewAttachment(byte[] file, string name)
        {
            return FactoryUtils.NewObject(() => new MeetingAttachment(file, name));
        }

        public Results<TaskName> NewTaskName(string taskName)
        {
            return FactoryUtils.NewObject(() => new TaskName(taskName));
        }

        public Results<TaskDescription> NewTaskDescription(string taskDescription)
        {
            return FactoryUtils.NewObject(() => new TaskDescription(taskDescription));
        }

        public Results<TaskStartDate> NewTaskStartDate(string taskStartDate)
        {
            return FactoryUtils.NewObject(() => new TaskStartDate(taskStartDate));
        }

        public Results<TaskEndDate> NewTaskEndDate(string taskEndDate)
        {
            return FactoryUtils.NewObject(() => new TaskEndDate(taskEndDate));
        }

        public Results<TaskExpectedEndDate> NewTaskExpectedEndDate(string taskExpectedEndDate)
        {
            return FactoryUtils.NewObject(() => new TaskExpectedEndDate(taskExpectedEndDate));
        }

        public Results<MeetingTask> NewTask(TaskName name, TaskDescription description, TaskStartDate startDate,
                TaskEndDate endDate, TaskExpectedEndDate expectedEndDate, AccountId account)
        {
            return FactoryUtils.NewObject(() => new MeetingTask(name, description, startDate, endDate, expectedEndDate, account));
        }
    }
}

using System;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingAttachment : IValueObject
    {

        public File File { get; private set; }

        public string Name { get; private set; }

        protected MeetingAttachment() {}

        public MeetingAttachment(byte[] bytes, string name)
        {
            Error err = applyFile(bytes, name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyFile(byte[] bytes, string name)
        {
            if(bytes == null)
                return MeetingErrors.NULL_FILE_BYTES.Copy();
            if(name == null)
                return MeetingErrors.NULL_FILE_NAME.Copy();
            string trimmedName = name.Trim();
            if(trimmedName.Length == 0)
                return MeetingErrors.EMPTY_FILE_NAME.Copy();
            if(trimmedName.Length < 2)
                return MeetingErrors.FILE_NAME_TOO_SMALL.Copy().WithParams(trimmedName, 2);
            if(trimmedName.Length > 200)
                return MeetingErrors.FILE_NAME_TOO_LARGE.Copy().WithParams(trimmedName, 200);
            if(bytes.Length < 2)
                return MeetingErrors.FILE_SIZE_TOO_SMALL.Copy().WithParams(trimmedName, 2);
            if(bytes.Length > 5000)
                return MeetingErrors.FILE_SIZE_TOO_LARGE.Copy().WithParams(trimmedName, 5000);
            File outFile;
            if(!File.TryCreate(bytes, out outFile))
                return MeetingErrors.INVALID_FILE.Copy().WithParams(trimmedName);
            this.File = outFile;
            this.Name = trimmedName;
            return null;
        }

        public string Value()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingAttachment attachment
                && this.Name == attachment.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.File);
        }

        public override string ToString()
        {
            return $"MeetingAttachment: [{this.File}]";
        }
    }
}
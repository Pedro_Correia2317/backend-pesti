

using System;
using Backend.Accounts;
using Backend.Projects;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class Meeting : Entity<MeetingId>, IAggregateRoot
    {

        public MeetingStartDate StartDate { get; private set; }

        public MeetingDescription Description { get; private set; }

        public MeetingEndDate EndDate { get; private set; }

        public MeetingRecordingLink RecordingLink { get; private set; }

        public MeetingTranscriptLink TranscriptLink { get; private set; }

        public MeetingDependencies Dependencies { get; private set; }

        public MeetingNextSteps NextSteps { get; private set; }

        public MeetingDecisions Decisions { get; private set; }

        public MeetingParticipantList Participants { get; private set; }

        public MeetingAttachmentList Attachments { get; private set; }

        public MeetingTaskList Tasks { get; private set; }

        public ProjectId Project { get; private set; }

        public MeetingId NextMeeting { get; private set; }

        public TenantId Tenant { get; private set; }

        public AccountId Creator { get; private set; }

        protected Meeting() {}

        public Meeting(MeetingId id, MeetingStartDate startDate, MeetingEndDate endDate,
                MeetingDescription desc, TenantId tenant, AccountId creator)
        {
            if (DomainUtils.HasNullFields(id, startDate, endDate, desc, tenant, creator))
            {
                throw new BusinessRuleFailureException(MeetingErrors.NULL_DATA_IN_MEETING.Copy());
            }
            this.BusinessId = id;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Description = desc;
            this.Tenant = tenant;
            this.Creator = creator;
        }

        public Results<bool> AddParticipant(MeetingParticipant participant)
        {
            return this.Participants.AddNewParticipant(participant);
        }

        public Results<bool> RemoveParticipant(AccountId participant)
        {
            return this.Participants.RemoveParticipant(participant);
        }

        public Results<bool> AddAttachment(MeetingAttachment attachment)
        {
            return this.Attachments.AddNewAttachment(attachment);
        }

        public Results<bool> RemoveAttachment(string attachmentName)
        {
            return this.Attachments.RemoveAttachment(attachmentName);
        }

        public Results<bool> AddTask(MeetingTask task)
        {
            return this.Tasks.AddNewTask(task);
        }

        public Results<bool> RemoveTask(TaskName task)
        {
            return this.Tasks.RemoveTask(task);
        }

        public Results<bool> AddNextMeeting(MeetingId meeting)
        {
            if(meeting == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_MEETING.Copy());
            }
            this.NextMeeting = meeting;
            return Results<bool>.Of(true);
        }

        public Results<bool> AddProject(ProjectId project)
        {
            if(project == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_PROJECT.Copy());
            }
            this.Project = project;
            return Results<bool>.Of(true);
        }

        public Results<bool> RemoveProject()
        {
            this.Project = null;
            return Results<bool>.Of(true);
        }

        public Results<bool> RemoveNextMeeting()
        {
            this.NextMeeting = null;
            return Results<bool>.Of(true);
        }

        public Results<bool> AddDependencies(MeetingDependencies dependencies)
        {
            if(dependencies == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_DEPENDENCIES.Copy());
            }
            this.Dependencies = dependencies;
            return Results<bool>.Of(true);
        }

        public Results<bool> AddDecisions(MeetingDecisions decisions)
        {
            if(decisions == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_DECISIONS.Copy());
            }
            this.Decisions = decisions;
            return Results<bool>.Of(true);
        }

        public Results<bool> AddNextSteps(MeetingNextSteps nextSteps)
        {
            if(nextSteps == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_NEXT_STEPS.Copy());
            }
            this.NextSteps = nextSteps;
            return Results<bool>.Of(true);
        }

        public Results<bool> AddRecordingLink(MeetingRecordingLink recordingLink)
        {
            if(recordingLink == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_MEETING_RECORDING_LINK.Copy());
            }
            this.RecordingLink = recordingLink;
            return Results<bool>.Of(true);
        }

        public Results<bool> HasTenant(TenantId tenant)
        {
            if (this.Tenant.Equals(tenant))
            {
                return Results<bool>.Of(true);
            }
            return Results<bool>.Of(MeetingErrors.WRONG_TENANT.Copy().WithParams(this.BusinessId.Value, tenant?.Value));
        }

        public Results<bool> AddTranscriptLink(MeetingTranscriptLink transcriptLink)
        {
            if(transcriptLink == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_MEETING_TRANSCRIPT_LINK.Copy());
            }
            this.TranscriptLink = transcriptLink;
            return Results<bool>.Of(true);
        }

        public Results<bool> HasLessAttachmentsThan(int numAttachments)
        {
            return this.Attachments.HasLessAttachmentsThan(numAttachments);
        }

        public Results<bool> HasParticipant(AccountId requesterId)
        {
            return this.Participants.HasParticipant(requesterId);
        }

        public Results<MeetingAttachment> GetAttachmentWithName(string fileName)
        {
            return this.Attachments.GetAttachmentWithName(fileName);
        }

        public override bool Equals(object obj)
        {
            return obj is Meeting meeting
                && this.BusinessId.Equals(meeting.BusinessId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.BusinessId);
        }

        public override string ToString()
        {
            return $"MeetingTask: [{this.BusinessId}, {this.Description}, {this.StartDate}, {this.EndDate}, " 
                    + $"{this.RecordingLink}, {this.TranscriptLink}, {this.Dependencies}, {this.NextSteps}, " 
                    + $"{this.Decisions}, {this.Participants}, {this.Attachments}, {this.Tasks}, {this.Project}, "
                    + $"{this.NextMeeting}, {this.Tenant}]";
        }
    }
}


using System;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingId : EntityId, IValueObject
    {
        
        protected MeetingId() : base() {}

        public MeetingId(string id)
        {
            Error err = applyId(id);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyId(string id)
        {
            if(id == null)
                return MeetingErrors.NULL_ID.Copy();
            string trimmedId = id.Trim();
            Guid outGuid;
            if(!Guid.TryParse(trimmedId, out outGuid))
                return MeetingErrors.INVALID_ID.Copy().WithParams(trimmedId);
            this.Value = trimmedId;
            return null;
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingId id &&
                   base.Equals(obj) &&
                   Value == id.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Value);
        }

        public override string ToString()
        {
            return "MeetingId: [" + this.Value + "]";
        }
    }
}
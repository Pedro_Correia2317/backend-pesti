

using System;
using Backend.Utils;

namespace Backend.Meetings
{
    public class TaskEndDate : IValueObject
    {

        public Date Date { get; private set; }
        
        protected TaskEndDate() {}

        public TaskEndDate(string date)
        {
            Error err = applyDate(date);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        public TaskEndDate(DateTime date)
        {
            this.Date = Date.Create(date);
        }

        private Error applyDate(string date)
        {
            if(date == null)
                return MeetingErrors.NULL_TASK_END_DATE.Copy();
            string trimmedDate = date.Trim();
            Date outDate;
            if(!Date.TryCreate(trimmedDate, out outDate))
                return MeetingErrors.INVALID_TASK_END_DATE.Copy().WithParams(trimmedDate);
            this.Date = outDate;
            return null;
        }

        public string Value()
        {
            return this.Date.GetValue();
        }

        public override bool Equals(object obj)
        {
            return obj is TaskEndDate id &&
                   base.Equals(obj) &&
                   this.Date == id.Date;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Date);
        }

        public override string ToString()
        {
            return "TaskEndDate: [" + this.Date + "]";
        }
    }
}
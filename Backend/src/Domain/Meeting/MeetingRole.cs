

using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingRole : IValueObject
    {
        public string Name { get; private set; }

        protected MeetingRole() {}

        public MeetingRole(string Name)
        {
            this.Name = Name;
        }

        public string GetName()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingRole type &&
                   Name == type.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public override string ToString()
        {
            return $"MeetingRole: [{this.Name}]";
        }
    }

    public static class MeetingRoles
    {
        public static readonly MeetingRole Organizer = new MeetingRole("Organizer");
        public static readonly MeetingRole Presenter = new MeetingRole("Presenter");
        public static readonly MeetingRole Participant = new MeetingRole("Participant");
        private static Dictionary<string, MeetingRole> AllRoles = new Dictionary<string, MeetingRole>();

        static MeetingRoles()
        {
            AllRoles.Add("Organizer", Organizer);
            AllRoles.Add("Presenter", Presenter);
            AllRoles.Add("Participant", Participant);
        }

        public static MeetingRole GetByName(string type)
        {
            if(type == null)
            {
                Error e = MeetingErrors.NULL_ROLE.Copy();
                throw new BusinessRuleFailureException(e);
            }
            MeetingRole permissions = AllRoles.GetValueOrDefault(type);
            if(permissions == null)
            {
                Error e = MeetingErrors.UNKNOWN_ROLE.Copy().WithParams(type);
                throw new BusinessRuleFailureException(e);
            }
            return permissions;
        }

        public static MeetingRole[] GetAll()
        {
            return AllRoles.Values.ToArray();
        }
    }
}
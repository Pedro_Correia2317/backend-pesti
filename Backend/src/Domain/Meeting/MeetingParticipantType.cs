


using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingParticipantType : IValueObject
    {
        public string Name { get; private set; }

        protected MeetingParticipantType() {}

        public MeetingParticipantType(string Name)
        {
            this.Name = Name;
        }

        public string GetName()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingParticipantType type &&
                   Name == type.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public override string ToString()
        {
            return $"MeetingParticipantType: [{this.Name}]";
        }
    }

    public static class MeetingParticipantTypes
    {
        public static readonly MeetingParticipantType InTenant = new MeetingParticipantType("InTenant");
        public static readonly MeetingParticipantType Guest = new MeetingParticipantType("Guest");
        public static readonly MeetingParticipantType Federate = new MeetingParticipantType("Federate");
        public static readonly MeetingParticipantType Anonymous = new MeetingParticipantType("Anonymous");
        private static Dictionary<string, MeetingParticipantType> AllParticipantTypes = new Dictionary<string, MeetingParticipantType>();

        static MeetingParticipantTypes()
        {
            AllParticipantTypes.Add("InTenant", InTenant);
            AllParticipantTypes.Add("Guest", Guest);
            AllParticipantTypes.Add("Federate", Federate);
            AllParticipantTypes.Add("Anonymous", Anonymous);
        }

        public static MeetingParticipantType GetByName(string type)
        {
            if(type == null)
            {
                Error e = MeetingErrors.NULL_PARTICIPANT_TYPE.Copy();
                throw new BusinessRuleFailureException(e);
            }
            MeetingParticipantType permissions = AllParticipantTypes.GetValueOrDefault(type);
            if(permissions == null)
            {
                Error e = MeetingErrors.UNKNOWN_PARTICIPANT_TYPE.Copy().WithParams(type);
                throw new BusinessRuleFailureException(e);
            }
            return permissions;
        }

        public static MeetingParticipantType[] GetAll()
        {
            return AllParticipantTypes.Values.ToArray();
        }
    }
}
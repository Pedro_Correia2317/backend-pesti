

using Backend.Accounts;
using Backend.Tenants;

namespace Backend.Meetings
{
    public class MeetingContainer
    {
        public MeetingId Id;

        public MeetingStartDate StartDate;

        public MeetingEndDate EndDate;

        public MeetingDescription Description;

        public TenantId Tenant;

        public AccountId Creator;
    }
}
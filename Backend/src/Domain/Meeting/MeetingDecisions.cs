

using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingDecisions : IValueObject
    {
        private static readonly string PATTERN = "[\\p{L}\\d\\s]+";

        public string Decisions { get; private set; }
        
        protected MeetingDecisions() {}

        public MeetingDecisions(string name)
        {
            Error err = applyDecisions(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyDecisions(string name)
        {
            if(name == null)
                return MeetingErrors.NULL_DECISIONS.Copy();
            string trimmedDecisions = name.Trim();
            if(trimmedDecisions.Length == 0)
                return MeetingErrors.EMPTY_DECISIONS.Copy();
            if(trimmedDecisions.Length < 2)
                return MeetingErrors.DECISIONS_TOO_SMALL.Copy().WithParams(trimmedDecisions, 2);
            if(trimmedDecisions.Length > 1000)
                return MeetingErrors.DECISIONS_TOO_LARGE.Copy().WithParams(trimmedDecisions, 1000);
            if(!Regex.IsMatch(trimmedDecisions, PATTERN))
                return MeetingErrors.INVALID_DECISIONS.Copy().WithParams(trimmedDecisions, PATTERN);
            this.Decisions = trimmedDecisions;
            return null;
        }

        public string Value()
        {
            return this.Decisions;
        }

        public override string ToString()
        {
            return "MeetingDecisions: [" + this.Decisions + "]";
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingDecisions decisions &&
                   Decisions == decisions.Decisions;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Decisions);
        }
    }
}
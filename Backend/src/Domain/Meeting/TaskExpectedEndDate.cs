

using System;
using Backend.Utils;

namespace Backend.Meetings
{
    public class TaskExpectedEndDate : IValueObject
    {

        public Date Date { get; private set; }
        
        protected TaskExpectedEndDate() {}

        public TaskExpectedEndDate(string date)
        {
            Error err = applyDate(date);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        public TaskExpectedEndDate(DateTime date)
        {
            this.Date = Date.Create(date);
        }

        private Error applyDate(string date)
        {
            if(date == null)
                return MeetingErrors.NULL_TASK_EXPECTED_END_DATE.Copy();
            string trimmedDate = date.Trim();
            Date outDate;
            if(!Date.TryCreate(trimmedDate, out outDate))
                return MeetingErrors.INVALID_TASK_EXPECTED_END_DATE.Copy().WithParams(trimmedDate);
            this.Date = outDate;
            return null;
        }

        public string Value()
        {
            return this.Date.GetValue();
        }

        public override bool Equals(object obj)
        {
            return obj is TaskExpectedEndDate id &&
                   base.Equals(obj) &&
                   this.Date == id.Date;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Date);
        }

        public override string ToString()
        {
            return "TaskExpectedEndDate: [" + this.Date + "]";
        }
    }
}


using System;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingStartDate : IValueObject
    {

        public Date Date { get; private set; }
        
        protected MeetingStartDate() {}

        public MeetingStartDate(string date)
        {
            Error err = applyDate(date);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        public MeetingStartDate(DateTime date)
        {
            this.Date = Date.Create(date);
        }

        private Error applyDate(string date)
        {
            if(date == null)
                return MeetingErrors.NULL_START_DATE.Copy();
            string trimmedDate = date.Trim();
            Date outDate;
            if(!Date.TryCreate(trimmedDate, out outDate))
                return MeetingErrors.INVALID_START_DATE.Copy().WithParams(trimmedDate);
            this.Date = outDate;
            return null;
        }

        public string Value()
        {
            return this.Date.GetValue();
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingStartDate id &&
                   base.Equals(obj) &&
                   this.Date == id.Date;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Date);
        }

        public override string ToString()
        {
            return $"MeetingStartDate: [{this.Date}]";
        }

    }
}



using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingTaskList : IValueObject
    {

        public List<MeetingTask> TaskList { get; private set; }

        protected MeetingTaskList() {}

        public MeetingTaskList(MeetingTask task)
        {
            this.TaskList = new List<MeetingTask>();
            this.AddTaskToList(task);
        }

        private void AddTaskToList(MeetingTask task)
        {
            if(task == null)
            {
                throw new BusinessRuleFailureException(MeetingErrors.NULL_TASK.Copy());
            }
            this.TaskList.Add(task);
        }

        public Results<bool> AddNewTask(MeetingTask newTask)
        {
            if(newTask == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_TASK.Copy());
            }
            if(this.TaskList.Count > 20)
            {
                return Results<bool>.Of(MeetingErrors.MEETING_HAS_MAX_NUMBER_OF_TASKS.Copy().WithParams(20));
            }
            if(this.TaskList.Contains(newTask))
            {
                return Results<bool>.Of(MeetingErrors.TASK_ALREADY_UPLOADED.Copy().WithParams(newTask.Name));
            }
            this.TaskList.Add(newTask);
            return Results<bool>.Of(true);
        }

        public Results<bool> RemoveTask(TaskName taskName)
        {
            if(taskName == null)
            {
                return Results<bool>.Of(MeetingErrors.NULL_TASK.Copy());
            }
            MeetingTask taskToRemove = this.TaskList.Find((x) => x.Name.Equals(taskName));
            if(taskToRemove == null)
            {
                return Results<bool>.Of(MeetingErrors.TASK_NOT_AVAILABLE.Copy().WithParams(taskName.Value()));
            }
            if(!this.TaskList.Remove(taskToRemove))
            {
                return Results<bool>.Of(MeetingErrors.COULD_NOT_REMOVE_TASK.Copy().WithParams(taskToRemove.Name));
            }
            return Results<bool>.Of(true);
        }

        public ICollection<MeetingTask> ToCollection()
        {
            ICollection<MeetingTask> collection = new List<MeetingTask>();
            foreach (var task in TaskList)
            {
                collection.Add(task);
            }
            return collection;
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingTaskList tasks &&
                TaskList.All(tasks.TaskList.Contains);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TaskList);
        }

        public override string ToString()
        {
            return $"MeetingTaskList [{this.TaskList}]";
        }
    }
}
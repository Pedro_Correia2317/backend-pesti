

using System;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingTranscriptLink : IValueObject
    {

        public Url Link { get; private set; }
        
        protected MeetingTranscriptLink() {}

        public MeetingTranscriptLink(string url)
        {
            Error err = applyLink(url);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyLink(string link)
        {
            if(link == null)
                return MeetingErrors.NULL_MEETING_TRANSCRIPT_LINK.Copy();
            string trimmedUrl = link.Trim();
            if(trimmedUrl.Length == 0)
                return MeetingErrors.EMPTY_MEETING_TRANSCRIPT_LINK.Copy();
            if(trimmedUrl.Length < 2)
                return MeetingErrors.MEETING_TRANSCRIPT_LINK_TOO_SMALL.Copy().WithParams(trimmedUrl, 2);
            if(trimmedUrl.Length > 2000)
                return MeetingErrors.MEETING_TRANSCRIPT_LINK_TOO_LARGE.Copy().WithParams(trimmedUrl, 2000);
            Url outUrl;
            if(!Url.TryCreate(trimmedUrl, out outUrl))
                return MeetingErrors.INVALID_MEETING_TRANSCRIPT_LINK.Copy().WithParams(trimmedUrl);
            this.Link = outUrl;
            return null;
        }

        public string Value()
        {
            return this.Link.GetValue();
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingTranscriptLink id &&
                   base.Equals(obj) &&
                   this.Link == id.Link;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), this.Link);
        }

        public override string ToString()
        {
            return $"MeetingTranscriptLink: [{this.Link}]";
        }
    }
}
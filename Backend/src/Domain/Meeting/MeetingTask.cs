

using System;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingTask : IValueObject
    {
        public TaskName Name { get; private set; }
        public TaskDescription Description { get; private set; }
        public TaskStartDate StartDate { get; private set; }
        public TaskEndDate EndDate { get; private set; }
        public TaskExpectedEndDate ExpectedEndDate { get; private set; }
        public AccountId Responsible { get; private set; }

        protected MeetingTask() {}

        public MeetingTask(TaskName name, TaskDescription description, TaskStartDate startDate,
            TaskEndDate endDate, TaskExpectedEndDate expectedEndDate, AccountId responsible)
        {
            this.Name = name;
            this.Description = description;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.ExpectedEndDate = expectedEndDate;
            this.Responsible = responsible;
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingTask task
                && this.Name.Equals(task.Name);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.Name);
        }

        public override string ToString()
        {
            return $"MeetingTask: [{this.Name}, {this.Description}, {this.StartDate}, {this.EndDate}, {this.ExpectedEndDate}, {this.Responsible}]";
        }
    }
}
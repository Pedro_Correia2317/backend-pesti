

using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingDependencies : IValueObject
    {
        private static readonly string PATTERN = "[\\p{L}\\d\\s]+";

        public string Dependencies { get; private set; }
        
        protected MeetingDependencies() {}

        public MeetingDependencies(string name)
        {
            Error err = applyDependencies(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyDependencies(string name)
        {
            if(name == null)
                return MeetingErrors.NULL_DEPENDENCIES.Copy();
            string trimmedDependencies = name.Trim();
            if(trimmedDependencies.Length == 0)
                return MeetingErrors.EMPTY_DEPENDENCIES.Copy();
            if(trimmedDependencies.Length < 2)
                return MeetingErrors.DEPENDENCIES_TOO_SMALL.Copy().WithParams(trimmedDependencies, 2);
            if(trimmedDependencies.Length > 1000)
                return MeetingErrors.DEPENDENCIES_TOO_LARGE.Copy().WithParams(trimmedDependencies, 1000);
            if(!Regex.IsMatch(trimmedDependencies, PATTERN))
                return MeetingErrors.INVALID_DEPENDENCIES.Copy().WithParams(trimmedDependencies, PATTERN);
            this.Dependencies = trimmedDependencies;
            return null;
        }

        public string Value()
        {
            return this.Dependencies;
        }

        public override string ToString()
        {
            return "MeetingDependencies: [" + this.Dependencies + "]";
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingDependencies dependencies &&
                   Dependencies == dependencies.Dependencies;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Dependencies);
        }
    }
}
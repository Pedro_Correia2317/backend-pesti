

using System;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingParticipant : IValueObject
    {
        public AccountId User { get; private set; }

        public MeetingParticipantType Type { get; private set; }

        public MeetingRole Role { get; private set; }

        protected MeetingParticipant() {}

        public MeetingParticipant(AccountId user, MeetingParticipantType type, MeetingRole role)
        {
            Error err = ApplyParticipant(user, type, role);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error ApplyParticipant(AccountId user, MeetingParticipantType type, MeetingRole role)
        {
            if(user == null)
                return MeetingErrors.NULL_USER.Copy();
            if(type == null)
                return MeetingErrors.NULL_PARTICIPANT_TYPE.Copy();
            if(role == null)
                return MeetingErrors.NULL_ROLE.Copy();
            this.User = user;
            this.Type = type;
            this.Role = role;
            return null;
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingParticipant participant
                && this.User == participant.User;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.User);
        }

        public override string ToString()
        {
            return $"MeetingParticipant: [{this.User}, {this.Type}, {this.Role}]";
        }
        
    }
}


using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingErrors
    {
        public static readonly string TYPE = "Meeting";
        public static readonly Error NULL_DATA_IN_MEETING = new Error("There is null data in the meeting", "NULL_DATA_IN_MEETING", TYPE);
        public static readonly Error UNKNOWN_ID = new Error("No meeting with the id '{{0}}' exists", "UNKNOWN_ID", TYPE);
        public static readonly Error NULL_ID = new Error("No meeting id has been inserted", "NULL_ID", TYPE);
        public static readonly Error INVALID_ID = new Error("The id '{{0}}' is not a Guid", "INVALID_ID", TYPE);
        public static readonly Error NULL_START_DATE = new Error("No starting date has been inserted", "NULL_START_DATE", TYPE);
        public static readonly Error INVALID_START_DATE = new Error("The starting date '{{0}}' is invalid", "INVALID_START_DATE", TYPE);
        public static readonly Error NULL_END_DATE = new Error("No ending date has been inserted", "NULL_END_DATE", TYPE);
        public static readonly Error INVALID_END_DATE = new Error("The ending date '{{0}}' is invalid", "INVALID_END_DATE", TYPE);
        public static readonly Error NULL_MEETING_RECORDING_LINK = new Error("No meeting recording has been inserted", "NULL_MEETING_RECORDING_LINK", TYPE);
        public static readonly Error EMPTY_MEETING_RECORDING_LINK = new Error("The inserted meeting recording link is empty", "EMPTY_MEETING_RECORDING_LINK", TYPE);
        public static readonly Error MEETING_RECORDING_LINK_TOO_SMALL = new Error("The meeting recording link '{{0}}' can't be smaller than {{1}} chars", "MEETING_RECORDING_LINK_TOO_SMALL", TYPE);
        public static readonly Error MEETING_RECORDING_LINK_TOO_LARGE = new Error("The meeting recording link '{{0}}' can't have more than {{1}} chars", "MEETING_RECORDING_LINK_TOO_LARGE", TYPE);
        public static readonly Error INVALID_MEETING_RECORDING_LINK = new Error("The meeting recording link '{{0}}' is invalid", "INVALID_MEETING_RECORDING_LINK", TYPE);
        public static readonly Error NULL_MEETING_TRANSCRIPT_LINK = new Error("No meeting transcript has been inserted", "NULL_MEETING_TRANSCRIPT_LINK", TYPE);
        public static readonly Error EMPTY_MEETING_TRANSCRIPT_LINK = new Error("The inserted meeting transcript link is empty", "EMPTY_MEETING_TRANSCRIPT_LINK", TYPE);
        public static readonly Error MEETING_TRANSCRIPT_LINK_TOO_SMALL = new Error("The meeting transcript link '{{0}}' can't be smaller than {{1}} chars", "MEETING_TRANSCRIPT_LINK_TOO_SMALL", TYPE);
        public static readonly Error MEETING_TRANSCRIPT_LINK_TOO_LARGE = new Error("The meeting transcript link '{{0}}' can't have more than {{1}} chars", "MEETING_TRANSCRIPT_LINK_TOO_LARGE", TYPE);
        public static readonly Error INVALID_MEETING_TRANSCRIPT_LINK = new Error("The meeting transcript link '{{0}}' is invalid", "INVALID_MEETING_TRANSCRIPT_LINK", TYPE);
        public static readonly Error NULL_DEPENDENCIES = new Error("No dependencies have been inserted", "NULL_DEPENDENCIES", TYPE);
        public static readonly Error EMPTY_DEPENDENCIES = new Error("The inserted dependencies is empty", "EMPTY_DEPENDENCIES", TYPE);
        public static readonly Error DEPENDENCIES_TOO_SMALL = new Error("The dependencies '{{0}}' can't be smaller than {{1}} chars", "DEPENDENCIES_TOO_SMALL", TYPE);
        public static readonly Error DEPENDENCIES_TOO_LARGE = new Error("The dependencies '{{0}}' can't have more than {{1}} chars", "DEPENDENCIES_TOO_LARGE", TYPE);
        public static readonly Error INVALID_DEPENDENCIES = new Error("The dependencies '{{0}}' does not follow the '{{1}}' pattern", "INVALID_DEPENDENCIES", TYPE);
        public static readonly Error NULL_NEXT_STEPS = new Error("No next steps have been inserted", "NULL_NEXT_STEPS", TYPE);
        public static readonly Error EMPTY_NEXT_STEPS = new Error("The inserted next steps is empty", "EMPTY_NEXT_STEPS", TYPE);
        public static readonly Error NEXT_STEPS_TOO_SMALL = new Error("The next steps '{{0}}' can't be smaller than {{1}} chars", "NEXT_STEPS_TOO_SMALL", TYPE);
        public static readonly Error NEXT_STEPS_TOO_LARGE = new Error("The next steps '{{0}}' can't have more than {{1}} chars", "NEXT_STEPS_TOO_LARGE", TYPE);
        public static readonly Error INVALID_NEXT_STEPS = new Error("The next steps '{{0}}' does not follow the '{{1}}' pattern", "INVALID_NEXT_STEPS", TYPE);
        public static readonly Error NULL_DECISIONS = new Error("No decisions have been inserted", "NULL_DECISIONS", TYPE);
        public static readonly Error EMPTY_DECISIONS = new Error("The inserted decisions is empty", "EMPTY_DECISIONS", TYPE);
        public static readonly Error DECISIONS_TOO_SMALL = new Error("The decisions '{{0}}' can't be smaller than {{1}} chars", "DECISIONS_TOO_SMALL", TYPE);
        public static readonly Error DECISIONS_TOO_LARGE = new Error("The decisions '{{0}}' can't have more than {{1}} chars", "DECISIONS_TOO_LARGE", TYPE);
        public static readonly Error INVALID_DECISIONS = new Error("The decisions '{{0}}' does not follow the '{{1}}' pattern", "INVALID_DECISIONS", TYPE);
        public static readonly Error NULL_FILE_BYTES = new Error("The file itself is null", "NULL_FILE_BYTES", TYPE);
        public static readonly Error NULL_FILE_NAME = new Error("The file name is null", "NULL_FILE_NAME", TYPE);
        public static readonly Error EMPTY_FILE_NAME = new Error("The file name is empty", "EMPTY_FILE_NAME", TYPE);
        public static readonly Error FILE_NAME_TOO_SMALL = new Error("The file name '{{0}}' can't be smaller than {{1}} chars", "FILE_NAME_TOO_SMALL", TYPE);
        public static readonly Error FILE_NAME_TOO_LARGE = new Error("The file name '{{0}}' can't be bigger than {{1}} chars", "FILE_NAME_TOO_LARGE", TYPE);
        public static readonly Error FILE_SIZE_TOO_SMALL = new Error("The file name '{{0}}' is smaller than {{1}} bytes", "FILE_SIZE_TOO_SMALL", TYPE);
        public static readonly Error FILE_SIZE_TOO_LARGE = new Error("The file name '{{0}}' is bigger than {{1}} bytes", "FILE_SIZE_TOO_LARGE", TYPE);
        public static readonly Error INVALID_FILE = new Error("The file name '{{0}}' is considered invalid", "INVALID_FILE", TYPE);
        public static readonly Error NULL_ROLE = new Error("The role is null", "NULL_ROLE", TYPE);
        public static readonly Error UNKNOWN_ROLE = new Error("The role '{{0}}' is not recognized by the system", "UNKNOWN_ROLE", TYPE);
        public static readonly Error NULL_PARTICIPANT_TYPE = new Error("The participant type is null", "NULL_PARTICIPANT_TYPE", TYPE);
        public static readonly Error UNKNOWN_PARTICIPANT_TYPE = new Error("The participant type '{{0}}' is not recognized by the system", "UNKNOWN_PARTICIPANT_TYPE", TYPE);
        public static readonly Error NULL_USER = new Error("A null user has been inserted", "NULL_USER", TYPE);
        public static readonly Error NULL_TASK_DESCRIPTION = new Error("The task description is null", "NULL_TASK_DESCRIPTION", TYPE);
        public static readonly Error EMPTY_TASK_DESCRIPTION = new Error("The task description is empty", "EMPTY_TASK_DESCRIPTION", TYPE);
        public static readonly Error TASK_DESCRIPTION_TOO_SMALL = new Error("The task description '{{0}}' can't be smaller than {{1}} chars", "TASK_DESCRIPTION_TOO_SMALL", TYPE);
        public static readonly Error TASK_DESCRIPTION_TOO_LARGE = new Error("The task description '{{0}}' can't be larger than {{1}} chars", "TASK_DESCRIPTION_TOO_LARGE", TYPE);
        public static readonly Error INVALID_TASK_DESCRIPTION = new Error("The task description '{{0}}' does not obey the {{1}} pattern", "INVALID_TASK_DESCRIPTION", TYPE);
        public static readonly Error NULL_TASK_NAME = new Error("The task name is null", "NULL_TASK_NAME", TYPE);
        public static readonly Error EMPTY_TASK_NAME = new Error("The task name is empty", "EMPTY_TASK_NAME", TYPE);
        public static readonly Error TASK_NAME_TOO_SMALL = new Error("The task name '{{0}}' can't be smaller than {{1}} chars", "TASK_NAME_TOO_SMALL", TYPE);
        public static readonly Error TASK_NAME_TOO_LARGE = new Error("The task name '{{0}}' can't be larger than {{1}} chars", "TASK_NAME_TOO_LARGE", TYPE);
        public static readonly Error INVALID_TASK_NAME = new Error("The task name '{{0}}' does not obey the {{1}} pattern", "INVALID_TASK_NAME", TYPE);
        public static readonly Error NULL_TASK_START_DATE = new Error("The task starting date is null", "NULL_TASK_START_DATE", TYPE);
        public static readonly Error INVALID_TASK_START_DATE = new Error("The task starting date '{{0}}' is not valid", "INVALID_TASK_START_DATE", TYPE);
        public static readonly Error NULL_TASK_END_DATE = new Error("The task ending date is null", "NULL_TASK_END_DATE", TYPE);
        public static readonly Error INVALID_TASK_END_DATE = new Error("The task ending date '{{0}}' is not valid", "INVALID_TASK_END_DATE", TYPE);
        public static readonly Error NULL_TASK_EXPECTED_END_DATE = new Error("The task expected ending date is null", "NULL_TASK_EXPECTED_END_DATE", TYPE);
        public static readonly Error INVALID_TASK_EXPECTED_END_DATE = new Error("The task expected ending date '{{0}}' is not valid", "INVALID_TASK_EXPECTED_END_DATE", TYPE);
        public static readonly Error NULL_DESCRIPTION = new Error("The description is null", "NULL_DESCRIPTION", TYPE);
        public static readonly Error EMPTY_DESCRIPTION = new Error("The description is empty", "EMPTY_DESCRIPTION", TYPE);
        public static readonly Error DESCRIPTION_TOO_SMALL = new Error("The description '{{0}}' can't be smaller than {{1}} chars", "DESCRIPTION_TOO_SMALL", TYPE);
        public static readonly Error DESCRIPTION_TOO_LARGE = new Error("The description '{{0}}' can't be larger than {{1}} chars", "DESCRIPTION_TOO_LARGE", TYPE);
        public static readonly Error INVALID_DESCRIPTION = new Error("The description '{{0}}' does not obey the {{1}} pattern", "INVALID_DESCRIPTION", TYPE);
        public static readonly Error NULL_ATTACHMENT = new Error("The attachment given is null", "NULL_ATTACHMENT", TYPE);
        public static readonly Error MEETING_HAS_MAX_NUMBER_OF_ATTACHMENTS = new Error("The meeting has already {{0}} attachments", "MEETING_HAS_MAX_NUMBER_OF_ATTACHMENTS", TYPE);
        public static readonly Error ATTACHMENT_ALREADY_UPLOADED = new Error("The attachment '{{0}}' has already been uploaded", "ATTACHMENT_ALREADY_UPLOADED", TYPE);
        public static readonly Error ATTACHMENT_NOT_AVAILABLE = new Error("The given attachment to remove '{{0}}' does not belong to the meeting", "ATTACHMENT_NOT_AVAILABLE", TYPE);
        public static readonly Error COULD_NOT_REMOVE_ATTACHMENT = new Error("Could not remove the attachment '{{0}}' for some reason", "COULD_NOT_REMOVE_ATTACHMENT", TYPE);
        public static readonly Error NULL_TASK = new Error("The task given is null", "NULL_TASK", TYPE);
        public static readonly Error MEETING_HAS_MAX_NUMBER_OF_TASKS = new Error("The meeting has already {{0}} tasks", "MEETING_HAS_MAX_NUMBER_OF_TASKS", TYPE);
        public static readonly Error TASK_ALREADY_UPLOADED = new Error("The task '{{0}}' has already been uploaded", "TASK_ALREADY_UPLOADED", TYPE);
        public static readonly Error TASK_NOT_AVAILABLE = new Error("The given task to remove '{{0}}' does not belong to the meeting", "TASK_NOT_AVAILABLE", TYPE);
        public static readonly Error COULD_NOT_REMOVE_TASK = new Error("Could not remove the task '{{0}}' for some reason", "COULD_NOT_REMOVE_TASK", TYPE);
        public static readonly Error NULL_PARTICIPANT = new Error("The participant given is null", "NULL_PARTICIPANT", TYPE);
        public static readonly Error MEETING_HAS_MAX_NUMBER_OF_PARTICIPANTS = new Error("The meeting has already {{0}} participants", "MEETING_HAS_MAX_NUMBER_OF_PARTICIPANTS", TYPE);
        public static readonly Error PARTICIPANT_ALREADY_EXISTS = new Error("The participant '{{0}}' has already been uploaded", "PARTICIPANT_ALREADY_EXISTS", TYPE);
        public static readonly Error PARTICIPANT_NOT_AVAILABLE = new Error("The given participant '{{0}}' does not belong to the meeting", "PARTICIPANT_NOT_AVAILABLE", TYPE);
        public static readonly Error COULD_NOT_REMOVE_PARTICIPANT = new Error("Could not remove the participant '{{0}}' for some reason", "COULD_NOT_REMOVE_PARTICIPANT", TYPE);
        public static readonly Error NULL_MEETING = new Error("The meeting for next meeting is null", "NULL_MEETING", TYPE);
        public static readonly Error NULL_PROJECT = new Error("The inserted project is null", "NULL_PROJECT", TYPE);
        public static readonly Error WRONG_TENANT = new Error("The meeting '{{0}}' does not belong to the tenant '{{1}}'", "WRONG_TENANT", TYPE);
    }
}
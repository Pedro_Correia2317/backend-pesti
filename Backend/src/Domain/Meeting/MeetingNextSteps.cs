

using System;
using System.Text.RegularExpressions;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingNextSteps : IValueObject
    {
        private static readonly string PATTERN = "[\\p{L}\\d\\s]+";

        public string NextSteps { get; private set; }
        
        protected MeetingNextSteps() {}

        public MeetingNextSteps(string name)
        {
            Error err = applyNextSteps(name);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyNextSteps(string name)
        {
            if(name == null)
                return MeetingErrors.NULL_NEXT_STEPS.Copy();
            string trimmedNextSteps = name.Trim();
            if(trimmedNextSteps.Length == 0)
                return MeetingErrors.EMPTY_NEXT_STEPS.Copy();
            if(trimmedNextSteps.Length < 2)
                return MeetingErrors.NEXT_STEPS_TOO_SMALL.Copy().WithParams(trimmedNextSteps, 2);
            if(trimmedNextSteps.Length > 1000)
                return MeetingErrors.NEXT_STEPS_TOO_LARGE.Copy().WithParams(trimmedNextSteps, 1000);
            if(!Regex.IsMatch(trimmedNextSteps, PATTERN))
                return MeetingErrors.INVALID_NEXT_STEPS.Copy().WithParams(trimmedNextSteps, PATTERN);
            this.NextSteps = trimmedNextSteps;
            return null;
        }

        public string Value()
        {
            return this.NextSteps;
        }

        public override string ToString()
        {
            return "MeetingNextSteps: [" + this.NextSteps + "]";
        }

        public override bool Equals(object obj)
        {
            return obj is MeetingNextSteps nextSteps &&
                   NextSteps == nextSteps.NextSteps;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(NextSteps);
        }
    }
}


using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantPermissions : IValueObject
    {
        public string Name { get; private set; }
        private bool CanGiveLicenses;
        private bool CanCreateProjects;
        private bool CanCreateMeetings;

        protected TenantPermissions() {}

        public TenantPermissions(string Name, bool CanGiveLicenses, bool CanCreateProjects, bool CanCreateMeetings)
        {
            this.Name = Name;
            this.CanGiveLicenses = CanGiveLicenses;
            this.CanCreateProjects = CanCreateProjects;
            this.CanCreateMeetings = CanCreateMeetings;
        }

        public string GetName()
        {
            return this.Name;
        }

        public bool HasPermissionsToGiveLicense()
        {
            return this.CanGiveLicenses;
        }

        public bool HasPermissionsToCreateProject()
        {
            return this.CanCreateProjects;
        }

        public bool HasPermissionsToDeleteProject()
        {
            return this.CanCreateProjects;
        }

        public bool HasPermissionsToUpdateProject()
        {
            return this.CanCreateProjects;
        }

        public bool HasPermissionsToCreateMeeting()
        {
            return this.CanCreateMeetings;
        }

        public bool HasPermissionsToDeleteMeeting()
        {
            return this.CanCreateMeetings;
        }

        public bool HasPermissionsToUpdateMeeting()
        {
            return this.CanCreateMeetings;
        }

        public override bool Equals(object obj)
        {
            return obj is TenantPermissions type &&
                   Name == type.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public override string ToString()
        {
            return $"TenantPermissions: [{this.Name}, {this.CanGiveLicenses}, {this.CanCreateProjects}]";
        }
    }

    public static class TenantPermissionList
    {
        public static readonly TenantPermissions Administrator = new TenantPermissions("Administrator", true, true, true);
        public static readonly TenantPermissions Client = new TenantPermissions("Client", false, false, false);
        public static readonly TenantPermissions Owner = new TenantPermissions("Owner", true, true, true);
        private static Dictionary<string, TenantPermissions> AllPermissions = new Dictionary<string, TenantPermissions>();

        static TenantPermissionList()
        {
            AllPermissions.Add("Administrator", Administrator);
            AllPermissions.Add("Client", Client);
            AllPermissions.Add("Owner", Owner);
        }

        public static TenantPermissions GetByName(string type)
        {
            if(type == null)
            {
                Error e = TenantErrors.NULL_PERMISSIONS.Copy();
                throw new BusinessRuleFailureException(e);
            }
            TenantPermissions permissions = AllPermissions.GetValueOrDefault(type + "");
            if(permissions == null)
            {
                Error e = TenantErrors.UNKNOWN_PERMISSIONS.Copy().WithParams(type);
                throw new BusinessRuleFailureException(e);
            }
            return permissions;
        }

        public static TenantPermissions[] GetAll()
        {
            return AllPermissions.Values.ToArray();
        }
    }
}
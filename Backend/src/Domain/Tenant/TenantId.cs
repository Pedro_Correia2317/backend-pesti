

using System;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantId : EntityId, IValueObject
    {
        
        protected TenantId() : base() {}

        public TenantId(string id)
        {
            Error err = applyId(id);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        private Error applyId(string id)
        {
            if(id == null)
                return TenantErrors.NULL_ID.Copy();
            string trimmedId = id.Trim();
            Guid outGuid;
            if(!Guid.TryParse(trimmedId, out outGuid))
                return TenantErrors.INVALID_ID.Copy().WithParams(trimmedId);
            this.Value = trimmedId;
            return null;
        }

        public override bool Equals(object obj)
        {
            return obj is TenantId id &&
                   base.Equals(obj) &&
                   Value == id.Value;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Value);
        }

        public override string ToString()
        {
            return "TenantId: [" + this.Value + "]";
        }
    }
}
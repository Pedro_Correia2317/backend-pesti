

using System;
using System.Collections.Generic;
using Backend.Accounts;
using Backend.Licenses;
using Backend.Utils;

namespace Backend.Tenants
{
    public class Tenant : Entity<TenantId>, IAggregateRoot
    {

        public TenantName Name { get; set; }

        public TenantLicenses Licenses { get; private set; }

        public TenantMembers Members { get; private set; }

        protected Tenant() {}

        public Tenant(TenantId id, TenantName name, TenantMember member)
        {
            if(DomainUtils.HasNullFields(id, name, member))
            {
                throw new BusinessRuleFailureException(TenantErrors.NULL_DATA_IN_TENANT.Copy());
            }
            if(!member.IsOwner())
            {
                throw new BusinessRuleFailureException(TenantErrors.NO_OWNER_IN_TENANT.Copy());
            }
            this.BusinessId = id;
            this.Name = name;
            this.Licenses = new TenantLicenses();
            this.Members = new TenantMembers(member);
        }

        public bool AssociateLicenses(ICollection<License> list)
        {
            bool success = false;
            TenantLicenses newLicenses = this.Licenses.AssociateLicenses(list);
            if(newLicenses != null)
            {
                this.Licenses = newLicenses;
                success = true;
            }
            return success;
        }

        public Results<bool> AddMember(TenantMember member)
        {
            return this.Members.AddNewMember(member);
        }

        public Results<bool> RemoveMember(AccountId aId)
        {
            return this.Members.RemoveMember(aId);
        }

        public Results<bool> HasPermissionsToCreateProject(AccountId account)
        {
            return this.Members.HasPermissionsToCreateProject(account);
        }

        public Results<bool> HasPermissionsToDeleteProject(AccountId account)
        {
            return this.Members.HasPermissionsToDeleteProject(account);
        }

        public Results<bool> HasPermissionsToUpdateProject(AccountId account)
        {
            return this.Members.HasPermissionsToUpdateProject(account);
        }

        public Results<bool> HasPermissionsToCreateMeeting(AccountId account)
        {
            return this.Members.HasPermissionsToCreateMeeting(account);
        }

        public Results<bool> HasPermissionsToDeleteMeeting(AccountId account)
        {
            return this.Members.HasPermissionsToDeleteMeeting(account);
        }

        public Results<bool> HasPermissionsToUpdateMeeting(AccountId account)
        {
            return this.Members.HasPermissionsToUpdateMeeting(account);
        }

        public Results<bool> HasLicense(LicenseId licenseId)
        {
            return this.Licenses.HasLicense(licenseId);
        }

        public Results<bool> HasMember(AccountId account)
        {
            return this.Members.HasMember(account);
        }

        public Results<bool> HasPermissionsToGiveLicense(AccountId account)
        {
            return this.Members.HasPermissionsToGiveLicense(account);
        }

        public Results<bool> HasOwner(AccountId businessId)
        {
            return this.Members.HasOwner(businessId);
        }

        public override bool Equals(object obj)
        {
            return obj is Tenant tenant && this.BusinessId.Equals(tenant.BusinessId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(BusinessId);
        }

        public override string ToString()
        {
            return $"Tenant: [{this.BusinessId}, {this.Name}, {this.Licenses}, {this.Members}]";
        }
    }
}
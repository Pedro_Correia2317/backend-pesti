

namespace Backend.Tenants
{
    public class TenantContainer
    {
        public TenantId Id;

        public TenantName Name;

        public TenantMember Owner;
    }
}
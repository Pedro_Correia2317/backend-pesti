

using Backend.Accounts;
using Backend.Utils;

namespace Backend.Tenants
{
    public interface ITenantFactory
    {

        TenantContainer NewContainer();

        Results<TenantId> NewId(string id);

        Results<TenantName> NewName(string name);

        Results<TenantPermissions> NewPermissions(string permissions);

        Results<TenantMember> NewMember(AccountId account, TenantPermissions permissions);

        Results<Tenant> NewTenant(TenantContainer container);
        TenantPermissions NewOwnerPermissions();
    }
}
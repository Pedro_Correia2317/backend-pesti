

using Backend.Accounts;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantFactoryImpl : ITenantFactory
    {
        public TenantContainer NewContainer()
        {
            return new TenantContainer();
        }

        public Results<TenantId> NewId(string id)
        {
            return FactoryUtils.NewObject(() => new TenantId(id));
        }

        public Results<TenantMember> NewMember(AccountId account, TenantPermissions permissions)
        {
            return FactoryUtils.NewObject(() => new TenantMember(account, permissions));
        }

        public Results<TenantName> NewName(string name)
        {
            return FactoryUtils.NewObject(() => new TenantName(name));
        }

        public TenantPermissions NewOwnerPermissions()
        {
             return TenantPermissionList.Owner;
        }

        public Results<TenantPermissions> NewPermissions(string permissions)
        {
            return FactoryUtils.NewObject(() => TenantPermissionList.GetByName(permissions));
        }

        public Results<Tenant> NewTenant(TenantContainer container)
        {
            return FactoryUtils.NewObject(() => new Tenant(container?.Id, container?.Name, container?.Owner));
        }
    }
}
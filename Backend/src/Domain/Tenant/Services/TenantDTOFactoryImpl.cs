

using System.Collections.Generic;
using Backend.Licenses;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantDTOFactoryImpl : ITenantDTOFactory
    {
        public TenantDTO CreateBasicDTO(Tenant tenant)
        {
            TenantDTO dto = new TenantDTO();
            if (tenant != null)
            {
                dto.id = tenant.BusinessId.Value;
                dto.name = tenant.Name.Value();
            } else {
                dto.id = "";
                dto.name = "";
            }
            return dto;
        }

        public TenantDTO[] CreateListOfBasicDTO(Tenant[] tenants)
        {
            return FactoryUtils.NewArrayDTO(tenants, this.CreateBasicDTO);
        }

        public TenantPermissionsDTO CreatePermissionsDTO(TenantPermissions permission)
        {
            TenantPermissionsDTO dto = new TenantPermissionsDTO();
            dto.key = permission != null? permission.Name : "";
            dto.description = permission != null? permission.Name : "";
            return dto;
        }

        public TenantPermissionsDTO[] CreateListOfPermissionsDTO(TenantPermissions[] permissions)
        {
            return FactoryUtils.NewArrayDTO(permissions, this.CreatePermissionsDTO);
        }

        public QueryParams CreateQueryParamsOf(ConsultTenantsParameters request)
        {
            QueryParams queryParams = new QueryParams();
            queryParams.page = request.page;
            queryParams.size = request.size;
            queryParams.search = request.search;
            queryParams.filters = request.filters;
            queryParams.sort = request.sort;
            queryParams.order = request.order;
            queryParams.condition = request.condition;
            return queryParams;
        }

        public TenantMemberDTO CreateMemberDTO(TenantMember member)
        {
            TenantMemberDTO dto = new TenantMemberDTO();
            if (member != null)
            {
                dto.account = member.Account.Value;
                dto.permissions = this.CreatePermissionsDTO(member.Permissions);
            } else {
                dto.account = "";
                dto.permissions = this.CreatePermissionsDTO(null);
            }
            return dto;
        }

        public TenantLicenseDTO CreateLicenseDTO(LicenseId license)
        {
            TenantLicenseDTO dto = new TenantLicenseDTO();
            if (license != null)
            {
                dto.license = license.Value;
            } else {
                dto.license = "";
            }
            return dto;
        }

        public TenantMemberDTO[] CreateListOfMembersDTO(ICollection<TenantMember> members)
        {
            return FactoryUtils.NewArrayDTO(members, this.CreateMemberDTO);
        }

        public TenantLicenseDTO[] CreateListOfLicensesDTO(ICollection<LicenseId> licenses)
        {
            return FactoryUtils.NewArrayDTO(licenses, this.CreateLicenseDTO);
        }
    }
}


using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Tenants
{
    public interface ITenantRepository : IRepository<Tenant, TenantId>
    {
        Task<QueryResults<Tenant>> FindByMember(AccountId account);
        Task<QueryResults<bool>> DoesNotHaveMember(AccountId account);
    }
}


using System.Collections.Generic;
using Backend.Licenses;
using Backend.Utils;

namespace Backend.Tenants
{
    public interface ITenantDTOFactory
    {
        TenantDTO CreateBasicDTO(Tenant tenant);
        TenantPermissionsDTO CreatePermissionsDTO(TenantPermissions permission);
        QueryParams CreateQueryParamsOf(ConsultTenantsParameters request);
        TenantDTO[] CreateListOfBasicDTO(Tenant[] tenants);
        TenantPermissionsDTO[] CreateListOfPermissionsDTO(TenantPermissions[] permissions);
        TenantMemberDTO CreateMemberDTO(TenantMember member);
        TenantLicenseDTO CreateLicenseDTO(LicenseId license);
        TenantMemberDTO[] CreateListOfMembersDTO(ICollection<TenantMember> members);
        TenantLicenseDTO[] CreateListOfLicensesDTO(ICollection<LicenseId> licenses);
    }
}
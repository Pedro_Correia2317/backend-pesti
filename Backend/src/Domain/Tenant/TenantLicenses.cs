

using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Licenses;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantLicenses : IValueObject
    {

        public HashSet<LicenseId> Licenses { get; private set; }

        public TenantLicenses()
        {
            this.Licenses = new HashSet<LicenseId>();
        }

        public TenantLicenses(LicenseId id)
        {
            if(id == null)
            {
                throw new BusinessRuleFailureException(TenantErrors.NULL_LICENSE.Copy());
            }
            this.Licenses = new HashSet<LicenseId>();
            this.Licenses.Add(id);
        }

        private TenantLicenses(HashSet<LicenseId> list)
        {
            this.Licenses = list;
        }

        public override bool Equals(object obj)
        {
            return obj is TenantLicenses licenses &&
                this.Licenses.All(licenses.Licenses.Contains);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Licenses);
        }

        public TenantLicenses AssociateLicenses(ICollection<License> list)
        {
            if (list == null)
                return null;
            HashSet<LicenseId> set = new HashSet<LicenseId>(this.Licenses);
            foreach (License license in list)
            {
                if (license == null || !set.Add(license.BusinessId))
                {
                    return null;
                }
            }
            return new TenantLicenses(set);
        }

        public ICollection<LicenseId> ToCollection()
        {
            ICollection<LicenseId> collection = new List<LicenseId>();
            foreach (var attachment in this.Licenses)
            {
                collection.Add(attachment);
            }
            return collection;
        }

        public override string ToString()
        {
            return $"TenantLicenses [{this.Licenses}]";
        }

        public Results<bool> HasLicense(LicenseId licenseId)
        {
            if(licenseId == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_LICENSE.Copy());
            }
            if(!this.Licenses.Contains(licenseId))
            {
                return Results<bool>.Of(TenantErrors.LICENSE_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(licenseId.Value));
            }
            return Results<bool>.Of(true);
        }
    }
}


using System;
using System.Linq;
using System.Collections.Generic;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantMembers : IValueObject
    {

        public List<TenantMember> Members { get; private set; }

        protected TenantMembers() {}

        public TenantMembers(TenantMember member)
        {
            this.Members = new List<TenantMember>();
            this.AddMemberToList(member);
        }

        private void AddMemberToList(TenantMember member)
        {
            if(member == null)
            {
                throw new BusinessRuleFailureException(TenantErrors.NULL_MEMBER.Copy());
            }
            this.Members.Add(member);
        }

        public Results<bool> AddNewMember(TenantMember newMember)
        {
            if(newMember == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_MEMBER.Copy());
            }
            if(newMember.IsOwner())
            {
                return Results<bool>.Of(TenantErrors.ONLY_ONE_OWNER_PER_TENANT.Copy().WithParams(newMember.Account.Value));
            }
            if(this.Members.Count > 10000)
            {
                return Results<bool>.Of(TenantErrors.TENANT_HAS_REACHED_MAX_MEMBERS.Copy().WithParams(10000));
            }
            if(this.Members.Contains(newMember))
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_ALREADY_BELONGS_TO_TENANT.Copy().WithParams(newMember.Account.Value));
            }
            this.Members.Add(newMember);
            return Results<bool>.Of(true);
        }

        public Results<bool> RemoveMember(AccountId aId)
        {
            if(aId == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_MEMBER.Copy());
            }
            TenantMember memberToDelete = this.Members.Find((x) => x.Account.Equals(aId));
            if(memberToDelete == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(aId.Value));
            }
            if(memberToDelete.IsOwner())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_IS_OWNER.Copy().WithParams(aId.Value));
            }
            if(!this.Members.Remove(memberToDelete))
            {
                return Results<bool>.Of(TenantErrors.COULD_NOT_REMOVE_MEMBER.Copy().WithParams(aId.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasPermissionsToCreateMeeting(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            TenantMember member = this.Members.Find(x => x.Account.Equals(account));
            if(member == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            if(!member.Permissions.HasPermissionsToCreateMeeting())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_CANNOT_CREATE_MEETING.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasOwner(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            TenantMember member = this.Members.Find(x => x.Account.Equals(account));
            if(member == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            if(!member.IsOwner())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_IS_NOT_OWNER.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasPermissionsToDeleteMeeting(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            TenantMember member = this.Members.Find(x => x.Account.Equals(account));
            if(member == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            if(!member.Permissions.HasPermissionsToDeleteMeeting())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_CANNOT_DELETE_MEETING.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasPermissionsToUpdateMeeting(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            TenantMember member = this.Members.Find(x => x.Account.Equals(account));
            if(member == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            if(!member.Permissions.HasPermissionsToUpdateMeeting())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_CANNOT_UPDATE_MEETING.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasPermissionsToGiveLicense(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            TenantMember member = this.Members.Find(x => x.Account.Equals(account));
            if(member == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            if(!member.Permissions.HasPermissionsToGiveLicense())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_CANNOT_GIVE_LICENSES.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasPermissionsToCreateProject(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            TenantMember member = this.Members.Find(x => x.Account.Equals(account));
            if(member == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            if(!member.Permissions.HasPermissionsToCreateProject())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_CANNOT_CREATE_PROJECT.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasPermissionsToDeleteProject(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            TenantMember member = this.Members.Find(x => x.Account.Equals(account));
            if(member == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            if(!member.Permissions.HasPermissionsToDeleteProject())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_CANNOT_DELETE_PROJECT.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasPermissionsToUpdateProject(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            TenantMember member = this.Members.Find(x => x.Account.Equals(account));
            if(member == null)
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            if(!member.Permissions.HasPermissionsToUpdateProject())
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_CANNOT_UPDATE_PROJECT.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public Results<bool> HasMember(AccountId account)
        {
            if(account == null)
            {
                return Results<bool>.Of(TenantErrors.NULL_ACCOUNT.Copy());
            }
            if(!this.Members.Any(x => x.Account.Equals(account)))
            {
                return Results<bool>.Of(TenantErrors.ACCOUNT_DOES_NOT_BELONG_TO_TENANT.Copy().WithParams(account.Value));
            }
            return Results<bool>.Of(true);
        }

        public ICollection<TenantMember> ToCollection()
        {
            ICollection<TenantMember> collection = new List<TenantMember>();
            foreach (var attachment in this.Members)
            {
                collection.Add(attachment);
            }
            return collection;
        }

        public override bool Equals(object obj)
        {
            return obj is TenantMembers accounts &&
                Members.All(accounts.Members.Contains);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Members);
        }

        public override string ToString()
        {
            return $"TenantMembers [{this.Members}]";
        }
    }
}


using System;
using System.Collections.Generic;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantMember : IValueObject
    {
        public AccountId Account { get; set; }

        public TenantPermissions Permissions { get; set; }

        protected TenantMember() {}

        public TenantMember(AccountId account, TenantPermissions permissions)
        {
            Error err = this.ApplyMember(account, permissions);
            if(err != null)
            {
                throw new BusinessRuleFailureException(err);
            }
        }

        public bool IsOwner()
        {
            return this.Permissions.Equals(TenantPermissionList.Owner);
        }

        private Error ApplyMember(AccountId account, TenantPermissions permissions)
        {
            if(account == null)
            {
                return TenantErrors.NULL_ACCOUNT.Copy();
            }
            if(permissions == null)
            {
                return TenantErrors.NULL_PERMISSIONS.Copy();
            }
            this.Account = account;
            this.Permissions = permissions;
            return null;
        }

        public override bool Equals(object obj)
        {
            return obj is TenantMember member &&
                this.Account.Equals(member.Account);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Account);
        }

        public override string ToString()
        {
            return $"TenantMember: [{this.Account}, {this.Permissions}]";
        }
    }
}
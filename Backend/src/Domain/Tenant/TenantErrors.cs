

using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantErrors
    {
        public static readonly string TYPE = "Tenant";
        public static readonly Error NULL_DATA_IN_TENANT = new Error("There is null data trying to create tenant", "NULL_DATA_IN_TENANT", TYPE);
        public static readonly Error NO_OWNER_IN_TENANT = new Error("There needs to be an owner on a tenant", "NO_OWNER_IN_TENANT", TYPE);
        public static readonly Error NULL_LICENSE = new Error("Trying to pass a null license", "NULL_LICENSE", TYPE);
        public static readonly Error NULL_MEMBER = new Error("Trying to pass a null member", "NULL_MEMBER", TYPE);
        public static readonly Error NULL_ID = new Error("No tenant id has been inserted", "NULL_ID", TYPE);
        public static readonly Error UNKNOWN_ID = new Error("No tenant has the id '{{0}}'", "UNKNOWN_ID", TYPE);
        public static readonly Error INVALID_ID = new Error("The tenant id '{{0}}' is not a Guid", "INVALID_ID", TYPE);
        public static readonly Error NULL_NAME = new Error("The inserted name is null", "NULL_NAME", TYPE);
        public static readonly Error EMPTY_NAME = new Error("The inserted name is empty", "EMPTY_NAME", TYPE);
        public static readonly Error NAME_TOO_SMALL = new Error("The inserted name '{{0}}' is smaller than {{1}} chars", "NAME_TOO_SMALL", TYPE);
        public static readonly Error NAME_TOO_LARGE = new Error("The inserted name '{{0}}' is larger than {{1}} chars", "NAME_TOO_LARGE", TYPE);
        public static readonly Error INVALID_NAME = new Error("The inserted name '{{0}}' does not obey the {{1}} regex", "INVALID_NAME", TYPE);
        public static readonly Error NULL_PERMISSIONS = new Error("The inserted permissions is null", "NULL_PERMISSIONS", TYPE);
        public static readonly Error UNKNOWN_PERMISSIONS = new Error("The inserted permissions '{{0}}' is unknown", "UNKNOWN_PERMISSIONS", TYPE);
        public static readonly Error NULL_ACCOUNT = new Error("No account id has been inserted", "NULL_ACCOUNT", TYPE);
        public static readonly Error ONLY_ONE_OWNER_PER_TENANT = new Error("The account '{{0}}' tried to be a owner", "ONLY_ONE_OWNER_PER_TENANT", TYPE);
        public static readonly Error TENANT_HAS_REACHED_MAX_MEMBERS = new Error("A tenant can't have more than {{0}} members", "TENANT_HAS_REACHED_MAX_MEMBERS", TYPE);
        public static readonly Error ACCOUNT_ALREADY_BELONGS_TO_TENANT = new Error("The account {{0}} already belongs to the tenant", "ACCOUNT_ALREADY_BELONGS_TO_TENANT", TYPE);
        public static readonly Error ACCOUNT_IS_OWNER = new Error("The account {{0}} is the owner of the tenant, can't remove it", "ACCOUNT_IS_OWNER", TYPE);
        public static readonly Error ACCOUNT_IS_NOT_OWNER = new Error("The account {{0}} is not the owner of the tenant", "ACCOUNT_IS_NOT_OWNER", TYPE);
        public static readonly Error COULD_NOT_REMOVE_MEMBER = new Error("The account {{0}} could not be removed from the tenant, unexpectedly", "COULD_NOT_REMOVE_MEMBER", TYPE);
        public static readonly Error LICENSE_DOES_NOT_BELONG_TO_TENANT = new Error("The license '{{0}}' does not belongs to the tenant", "LICENSE_DOES_NOT_BELONG_TO_TENANT", TYPE);
        public static readonly Error ACCOUNT_DOES_NOT_BELONG_TO_TENANT = new Error("The account '{{0}}' does not belongs to the tenant", "ACCOUNT_DOES_NOT_BELONG_TO_TENANT", TYPE);
        public static readonly Error ACCOUNT_CANNOT_GIVE_LICENSES = new Error("The account '{{0}}' does not have the permissions to give licenses", "ACCOUNT_CANNOT_GIVE_LICENSES", TYPE);
        public static readonly Error ACCOUNT_HAS_NO_TENANT = new Error("The account '{{0}}' does not have any tenant associated with it", "ACCOUNT_HAS_NO_TENANT", TYPE);
        public static readonly Error ACCOUNT_CANNOT_CREATE_PROJECT = new Error("The account '{{0}}' does not have the permissions to create projects", "ACCOUNT_CANNOT_CREATE_PROJECT", TYPE);
        public static readonly Error ACCOUNT_CANNOT_DELETE_PROJECT = new Error("The account '{{0}}' does not have the permissions to delete projects", "ACCOUNT_CANNOT_DELETE_PROJECT", TYPE);
        public static readonly Error ACCOUNT_CANNOT_UPDATE_PROJECT = new Error("The account '{{0}}' does not have the permissions to update projects", "ACCOUNT_CANNOT_UPDATE_PROJECT", TYPE);
        public static readonly Error ACCOUNT_CANNOT_CREATE_MEETING = new Error("The account '{{0}}' does not have the permissions to create meetings", "ACCOUNT_CANNOT_CREATE_MEETING", TYPE);
        public static readonly Error ACCOUNT_CANNOT_DELETE_MEETING = new Error("The account '{{0}}' does not have the permissions to delete meetings", "ACCOUNT_CANNOT_DELETE_MEETING", TYPE);
        public static readonly Error ACCOUNT_CANNOT_UPDATE_MEETING = new Error("The account '{{0}}' does not have the permissions to update meetings", "ACCOUNT_CANNOT_UPDATE_MEETING", TYPE);
    }
}
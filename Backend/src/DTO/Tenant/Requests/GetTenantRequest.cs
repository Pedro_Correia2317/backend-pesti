

namespace Backend.Tenants
{
    public class GetTenantRequest
    {
        public string id;

        public override string ToString()
        {
            return $"GetTenantRequest: [{id}]";
        }

    }
}
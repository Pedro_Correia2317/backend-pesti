
namespace Backend.Tenants
{
    public class ConsultLicensesOfSpecificTenantRequest
    {
        public string id;

        public override string ToString()
        {
            return $"ConsultLicensesRequest: [{id}]";
        }
    }
}
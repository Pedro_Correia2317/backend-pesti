

namespace Backend.Tenants
{
    public class DeleteTenantRequest
    {
        public string code;

        public string entryDescription;

        public override string ToString()
        {
            return $"DeleteTenantRequest: [{code}, {entryDescription}]";
        }
    }
}
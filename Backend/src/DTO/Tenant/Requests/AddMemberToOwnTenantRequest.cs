

namespace Backend.Tenants
{
    public class AddMemberToOwnTenantRequest
    {

        public string account;

        public string permissions;

        public override string ToString()
        {
            return $"AddMemberToOwnTenantRequest: [{account}, {permissions}]";
        }
    }
}
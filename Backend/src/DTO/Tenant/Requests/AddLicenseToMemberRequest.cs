

namespace Backend.Tenants
{
    public class AddLicenseToMemberRequest
    {
        public string account;

        public string license;

        public override string ToString()
        {
            return $"AddLicenseToMemberRequest: [{account}, {license}]";
        }
    }
}
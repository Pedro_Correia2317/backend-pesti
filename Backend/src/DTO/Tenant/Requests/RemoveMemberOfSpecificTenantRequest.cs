

namespace Backend.Tenants
{
    public class RemoveMemberOfSpecificTenantRequest
    {
        public string tenant;

        public string account;

        public string entryDescription;

        public override string ToString()
        {
            return $"RemoveMemberRequest: [{tenant}, {account}, {entryDescription}]";
        }
    }
}
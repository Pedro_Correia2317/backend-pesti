

namespace Backend.Tenants
{
    public class UpdateTenantRequest
    {

        public string id;

        public string name;

        public string entryDescription;

        public override string ToString()
        {
            return $"UpdateTenantRequest: [{id}, {name}, {entryDescription}]";
        }

    }
}


namespace Backend.Tenants
{
    public class CreateTenantRequest
    {

        public string entryDescription;

        public string name;

        public string owner;

        public override string ToString()
        {
            return $"CreateTenantRequest: [{name}, {owner}, {entryDescription}]";
        }

    }
}
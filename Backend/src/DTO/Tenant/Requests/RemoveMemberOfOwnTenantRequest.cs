

namespace Backend.Tenants
{
    public class RemoveMemberOfOwnTenantRequest
    {

        public string account;

        public override string ToString()
        {
            return $"RemoveMemberOfOwnTenantRequest: [{account}]";
        }
    }
}
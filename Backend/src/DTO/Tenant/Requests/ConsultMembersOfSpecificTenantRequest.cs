
namespace Backend.Tenants
{
    public class ConsultMembersOfSpecificTenantRequest
    {
        public string id;

        public override string ToString()
        {
            return $"ConsultMembersRequest: [{id}]";
        }
    }
}


namespace Backend.Tenants
{
    public class AddMemberToSpecificTenantRequest
    {
        public string tenant;

        public string account;

        public string permissions;

        public string entryDescription;

        public override string ToString()
        {
            return $"AddMemberRequest: [{tenant}, {account}, {permissions}, {entryDescription}]";
        }
    }
}
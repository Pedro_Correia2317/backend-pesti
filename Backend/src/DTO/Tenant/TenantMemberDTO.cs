

using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantMemberDTO : IDTO
    {
        public string account;

        public TenantPermissionsDTO permissions;
        
    }
}
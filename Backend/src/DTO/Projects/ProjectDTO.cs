

using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectDTO : IDTO
    {
        public string id;

        public string name;

        public string description;

        public string tenant;
    }
}
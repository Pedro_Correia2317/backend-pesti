
namespace Backend.Projects
{
    public class DeleteProjectRequest
    {
        public string id;

        public override string ToString()
        {
            return $"DeleteProjectRequest: [{id}]";
        }
    }
}
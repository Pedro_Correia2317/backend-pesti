
namespace Backend.Projects
{
    public class CreateProjectRequest
    {
        public string name;

        public string description;

        public override string ToString()
        {
            return $"CreateProjectRequest: [{name}, {description}]";
        }
    }
}
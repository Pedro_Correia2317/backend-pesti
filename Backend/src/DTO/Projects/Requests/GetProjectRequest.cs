

namespace Backend.Projects
{
    public class GetProjectRequest
    {
        public string id;

        public override string ToString()
        {
            return $"GetProjectRequest: [{id}]";
        }
    }
}
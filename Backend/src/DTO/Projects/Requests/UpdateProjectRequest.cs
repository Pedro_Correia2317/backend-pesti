
namespace Backend.Projects
{
    public class UpdateProjectRequest
    {

        public string id;
        
        public string name;

        public string description;

        public override string ToString()
        {
            return $"UpdateProjectRequest: [{id}, {name}, {description}]";
        }
    }
}
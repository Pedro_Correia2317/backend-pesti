

using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingTaskDTO : IDTO
    {
        public string name;

        public string description;

        public string startDate;

        public string endDate;

        public string expectedEndDate;

        public string responsible;
    }
}
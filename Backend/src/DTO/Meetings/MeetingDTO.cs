

using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingDTO : IDTO
    {

        public string id;

        public string startDate;

        public string endDate;

        public string description;

        public string recordingLink;

        public string transcriptLink;

        public string dependencies;

        public string nextSteps;

        public string decisions;

        public string project;

        public string nextMeeting;

        public string tenant;
        
    }
}
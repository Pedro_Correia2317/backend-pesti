

namespace Backend.Meetings
{
    public class UpdateMeetingRequest
    {

        public string id;

        public string recordingLink;

        public string transcriptLink;

        public string dependencies;

        public string nextSteps;

        public string decisions;

        public string nextMeeting;

        public override string ToString()
        {
            return $"UpdateMeetingRequest: [{id}, {recordingLink}, {transcriptLink}, {dependencies}"
                    + $", {nextSteps}, {decisions}, {nextMeeting}]";
        }
        
    }
}
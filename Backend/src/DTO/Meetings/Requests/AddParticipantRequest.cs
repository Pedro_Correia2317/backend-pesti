

namespace Backend.Meetings
{
    public class AddParticipantRequest
    {
        public string meeting;
        
        public string participant;
        
        public string role;
        
        public string type;

        public override string ToString()
        {
            return $"AddParticipantRequest: [{meeting}, {participant}, {role}, {type}]";
        }

    }
}
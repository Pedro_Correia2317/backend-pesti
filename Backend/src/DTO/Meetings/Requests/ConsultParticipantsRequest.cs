

namespace Backend.Meetings
{
    public class ConsultParticipantsRequest
    {
        public string meeting;
        
        public override string ToString()
        {
            return $"ConsultParticipantsRequest: [{meeting}]";
        }
    }
}
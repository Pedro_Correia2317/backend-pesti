

namespace Backend.Meetings
{
    public class RemoveNextMeetingRequest
    {
        public string meeting;

        public override string ToString()
        {
            return $"RemoveNextMeetingRequest: [{meeting}]";
        }

    }
}
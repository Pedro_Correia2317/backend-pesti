
namespace Backend.Meetings
{
    public class DeleteMeetingRequest
    {
        public string id;

        public override string ToString()
        {
            return $"DeleteMeetingRequest: [{id}]";
        }
    }
}
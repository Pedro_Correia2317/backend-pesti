

namespace Backend.Meetings
{
    public class GetMeetingRequest
    {
        public string id;

        public override string ToString()
        {
            return $"GetMeetingRequest: [{id}]";
        }
    }
}
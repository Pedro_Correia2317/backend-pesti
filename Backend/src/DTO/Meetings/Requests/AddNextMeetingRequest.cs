

namespace Backend.Meetings
{
    public class AddNextMeetingRequest
    {
        public string meeting;
        
        public string nextmeeting;

        public override string ToString()
        {
            return $"AddNextMeetingRequest: [{meeting}, {nextmeeting}]";
        }

    }
}
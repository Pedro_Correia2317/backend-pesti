

namespace Backend.Meetings
{
    public class RemoveParticipantRequest
    {
        public string meeting;
        
        public string participant;

        public override string ToString()
        {
            return $"RemoveParticipantRequest: [{meeting}, {participant}]";
        }

    }
}
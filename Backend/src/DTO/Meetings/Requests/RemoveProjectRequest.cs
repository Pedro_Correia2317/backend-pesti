

namespace Backend.Meetings
{
    public class RemoveProjectRequest
    {
        public string meeting;

        public override string ToString()
        {
            return $"RemoveProjectRequest: [{meeting}]";
        }

    }
}
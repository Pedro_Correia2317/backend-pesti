

using Microsoft.AspNetCore.Http;

namespace Backend.Meetings
{
    public class AddAttachmentRequest
    {
        public string meeting;

        public IFormFile file;

        public override string ToString()
        {
            return $"AddAttachmentRequest: [{meeting}, {file}]";
        }
    }
}
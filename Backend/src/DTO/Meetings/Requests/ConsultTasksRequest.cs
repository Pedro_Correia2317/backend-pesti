
namespace Backend.Meetings
{
    public class ConsultTasksRequest
    {
        public string meeting;
        
        public override string ToString()
        {
            return $"ConsultTasksRequest: [{meeting}]";
        }
    }
}


namespace Backend.Meetings
{
    public class AddTaskRequest
    {
        public string meeting;
        
        public string name;
        
        public string startDate;
        
        public string endDate;
        
        public string expectedEndDate;
        
        public string description;
        
        public string responsible;

        public override string ToString()
        {
            return $"AddTaskRequest: [{meeting}, {name}, {startDate}, {endDate}, {expectedEndDate}, {description}, {responsible}]";
        }

    }
}

using Microsoft.AspNetCore.Mvc;

namespace Backend.Meetings
{
    public class GetAttachmentFileRequest
    {
        public string meeting;

        [FromQuery(Name = "fileName")]
        public string fileName { get; set; }
        
        public override string ToString()
        {
            return $"GetAttachmentFileRequest: [{meeting}, {fileName}]";
        }
    }
}
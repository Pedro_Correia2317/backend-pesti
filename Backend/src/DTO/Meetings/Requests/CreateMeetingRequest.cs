

namespace Backend.Meetings
{
    public class CreateMeetingRequest
    {
        public string startDate;

        public string endDate;

        public string description;

        public override string ToString()
        {
            return $"CreateMeetingRequest: [{startDate}, {endDate}, {description}]";
        }
    }
}
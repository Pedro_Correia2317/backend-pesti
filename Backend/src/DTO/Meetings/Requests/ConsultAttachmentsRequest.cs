

namespace Backend.Meetings
{
    public class ConsultAttachmentsRequest
    {
        public string meeting;
        
        public override string ToString()
        {
            return $"ConsultAttachmentsRequest: [{meeting}]";
        }
    }
}
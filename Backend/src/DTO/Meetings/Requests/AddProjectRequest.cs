

namespace Backend.Meetings
{
    public class AddProjectRequest
    {
        public string meeting;
        
        public string project;

        public override string ToString()
        {
            return $"AddProjectRequest: [{meeting}, {project}]";
        }

    }
}
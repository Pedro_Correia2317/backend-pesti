

namespace Backend.Meetings
{
    public class RemoveTaskRequest
    {
        public string meeting;
        
        public string task;

        public override string ToString()
        {
            return $"RemoveTaskRequest: [{meeting}, {task}]";
        }

    }
}


namespace Backend.Meetings
{
    public class RemoveAttachmentRequest
    {
        public string meeting;
        
        public string attachment;

        public override string ToString()
        {
            return $"RemoveAttachmentRequest: [{meeting}, {attachment}]";
        }

    }
}
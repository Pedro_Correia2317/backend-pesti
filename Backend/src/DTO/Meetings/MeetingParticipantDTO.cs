

using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingParticipantDTO : IDTO
    {
        public string account;

        public MeetingParticipantTypeDTO participantType;

        public MeetingRoleDTO meetingRole;
        
    }
}


namespace Backend.Accounts
{
    public class CreateAccountRequest
    {
        public string entryDescription;
        public string name;
        public string email;
        public string type;

        public override string ToString()
        {
            return $"CreateAccountRequest: [{name}, {email}, {type}, {entryDescription}]";
        }
    }
}
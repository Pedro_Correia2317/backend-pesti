

namespace Backend.Accounts
{
    public class ChangeAccountStatusRequest
    {
        public string account;

        public string status;

        public string entryDescription;

        public override string ToString()
        {
            return $"ChangeAccountStatusRequest: [{account}, {status}, {entryDescription}]";
        }
    }
}
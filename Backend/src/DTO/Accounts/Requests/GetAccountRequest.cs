

namespace Backend.Accounts
{
    public class GetAccountRequest
    {
        public string id;

        public override string ToString()
        {
            return $"GetAccountRequest: [{id}]";
        }

    }
}
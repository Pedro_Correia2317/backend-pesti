

namespace Backend.Accounts
{
    public class UpdateAccountRequest
    {
        public string code;
        public string name;
        public string entryDescription;

        public override string ToString()
        {
            return $"UpdateAccountRequest: [{code}, {name}, {entryDescription}]";
        }
    }
}
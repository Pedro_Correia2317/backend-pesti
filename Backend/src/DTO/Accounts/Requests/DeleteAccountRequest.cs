

namespace Backend.Accounts
{
    public class DeleteAccountRequest
    {
        public string code;
        public string entryDescription;

        public override string ToString()
        {
            return $"DeleteAccountRequest: [{code}, {entryDescription}]";
        }
    }
}
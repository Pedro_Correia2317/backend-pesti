

using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountDTO : IDTO
    {
        public string id;

        public string name;

        public string email;

        public AccountStatusDTO status;

        public AccountTypeDTO type;
    }
}


using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountDetailsDTO : IDTO
    {
        public bool itExists;

        public bool isOwnerOfTenant;

        public bool hasLicense;

        public bool isActive; 

        public bool canCreateProjects; 

        public bool canDeleteProjects; 

        public bool canUpdateProjects; 

        public bool canCreateMeetings; 

        public bool canDeleteMeetings; 

        public bool canUpdateMeetings; 

        public AccountTypeDTO type;
    }
}
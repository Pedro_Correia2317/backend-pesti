

using Microsoft.AspNetCore.Mvc;

namespace Backend.HistoryEntries
{
    public class ConsultEntriesParameters
    {
        
        [FromQuery(Name = "page")]
        public int page { get; set; } = 1;
        
        [FromQuery(Name = "size")]
        public int size { get; set; } = 24;
        
        [FromQuery(Name = "sort")]
        public string sort { get; set; } = "";
        
        [FromQuery(Name = "order")]
        public string order { get; set; } = "asc";
        
        [FromQuery(Name = "search")]
        public string search { get; set; } = "";
        
        [FromQuery(Name = "filter")]
        public string[] filters { get; set; } = new string[] { "" };
        
        [FromQuery(Name = "condition")]
        public string condition { get; set; } = "and";
        
        [FromQuery(Name = "tenant")]
        public string tenant { get; set; } = "";
        
        [FromQuery(Name = "account")]
        public string account { get; set; } = "";

        public override string ToString()
        {
            return $"ConsultEntriesParameters: [{page}, {size}, {sort}, {order}, {search}, {filters}, {condition}, {tenant}, {account}]";
        }
        
    }
}
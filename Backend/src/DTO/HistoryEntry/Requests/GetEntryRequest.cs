

namespace Backend.HistoryEntries
{
    public class GetEntryRequest
    {
        public string id;

        public override string ToString()
        {
            return $"GetEntryRequest: [{id}]";
        }

    }
}


using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class EntryDTO : IDTO
    {

        public string id;

        public string description;

        public string date;

        public EntryTypeDTO type;

        public string address;

        public string tenant;

        public string account;
        
    }
}
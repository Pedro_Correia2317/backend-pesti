

namespace Backend.Utils
{
    public class GetPageResults<T> where T : IDTO
    {
        public bool success;

        public ErrorDTO[] notifications;

        public QueryDTO<T> results;
    }
    
    public class QueryDTO<T> where T : IDTO
    {
        public int totalNumber;

        public T[] results;
    }

}
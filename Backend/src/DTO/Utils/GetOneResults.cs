

namespace Backend.Utils
{
    public class GetOneResults<T> where T : IDTO
    {
        public bool finishedSuccesfully;

        public ErrorDTO[] notifications;

        public T result;
        
    }
}
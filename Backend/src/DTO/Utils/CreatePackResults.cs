

namespace Backend.Utils
{
    public class CreatePackResults<T> where T : IDTO
    {
        public bool success;

        public ErrorDTO[] notifications;

        public T[] result;
    }
}
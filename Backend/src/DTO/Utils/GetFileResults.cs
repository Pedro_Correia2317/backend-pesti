

using System.IO;

namespace Backend.Utils
{
    public class GetFileResults
    {
        public bool WasFinishedSuccessfully;

        public ErrorDTO[] Notifications;

        public Stream FileStream;

        public string FileName;
        
    }
}
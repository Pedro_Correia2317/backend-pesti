
using System;

namespace Backend.Utils
{
    public class CreateResults<T> where T : IDTO
    {
        public bool success;

        public ErrorDTO[] notifications;

        public T result;
        
    }
}
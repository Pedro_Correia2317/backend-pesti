

namespace Backend.Licenses
{
    public class DeleteLicenseRequest
    {
        public string code;

        public override string ToString()
        {
            return $"DeleteLicenseRequest: [{code}]";
        }
    }
}
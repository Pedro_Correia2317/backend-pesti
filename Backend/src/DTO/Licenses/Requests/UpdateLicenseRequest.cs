

namespace Backend.Licenses
{
    public class UpdateLicenseRequest
    {
        public string code;
        public int? numberMeetings;
        public int? remainingMeetings;
        public string licenseType;
        public string duration;

        public override string ToString()
        {
            return $"UpdateLicenseRequest: [{code}, {numberMeetings}, {remainingMeetings}, {licenseType}, {duration}]";
        }
    }
}
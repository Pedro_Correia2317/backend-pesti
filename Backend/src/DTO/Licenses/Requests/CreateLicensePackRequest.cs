

namespace Backend.Licenses
{
    public class CreateLicensePackRequest
    {

        public string entryDescription;
        public string tenant;
        public LicenseInCreatePack[] licenses = new LicenseInCreatePack[0];

        public override string ToString()
        {
            return $"CreateLicensePackRequest: [{tenant}, {licenses}, {entryDescription}]";
        }
        
    }

    public class LicenseInCreatePack
    {
        public string duration;
        public int? numberMeetings;
        public string licenseType;
        public int? quantity;

        public override string ToString()
        {
            return $"LicenseInCreatePack: [{duration}, {numberMeetings}, {licenseType}, {quantity}]";
        }
    }
}


namespace Backend.Licenses
{
    public class CreateLicenseRequest
    {
        public string date;
        public string duration;
        public int? numberMeetings;
        public string licenseType;
        public string account;
        public string entryDescription;

        public override string ToString()
        {
            return $"CreateLicenseRequest: [{date}, {duration}, {numberMeetings}, {licenseType}, {account}, {entryDescription}]";
        }
    }
}


namespace Backend.Licenses
{
    public class GetLicenseRequest
    {
        public string id;

        public override string ToString()
        {
            return $"GetLicenseRequest: [{id}]";
        }

    }
}
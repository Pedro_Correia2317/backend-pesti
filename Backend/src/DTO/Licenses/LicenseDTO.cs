

using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseDTO : IDTO
    {
        public string id;

        public string date;

        public string duration;

        public int numberMaxMeetings;

        public int numberRemainingMeetings;

        public string account;

        public LicenseTypeDTO licenseType;
    }
}


using Microsoft.AspNetCore.Http;
using Microsoft.Identity.Web;
using System;
using System.Linq;
using System.Security.Claims;

namespace Backend.Utils
{
    public class ControllerUtils
    {
        public static RequestInfo ObtainRequestInfo(HttpRequest request)
        {
            var claims = request.HttpContext.User.Claims;
            RequestInfo info = new RequestInfo();
            info.Address = request.HttpContext.Connection.RemoteIpAddress.ToString();
            info.AccountEmail = claims.First(x => x.Type == "upn").Value;
            return info;
        }
    }
}

using System;
using System.Threading.Tasks;
using Backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Projects
{
    [ApiController]
    [Route("projects")]
    [Authorize]
    public class ProjectsController : ControllerBase
    {
        private IProjectService Service;

        public ProjectsController(IProjectService service)
        {
            this.Service = service;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetByCode([FromServices] ConsultProjectServices services, string id)
        {
            GetProjectRequest request = new GetProjectRequest();
            request.id = id;
            GetOneResults<ProjectDTO> results = await this.Service.GetOne(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.finishedSuccesfully? Ok(results.result) : BadRequest(results.notifications);
        }

        [HttpGet]
        public async Task<ActionResult<ProjectDTO>> GetPage([FromServices] ConsultProjectServices services, [FromQuery] ConsultProjectsParameters param)
        {
            GetPageResults<ProjectDTO> results = await this.Service.GetTenantsProjectPage(services, param, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : BadRequest(results.notifications);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Create([FromServices] CreateProjectServices parameters, CreateProjectRequest request)
        {
            CreateResults<ProjectDTO> results = await this.Service.Create(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return CreatedAtAction(nameof(GetByCode), new { id = results.result.id }, results.result);
            }
            return UnprocessableEntity(results.notifications);
        }

        [HttpDelete("{code}")]
        public async Task<ActionResult> Delete([FromServices] DeleteProjectServices parameters, string code)
        {
            DeleteProjectRequest request = new DeleteProjectRequest();
            request.id = code;
            DeleteResults results = await this.Service.Delete(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            if(results.itExists){
                return UnprocessableEntity(results.notifications);
            }
            return NotFound(results.notifications);
        }

        [HttpPut("{code}")]
        public async Task<ActionResult> Update([FromServices] UpdateProjectServices services, UpdateProjectRequest request, string code)
        {
            request.id = code;
            UpdateResults results = await this.Service.Update(services, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            if(results.itExists){
                return UnprocessableEntity(results.notifications);
            }
            return NotFound(results.notifications);
        }
    }
}
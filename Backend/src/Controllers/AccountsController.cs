
using System;
using System.Threading.Tasks;
using Backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web.Resource;

namespace Backend.Accounts
{
    [ApiController]
    [Route("accounts")]
    [Authorize]
    public class AccountsController : ControllerBase
    {
        private IAccountService Service;

        public AccountsController(IAccountService service)
        {
            this.Service = service;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AccountDTO>> GetByCode([FromServices] ConsultAccountServices services, string id)
        {
            GetAccountRequest request = new GetAccountRequest();
            request.id = id;
            GetOneResults<AccountDTO> results = await this.Service.GetOne(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.finishedSuccesfully? Ok(results.result) : BadRequest(results.notifications);
        }

        [HttpGet]
        public async Task<ActionResult<AccountDTO>> GetPage([FromServices] ConsultAccountServices services, [FromQuery] ConsultAccountsParameters param)
        {
            GetPageResults<AccountDTO> results = await this.Service.GetPage(services, param, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok(results.results);
            }
            return BadRequest(results.notifications);
        }


        [HttpGet("/accountDetails")]
        public async Task<ActionResult<AccountDetailsDTO>> GetAccountDetails([FromServices] GetAccountDetailsServices services)
        {
            GetOneResults<AccountDetailsDTO> results = await this.Service.GetAccountDetails(services, ControllerUtils.ObtainRequestInfo(Request));
            return results.finishedSuccesfully? Ok(results.result) : BadRequest(results.notifications);
        }

        [HttpPost]
        public async Task<ActionResult<AccountDTO>> Create([FromServices] CreateAccountServices parameters, CreateAccountRequest request)
        {
            CreateResults<AccountDTO> results = await this.Service.Create(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success)
            {
                return CreatedAtAction(nameof(GetByCode), new { id = results.result.id }, results.result);
            }
            return UnprocessableEntity(results.notifications);
        }

        [HttpDelete("{code}")]
        public async Task<ActionResult> Delete([FromServices] DeleteAccountServices parameters, DeleteAccountRequest request, string code)
        {
            request.code = code;
            DeleteResults results = await this.Service.Delete(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            if(results.itExists){
                return UnprocessableEntity(results.notifications);
            }
            return NotFound(results.notifications);
        }

        [HttpPut("{code}")]
        public async Task<ActionResult> Update([FromServices] UpdateAccountServices services, UpdateAccountRequest request, string code)
        {
            request.code = code;
            UpdateResults results = await this.Service.Update(services, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            if(results.itExists){
                return UnprocessableEntity(results.notifications);
            }
            return NotFound(results.notifications);
        }

        [HttpPut("/accountStatus")]
        public async Task<ActionResult> ChangeStatus([FromServices] ChangeAccountStatusServices services, ChangeAccountStatusRequest request)
        {
            StandardResults results = await this.Service.ChangeStatus(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpGet("/accountTypes")]
        public ActionResult GetTypes([FromServices] IAccountDTOFactory dtoFactory, [FromServices] ITranslatorService translator)
        {
            AccountTypeDTO[] dtoArray = this.Service.GetAccountTypes(dtoFactory, translator);
            return Ok(dtoArray);
        }

        [HttpGet("/accountStatus")]
        public ActionResult GetStatus([FromServices] IAccountDTOFactory dtoFactory, [FromServices] ITranslatorService translator)
        {
            AccountStatusDTO[] dtoArray = this.Service.GetAccountStatuses(dtoFactory, translator);
            return Ok(dtoArray);
        }
    }
}
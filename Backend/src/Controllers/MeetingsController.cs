

using System;
using System.Threading.Tasks;
using Backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Meetings
{
    [ApiController]
    [Route("meetings")]
    [Authorize]
    public class MeetingsController : ControllerBase
    {
        private IMeetingService Service;

        public MeetingsController(IMeetingService service)
        {
            this.Service = service;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MeetingDTO>> GetByCode([FromServices] ConsultMeetingServices services, string id)
        {
            GetMeetingRequest request = new GetMeetingRequest();
            request.id = id;
            GetOneResults<MeetingDTO> results = await this.Service.GetOne(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.finishedSuccesfully? Ok(results.result) : BadRequest(results.notifications);
        }

        [HttpGet]
        public async Task<ActionResult<MeetingDTO>> GetPage([FromServices] ConsultMeetingServices services, [FromQuery] ConsultMeetingsParameters param)
        {
            GetPageResults<MeetingDTO> results = await this.Service.GetTenantMeetingsPage(services, param, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : BadRequest(results.notifications);
        }

        [HttpPost]
        public async Task<ActionResult<MeetingDTO>> Create([FromServices] CreateMeetingServices parameters, CreateMeetingRequest request)
        {
            CreateResults<MeetingDTO> results = await this.Service.Create(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return CreatedAtAction(nameof(GetByCode), new { id = results.result.id }, results.result);
            }
            return UnprocessableEntity(results.notifications);
        }

        [HttpDelete("{code}")]
        public async Task<ActionResult> Delete([FromServices] DeleteMeetingServices parameters, string code)
        {
            DeleteMeetingRequest request = new DeleteMeetingRequest();
            request.id = code;
            DeleteResults results = await this.Service.Delete(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            return results.itExists? UnprocessableEntity(results.notifications) : NotFound(results.notifications);
        }

        [HttpPut("{code}")]
        public async Task<ActionResult> Update([FromServices] UpdateMeetingServices services, UpdateMeetingRequest request, string code)
        {
            request.id = code;
            UpdateResults results = await this.Service.Update(services, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            return results.itExists? UnprocessableEntity(results.notifications) : NotFound(results.notifications);
        }

        [HttpGet("/meetingRoles")]
        public ActionResult GetMeetingRoles([FromServices] IMeetingDTOFactory dtoFactory, [FromServices] ITranslatorService translator)
        {
            MeetingRoleDTO[] dtoArray = this.Service.GetRoles(dtoFactory, translator);
            return Ok(dtoArray);
        }

        [HttpGet("/meetingParticipantTypes")]
        public ActionResult GetMeetingParticipantTypes([FromServices] IMeetingDTOFactory dtoFactory, [FromServices] ITranslatorService translator)
        {
            MeetingParticipantTypeDTO[] dtoArray = this.Service.GetParticipantTypes(dtoFactory, translator);
            return Ok(dtoArray);
        }

        [HttpGet("/meetingAttachment/{code}")]
        public async Task<ActionResult> GetAttachments([FromServices] ConsultMeetingServices services, string code)
        {
            ConsultAttachmentsRequest request = new ConsultAttachmentsRequest();
            request.meeting = code;
            GetPageResults<MeetingAttachmentDTO> results = await this.Service.ConsultAttachments(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : UnprocessableEntity(results.notifications);
        }

        [HttpGet("/meetingAttachmentFile/{code}")]
        public async Task<ActionResult> GetAttachmentFile([FromServices] ConsultMeetingServices services, [FromQuery] GetAttachmentFileRequest request, string code)
        {
            request.meeting = code;
            GetFileResults results = await this.Service.GetAttachmentFile(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.WasFinishedSuccessfully? File(results.FileStream, "application/octet-stream", results.FileName)
                : UnprocessableEntity(results.Notifications);
        }

        [HttpPut("/meetingAttachment/{code}"), DisableRequestSizeLimit]
        public async Task<ActionResult> AddAttachment([FromServices] AddAttachmentServices services, IFormFile file, string code)
        {
            AddAttachmentRequest request = new AddAttachmentRequest();
            request.file = file;
            request.meeting = code;
            StandardResults results = await this.Service.AddAttachment(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpDelete("/meetingAttachment/{code}")]
        public async Task<ActionResult> RemoveAttachment([FromServices] RemoveAttachmentServices services, RemoveAttachmentRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.RemoveAttachment(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpPut("/nextMeeting/{code}")]
        public async Task<ActionResult> AddNextMeeting([FromServices] AddNextMeetingServices services, AddNextMeetingRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.AddNextMeeting(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpDelete("/nextMeeting/{code}")]
        public async Task<ActionResult> RemoveNextMeeting([FromServices] RemoveNextMeetingServices services, RemoveNextMeetingRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.RemoveNextMeeting(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpGet("/meetingParticipant/{code}")]
        public async Task<ActionResult> GetParticipants([FromServices] ConsultMeetingServices services, string code)
        {
            ConsultParticipantsRequest request = new ConsultParticipantsRequest();
            request.meeting = code;
            GetPageResults<MeetingParticipantDTO> results = await this.Service.ConsultParticipants(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : UnprocessableEntity(results.notifications);
        }

        [HttpPut("/meetingParticipant/{code}")]
        public async Task<ActionResult> AddParticipant([FromServices] AddParticipantServices services, AddParticipantRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.AddParticipant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpDelete("/meetingParticipant/{code}")]
        public async Task<ActionResult> RemoveParticipant([FromServices] RemoveParticipantServices services, RemoveParticipantRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.RemoveParticipant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpPut("/meetingProject/{code}")]
        public async Task<ActionResult> AddProject([FromServices] AddProjectServices services, AddProjectRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.AddProject(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpDelete("/meetingProject/{code}")]
        public async Task<ActionResult> RemoveProject([FromServices] RemoveProjectServices services, RemoveProjectRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.RemoveProject(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpGet("/meetingTask/{code}")]
        public async Task<ActionResult> GetTasks([FromServices] ConsultMeetingServices services, string code)
        {
            ConsultTasksRequest request = new ConsultTasksRequest();
            request.meeting = code;
            GetPageResults<MeetingTaskDTO> results = await this.Service.ConsultTask(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : UnprocessableEntity(results.notifications);
        }

        [HttpPut("/meetingTask/{code}")]
        public async Task<ActionResult> AddTask([FromServices] AddTaskServices services, AddTaskRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.AddTask(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpDelete("/meetingTask/{code}")]
        public async Task<ActionResult> RemoveTask([FromServices] RemoveTaskServices services, RemoveTaskRequest request, string code)
        {
            request.meeting = code;
            StandardResults results = await this.Service.RemoveTask(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }
    }
}

using System;
using System.Threading.Tasks;
using Backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Licenses
{
    [ApiController]
    [Route("licenses")]
    [Authorize]
    public class LicensesController : ControllerBase
    {
        private ILicenseService Service;

        public LicensesController(ILicenseService service)
        {
            this.Service = service;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<LicenseDTO>> GetByCode([FromServices] ConsultLicenseServices services, string id)
        {
            GetLicenseRequest request = new GetLicenseRequest();
            request.id = id;
            GetOneResults<LicenseDTO> results = await this.Service.GetOne(services, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.finishedSuccesfully){
                return Ok(results.result);
            }
            return BadRequest(results.notifications);
        }

        [HttpGet]
        public async Task<ActionResult<LicenseDTO>> GetPage([FromServices] ConsultLicenseServices services, [FromQuery] ConsultLicensesParameters param)
        {
            GetPageResults<LicenseDTO> results = await this.Service.GetPage(services, param, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok(results.results);
            }
            return BadRequest(results.notifications);
        }

        [HttpGet("/licenseTypes")]
        public ActionResult GetLicenseTypes([FromServices] ILicenseDTOFactory dtoFactory, [FromServices] ITranslatorService translator)
        {
            LicenseTypeDTO[] dtoArray = this.Service.GetLicenseTypes(dtoFactory, translator);
            return Ok(dtoArray);
        }

        [HttpPost]
        public async Task<ActionResult<LicenseDTO>> Create([FromServices] CreateLicenseServices parameters, CreateLicenseRequest request)
        {
            CreateResults<LicenseDTO> results = await this.Service.Create(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return CreatedAtAction(nameof(GetByCode), new { id = results.result.id }, results.result);
            }
            return UnprocessableEntity(results.notifications);
        }

        [HttpPost("/licensesPack")]
        public async Task<ActionResult<LicenseDTO>> CreatePack([FromServices] CreateLicensePackServices parameters, CreateLicensePackRequest request)
        {
            CreatePackResults<LicenseDTO> results = await this.Service.CreatePack(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok(results.result);
            }
            return UnprocessableEntity(results.notifications);
        }

        [HttpDelete("{code}")]
        public async Task<ActionResult> Delete([FromServices] DeleteLicenseServices parameters, DeleteLicenseRequest request, string code)
        {
            request.code = code;
            DeleteResults results = await this.Service.Delete(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            if(results.itExists){
                return UnprocessableEntity(results.notifications);
            }
            return NotFound(results.notifications);
        }

        [HttpPut("{code}")]
        public async Task<ActionResult> Update([FromServices] UpdateLicenseServices services, UpdateLicenseRequest request, string code)
        {
            request.code = code;
            UpdateResults results = await this.Service.Update(services, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            if(results.itExists){
                return UnprocessableEntity(results.notifications);
            }
            return NotFound(results.notifications);
        }
    }
}
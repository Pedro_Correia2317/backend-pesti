
using System;
using System.Threading.Tasks;
using Backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.HistoryEntries
{
    [ApiController]
    [Route("historyEntries")]
    [Authorize]
    public class HistoryEntriesController : ControllerBase
    {
        private IHistoryEntryService Service;

        public HistoryEntriesController(IHistoryEntryService service)
        {
            this.Service = service;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EntryDTO>> GetByCode([FromServices] ConsultEntryServices services, string id)
        {
            GetEntryRequest request = new GetEntryRequest();
            request.id = id;
            GetOneResults<EntryDTO> results = await this.Service.GetOne(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.finishedSuccesfully? Ok(results.result) : BadRequest(results.notifications);
        }

        [HttpGet]
        public async Task<ActionResult<EntryDTO>> GetPage([FromServices] ConsultEntryServices services, [FromQuery] ConsultEntriesParameters param)
        {
            GetPageResults<EntryDTO> results = await this.Service.GetPage(services, param, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : BadRequest(results.notifications);
        }

        [HttpGet("/entryTypes")]
        public ActionResult GetEntryTypes([FromServices] IHistoryEntryDTOFactory factory, [FromServices] ITranslatorService translator)
        {
            EntryTypeDTO[] dtoArray = this.Service.GetEntryTypes(factory, translator);
            return Ok(dtoArray);
        }
    }
}
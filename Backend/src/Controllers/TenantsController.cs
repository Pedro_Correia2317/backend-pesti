
using System;
using System.Threading.Tasks;
using Backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Tenants
{
    [ApiController]
    [Route("tenants")]
    [Authorize]
    public class TenantsController : ControllerBase
    {
        private ITenantService Service;

        public TenantsController(ITenantService service)
        {
            this.Service = service;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TenantDTO>> GetByCode([FromServices] ConsultTenantServices services, string id)
        {
            GetTenantRequest request = new GetTenantRequest();
            request.id = id;
            GetOneResults<TenantDTO> results = await this.Service.GetOne(services, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.finishedSuccesfully){
                return Ok(results.result);
            }
            return BadRequest(results.notifications);
        }

        [HttpGet]
        public async Task<ActionResult<TenantDTO>> GetPage([FromServices] ConsultTenantServices services, [FromQuery] ConsultTenantsParameters param)
        {
            GetPageResults<TenantDTO> results = await this.Service.GetPage(services, param, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : BadRequest(results.notifications);
        }

        [HttpPost]
        public async Task<ActionResult<TenantDTO>> Create([FromServices] CreateTenantServices parameters, CreateTenantRequest request)
        {
            CreateResults<TenantDTO> results = await this.Service.Create(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return CreatedAtAction(nameof(GetByCode), new { id = results.result.id }, results.result);
            }
            return UnprocessableEntity(results.notifications);
        }

        [HttpDelete("{code}")]
        public async Task<ActionResult> Delete([FromServices] DeleteTenantServices parameters, DeleteTenantRequest request, string code)
        {
            request.code = code;
            DeleteResults results = await this.Service.Delete(parameters, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            if(results.itExists){
                return UnprocessableEntity(results.notifications);
            }
            return NotFound(results.notifications);
        }

        [HttpPut("{code}")]
        public async Task<ActionResult> Update([FromServices] UpdateTenantServices services, UpdateTenantRequest request, string code)
        {
            request.id = code;
            UpdateResults results = await this.Service.Update(services, request, ControllerUtils.ObtainRequestInfo(Request));
            if(results.success){
                return Ok();
            }
            if(results.itExists){
                return UnprocessableEntity(results.notifications);
            }
            return NotFound(results.notifications);
        }

        [HttpGet("/tenantMembers/{code}")]
        public async Task<ActionResult> GetMembers([FromServices] ConsultTenantServices services, string code)
        {
            ConsultMembersOfSpecificTenantRequest request = new ConsultMembersOfSpecificTenantRequest();
            request.id = code;
            GetPageResults<TenantMemberDTO> results = await this.Service.ConsultMembersOfSpecificTenant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : UnprocessableEntity(results.notifications);
        }

        [HttpGet("/tenantMembers")]
        public async Task<ActionResult> GetMembersOfOwnTenant([FromServices] ConsultTenantServices services)
        {
            ConsultMembersOfOwnTenantRequest request = new ConsultMembersOfOwnTenantRequest();
            GetPageResults<TenantMemberDTO> results = await this.Service.ConsultMembersOfOwnTenant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : UnprocessableEntity(results.notifications);
        }

        [HttpPut("/tenantMembers/{code}")]
        public async Task<ActionResult> AddMember([FromServices] AddMemberServices services, AddMemberToSpecificTenantRequest request, string code)
        {
            request.tenant = code;
            StandardResults results = await this.Service.AddMemberToSpecificTenant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpPut("/tenantMembers")]
        public async Task<ActionResult> AddMemberToOwnTenant([FromServices] AddMemberServices services, AddMemberToOwnTenantRequest request)
        {
            StandardResults results = await this.Service.AddMemberToOwnTenant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpDelete("/tenantMembers/{code}")]
        public async Task<ActionResult> RemoveMember([FromServices] RemoveMemberServices services, RemoveMemberOfSpecificTenantRequest request, string code)
        {
            request.tenant = code;
            StandardResults results = await this.Service.RemoveMemberOfSpecificTenant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpDelete("/tenantMembers")]
        public async Task<ActionResult> RemoveMemberOfOwnTenant([FromServices] RemoveMemberServices services, RemoveMemberOfOwnTenantRequest request)
        {
            StandardResults results = await this.Service.RemoveMemberOfOwnTenant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpGet("/tenantLicenses/{code}")]
        public async Task<ActionResult> GetLicenses([FromServices] ConsultTenantServices services, string code)
        {
            ConsultLicensesOfSpecificTenantRequest request = new ConsultLicensesOfSpecificTenantRequest();
            request.id = code;
            GetPageResults<TenantLicenseDTO> results = await this.Service.ConsultLicensesOfSpecificTenant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : UnprocessableEntity(results.notifications);
        }

        [HttpGet("/tenantLicenses")]
        public async Task<ActionResult> GetLicensesOfOwnTenant([FromServices] ConsultTenantServices services)
        {
            ConsultLicensesOfOwnTenantRequest request = new ConsultLicensesOfOwnTenantRequest();
            GetPageResults<TenantLicenseDTO> results = await this.Service.ConsultLicensesOfOwnTenant(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok(results.results) : UnprocessableEntity(results.notifications);
        }

        [HttpPut("/membersLicense")]
        public async Task<ActionResult> AddLicenseToMember([FromServices] AddLicenseToMemberServices services, AddLicenseToMemberRequest request)
        {
            StandardResults results = await this.Service.AddLicenseToMember(services, request, ControllerUtils.ObtainRequestInfo(Request));
            return results.success? Ok() : UnprocessableEntity(results.notifications);
        }

        [HttpGet("/tenantPermissions")]
        public ActionResult GetTenantPermissions([FromServices] ITenantDTOFactory dtoFactory, [FromServices] ITranslatorService translator)
        {
            TenantPermissionsDTO[] dtoArray = this.Service.GetTenantPermissions(dtoFactory, translator);
            return Ok(dtoArray);
        }
    }
}
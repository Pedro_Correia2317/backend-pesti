

using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public interface IHistoryEntryService
    {

        Task<GetOneResults<EntryDTO>> GetOne(ConsultEntryServices services, GetEntryRequest request, RequestInfo info);

        Task<GetPageResults<EntryDTO>> GetPage(ConsultEntryServices services, ConsultEntriesParameters request, RequestInfo info);

        EntryTypeDTO[] GetEntryTypes(IHistoryEntryDTOFactory factory, ITranslatorService translator);
    }
}
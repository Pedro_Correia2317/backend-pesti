

using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class HistoryEntryServiceImpl : IHistoryEntryService
    {

        public async Task<GetOneResults<EntryDTO>> GetOne(ConsultEntryServices services, GetEntryRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                EntryId id = services.Collector.Analyse(services.Factory.NewId(request.id));
                HistoryEntry entry = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    EntryDTO dto = services.DTOFactory.CreateBasicDTO(entry);
                    dto = services.Translator.Translate(dto);
                    return services.UtilsDTOFactory.CreateGetOneResultsDTO(dto);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetOneResultsDTO<EntryDTO>(errors);
        }

        public async Task<GetPageResults<EntryDTO>> GetPage(ConsultEntryServices services, ConsultEntriesParameters request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                EntryQueryParams queryParams = services.DTOFactory.CreateQueryParamsOf(request);
                int numberElements;
                HistoryEntry[] entries = services.Collector.Analyse(await services.Repository.FindByPage(queryParams), out numberElements);
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    EntryDTO[] listDTO = services.DTOFactory.CreateListOfBasicDTO(entries);
                    listDTO = services.Translator.Translate(listDTO);
                    return services.UtilsDTOFactory.CreateGetPageResultsDTO(listDTO, numberElements);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<EntryDTO>(errors);
        }
        public EntryTypeDTO[] GetEntryTypes(IHistoryEntryDTOFactory factory, ITranslatorService translator)
        {
            EntryType[] types = EntryTypes.GetAll();
            EntryTypeDTO[] arrayDTO = factory.CreateListOfTypeDTO(types);
            return translator.Translate(arrayDTO);
        }
    }
}

using Backend.Accounts;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class ConsultEntryServices
    {

        public IErrorCollector Collector;

        public IHistoryEntryDTOFactory DTOFactory;

        public IHistoryEntryFactory Factory;

        public IHistoryEntryRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUtilsDTOFactory UtilsDTOFactory;

        public ConsultEntryServices(IErrorCollector collector, IHistoryEntryDTOFactory dTOFactory,
                IHistoryEntryFactory factory, IHistoryEntryRepository repository, IAccountFactory accountFactory,
                IAccountRepository accountRepository, ITranslatorService translator, ILoggerService logger,
                IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            Translator = translator;
            Logger = logger;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
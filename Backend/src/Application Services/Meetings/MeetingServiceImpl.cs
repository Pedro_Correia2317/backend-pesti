
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Licenses;
using Backend.Projects;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingServiceImpl : IMeetingService
    {

        public MeetingRoleDTO[] GetRoles(IMeetingDTOFactory factory, ITranslatorService translator)
        {
            MeetingRole[] roles = MeetingRoles.GetAll();
            MeetingRoleDTO[] arrayDTO = factory.CreateListOfRolesDTO(roles);
            return translator.Translate(arrayDTO);
        }

        public MeetingParticipantTypeDTO[] GetParticipantTypes(IMeetingDTOFactory factory, ITranslatorService translator)
        {
            MeetingParticipantType[] types = MeetingParticipantTypes.GetAll();
            MeetingParticipantTypeDTO[] arrayDTO = factory.CreateListOfParticipantTypesDTO(types);
            return translator.Translate(arrayDTO);
        }

        public async Task<CreateResults<MeetingDTO>> Create(CreateMeetingServices services, CreateMeetingRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            Meeting meeting = await this.CreateMeeting(services, request, info);
            if (await services.Collector.IfZeroErrorsDo(() => services.Repository.Save(meeting))
                && services.Collector.Analyse(await services.UnitOfWork.Commit()))
            {
                MeetingDTO dto = services.DTOFactory.CreateBasicDTO(meeting);
                services.Logger.logInfo("Successfully processed request " + request);
                return services.UtilsDTOFactory.CreateResultsDTO(dto);
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateResultsDTO<MeetingDTO>(errors);
        }

        private async Task<Meeting> CreateMeeting(CreateMeetingServices services, CreateMeetingRequest request, RequestInfo info)
        {
            MeetingContainer container = services.Factory.NewContainer();
            string id = services.Collector.Analyse(services.CodeGenerator.NewMeetingCode());
            container.Id = services.Collector.Analyse(services.Factory.NewId(id));
            container.StartDate = services.Collector.Analyse(services.Factory.NewStartDate(request.startDate));
            container.EndDate = services.Collector.Analyse(services.Factory.NewEndDate(request.endDate));
            container.Description = services.Collector.Analyse(services.Factory.NewDescription(request.description));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            container.Creator = account?.BusinessId;
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.TenantRepository.FindByMember(container.Creator));
            services.Collector.IfZeroErrorsDo(() => tenant.HasPermissionsToCreateMeeting(container.Creator));
            container.Tenant = tenant?.BusinessId;
            License license = await services.Collector.IfZeroErrorsDo(() => services.LicenseRepository.FindByAccount(container.Creator));
            services.Collector.IfZeroErrorsDo(() => license.DecreaseOneMeeting());
            return services.Collector.IfZeroErrorsDo(() => services.Factory.NewMeeting(container));
        }

        public async Task<DeleteResults> Delete(DeleteMeetingServices services, DeleteMeetingRequest request, RequestInfo info)
        {
            bool projectExists = false;
            services.Logger.logInfo("Processing request " + request);
            AccountEmail accountEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(accountEmail));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.TenantRepository.FindByMember(account.BusinessId));
            MeetingId id = services.Collector.IfZeroErrorsDo(() => services.Factory.NewId(request.id));
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors()
                && services.Collector.Analyse(meeting.HasTenant(tenant.BusinessId))
                && services.Collector.Analyse(tenant.HasPermissionsToDeleteMeeting(account.BusinessId)))
            {
                projectExists = true;
                if (services.Collector.Analyse(await services.Repository.Delete(meeting))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateDeleteResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateDeleteResultsDTO(errors, projectExists);
        }

        public async Task<GetOneResults<MeetingDTO>> GetOne(ConsultMeetingServices services, GetMeetingRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.id));
                Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    MeetingDTO dto = services.DTOFactory.CreateBasicDTO(meeting);
                    return services.UtilsDTOFactory.CreateGetOneResultsDTO(dto);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetOneResultsDTO<MeetingDTO>(errors);
        }

        public async Task<GetPageResults<MeetingDTO>> GetPage(ConsultMeetingServices services, ConsultMeetingsParameters request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                QueryParams queryParams = services.DTOFactory.CreateQueryParamsOf(request);
                int numberElements;
                Meeting[] meetings = services.Collector.Analyse(await services.Repository.FindByPage(queryParams), out numberElements);
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    MeetingDTO[] listDTO = services.DTOFactory.CreateListOfBasicDTO(meetings);
                    return services.UtilsDTOFactory.CreateGetPageResultsDTO(listDTO, numberElements);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<MeetingDTO>(errors);
        }

        public async Task<GetPageResults<MeetingDTO>> GetTenantMeetingsPage(ConsultMeetingServices services, ConsultMeetingsParameters request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            QueryParams queryParams = services.DTOFactory.CreateQueryParamsOf(request);
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            if (services.Collector.Analyse(account.Status.IsActive()))
            {
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.TenantRepository.FindByMember(account.BusinessId));
                if (services.Collector.HasNoErrors())
                {
                    int numberElements;
                    Meeting[] meetings = services.Collector.Analyse(await services.Repository.FindByPageWithTenant(queryParams, tenant), out numberElements);
                    if (services.Collector.HasNoErrors())
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        MeetingDTO[] listDTO = services.DTOFactory.CreateListOfBasicDTO(meetings);
                        return services.UtilsDTOFactory.CreateGetPageResultsDTO(listDTO, numberElements);
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<MeetingDTO>(errors);
        }

        public async Task<UpdateResults> Update(UpdateMeetingServices services, UpdateMeetingRequest request, RequestInfo info)
        {
            bool projectExists = false;
            services.Logger.logInfo("Processing request " + request);
            AccountEmail accountEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(accountEmail));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.TenantRepository.FindByMember(account.BusinessId));
            MeetingId id = services.Collector.IfZeroErrorsDo(() => services.Factory.NewId(request.id));
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors()
                && services.Collector.Analyse(meeting.HasTenant(tenant.BusinessId))
                && services.Collector.Analyse(tenant.HasPermissionsToUpdateMeeting(account.BusinessId)))
            {
                projectExists = true;
                ServiceUtils.UpdateData(services.Collector, request.recordingLink, services.Factory.NewRecordingLink, x => meeting.AddRecordingLink(x));
                ServiceUtils.UpdateData(services.Collector, request.transcriptLink, services.Factory.NewTranscriptLink, x => meeting.AddTranscriptLink(x));
                ServiceUtils.UpdateData(services.Collector, request.dependencies, services.Factory.NewDependencies, x => meeting.AddDependencies(x));
                ServiceUtils.UpdateData(services.Collector, request.nextSteps, services.Factory.NewNextSteps, x => meeting.AddNextSteps(x));
                ServiceUtils.UpdateData(services.Collector, request.decisions, services.Factory.NewDecisions, x => meeting.AddDecisions(x));
                ServiceUtils.UpdateData(services.Collector, request.nextMeeting, services.Factory.NewId, x => meeting.AddNextMeeting(x));
                if (await services.Collector.IfZeroErrorsDo(() => services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateUpdateResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateUpdateResultsDTO(errors, projectExists);
        }

        public async Task<StandardResults> AddProject(AddProjectServices services, AddProjectRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            ProjectId pId = services.Collector.IfZeroErrorsDo(() => services.ProjectFactory.NewId(request.project));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            await services.Collector.IfZeroErrorsDo(() => services.ProjectRepository.FindById(pId));
            if (services.Collector.HasNoErrors())
            {
                Tenant tenant = services.Collector.Analyse(await services.TenantRepository.FindById(meeting.Tenant));
                if (services.Collector.IfZeroErrorsDo(() => tenant.HasPermissionsToUpdateMeeting(account.BusinessId))
                    && services.Collector.Analyse(meeting.AddProject(pId))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> RemoveProject(RemoveProjectServices services, RemoveProjectRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors())
            {
                Tenant tenant = services.Collector.Analyse(await services.TenantRepository.FindById(meeting.Tenant));
                if (services.Collector.IfZeroErrorsDo(() => tenant.HasPermissionsToUpdateMeeting(account.BusinessId))
                    && services.Collector.Analyse(meeting.RemoveProject())
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> AddParticipant(AddParticipantServices services, AddParticipantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail requesterEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            AccountEmail participantEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(request.participant));
            Account requesterAccount = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(requesterEmail));
            Account participantAccount = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(participantEmail));
            services.Collector.IfZeroErrorsDo(() => requesterAccount.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors())
            {
                Tenant tenant = services.Collector.Analyse(await services.TenantRepository.FindById(meeting.Tenant));
                MeetingParticipantType type = services.Collector.Analyse(services.Factory.NewParticipantType(request.type));
                MeetingRole role = services.Collector.Analyse(services.Factory.NewRole(request.role));
                MeetingParticipant participant = services.Collector.Analyse(services.Factory.NewParticipant(participantAccount.BusinessId, type, role));
                if (services.Collector.IfZeroErrorsDo(() => tenant.HasPermissionsToUpdateMeeting(requesterAccount.BusinessId))
                    && services.Collector.Analyse(meeting.AddParticipant(participant))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> RemoveParticipant(RemoveParticipantServices services, RemoveParticipantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail requesterEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            AccountEmail participantEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(request.participant));
            Account requesterAccount = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(requesterEmail));
            Account participantAccount = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(participantEmail));
            services.Collector.IfZeroErrorsDo(() => requesterAccount.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors())
            {
                Tenant tenant = services.Collector.Analyse(await services.TenantRepository.FindById(meeting.Tenant));
                if (services.Collector.IfZeroErrorsDo(() => tenant.HasPermissionsToUpdateMeeting(requesterAccount.BusinessId))
                    && services.Collector.Analyse(meeting.RemoveParticipant(participantAccount.BusinessId))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> AddAttachment(AddAttachmentServices services, AddAttachmentRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            byte[] fileBytes = services.Collector.Analyse(services.Analyser.GetBytesFromFile(request.file));
            string name = services.Collector.Analyse(services.Analyser.GetNameFromFile(request.file));
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            MeetingAttachment attachment = services.Collector.IfZeroErrorsDo(() => services.Factory.NewAttachment(fileBytes, name));
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors())
            {
                License license = services.Collector.Analyse(await services.LicenseRepository.FindByAccount(meeting.Creator));
                if (services.Collector.IfZeroErrorsDo(() => meeting.HasParticipant(account.BusinessId))
                    && services.Collector.Analyse(meeting.HasLessAttachmentsThan(license.Type.GetNumberOfAttachmentsAllowed()))
                    && services.Collector.Analyse(meeting.AddAttachment(attachment))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> RemoveAttachment(RemoveAttachmentServices services, RemoveAttachmentRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.IfZeroErrorsDo(() => meeting.HasParticipant(account.BusinessId))
                && services.Collector.Analyse(meeting.RemoveAttachment(request.attachment))
                && services.Collector.Analyse(await services.UnitOfWork.Commit()))
            {
                services.Logger.logInfo("Successfully processed request " + request);
                return services.UtilsDTOFactory.CreateStandardResultsDTO();
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> AddTask(AddTaskServices services, AddTaskRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            TaskName taskN = services.Collector.Analyse(services.Factory.NewTaskName(request.name));
            TaskDescription taskD = services.Collector.Analyse(services.Factory.NewTaskDescription(request.description));
            TaskStartDate taskSD = services.Collector.Analyse(services.Factory.NewTaskStartDate(request.startDate));
            TaskEndDate taskED = services.Collector.Analyse(services.Factory.NewTaskEndDate(request.endDate));
            TaskExpectedEndDate taskEED = services.Collector.Analyse(services.Factory.NewTaskExpectedEndDate(request.expectedEndDate));
            AccountId taskR = services.Collector.Analyse(services.AccountFactory.NewId(request.responsible));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindById(taskR));
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors())
            {
                MeetingTask task = services.Collector.Analyse(services.Factory.NewTask(taskN, taskD, taskSD, taskED, taskEED, taskR));
                if (services.Collector.IfZeroErrorsDo(() => meeting.HasParticipant(account.BusinessId))
                    && services.Collector.Analyse(meeting.AddTask(task))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> RemoveTask(RemoveTaskServices services, RemoveTaskRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            TaskName taskName = services.Collector.Analyse(services.Factory.NewTaskName(request.task));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.IfZeroErrorsDo(() => meeting.HasParticipant(account.BusinessId))
                && services.Collector.Analyse(meeting.RemoveTask(taskName))
                && services.Collector.Analyse(await services.UnitOfWork.Commit()))
            {
                services.Logger.logInfo("Successfully processed request " + request);
                return services.UtilsDTOFactory.CreateStandardResultsDTO();
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> AddNextMeeting(AddNextMeetingServices services, AddNextMeetingRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            MeetingId nextMeetingId = services.Collector.Analyse(services.Factory.NewId(request.nextmeeting));
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(nextMeetingId));
            if (services.Collector.HasNoErrors())
            {
                Tenant tenant = services.Collector.Analyse(await services.TenantRepository.FindById(meeting.Tenant));
                if (services.Collector.IfZeroErrorsDo(() => tenant.HasPermissionsToUpdateMeeting(account.BusinessId))
                    && services.Collector.Analyse(meeting.AddNextMeeting(nextMeetingId))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> RemoveNextMeeting(RemoveNextMeetingServices services, RemoveNextMeetingRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors())
            {
                Tenant tenant = services.Collector.Analyse(await services.TenantRepository.FindById(meeting.Tenant));
                if (services.Collector.IfZeroErrorsDo(() => tenant.HasPermissionsToUpdateMeeting(account.BusinessId))
                    && services.Collector.Analyse(meeting.RemoveNextMeeting())
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<GetPageResults<MeetingParticipantDTO>> ConsultParticipants(ConsultMeetingServices services, ConsultParticipantsRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors() && services.Collector.Analyse(meeting.HasParticipant(account.BusinessId)))
            {
                services.Logger.logInfo("Successfully processed request " + request);
                MeetingParticipantDTO[] dtoList = services.DTOFactory.CreateListOfParticipantsDTO(meeting.Participants.ToCollection());
                return services.UtilsDTOFactory.CreateGetPageResultsDTO(dtoList, dtoList.Length);
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<MeetingParticipantDTO>(errors);
        }

        public async Task<GetPageResults<MeetingAttachmentDTO>> ConsultAttachments(ConsultMeetingServices services, ConsultAttachmentsRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors() && services.Collector.Analyse(meeting.HasParticipant(account.BusinessId)))
            {
                services.Logger.logInfo("Successfully processed request " + request);
                MeetingAttachmentDTO[] dtoList = services.DTOFactory.CreateListOfAttachmentsDTO(meeting.Attachments.ToCollection());
                return services.UtilsDTOFactory.CreateGetPageResultsDTO(dtoList, dtoList.Length);
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<MeetingAttachmentDTO>(errors);
        }

        public async Task<GetPageResults<MeetingTaskDTO>> ConsultTask(ConsultMeetingServices services, ConsultTasksRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors() && services.Collector.Analyse(meeting.HasParticipant(account.BusinessId)))
            {
                services.Logger.logInfo("Successfully processed request " + request);
                MeetingTaskDTO[] dtoList = services.DTOFactory.CreateListOfTasksDTO(meeting.Tasks.ToCollection());
                return services.UtilsDTOFactory.CreateGetPageResultsDTO(dtoList, dtoList.Length);
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<MeetingTaskDTO>(errors);
        }

        public async Task<GetFileResults> GetAttachmentFile(ConsultMeetingServices services, GetAttachmentFileRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            MeetingId id = services.Collector.Analyse(services.Factory.NewId(request.meeting));
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Meeting meeting = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            MeetingAttachment attachment = services.Collector.IfZeroErrorsDo(() => meeting.GetAttachmentWithName(request.fileName));
            if (services.Collector.HasNoErrors() && services.Collector.Analyse(meeting.HasParticipant(account.BusinessId)))
            {
                services.Logger.logInfo("Successfully processed request " + request);
                return services.UtilsDTOFactory.CreateGetFileResults(attachment.File, attachment.Name);
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetFileResults(errors);
        }
    }
}
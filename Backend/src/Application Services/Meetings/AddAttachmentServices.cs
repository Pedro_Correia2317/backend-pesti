

using Backend.Accounts;
using Backend.Licenses;
using Backend.Utils;

namespace Backend.Meetings
{
    public class AddAttachmentServices
    {
        public IErrorCollector Collector;

        public ILoggerService Logger;

        public ITranslatorService Translator;

        public IMeetingFactory Factory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ILicenseRepository LicenseRepository;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public IRequestAnalyser Analyser;

        public AddAttachmentServices(IErrorCollector collector, ILoggerService logger, ITranslatorService translator,
                IMeetingFactory factory, IMeetingRepository repository, IAccountFactory accountFactory,
                ILicenseRepository licenseRepository, IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory,
                IRequestAnalyser analyser, IAccountRepository accountRepository)
        {
            Collector = collector;
            Logger = logger;
            Translator = translator;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            LicenseRepository = licenseRepository;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
            Analyser = analyser;
        }
    }
}


using Backend.Accounts;
using Backend.Projects;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class AddProjectServices
    {
        public IErrorCollector Collector;

        public ILoggerService Logger;

        public ITranslatorService Translator;

        public IMeetingFactory Factory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public IProjectFactory ProjectFactory;

        public IProjectRepository ProjectRepository;

        public ITenantRepository TenantRepository;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public AddProjectServices(IErrorCollector collector, ILoggerService logger, ITranslatorService translator,
                IMeetingFactory factory, IMeetingRepository repository, IAccountFactory accountFactory,
                IProjectFactory pFactory, IProjectRepository projectRepository, ITenantRepository tenantRepository,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory, IAccountRepository accountRepository)
        {
            Collector = collector;
            Logger = logger;
            Translator = translator;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            ProjectFactory = pFactory;
            ProjectRepository = projectRepository;
            TenantRepository = tenantRepository;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
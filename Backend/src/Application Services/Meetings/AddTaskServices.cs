

using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public class AddTaskServices
    {
        public IErrorCollector Collector;

        public ILoggerService Logger;

        public ITranslatorService Translator;

        public IMeetingFactory Factory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public AddTaskServices(IErrorCollector collector, ILoggerService logger, ITranslatorService translator,
                IMeetingFactory factory, IMeetingRepository repository, IAccountFactory accountFactory,
                IAccountRepository accountRepository, IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Logger = logger;
            Translator = translator;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
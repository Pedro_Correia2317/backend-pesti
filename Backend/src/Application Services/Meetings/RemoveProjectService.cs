

using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class RemoveProjectServices
    {
        public IErrorCollector Collector;

        public ILoggerService Logger;

        public ITranslatorService Translator;

        public IMeetingFactory Factory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITenantRepository TenantRepository;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public RemoveProjectServices(IErrorCollector collector, ILoggerService logger, ITranslatorService translator,
                IMeetingFactory factory, IMeetingRepository repository, IAccountFactory accountFactory,
                ITenantRepository tenantRepository, IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory,
                IAccountRepository accountRepository)
        {
            Collector = collector;
            Logger = logger;
            Translator = translator;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            TenantRepository = tenantRepository;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}


using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class UpdateMeetingServices
    {

        public IErrorCollector Collector;

        public IMeetingFactory Factory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITenantRepository TenantRepository;

        public ITranslatorService Translator;

        public ICodeGeneratorService CodeGenerator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public UpdateMeetingServices(IErrorCollector collector, IMeetingFactory factory, IMeetingRepository repository,
                IAccountFactory accountFactory, IAccountRepository accountRepository, ITenantRepository tenantRepository,
                ITranslatorService translator, ICodeGeneratorService codeGenerator, ILoggerService logger,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            TenantRepository = tenantRepository;
            Translator = translator;
            CodeGenerator = codeGenerator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
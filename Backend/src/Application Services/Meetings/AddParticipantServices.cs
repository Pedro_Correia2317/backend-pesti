

using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class AddParticipantServices
    {
        public IErrorCollector Collector;

        public ILoggerService Logger;

        public ITranslatorService Translator;

        public IMeetingFactory Factory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITenantRepository TenantRepository;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public AddParticipantServices(IErrorCollector collector, ILoggerService logger, ITranslatorService translator,
                IMeetingFactory factory, IMeetingRepository repository, IAccountFactory accountFactory,
                IAccountRepository accountRepository, ITenantRepository tenantRepository, IUnitOfWork unitOfWork,
                IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Logger = logger;
            Translator = translator;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            TenantRepository = tenantRepository;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
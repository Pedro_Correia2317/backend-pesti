

using Backend.Accounts;
using Backend.Utils;

namespace Backend.Meetings
{
    public class RemoveAttachmentServices
    {
        public IErrorCollector Collector;

        public ILoggerService Logger;

        public ITranslatorService Translator;

        public IMeetingFactory Factory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public RemoveAttachmentServices(IErrorCollector coll, ILoggerService logger, ITranslatorService translator,
                IMeetingFactory factory, IMeetingRepository repository, IAccountFactory accountFactory,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory, IAccountRepository accountRepository)
        {
            Collector = coll;
            Logger = logger;
            Translator = translator;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}


using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class ConsultMeetingServices
    {

        public IErrorCollector Collector;

        public IMeetingDTOFactory DTOFactory;

        public IMeetingFactory Factory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITenantRepository TenantRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUtilsDTOFactory UtilsDTOFactory;

        public ConsultMeetingServices(IErrorCollector coll, IMeetingDTOFactory dTOFactory, IMeetingFactory factory,
                IMeetingRepository repository, IAccountFactory accountFactory, ITenantRepository tenantRepository,
                ITranslatorService translator, ILoggerService logger, IUtilsDTOFactory utilsDTOFactory,
                IAccountRepository accountRepository)
        {
            Collector = coll;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            TenantRepository = tenantRepository;
            Translator = translator;
            Logger = logger;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
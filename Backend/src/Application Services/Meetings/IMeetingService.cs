

using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.Meetings
{
    public interface IMeetingService
    {
        MeetingRoleDTO[] GetRoles(IMeetingDTOFactory factory, ITranslatorService translator);

        MeetingParticipantTypeDTO[] GetParticipantTypes(IMeetingDTOFactory factory, ITranslatorService translator);

        Task<CreateResults<MeetingDTO>> Create(CreateMeetingServices services, CreateMeetingRequest request, RequestInfo info);

        Task<GetOneResults<MeetingDTO>> GetOne(ConsultMeetingServices services, GetMeetingRequest request, RequestInfo info);

        Task<GetPageResults<MeetingDTO>> GetPage(ConsultMeetingServices services, ConsultMeetingsParameters request, RequestInfo info);

        Task<GetPageResults<MeetingDTO>> GetTenantMeetingsPage(ConsultMeetingServices services, ConsultMeetingsParameters request, RequestInfo info);
        
        Task<DeleteResults> Delete(DeleteMeetingServices services, DeleteMeetingRequest request, RequestInfo info);

        Task<UpdateResults> Update(UpdateMeetingServices services, UpdateMeetingRequest request, RequestInfo info);

        Task<StandardResults> AddProject(AddProjectServices services, AddProjectRequest request, RequestInfo info);

        Task<StandardResults> RemoveProject(RemoveProjectServices services, RemoveProjectRequest request, RequestInfo info);

        Task<StandardResults> AddParticipant(AddParticipantServices services, AddParticipantRequest request, RequestInfo info);

        Task<StandardResults> RemoveParticipant(RemoveParticipantServices services, RemoveParticipantRequest request, RequestInfo info);

        Task<GetPageResults<MeetingParticipantDTO>> ConsultParticipants(ConsultMeetingServices services, ConsultParticipantsRequest request, RequestInfo info);

        Task<StandardResults> AddAttachment(AddAttachmentServices services, AddAttachmentRequest request, RequestInfo info);

        Task<StandardResults> RemoveAttachment(RemoveAttachmentServices services, RemoveAttachmentRequest request, RequestInfo info);

        Task<GetPageResults<MeetingAttachmentDTO>> ConsultAttachments(ConsultMeetingServices services, ConsultAttachmentsRequest request, RequestInfo info);

        Task<GetFileResults> GetAttachmentFile(ConsultMeetingServices services, GetAttachmentFileRequest request, RequestInfo info);

        Task<StandardResults> AddTask(AddTaskServices services, AddTaskRequest request, RequestInfo info);

        Task<StandardResults> RemoveTask(RemoveTaskServices services, RemoveTaskRequest request, RequestInfo info);

        Task<GetPageResults<MeetingTaskDTO>> ConsultTask(ConsultMeetingServices services, ConsultTasksRequest request, RequestInfo info);

        Task<StandardResults> AddNextMeeting(AddNextMeetingServices services, AddNextMeetingRequest request, RequestInfo info);

        Task<StandardResults> RemoveNextMeeting(RemoveNextMeetingServices services, RemoveNextMeetingRequest request, RequestInfo info);
        
    }
}
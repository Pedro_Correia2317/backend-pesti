

using Backend.Accounts;
using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class CreateMeetingServices
    {

        public IErrorCollector Collector;

        public ILoggerService Logger;

        public IMeetingFactory Factory;

        public IMeetingDTOFactory DTOFactory;

        public IMeetingRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITenantRepository TenantRepository;

        public ILicenseRepository LicenseRepository;

        public IUtilsDTOFactory UtilsDTOFactory;

        public IUnitOfWork UnitOfWork;

        public ITranslatorService Translator;

        public ICodeGeneratorService CodeGenerator;

        public CreateMeetingServices(IErrorCollector collector, ILoggerService logger, IMeetingFactory factory,
                IMeetingDTOFactory dTOFactory, IMeetingRepository repository, IAccountFactory accountFactory,
                ITenantRepository tenantRepository, IUtilsDTOFactory utilsDTOFactory, IUnitOfWork unitOfWork,
                ILicenseRepository licenseRepo, ITranslatorService translator, ICodeGeneratorService codeGenerator,
                IAccountRepository accountRepository)
        {
            Collector = collector;
            Logger = logger;
            Factory = factory;
            DTOFactory = dTOFactory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            TenantRepository = tenantRepository;
            LicenseRepository = licenseRepo;
            UtilsDTOFactory = utilsDTOFactory;
            UnitOfWork = unitOfWork;
            Translator = translator;
            CodeGenerator = codeGenerator;
        }
    }
}
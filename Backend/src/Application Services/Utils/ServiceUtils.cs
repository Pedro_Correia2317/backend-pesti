

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Tenants;

namespace Backend.Utils
{
    public class ServiceUtils
    {
        public static HistoryEntry CreateEntry(IErrorCollector col, IHistoryEntryFactory fact,
                EntryType type, string desc, RequestInfo info, TenantId id)
        {
            var entryCreation = desc == null ? fact.NewEntry(type, info.Address, id) : fact.NewEntry(type, info.Address, desc, id);
            return col.Analyse(entryCreation);
        }
        public static HistoryEntry CreateEntry(IErrorCollector col, IHistoryEntryFactory fact,
                EntryType type, string desc, RequestInfo info, AccountId id)
        {
            var entryCreation = desc == null ? fact.NewEntry(type, info.Address, id) : fact.NewEntry(type, info.Address, desc, id);
            return col.Analyse(entryCreation);
        }

        public static HistoryEntry CreateEntry(IErrorCollector col, IHistoryEntryFactory fact,
                EntryType type, string desc, RequestInfo info, TenantId id, params object[] args)
        {
            var entryCreation = desc == null ? fact.NewEntry(type, info.Address, id, args) : fact.NewEntry(type, info.Address, desc, id);
            return col.Analyse(entryCreation);
        }

        public static HistoryEntry CreateEntry(IErrorCollector col, IHistoryEntryFactory fact,
                EntryType type, RequestInfo info, TenantId id, params object[] args)
        {
            return col.Analyse(fact.NewEntry(type, info.Address, id, args));
        }

        public static HistoryEntry CreateEntry(IErrorCollector col, IHistoryEntryFactory fact,
                EntryType type, string desc, RequestInfo info, AccountId id, params object[] args)
        {
            var entryCreation = desc == null ? fact.NewEntry(type, info.Address, id, args) : fact.NewEntry(type, info.Address, desc, id);
            return col.Analyse(entryCreation);
        }

        public static List<Error> LogAndTranslateErrors(IErrorCollector col, ILoggerService logger,
                ITranslatorService translator, string message)
        {
            List<Error> errors = col.ListErrors();
            logger.logAllNotificationsInfo(errors);
            errors = translator.TranslateAllMessages(errors);
            logger.logInfo(message);
            return errors;
        }

        public static bool UpdateData<A, T>(IErrorCollector col, A request,
                Func<A, Results<T>> createFunction, Func<T, T> setFunction)
        {
            bool hasChanged = false;
            if (request != null)
            {
                T updatedObject = col.Analyse(createFunction(request));
                if (col.HasNoErrors())
                {
                    setFunction(updatedObject);
                    hasChanged = true;
                }
            }
            return hasChanged;
        }

        public static bool UpdateData<A, T>(IErrorCollector col, A request,
                Func<A, Results<T>> createFunction, Func<T, Results<bool>> setFunction)
        {
            bool hasChanged = false;
            if (request != null)
            {
                T updatedObject = col.Analyse(createFunction(request));
                col.IfZeroErrorsDo(() => setFunction(updatedObject));
                if (col.HasNoErrors())
                {
                    hasChanged = true;
                }
            }
            return hasChanged;
        }

        public async static Task<bool> HasPermissions(IErrorCollector collector, IAccountFactory factory,
                IAccountRepository repository, RequestInfo info, Func<Account, Results<bool>> permissionsFunction)
        {
            AccountEmail email = collector.Analyse(factory.NewEmail(info.AccountEmail));
            Account adminAccount = await collector.IfZeroErrorsDo(() => repository.FindByEmail(email));
            collector.IfZeroErrorsDo(() => permissionsFunction(adminAccount));
            collector.IfZeroErrorsDo(() => adminAccount.Status.IsActive());
            return collector.HasNoErrors();
        }
    }
}
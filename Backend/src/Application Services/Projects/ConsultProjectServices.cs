

using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Projects
{
    public class ConsultProjectServices
    {

        public IErrorCollector Collector;

        public IProjectDTOFactory DTOFactory;

        public IProjectFactory Factory;

        public IProjectRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITenantRepository TenantRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUtilsDTOFactory UtilsDTOFactory;

        public ConsultProjectServices(IErrorCollector coll, IProjectDTOFactory dTOFactory, IProjectFactory factory,
                IProjectRepository repository, IAccountFactory accountFactory, IAccountRepository accountRepo,
                ITenantRepository tenantRepository, ITranslatorService translator, ILoggerService logger,
                IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = coll;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepo;
            TenantRepository = tenantRepository;
            Translator = translator;
            Logger = logger;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
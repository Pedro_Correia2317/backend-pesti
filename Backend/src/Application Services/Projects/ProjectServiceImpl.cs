

using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectServiceImpl : IProjectService
    {
        public async Task<CreateResults<ProjectDTO>> Create(CreateProjectServices services, CreateProjectRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            Project project = await this.CreateObject(services, request, info);
            if (await services.ErrorCollector.IfZeroErrorsDo(() => services.Repository.Save(project))
                && services.ErrorCollector.Analyse(await services.UnitOfWork.Commit()))
            {
                ProjectDTO dto = services.DTOFactory.CreateBasicDTO(project);
                services.Logger.logInfo("Successfully processed request " + request);
                return services.UtilsDTOFactory.CreateResultsDTO(dto);
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.ErrorCollector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateResultsDTO<ProjectDTO>(errors);
        }

        private async Task<Project> CreateObject(CreateProjectServices services, CreateProjectRequest request, RequestInfo info)
        {
            IErrorCollector collector = services.ErrorCollector;
            ProjectContainer container = services.Factory.NewContainer();
            string id = collector.Analyse(services.CodeGenerator.NewProjectCode());
            container.Id = collector.Analyse(services.Factory.NewId(id));
            container.Name = collector.Analyse(services.Factory.NewName(request.name));
            container.Description = collector.Analyse(services.Factory.NewDescription(request.description));
            AccountEmail accountEmail = collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(accountEmail));
            collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await collector.IfZeroErrorsDo(() => services.TenantRepository.FindByMember(account.BusinessId));
            collector.IfZeroErrorsDo(() => tenant.HasPermissionsToCreateProject(account.BusinessId));
            container.Tenant = tenant?.BusinessId;
            return collector.IfZeroErrorsDo(() => services.Factory.NewProject(container));
        }

        public async Task<DeleteResults> Delete(DeleteProjectServices services, DeleteProjectRequest request, RequestInfo info)
        {
            bool projectExists = false;
            services.Logger.logInfo("Processing request " + request);
            AccountEmail accountEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(accountEmail));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.TenantRepository.FindByMember(account.BusinessId));
            ProjectId id = services.Collector.IfZeroErrorsDo(() => services.Factory.NewId(request.id));
            Project project = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors() 
                && services.Collector.Analyse(project.HasTenant(tenant.BusinessId))
                && services.Collector.Analyse(tenant.HasPermissionsToDeleteProject(account.BusinessId)))
            {
                projectExists = true;
                if (services.Collector.Analyse(await services.Repository.Delete(project))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateDeleteResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateDeleteResultsDTO(errors, projectExists);
        }

        public async Task<GetOneResults<ProjectDTO>> GetOne(ConsultProjectServices services, GetProjectRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                ProjectId id = services.Collector.Analyse(services.Factory.NewId(request.id));
                Project project = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    ProjectDTO dto = services.DTOFactory.CreateBasicDTO(project);
                    return services.UtilsDTOFactory.CreateGetOneResultsDTO(dto);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetOneResultsDTO<ProjectDTO>(errors);
        }

        public async Task<GetPageResults<ProjectDTO>> GetPage(ConsultProjectServices services, ConsultProjectsParameters request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                QueryParams queryParams = services.DTOFactory.CreateQueryParamsOf(request);
                int numberElements;
                Project[] projects = services.Collector.Analyse(await services.Repository.FindByPage(queryParams), out numberElements);
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    ProjectDTO[] listDTO = services.DTOFactory.CreateListOfBasicDTO(projects);
                    return services.UtilsDTOFactory.CreateGetPageResultsDTO(listDTO, numberElements);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<ProjectDTO>(errors);
        }

        public async Task<GetPageResults<ProjectDTO>> GetTenantsProjectPage(ConsultProjectServices services, ConsultProjectsParameters request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            QueryParams queryParams = services.DTOFactory.CreateQueryParamsOf(request);
            AccountEmail accountEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(accountEmail));
            if (services.Collector.Analyse(account.Status.IsActive()))
            {
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.TenantRepository.FindByMember(account.BusinessId));
                if (services.Collector.HasNoErrors())
                {
                    int numberElements;
                    Project[] projects = services.Collector.Analyse(await services.Repository.FindByPageWithTenant(queryParams, tenant), out numberElements);
                    if (services.Collector.HasNoErrors())
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        ProjectDTO[] listDTO = services.DTOFactory.CreateListOfBasicDTO(projects);
                        return services.UtilsDTOFactory.CreateGetPageResultsDTO(listDTO, numberElements);
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<ProjectDTO>(errors);
        }

        public async Task<UpdateResults> Update(UpdateProjectServices services, UpdateProjectRequest request, RequestInfo info)
        {
            bool projectExists = false;
            services.Logger.logInfo("Processing request " + request);
            AccountEmail accountEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(accountEmail));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.TenantRepository.FindByMember(account.BusinessId));
            ProjectId id = services.Collector.IfZeroErrorsDo(() => services.Factory.NewId(request.id));
            Project project = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
            if (services.Collector.HasNoErrors() 
                && services.Collector.Analyse(project.HasTenant(tenant.BusinessId))
                && services.Collector.Analyse(tenant.HasPermissionsToUpdateProject(account.BusinessId)))
            {
                projectExists = true;
                ServiceUtils.UpdateData(services.Collector, request.name, services.Factory.NewName, x => project.ChangeName(x));
                ServiceUtils.UpdateData(services.Collector, request.description, services.Factory.NewDescription, x => project.ChangeDescription(x));
                if (await services.Collector.IfZeroErrorsDo(() => services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateUpdateResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateUpdateResultsDTO(errors, projectExists);
        }
    }
}


using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Projects
{
    public class UpdateProjectServices
    {

        public IErrorCollector Collector;

        public IProjectFactory Factory;

        public IProjectRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITenantRepository TenantRepository;

        public ITranslatorService Translator;

        public ICodeGeneratorService CodeGenerator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public UpdateProjectServices(IErrorCollector collector, IProjectFactory factory, IProjectRepository repository,
                IAccountFactory accountFactory, IAccountRepository accountRepository, ITenantRepository tenantRepository,
                ITranslatorService translator, ICodeGeneratorService codeGenerator, ILoggerService logger,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            TenantRepository = tenantRepository;
            Translator = translator;
            CodeGenerator = codeGenerator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
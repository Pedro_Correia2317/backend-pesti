

using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.Projects
{
    public interface IProjectService
    {
        Task<CreateResults<ProjectDTO>> Create(CreateProjectServices services, CreateProjectRequest request, RequestInfo info);

        Task<GetOneResults<ProjectDTO>> GetOne(ConsultProjectServices services, GetProjectRequest request, RequestInfo info);

        Task<GetPageResults<ProjectDTO>> GetPage(ConsultProjectServices services, ConsultProjectsParameters request, RequestInfo info);

        Task<UpdateResults> Update(UpdateProjectServices services, UpdateProjectRequest request, RequestInfo info);
        
        Task<DeleteResults> Delete(DeleteProjectServices services, DeleteProjectRequest request, RequestInfo info);

        Task<GetPageResults<ProjectDTO>> GetTenantsProjectPage(ConsultProjectServices services, ConsultProjectsParameters request, RequestInfo info);
    }
}
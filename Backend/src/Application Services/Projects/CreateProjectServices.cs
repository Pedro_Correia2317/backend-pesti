

using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Projects
{
    public class CreateProjectServices
    {

        public IErrorCollector ErrorCollector;

        public IProjectDTOFactory DTOFactory;

        public IProjectFactory Factory;

        public IProjectRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITenantRepository TenantRepository;

        public ITranslatorService Translator;

        public ICodeGeneratorService CodeGenerator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public CreateProjectServices(IErrorCollector coll, IProjectDTOFactory dTOFactory, IProjectFactory factory,
                IProjectRepository repository, IAccountFactory accountFactory, ITenantRepository tenantRepository,
                ITranslatorService translator, ICodeGeneratorService codeGenerator, ILoggerService logger,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory, IAccountRepository accountRepository)
        {
            ErrorCollector = coll;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            TenantRepository = tenantRepository;
            Translator = translator;
            CodeGenerator = codeGenerator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
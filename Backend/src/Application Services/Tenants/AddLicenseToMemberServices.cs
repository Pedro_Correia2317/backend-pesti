

using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Utils;

namespace Backend.Tenants
{
    public class AddLicenseToMemberServices
    {
        public IErrorCollector Collector;

        public ITenantFactory Factory;

        public ITenantRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ILicenseFactory LicenseFactory;

        public ILicenseRepository LicenseRepository;

        public IHistoryEntryFactory EntryFactory;

        public IHistoryEntryRepository EntryRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public AddLicenseToMemberServices(IErrorCollector collector, ITenantFactory factory, 
                ITenantRepository repository, IAccountFactory accountFactory, ILicenseFactory licenseFactory,
                IAccountRepository accountRepo, ILicenseRepository licenseRepository, IHistoryEntryFactory entryFactory, 
                IHistoryEntryRepository entryRepository, ITranslatorService translator, ILoggerService logger,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepo;
            LicenseFactory = licenseFactory;
            LicenseRepository = licenseRepository;
            EntryFactory = entryFactory;
            EntryRepository = entryRepository;
            Translator = translator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
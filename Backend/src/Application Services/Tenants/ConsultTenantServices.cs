
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Tenants
{
    public class ConsultTenantServices
    {

        public IErrorCollector Collector;

        public ITenantDTOFactory DTOFactory;

        public ITenantFactory Factory;

        public ITenantRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUtilsDTOFactory UtilsDTOFactory;

        public ConsultTenantServices(IErrorCollector collector, ITenantDTOFactory dTOFactory, ITenantFactory factory,
                ITenantRepository repository, IAccountFactory accountFactory, IAccountRepository accountRepository,
                ITranslatorService translator, ILoggerService logger, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            Translator = translator;
            Logger = logger;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
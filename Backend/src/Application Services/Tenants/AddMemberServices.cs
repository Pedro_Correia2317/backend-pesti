

using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Tenants
{
    public class AddMemberServices
    {
        public IErrorCollector Collector;

        public ITenantFactory Factory;

        public ITenantRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public IHistoryEntryFactory EntryFactory;

        public IHistoryEntryRepository EntryRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public AddMemberServices(IErrorCollector collector, ITenantFactory factory, ITenantRepository repository,
                IAccountFactory accountFactory, IAccountRepository accountRepository, IHistoryEntryFactory eFactory,
                IHistoryEntryRepository entryRepository, ITranslatorService translator, ILoggerService logger,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            EntryFactory = eFactory;
            EntryRepository = entryRepository;
            Translator = translator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
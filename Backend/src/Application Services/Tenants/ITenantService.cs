

using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.Tenants
{
    public interface ITenantService
    {

        Task<CreateResults<TenantDTO>> Create(CreateTenantServices services, CreateTenantRequest request, RequestInfo info);

        Task<GetOneResults<TenantDTO>> GetOne(ConsultTenantServices services, GetTenantRequest request, RequestInfo info);

        Task<GetPageResults<TenantDTO>> GetPage(ConsultTenantServices services, ConsultTenantsParameters request, RequestInfo info);

        TenantPermissionsDTO[] GetTenantPermissions(ITenantDTOFactory dTOFactory, ITranslatorService translator);

        Task<UpdateResults> Update(UpdateTenantServices services, UpdateTenantRequest request, RequestInfo info);
        
        Task<DeleteResults> Delete(DeleteTenantServices services, DeleteTenantRequest request, RequestInfo info);
        
        Task<StandardResults> AddMemberToSpecificTenant(AddMemberServices services, AddMemberToSpecificTenantRequest request, RequestInfo info);
        
        Task<StandardResults> RemoveMemberOfSpecificTenant(RemoveMemberServices services, RemoveMemberOfSpecificTenantRequest request, RequestInfo info);
        
        Task<StandardResults> AddLicenseToMember(AddLicenseToMemberServices services, AddLicenseToMemberRequest request, RequestInfo info);
        
        Task<GetPageResults<TenantMemberDTO>> ConsultMembersOfSpecificTenant(ConsultTenantServices services, ConsultMembersOfSpecificTenantRequest request, RequestInfo info);
        
        Task<GetPageResults<TenantLicenseDTO>> ConsultLicensesOfSpecificTenant(ConsultTenantServices services, ConsultLicensesOfSpecificTenantRequest request, RequestInfo info);
        
        Task<StandardResults> AddMemberToOwnTenant(AddMemberServices services, AddMemberToOwnTenantRequest request, RequestInfo info);
        
        Task<StandardResults> RemoveMemberOfOwnTenant(RemoveMemberServices services, RemoveMemberOfOwnTenantRequest request, RequestInfo info);
        
        Task<GetPageResults<TenantMemberDTO>> ConsultMembersOfOwnTenant(ConsultTenantServices services, ConsultMembersOfOwnTenantRequest request, RequestInfo info);
        
        Task<GetPageResults<TenantLicenseDTO>> ConsultLicensesOfOwnTenant(ConsultTenantServices services, ConsultLicensesOfOwnTenantRequest request, RequestInfo info);
    }
}
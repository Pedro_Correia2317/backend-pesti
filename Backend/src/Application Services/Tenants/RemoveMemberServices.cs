

using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Tenants
{
    public class RemoveMemberServices
    {
        public IErrorCollector Collector;

        public ITenantFactory Factory;

        public ITenantRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public IHistoryEntryFactory EntryFactory;

        public IHistoryEntryRepository EntryRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public RemoveMemberServices(IErrorCollector collector, ITenantFactory factory, ITenantRepository repository,
                IAccountFactory accountFactory, IAccountRepository accountRepository, IHistoryEntryFactory eFactory,
                IHistoryEntryRepository eRepository, ITranslatorService translator, ILoggerService logger,
                IUnitOfWork unit, IUtilsDTOFactory uDTOFactory)
        {
            Collector = collector;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            EntryFactory = eFactory;
            EntryRepository = eRepository;
            Translator = translator;
            Logger = logger;
            UnitOfWork = unit;
            UtilsDTOFactory = uDTOFactory;
        }
    }
}
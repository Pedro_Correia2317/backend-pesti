

using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Tenants
{
    public class CreateTenantServices
    {

        public IErrorCollector Collector;

        public ITenantDTOFactory DTOFactory;

        public ITenantFactory Factory;

        public IAccountFactory AccountFactory;

        public IHistoryEntryFactory EntryFactory;

        public ITenantRepository Repository;

        public IAccountRepository AccountRepository;

        public IHistoryEntryRepository EntryRepository;

        public ITranslatorService Translator;

        public ICodeGeneratorService CodeGenerator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public CreateTenantServices(IErrorCollector collector, ITenantDTOFactory dTOFactory,
                ITenantFactory factory, IAccountFactory accountFactory, IHistoryEntryFactory entryFactory,
                ITenantRepository repository, IAccountRepository accountRepository,
                IHistoryEntryRepository entryRepository, ITranslatorService translator,
                ICodeGeneratorService codeGenerator, ILoggerService logger, IUnitOfWork unitOfWork,
                IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            DTOFactory = dTOFactory;
            Factory = factory;
            AccountFactory = accountFactory;
            EntryFactory = entryFactory;
            Repository = repository;
            AccountRepository = accountRepository;
            EntryRepository = entryRepository;
            Translator = translator;
            CodeGenerator = codeGenerator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantServiceImpl : ITenantService
    {
        public async Task<CreateResults<TenantDTO>> Create(CreateTenantServices services,
            CreateTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                Tenant tenant = await this.CreateObject(services, request);
                if (await services.Collector.IfZeroErrorsDo(() => services.Repository.Save(tenant)))
                {
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.TenantCreation, request.entryDescription, info, tenant.BusinessId);
                    TenantDTO dto = services.DTOFactory.CreateBasicDTO(tenant);
                    dto = services.Translator.Translate(dto);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateResultsDTO(dto);
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateResultsDTO<TenantDTO>(errors);
        }

        private async Task<Tenant> CreateObject(CreateTenantServices services, CreateTenantRequest request)
        {
            TenantContainer container = services.Factory.NewContainer();
            AccountId id = services.Collector.Analyse(services.AccountFactory.NewId(request.owner));
            await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindById(id));
            string tenantId = services.Collector.Analyse(services.CodeGenerator.NewTenantCode());
            container.Id = services.Collector.Analyse(services.Factory.NewId(tenantId));
            container.Name = services.Collector.Analyse(services.Factory.NewName(request.name));
            TenantPermissions perm = services.Factory.NewOwnerPermissions();
            container.Owner = services.Collector.IfZeroErrorsDo(() => services.Factory.NewMember(id, perm));
            return services.Collector.IfZeroErrorsDo(() => services.Factory.NewTenant(container));
        }

        public async Task<DeleteResults> Delete(DeleteTenantServices services, DeleteTenantRequest request, RequestInfo info)
        {
            bool accountExists = false;
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                TenantId id = services.Collector.Analyse(services.Factory.NewId(request.code));
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.TenantDeletion, request.entryDescription, info, tenant.BusinessId);
                    accountExists = true;
                    if (services.Collector.Analyse(await services.Repository.Delete(tenant))
                        && services.Collector.Analyse(await services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateDeleteResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateDeleteResultsDTO(errors, accountExists);
        }

        public async Task<GetOneResults<TenantDTO>> GetOne(ConsultTenantServices services, GetTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                TenantId id = services.Collector.Analyse(services.Factory.NewId(request.id));
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    TenantDTO dto = services.DTOFactory.CreateBasicDTO(tenant);
                    dto = services.Translator.Translate(dto);
                    return services.UtilsDTOFactory.CreateGetOneResultsDTO(dto);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger,
                    services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetOneResultsDTO<TenantDTO>(errors);
        }

        public async Task<GetPageResults<TenantDTO>> GetPage(ConsultTenantServices services, ConsultTenantsParameters request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                QueryParams queryParams = services.DTOFactory.CreateQueryParamsOf(request);
                int numberElements;
                Tenant[] tenants = services.Collector.Analyse(await services.Repository.FindByPage(queryParams), out numberElements);
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    TenantDTO[] listDTO = services.DTOFactory.CreateListOfBasicDTO(tenants);
                    listDTO = services.Translator.Translate(listDTO);
                    return services.UtilsDTOFactory.CreateGetPageResultsDTO(listDTO, numberElements);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<TenantDTO>(errors);
        }

        public TenantPermissionsDTO[] GetTenantPermissions(ITenantDTOFactory dTOFactory, ITranslatorService translator)
        {
            TenantPermissions[] permissions = TenantPermissionList.GetAll();
            TenantPermissionsDTO[] arrayDTO = dTOFactory.CreateListOfPermissionsDTO(permissions);
            return translator.Translate(arrayDTO);
        }

        public async Task<UpdateResults> Update(UpdateTenantServices services, UpdateTenantRequest request, RequestInfo info)
        {
            bool accountExists = false;
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                TenantId code = services.Collector.Analyse(services.Factory.NewId(request.id));
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(code));
                if (services.Collector.HasNoErrors())
                {
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.TenantUpdate, request.entryDescription, info, tenant.BusinessId);
                    accountExists = true;
                    ServiceUtils.UpdateData(services.Collector, request.name, services.Factory.NewName, x => tenant.Name = x);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateUpdateResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateUpdateResultsDTO(errors, accountExists);
        }

        public async Task<StandardResults> AddMemberToSpecificTenant(AddMemberServices services, AddMemberToSpecificTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                AccountId aId = services.Collector.Analyse(services.AccountFactory.NewId(request.account));
                TenantPermissions permissions = services.Collector.Analyse(services.Factory.NewPermissions(request.permissions));
                TenantId tId = services.Collector.Analyse(services.Factory.NewId(request.tenant));
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(tId));
                await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindById(aId));
                if (services.Collector.HasNoErrors())
                {
                    TenantMember member = services.Collector.Analyse(services.Factory.NewMember(aId, permissions));
                    services.Collector.Analyse(tenant.AddMember(member));
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.TenantAddMember, request.entryDescription, info, tenant.BusinessId, request.account);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateStandardResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> RemoveMemberOfSpecificTenant(RemoveMemberServices services, RemoveMemberOfSpecificTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                TenantId tId = services.Collector.Analyse(services.Factory.NewId(request.tenant));
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(tId));
                AccountId aId = services.Collector.Analyse(services.AccountFactory.NewId(request.account));
                if (services.Collector.HasNoErrors())
                {
                    services.Collector.Analyse(tenant.RemoveMember(aId));
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.TenantRemoveMember, request.entryDescription, info, tenant.BusinessId, request.account);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateStandardResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> AddLicenseToMember(AddLicenseToMemberServices services, AddLicenseToMemberRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            AccountEmail adminEmail = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account adminAccount = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(adminEmail));
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindByMember(adminAccount.BusinessId));
            if (services.Collector.IfZeroErrorsDo(() => adminAccount.Status.IsActive()))
            {
                AccountId clientId = services.Collector.Analyse(services.AccountFactory.NewId(request.account));
                LicenseId licenseId = services.Collector.Analyse(services.LicenseFactory.NewId(request.license));
                services.Collector.Analyse(await services.Repository.DoesNotHaveMember(clientId));
                services.Collector.Analyse(tenant.HasPermissionsToGiveLicense(adminAccount.BusinessId));
                services.Collector.Analyse(tenant.HasMember(clientId));
                services.Collector.Analyse(tenant.HasLicense(licenseId));
                services.Collector.Analyse(await services.LicenseRepository.HasNoLicense(clientId));
                License license = services.Collector.Analyse(await services.LicenseRepository.FindById(licenseId));
                HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.AssociateLicenseToAccount, info, tenant.BusinessId, request.account);
                if (services.Collector.IfZeroErrorsDo(() => license.AddAccount(clientId))
                    && services.Collector.Analyse(await services.EntryRepository.Save(entry))
                    && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<GetPageResults<TenantMemberDTO>> ConsultMembersOfSpecificTenant(ConsultTenantServices services, ConsultMembersOfSpecificTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                TenantId id = services.Collector.Analyse(services.Factory.NewId(request.id));
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    TenantMemberDTO[] dtoList = services.DTOFactory.CreateListOfMembersDTO(tenant.Members.ToCollection());
                    dtoList = services.Translator.Translate(dtoList);
                    return services.UtilsDTOFactory.CreateGetPageResultsDTO(dtoList, dtoList.Length);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<TenantMemberDTO>(errors);
        }

        public async Task<GetPageResults<TenantLicenseDTO>> ConsultLicensesOfSpecificTenant(ConsultTenantServices services, ConsultLicensesOfSpecificTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                TenantId id = services.Collector.Analyse(services.Factory.NewId(request.id));
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    TenantLicenseDTO[] dtoList = services.DTOFactory.CreateListOfLicensesDTO(tenant.Licenses.ToCollection());
                    return services.UtilsDTOFactory.CreateGetPageResultsDTO(dtoList, dtoList.Length);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<TenantLicenseDTO>(errors);
        }

        public async Task<StandardResults> AddMemberToOwnTenant(AddMemberServices services, AddMemberToOwnTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindByMember(account.BusinessId));
            if (services.Collector.HasNoErrors())
            {
                AccountId aId = services.Collector.Analyse(services.AccountFactory.NewId(request.account));
                await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindById(aId));
                services.Collector.Analyse(await services.Repository.DoesNotHaveMember(aId));
                TenantPermissions permissions = services.Collector.Analyse(services.Factory.NewPermissions(request.permissions));
                TenantMember member = services.Collector.Analyse(services.Factory.NewMember(aId, permissions));
                services.Collector.Analyse(tenant.AddMember(member));
                if (await services.Collector.IfZeroErrorsDo(() => services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<StandardResults> RemoveMemberOfOwnTenant(RemoveMemberServices services, RemoveMemberOfOwnTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindByMember(account.BusinessId));
            if (services.Collector.HasNoErrors())
            {
                AccountId aId = services.Collector.Analyse(services.AccountFactory.NewId(request.account));
                services.Collector.IfZeroErrorsDo(() => tenant.RemoveMember(aId));
                if (await services.Collector.IfZeroErrorsDo(() => services.UnitOfWork.Commit()))
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    return services.UtilsDTOFactory.CreateStandardResultsDTO();
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<GetPageResults<TenantMemberDTO>> ConsultMembersOfOwnTenant(ConsultTenantServices services, ConsultMembersOfOwnTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindByMember(account.BusinessId));
            if (services.Collector.HasNoErrors())
            {
                services.Logger.logInfo("Successfully processed request " + request);
                TenantMemberDTO[] dtoList = services.DTOFactory.CreateListOfMembersDTO(tenant.Members.ToCollection());
                dtoList = services.Translator.Translate(dtoList);
                return services.UtilsDTOFactory.CreateGetPageResultsDTO(dtoList, dtoList.Length);
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<TenantMemberDTO>(errors);
        }

        public async Task<GetPageResults<TenantLicenseDTO>> ConsultLicensesOfOwnTenant(ConsultTenantServices services, ConsultLicensesOfOwnTenantRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            AccountEmail email = services.Collector.Analyse(services.AccountFactory.NewEmail(info.AccountEmail));
            Account account = await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindByEmail(email));
            services.Collector.IfZeroErrorsDo(() => account.Status.IsActive());
            Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindByMember(account.BusinessId));
            if (services.Collector.HasNoErrors())
            {
                services.Logger.logInfo("Successfully processed request " + request);
                TenantLicenseDTO[] dtoList = services.DTOFactory.CreateListOfLicensesDTO(tenant.Licenses.ToCollection());
                return services.UtilsDTOFactory.CreateGetPageResultsDTO(dtoList, dtoList.Length);
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<TenantLicenseDTO>(errors);
        }
    }
}
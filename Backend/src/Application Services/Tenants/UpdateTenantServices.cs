
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Tenants
{
    public class UpdateTenantServices
    {
        public IErrorCollector Collector;

        public ITenantFactory Factory;

        public ITenantRepository Repository;

        public IHistoryEntryFactory EntryFactory;

        public IHistoryEntryRepository EntryRepository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public UpdateTenantServices(IErrorCollector collector, ITenantFactory factory, ITenantRepository repository,
                IHistoryEntryFactory entryFactory, IHistoryEntryRepository entryRepository, IAccountFactory accountFactory,
                IAccountRepository accountRepository, ITranslatorService translator, ILoggerService logger,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Factory = factory;
            Repository = repository;
            EntryFactory = entryFactory;
            EntryRepository = entryRepository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            Translator = translator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
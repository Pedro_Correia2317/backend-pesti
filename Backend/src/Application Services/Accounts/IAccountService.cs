

using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.Accounts
{
    public interface IAccountService
    {

        Task<CreateResults<AccountDTO>> Create(CreateAccountServices services, CreateAccountRequest request, RequestInfo info);

        Task<GetOneResults<AccountDTO>> GetOne(ConsultAccountServices services, GetAccountRequest request, RequestInfo info);

        Task<GetPageResults<AccountDTO>> GetPage(ConsultAccountServices services, ConsultAccountsParameters request, RequestInfo info);

        AccountTypeDTO[] GetAccountTypes(IAccountDTOFactory licenseDTOFactory, ITranslatorService translator);

        AccountStatusDTO[] GetAccountStatuses(IAccountDTOFactory licenseDTOFactory, ITranslatorService translator);

        Task<UpdateResults> Update(UpdateAccountServices services, UpdateAccountRequest request, RequestInfo info);
        
        Task<DeleteResults> Delete(DeleteAccountServices services, DeleteAccountRequest request, RequestInfo info);
        
        Task<StandardResults> ChangeStatus(ChangeAccountStatusServices services, ChangeAccountStatusRequest request, RequestInfo info);
        
        Task<GetOneResults<AccountDetailsDTO>> GetAccountDetails(GetAccountDetailsServices services, RequestInfo info);
    }
}

using Backend.Utils;

namespace Backend.Accounts
{
    public class ConsultAccountServices
    {

        public IErrorCollector Collector;

        public IAccountDTOFactory DTOFactory;

        public IAccountFactory Factory;

        public IAccountRepository Repository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUtilsDTOFactory UtilsDTOFactory;

        public ConsultAccountServices(IErrorCollector collector, IAccountDTOFactory dTOFactory,
                IAccountFactory factory, IAccountRepository repository, ITranslatorService translator,
                ILoggerService logger, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            Translator = translator;
            Logger = logger;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}


using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountServiceImpl : IAccountService
    {
        public async Task<CreateResults<AccountDTO>> Create(CreateAccountServices services, CreateAccountRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.Factory, services.Repository, info, x => x.Type.IsSuperAdministrator()))
            {
                Account account = await this.CreateObject(services, request);
                if (await services.Collector.IfZeroErrorsDo(() => services.Repository.Save(account)))
                {
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.AccountCreation, request?.entryDescription, info, account.BusinessId);
                    AccountDTO dto = services.DTOFactory.CreateBasicDTO(account);
                    dto = services.Translator.Translate(dto);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateResultsDTO(dto);
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateResultsDTO<AccountDTO>(errors);
        }

        private async Task<Account> CreateObject(CreateAccountServices services, CreateAccountRequest request)
        {
            AccountContainer container = services.Factory.NewContainer();
            string id = services.Collector.Analyse(services.CodeGenerator.NewAccountCode());
            container.Id = services.Collector.Analyse(services.Factory.NewId(id));
            container.Name = services.Collector.Analyse(services.Factory.NewName(request.name));
            container.Email = services.Collector.Analyse(services.Factory.NewEmail(request.email));
            container.Status = services.Collector.Analyse(services.Factory.NewDefaultStatus());
            container.Type = services.Collector.Analyse(services.Factory.NewType(request.type));
            await services.Collector.IfZeroErrorsDo(() => services.Repository.IsEmailUnique(container.Email));
            return services.Collector.IfZeroErrorsDo(() => services.Factory.NewAccount(container));
        }

        public async Task<DeleteResults> Delete(DeleteAccountServices services, DeleteAccountRequest request, RequestInfo info)
        {
            bool accountExists = false;
            services.Logger.logInfo("Processing " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.Factory, services.Repository, info, x => x.Type.IsSuperAdministrator()))
            {
                AccountId id = services.Collector.Analyse(services.Factory.NewId(request.code));
                Account account = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    accountExists = true;
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.AccountDeletion, request?.entryDescription, info, account.BusinessId);
                    if (services.Collector.Analyse(await services.Repository.Delete(account))
                        && services.Collector.Analyse(await services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateDeleteResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateDeleteResultsDTO(errors, accountExists);
        }

        public AccountTypeDTO[] GetAccountTypes(IAccountDTOFactory accountDTOFactory, ITranslatorService translator)
        {
            AccountType[] accountTypes = AccountTypes.GetAll();
            AccountTypeDTO[] arrayDTO = accountDTOFactory.CreateListTypesDTO(accountTypes);
            return translator.Translate(arrayDTO);
        }

        public AccountStatusDTO[] GetAccountStatuses(IAccountDTOFactory accountDTOFactory, ITranslatorService translator)
        {
            AccountStatus[] accountStatuses = AccountStatuses.GetAll();
            AccountStatusDTO[] arrayDTO = accountDTOFactory.CreateListStatusDTO(accountStatuses);
            return translator.Translate(arrayDTO);
        }

        public async Task<GetOneResults<AccountDTO>> GetOne(ConsultAccountServices services, GetAccountRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.Factory, services.Repository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                AccountId id = services.Collector.Analyse(services.Factory.NewId(request.id));
                Account account = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    AccountDTO dto = services.DTOFactory.CreateBasicDTO(account);
                    dto = services.Translator.Translate(dto);
                    return services.UtilsDTOFactory.CreateGetOneResultsDTO(dto);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetOneResultsDTO<AccountDTO>(errors);
        }

        public async Task<GetPageResults<AccountDTO>> GetPage(ConsultAccountServices services, ConsultAccountsParameters request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.Factory, services.Repository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                QueryParams queryParams = services.DTOFactory.CreateQueryParamsOf(request);
                int numberElements;
                Account[] accounts = services.Collector.Analyse(await services.Repository.FindByPage(queryParams), out numberElements);
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    AccountDTO[] listDTO = services.DTOFactory.CreateListOfBasicDTO(accounts);
                    listDTO = services.Translator.Translate(listDTO);
                    return services.UtilsDTOFactory.CreateGetPageResultsDTO(listDTO, numberElements);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<AccountDTO>(errors);
        }

        public async Task<UpdateResults> Update(UpdateAccountServices services, UpdateAccountRequest request, RequestInfo info)
        {
            bool accountExists = false;
            services.Logger.logInfo("Processing request " + request.code);
            if (await ServiceUtils.HasPermissions(services.Collector, services.Factory, services.Repository, info, x => x.Type.IsSuperAdministrator()))
            {
                AccountId code = services.Collector.Analyse(services.Factory.NewId(request.code));
                Account account = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(code));
                if (services.Collector.HasNoErrors())
                {
                    accountExists = true;
                    ServiceUtils.UpdateData(services.Collector, request.name, services.Factory.NewName, x => account.Name = x);
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.AccountUpdate, request.entryDescription, info, account.BusinessId, request.name);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                            && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateUpdateResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateUpdateResultsDTO(errors, accountExists);
        }

        public async Task<StandardResults> ChangeStatus(ChangeAccountStatusServices services, ChangeAccountStatusRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing Request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.Factory, services.Repository, info, x => x.Type.IsSuperAdministrator()))
            {
                AccountId id = services.Collector.Analyse(services.Factory.NewId(request.account));
                AccountStatus status = services.Collector.Analyse(services.Factory.NewStatus(request.status));
                Account account = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Collector.Analyse(account.ChangeStatus(status));
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.AccountStatusChange, request.entryDescription, info, account.BusinessId, request.status);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                            && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateStandardResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateStandardResultsDTO(errors);
        }

        public async Task<GetOneResults<AccountDetailsDTO>> GetAccountDetails(GetAccountDetailsServices services, RequestInfo info)
        {
            services.Logger.logInfo("Processing Request " + info.AccountEmail);
            AccountEmail email = services.Collector.Analyse(services.Factory.NewEmail(info.AccountEmail));
            Account account = services.Collector.Analyse(await services.Repository.FindByEmail(email));
            Tenant tenant = services.Collector.Analyse(await services.TenantRepository.FindByMember(account.BusinessId));
            License license = services.Collector.Analyse(await services.LicenseRepository.FindByAccount(account.BusinessId));
            AccountDetailsDTO dto = services.DTOFactory.CreateDetailsDTO(account, license, tenant);
            dto = services.Translator.Translate(dto);
            services.Logger.logInfo("Successfully processed request " + info.AccountEmail);
            return services.UtilsDTOFactory.CreateGetOneResultsDTO<AccountDetailsDTO>(dto);
        }
    }
}
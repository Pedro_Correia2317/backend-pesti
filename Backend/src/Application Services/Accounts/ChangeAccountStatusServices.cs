

using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Accounts
{
    public class ChangeAccountStatusServices
    {

        public IErrorCollector Collector;

        public IAccountDTOFactory DTOFactory;

        public IAccountFactory Factory;

        public IAccountRepository Repository;

        public IHistoryEntryFactory EntryFactory;

        public IHistoryEntryRepository EntryRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public ChangeAccountStatusServices(IErrorCollector collector, IAccountDTOFactory dTOFactory,
                IAccountFactory factory, IAccountRepository repository, IHistoryEntryFactory entryFactory,
                IHistoryEntryRepository entryRepository, ITranslatorService translator, ILoggerService logger,
                IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            EntryFactory = entryFactory;
            EntryRepository = entryRepository;
            Translator = translator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
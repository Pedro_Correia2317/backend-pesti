

using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Accounts
{
    public class GetAccountDetailsServices
    {

        public IErrorCollector Collector;

        public IAccountDTOFactory DTOFactory;

        public IAccountFactory Factory;

        public IAccountRepository Repository;

        public ITenantRepository TenantRepository;

        public ILicenseRepository LicenseRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUtilsDTOFactory UtilsDTOFactory;

        public GetAccountDetailsServices(IErrorCollector collector, IAccountDTOFactory dTOFactory,
                IAccountFactory factory, IAccountRepository repository, ITenantRepository tenantRepository,
                ILicenseRepository licenseRepository, ITranslatorService translator, ILoggerService logger,
                IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            TenantRepository = tenantRepository;
            LicenseRepository = licenseRepository;
            Translator = translator;
            Logger = logger;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
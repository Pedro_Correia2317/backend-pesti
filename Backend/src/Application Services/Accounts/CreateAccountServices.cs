

using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Accounts
{
    public class CreateAccountServices
    {

        public IErrorCollector Collector;

        public IAccountDTOFactory DTOFactory;

        public IAccountFactory Factory;

        public IAccountRepository Repository;

        public IHistoryEntryFactory EntryFactory;

        public IHistoryEntryRepository EntryRepository;

        public ITranslatorService Translator;

        public ICodeGeneratorService CodeGenerator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public CreateAccountServices(IErrorCollector errorCollector, IAccountDTOFactory dTOFactory,
                IAccountFactory factory, IAccountRepository repository, IHistoryEntryFactory entryFactory,
                IHistoryEntryRepository entryRepository, ITranslatorService translator, IUnitOfWork unitOfWork,
                ICodeGeneratorService codeGenerator, ILoggerService logger, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = errorCollector;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            EntryFactory = entryFactory;
            EntryRepository = entryRepository;
            Translator = translator;
            CodeGenerator = codeGenerator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
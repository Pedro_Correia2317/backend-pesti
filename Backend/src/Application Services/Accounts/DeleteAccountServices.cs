

using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Accounts
{
    public class DeleteAccountServices
    {

        public IErrorCollector Collector;

        public IAccountFactory Factory;

        public IAccountRepository Repository;

        public IHistoryEntryFactory EntryFactory;

        public IHistoryEntryRepository EntryRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public DeleteAccountServices(IErrorCollector collector, IAccountFactory factory,
                IAccountRepository repository, IHistoryEntryFactory eFactory, IHistoryEntryRepository eRepository,
                ITranslatorService translator, ILoggerService logger, IUnitOfWork unitOfWork, 
                IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Factory = factory;
            Repository = repository;
            Translator = translator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
            EntryFactory = eFactory;
            EntryRepository = eRepository;
        }
    }
}
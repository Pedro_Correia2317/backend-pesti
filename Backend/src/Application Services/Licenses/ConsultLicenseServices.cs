
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Licenses
{
    public class ConsultLicenseServices
    {

        public IErrorCollector Collector;

        public ILicenseDTOFactory DTOFactory;

        public ILicenseFactory Factory;

        public ILicenseRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUtilsDTOFactory UtilsDTOFactory;

        public ConsultLicenseServices(IErrorCollector collector, ILicenseDTOFactory dTOFactory, ILicenseFactory factory,
                ILicenseRepository repository, IAccountFactory accountFactory, IAccountRepository accountRepository,
                ITranslatorService translator, ILoggerService logger, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            DTOFactory = dTOFactory;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            Translator = translator;
            Logger = logger;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
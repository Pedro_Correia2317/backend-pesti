

using Backend.Accounts;
using Backend.Utils;

namespace Backend.Licenses
{
    public class DeleteLicenseServices
    {

        public IErrorCollector Collector;

        public ILicenseFactory Factory;

        public ILicenseRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITranslatorService Translator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public DeleteLicenseServices(IErrorCollector collector, ILicenseFactory factory, ILicenseRepository repository,
                IAccountFactory accountFactory, IAccountRepository accountRepository, ITranslatorService translator,
                ILoggerService logger, IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            Factory = factory;
            Repository = repository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            Translator = translator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
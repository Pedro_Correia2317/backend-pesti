

using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Licenses
{
    public class CreateLicensePackServices
    {

        public IErrorCollector Collector;

        public IRequestAnalyser RequestAnalyser;

        public ILicenseDTOFactory DTOFactory;

        public ILicenseFactory Factory;

        public ITenantFactory TenantFactory;

        public IHistoryEntryFactory EntryFactory;

        public ILicenseRepository Repository;

        public ITenantRepository TenantRepository;

        public IHistoryEntryRepository EntryRepository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public ITranslatorService Translator;

        public ICodeGeneratorService CodeGenerator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public CreateLicensePackServices(IErrorCollector collector, IRequestAnalyser requestAnalyser,
                ILicenseDTOFactory dTOFactory, ILicenseFactory factory, ITenantFactory tenantFactory,
                IHistoryEntryFactory entryFactory, ILicenseRepository repository, ITenantRepository tenantRepository,
                IHistoryEntryRepository entryRepository, IAccountFactory accountFactory,
                IAccountRepository accountRepository, ITranslatorService translator, ICodeGeneratorService codeGenerator,
                ILoggerService logger, IUnitOfWork unitOfWork, IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            RequestAnalyser = requestAnalyser;
            DTOFactory = dTOFactory;
            Factory = factory;
            TenantFactory = tenantFactory;
            EntryFactory = entryFactory;
            Repository = repository;
            TenantRepository = tenantRepository;
            EntryRepository = entryRepository;
            AccountFactory = accountFactory;
            AccountRepository = accountRepository;
            Translator = translator;
            CodeGenerator = codeGenerator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}


using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.Licenses
{
    public interface ILicenseService
    {

        Task<CreateResults<LicenseDTO>> Create(CreateLicenseServices services, CreateLicenseRequest request, RequestInfo info);

        Task<CreatePackResults<LicenseDTO>> CreatePack(CreateLicensePackServices services, CreateLicensePackRequest request, RequestInfo info);

        Task<GetOneResults<LicenseDTO>> GetOne(ConsultLicenseServices services, GetLicenseRequest request, RequestInfo info);

        Task<GetPageResults<LicenseDTO>> GetPage(ConsultLicenseServices services, ConsultLicensesParameters request, RequestInfo info);

        LicenseTypeDTO[] GetLicenseTypes(ILicenseDTOFactory licenseDTOFactory, ITranslatorService translator);

        Task<UpdateResults> Update(UpdateLicenseServices services, UpdateLicenseRequest request, RequestInfo info);
        
        Task<DeleteResults> Delete(DeleteLicenseServices services, DeleteLicenseRequest request, RequestInfo info);
    }
}
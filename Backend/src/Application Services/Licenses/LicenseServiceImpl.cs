

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.HistoryEntries;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseServiceImpl : ILicenseService
    {

        public LicenseTypeDTO[] GetLicenseTypes(ILicenseDTOFactory licenseDTOFactory, ITranslatorService translator)
        {
            LicenseType[] licenseTypes = LicenseTypes.GetAll();
            LicenseTypeDTO[] arrayDTO = licenseDTOFactory.CreateListTypesDTO(licenseTypes);
            return translator.Translate(arrayDTO);
        }

        public async Task<CreateResults<LicenseDTO>> Create(CreateLicenseServices services, CreateLicenseRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                License license = await this.CreateLicense(services, request);
                if (await services.Collector.IfZeroErrorsDo(() => services.Repository.Save(license)))
                {
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.AssociateLicenseToAccount, request.entryDescription, info, license.Account, license.BusinessId.Value);
                    LicenseDTO dto = services.DTOFactory.CreateBasicDTO(license);
                    dto = services.Translator.Translate(dto);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateResultsDTO(dto);
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateResultsDTO<LicenseDTO>(errors);
        }

        private async Task<License> CreateLicense(CreateLicenseServices services, CreateLicenseRequest req)
        {
            LicenseContainer container = FillContainer(services.Collector, services.Factory, services.CodeGenerator, req.date, req.duration, req.numberMeetings, req.licenseType);
            container.Account = services.Collector.Analyse(services.AccountFactory.NewId(req.account));
            await services.Collector.IfZeroErrorsDo(() => services.AccountRepository.FindById(container.Account));
            await services.Collector.IfZeroErrorsDo(() => services.Repository.HasNoLicense(container.Account));
            return services.Collector.IfZeroErrorsDo(() => services.Factory.NewLicense(container));
        }

        private LicenseContainer FillContainer(IErrorCollector collector, ILicenseFactory factory,
                ICodeGeneratorService generator, string date, string dur, int? num, string type)
        {
            LicenseContainer container = factory.NewContainer();
            container.Type = collector.Analyse(factory.NewType(type));
            if (collector.HasNoErrors())
            {
                string id = collector.Analyse(generator.NewLicenseCode());
                container.Id = collector.Analyse(factory.NewId(id));
                container.Date = collector.Analyse(date != null ?
                    factory.NewDate(date) : factory.NewDate(DateTime.Now));
                container.Duration = collector.Analyse(dur == null && container.Type != null ?
                    factory.NewDuration(container.Type.Period) : factory.NewDuration(dur));
                container.NumberMeetings = collector.Analyse(num == null && container.Type != null ?
                    factory.NewMeetings(container.Type.NumberMeetings) : factory.NewMeetings(num));
                if (container.NumberMeetings != null)
                {
                    container.RemainingMeetings = collector.Analyse(num == null && container.Type != null ?
                        factory.NewRemainingMeetings(container.Type.NumberMeetings) : factory.NewRemainingMeetings(num));
                }
            }
            return container;
        }

        public async Task<DeleteResults> Delete(DeleteLicenseServices services, DeleteLicenseRequest request, RequestInfo info)
        {
            bool accountExists = false;
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                LicenseId id = services.Collector.Analyse(services.Factory.NewId(request.code));
                License license = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    accountExists = true;
                    if (services.Collector.Analyse(await services.Repository.Delete(license))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateDeleteResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateDeleteResultsDTO(errors, accountExists);
        }

        public async Task<GetOneResults<LicenseDTO>> GetOne(ConsultLicenseServices services, GetLicenseRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                LicenseId id = services.Collector.Analyse(services.Factory.NewId(request.id));
                License license = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(id));
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    LicenseDTO dto = services.DTOFactory.CreateBasicDTO(license);
                    dto = services.Translator.Translate(dto);
                    return services.UtilsDTOFactory.CreateGetOneResultsDTO(dto);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetOneResultsDTO<LicenseDTO>(errors);
        }

        public async Task<GetPageResults<LicenseDTO>> GetPage(ConsultLicenseServices services, ConsultLicensesParameters request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministratorOrAdministrator()))
            {
                QueryParams queryParams = services.DTOFactory.CreateQueryParamsOf(request);
                int numberElements;
                License[] licenses = services.Collector.Analyse(await services.Repository.FindByPage(queryParams), out numberElements);
                if (services.Collector.HasNoErrors())
                {
                    services.Logger.logInfo("Successfully processed request " + request);
                    LicenseDTO[] listDTO = services.DTOFactory.CreateListOfBasicDTO(licenses);
                    listDTO = services.Translator.Translate(listDTO);
                    return services.UtilsDTOFactory.CreateGetPageResultsDTO(listDTO, numberElements);
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateGetPageResultsDTO<LicenseDTO>(errors);
        }

        public async Task<UpdateResults> Update(UpdateLicenseServices services, UpdateLicenseRequest request, RequestInfo info)
        {
            bool accountExists = false;
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                LicenseId code = services.Collector.Analyse(services.Factory.NewId(request.code));
                License license = await services.Collector.IfZeroErrorsDo(() => services.Repository.FindById(code));
                if (services.Collector.HasNoErrors())
                {
                    accountExists = true;
                    ServiceUtils.UpdateData(services.Collector, request.numberMeetings, services.Factory.NewMeetings,
                        x => license.ChangeNumberMeetings(x, services.Collector.Analyse(services.Factory.NewRemainingMeetings(request.numberMeetings))));
                    ServiceUtils.UpdateData(services.Collector, request.remainingMeetings, services.Factory.NewRemainingMeetings, x => license.ChangeRemainingMeetings(x));
                    ServiceUtils.UpdateData(services.Collector, request.licenseType, services.Factory.NewType, x => license.ChangeType(x));
                    ServiceUtils.UpdateData(services.Collector, request.duration, services.Factory.NewDuration, x => license.ChangeDuration(x));
                    if (await services.Collector.IfZeroErrorsDo(() => services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreateUpdateResultsDTO();
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreateUpdateResultsDTO(errors, accountExists);
        }

        public async Task<CreatePackResults<LicenseDTO>> CreatePack(CreateLicensePackServices services, CreateLicensePackRequest request, RequestInfo info)
        {
            services.Logger.logInfo("Processing request " + request);
            if (await ServiceUtils.HasPermissions(services.Collector, services.AccountFactory, services.AccountRepository, info, x => x.Type.IsSuperAdministrator()))
            {
                services.Collector.Catch(services.RequestAnalyser.AnalyseRequest(request));
                TenantId id = services.Collector.Analyse(services.TenantFactory.NewId(request.tenant));
                Tenant tenant = await services.Collector.IfZeroErrorsDo(() => services.TenantRepository.FindById(id));
                var list = services.Collector.IfZeroErrorsDo(() => services.RequestAnalyser.CreateLicenses(request, (x) => this.CreateLicense(services, x)));
                if (services.Collector.IfZeroErrorsDo(() => tenant.AssociateLicenses(list))
                    && services.Collector.Analyse(await services.Repository.SaveAll(list))
                    && services.Collector.Analyse(await services.TenantRepository.Update(tenant)))
                {
                    HistoryEntry entry = ServiceUtils.CreateEntry(services.Collector, services.EntryFactory, EntryTypes.TenantBoughtLicenses, request.entryDescription, info, id, list.Count);
                    LicenseDTO[] licenseDTOs = services.DTOFactory.CreateListOfBasicDTO(list);
                    if (await services.Collector.IfZeroErrorsDo(() => services.EntryRepository.Save(entry))
                        && services.Collector.Analyse(await services.UnitOfWork.Commit()))
                    {
                        services.Logger.logInfo("Successfully processed request " + request);
                        return services.UtilsDTOFactory.CreatePackResults(licenseDTOs);
                    }
                }
            }
            List<Error> errors = ServiceUtils.LogAndTranslateErrors(services.Collector, services.Logger, services.Translator, "Error processing request " + request);
            return services.UtilsDTOFactory.CreatePackResults<LicenseDTO>(errors);
        }

        private License CreateLicense(CreateLicensePackServices services, LicenseInCreatePack request)
        {
            LicenseContainer container = FillContainer(services.Collector, services.Factory, services.CodeGenerator,
                    null, request.duration, request.numberMeetings, request.licenseType);
            return services.Collector.IfZeroErrorsDo(() => services.Factory.NewLicense(container));
        }
    }
}
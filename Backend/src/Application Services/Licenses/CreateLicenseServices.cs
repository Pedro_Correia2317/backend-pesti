

using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Utils;

namespace Backend.Licenses
{
    public class CreateLicenseServices
    {

        public IErrorCollector Collector;

        public ILicenseDTOFactory DTOFactory;

        public ILicenseFactory Factory;

        public ILicenseRepository Repository;

        public IAccountFactory AccountFactory;

        public IAccountRepository AccountRepository;

        public IHistoryEntryFactory EntryFactory;

        public IHistoryEntryRepository EntryRepository;

        public ITranslatorService Translator;

        public ICodeGeneratorService CodeGenerator;

        public ILoggerService Logger;

        public IUnitOfWork UnitOfWork;

        public IUtilsDTOFactory UtilsDTOFactory;

        public CreateLicenseServices(IErrorCollector collector, ILicenseDTOFactory dTOFactory,
                ILicenseFactory factory, IAccountFactory accountFactory, ILicenseRepository repository,
                IAccountRepository accountRepository, IHistoryEntryFactory entryFactory,
                IHistoryEntryRepository entryRepository, ITranslatorService translator,
                ICodeGeneratorService codeGenerator, ILoggerService logger, IUnitOfWork unitOfWork,
                IUtilsDTOFactory utilsDTOFactory)
        {
            Collector = collector;
            DTOFactory = dTOFactory;
            Factory = factory;
            AccountFactory = accountFactory;
            Repository = repository;
            AccountRepository = accountRepository;
            EntryFactory = entryFactory;
            EntryRepository = entryRepository;
            Translator = translator;
            CodeGenerator = codeGenerator;
            Logger = logger;
            UnitOfWork = unitOfWork;
            UtilsDTOFactory = utilsDTOFactory;
        }
    }
}
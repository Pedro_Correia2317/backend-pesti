using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Meetings;
using Backend.Projects;
using Backend.Tenants;
using Backend.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web;
using Microsoft.OpenApi.Models;

namespace Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy("DefaultPolicy", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });
            services.AddDbContext<BackendDBContext>(opt => opt.UseNpgsql(Configuration.GetConnectionString("BackendDBContext")));
            services.AddLocalization(opt => { opt.ResourcesPath = "Resources"; });
            ConfigureMyServices(services);
            services.AddControllers().AddNewtonsoftJson();
        
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddMicrosoftIdentityWebApiAuthentication(Configuration);
            services.AddAuthentication();
            services.AddAuthorization();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            var supportedCultures = new[] { "pt-PT" };
            var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures);
            app.UseRequestLocalization(localizationOptions);

            app.UseRouting();

            app.UseCors("DefaultPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void ConfigureMyServices(IServiceCollection services)
        {
            services.AddSingleton<ILoggerService, LoggerServiceImpl>();
            services.AddSingleton<ITranslatorService, TranslatorServiceImpl>();
            services.AddScoped<IErrorCollector, ErrorCollectorImpl>();
            services.AddScoped<IRequestAnalyser, RequestAnalyserImpl>();

            services.AddScoped<IUnitOfWork, UnitOfWorkImpl>();
            services.AddScoped<ILicenseRepository, LicenseRepositoryImpl>();
            services.AddScoped<IAccountRepository, AccountRepositoryImpl>();
            services.AddScoped<ITenantRepository, TenantRepositoryImpl>();
            services.AddScoped<IHistoryEntryRepository, HistoryEntryRepositoryImpl>();
            services.AddScoped<IProjectRepository, ProjectRepositoryImpl>();
            services.AddScoped<IMeetingRepository, MeetingRepositoryImpl>();

            services.AddSingleton<ILicenseService, LicenseServiceImpl>();
            services.AddSingleton<IAccountService, AccountServiceImpl>();
            services.AddSingleton<ITenantService, TenantServiceImpl>();
            services.AddSingleton<IHistoryEntryService, HistoryEntryServiceImpl>();
            services.AddSingleton<IProjectService, ProjectServiceImpl>();
            services.AddSingleton<IMeetingService, MeetingServiceImpl>();

            services.AddSingleton<ILicenseFactory, LicenseFactoryImpl>();
            services.AddSingleton<IAccountFactory, AccountFactoryImpl>();
            services.AddSingleton<ITenantFactory, TenantFactoryImpl>();
            services.AddSingleton<IHistoryEntryFactory, HistoryEntryFactoryImpl>();
            services.AddSingleton<IProjectFactory, ProjectFactoryImpl>();
            services.AddSingleton<IMeetingFactory, MeetingFactoryImpl>();
            services.AddSingleton<ICodeGeneratorService, CodeGeneratorServiceImpl>();

            services.AddSingleton<IUtilsDTOFactory, UtilsDTOFactoryImpl>();
            services.AddSingleton<ILicenseDTOFactory, LicenseDTOFactoryImpl>();
            services.AddSingleton<IAccountDTOFactory, AccountDTOFactoryImpl>();
            services.AddSingleton<ITenantDTOFactory, TenantDTOFactoryImpl>();
            services.AddSingleton<IProjectDTOFactory, ProjectDTOFactoryImpl>();
            services.AddSingleton<IMeetingDTOFactory, MeetingDTOFactoryImpl>();
            services.AddSingleton<IHistoryEntryDTOFactory, HistoryEntryDTOFactoryImpl>();

            services.AddScoped<CreateLicenseServices>();
            services.AddScoped<ConsultLicenseServices>();
            services.AddScoped<DeleteLicenseServices>();
            services.AddScoped<UpdateLicenseServices>();
            services.AddScoped<CreateLicensePackServices>();

            services.AddScoped<CreateAccountServices>();
            services.AddScoped<ConsultAccountServices>();
            services.AddScoped<DeleteAccountServices>();
            services.AddScoped<UpdateAccountServices>();
            services.AddScoped<ChangeAccountStatusServices>();
            services.AddScoped<GetAccountDetailsServices>();

            services.AddScoped<CreateTenantServices>();
            services.AddScoped<ConsultTenantServices>();
            services.AddScoped<DeleteTenantServices>();
            services.AddScoped<UpdateTenantServices>();
            services.AddScoped<AddMemberServices>();
            services.AddScoped<RemoveMemberServices>();
            services.AddScoped<AddLicenseToMemberServices>();

            services.AddScoped<ConsultEntryServices>();

            services.AddScoped<CreateProjectServices>();
            services.AddScoped<ConsultProjectServices>();
            services.AddScoped<DeleteProjectServices>();
            services.AddScoped<UpdateProjectServices>();

            services.AddScoped<CreateMeetingServices>();
            services.AddScoped<ConsultMeetingServices>();
            services.AddScoped<DeleteMeetingServices>();
            services.AddScoped<UpdateMeetingServices>();
            
            services.AddScoped<AddAttachmentServices>();
            services.AddScoped<AddNextMeetingServices>();
            services.AddScoped<AddParticipantServices>();
            services.AddScoped<AddProjectServices>();
            services.AddScoped<AddTaskServices>();
            services.AddScoped<RemoveAttachmentServices>();
            services.AddScoped<RemoveNextMeetingServices>();
            services.AddScoped<RemoveParticipantServices>();
            services.AddScoped<RemoveProjectServices>();
            services.AddScoped<RemoveTaskServices>();
        }
    }
}

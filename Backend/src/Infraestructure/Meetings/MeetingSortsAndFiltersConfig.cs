

using System;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingSortsAndFiltersConfig : BaseSortsAndFiltersConfig<Meeting>
    {

        public MeetingSortsAndFiltersConfig() : base(x => x.BusinessId, x => true) {}

        protected override void AddPossibleSorts()
        {
            this.Sorts.Add("tenant", x => x.Tenant.Value);
        }

        protected override void AddPossibleFilters()
        {
        }

        protected override void AddSearchFunction()
        {
            this.SearchQuery = (x, y) => true;
        }
    }
}
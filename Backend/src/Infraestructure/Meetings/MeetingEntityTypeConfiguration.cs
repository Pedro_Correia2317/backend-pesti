

using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Meetings
{
    public class MeetingEntityTypeConfiguration : IEntityTypeConfiguration<Meeting>
    {
        public void Configure(EntityTypeBuilder<Meeting> builder)
        {
            builder.ToTable("Reuniões");
            builder.Property(t => t.BusinessId).HasConversion(x => x.Value, x => new MeetingId(x)).HasColumnName("Id");
            builder.HasKey(x => x.BusinessId);
            builder.OwnsOne(x => x.StartDate).Property(x => x.Date).HasConversion(x => x.GetValue(), x => Date.Create(x)).HasColumnName("Data_Início");
            builder.OwnsOne(x => x.EndDate).Property(x => x.Date).HasConversion(x => x.GetValue(), x => Date.Create(x)).HasColumnName("Data_Fim");
            builder.OwnsOne(x => x.TranscriptLink).Property(x => x.Link).HasConversion(x => x.GetValue(), x => Url.Create(x)).HasColumnName("Link_Transcript");
            builder.OwnsOne(x => x.RecordingLink).Property(x => x.Link).HasConversion(x => x.GetValue(), x => Url.Create(x)).HasColumnName("Link_Gravação");
            builder.OwnsOne(x => x.Description).Property(x => x.Description).HasColumnName("Descrição");
            builder.OwnsOne(x => x.Dependencies).Property(x => x.Dependencies).HasColumnName("Dependências");
            builder.OwnsOne(x => x.NextSteps).Property(x => x.NextSteps).HasColumnName("Próximos_Passos");
            builder.OwnsOne(x => x.Decisions).Property(x => x.Decisions).HasColumnName("Decisões");
            builder.OwnsOne(x => x.Project).Property(x => x.Value).HasColumnName("Projeto");
            builder.OwnsOne(x => x.NextMeeting).Property(x => x.Value).HasColumnName("Próxima_Reunião");
            builder.OwnsOne(x => x.Tenant).Property(x => x.Value).HasColumnName("Tenant");
            builder.OwnsOne(x => x.Creator).Property(x => x.Value).HasColumnName("Creador");
            builder.OwnsOne(x => x.Participants, x => Configure(x));
            builder.OwnsOne(x => x.Attachments, x => Configure(x));
            builder.OwnsOne(x => x.Tasks, x => Configure(x));
        }

        public void Configure<T>(OwnedNavigationBuilder<T, MeetingParticipantList> rob) where T : class
        {
            var builder = rob.OwnsMany(x => x.ParticipantList).ToTable("Participantes_Reunião");
            builder.Property(x => x.User).HasConversion(x => x.Value, x => new AccountId(x)).HasColumnName("Id_Conta");
            builder.Property(x => x.Role).HasConversion(x => x.Name, x => MeetingRoles.GetByName(x)).HasColumnName("Papel_Reunião");
            builder.Property(x => x.Type).HasConversion(x => x.Name, x => MeetingParticipantTypes.GetByName(x)).HasColumnName("Tipo_Participante");
        }

        public void Configure<T>(OwnedNavigationBuilder<T, MeetingAttachmentList> rob) where T : class
        {
            var builder = rob.OwnsMany(x => x.AttachmentList).ToTable("Anexos_Reunião");
            builder.Property(x => x.File).HasConversion(x => x.Value, x => File.Create(x)).HasColumnName("Ficheiro");
            builder.Property(x => x.Name).HasColumnName("Nome");
        }

        public void Configure<T>(OwnedNavigationBuilder<T, MeetingTaskList> rob) where T : class
        {
            var builder = rob.OwnsMany(x => x.TaskList).ToTable("Tarefas_Reunião");
            builder.OwnsOne(x => x.Name).Property(x => x.Name).HasColumnName("Nome_Tarefa");
            builder.OwnsOne(x => x.Description).Property(x => x.Description).HasColumnName("Descrição_Tarefa");
            builder.OwnsOne(x => x.StartDate).Property(x => x.Date).HasConversion(x => x.Value, x => Date.Create(x)).HasColumnName("Data_Início_Tarefa");
            builder.OwnsOne(x => x.EndDate).Property(x => x.Date).HasConversion(x => x.Value, x => Date.Create(x)).HasColumnName("Data_Fim_Tarefa");
            builder.OwnsOne(x => x.ExpectedEndDate).Property(x => x.Date).HasConversion(x => x.Value, x => Date.Create(x)).HasColumnName("Data_Esperada_Fim_Tarefa");
            builder.Property(x => x.Responsible).HasConversion(x => x.Value, x => new AccountId(x)).HasColumnName("Responsável_Tarefa");
        }
    }
}


using System;
using System.Linq;
using System.Threading.Tasks;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Meetings
{
    public class MeetingRepositoryImpl : BaseRepository<Meeting, MeetingId>, IMeetingRepository
    {
        public MeetingRepositoryImpl(BackendDBContext context)
            : base(context.Meetings, new MeetingSortsAndFiltersConfig()) {}

        public override Task<QueryResults<Meeting>> FindById(MeetingId Id)
        {
            return FindByIdAux(Id, MeetingErrors.UNKNOWN_ID);
        }

        public Task<PageQueryResults<Meeting>> FindByPageWithTenant(QueryParams Params, Tenant tenant)
        {
            return FindByPageAux(Params, x => x.Tenant.Equals(tenant.BusinessId));
        }
    }
}
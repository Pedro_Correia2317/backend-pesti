

using Backend.Accounts;
using Backend.Tenants;
using Backend.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.HistoryEntries
{
    public class HistoryEntryEntityTypeConfiguration : IEntityTypeConfiguration<HistoryEntry>
    {
        public void Configure(EntityTypeBuilder<HistoryEntry> builder)
        {
            builder.ToTable("Histórico");
            builder.Property(t => t.BusinessId).HasConversion(x => x.Value, x => new EntryId(x)).HasColumnName("Id");
            builder.HasKey(x => x.BusinessId);
            builder.OwnsOne(x => x.Date, x => this.Configure(x));
            builder.OwnsOne(x => x.Description).Property(x => x.Description).HasColumnName("Descrição");
            //builder.HasOne<Account>().WithMany().HasForeignKey(x => x.Account);
            //builder.HasOne<Tenant>().WithMany().HasForeignKey(x => x.Tenant);
            builder.OwnsOne(x => x.Account).Property(x => x.Value).HasColumnName("Conta");
            builder.OwnsOne(x => x.Tenant).Property(x => x.Value).HasColumnName("Tenant");
            builder.OwnsOne(x => x.Address).Property(x => x.Address).HasConversion(x => x.GetValue(), x => InternetAddress.Create(x)).HasColumnName("Endereço");
            builder.Property(x => x.Type).HasColumnName("Tipo_Operação").HasConversion<string>(x => x.Name, x => EntryTypes.GetByName(x));
        }

        public void Configure<T>(OwnedNavigationBuilder<T, EntryDate> rob) where T : class
        {
            rob.Property(x => x.Date).HasConversion(x => x.GetValue(), x => Date.Create(x)).HasColumnName("Data");
        }
    }
}
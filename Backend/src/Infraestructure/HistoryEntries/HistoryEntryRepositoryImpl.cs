

using System;
using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class HistoryEntryRepositoryImpl : BaseRepository<HistoryEntry, EntryId>, IHistoryEntryRepository
    {
        public HistoryEntryRepositoryImpl(BackendDBContext context)
            : base(context.HistoryEntries, new HistoryEntrySortsAndFiltersConfig()) { }


        public override Task<QueryResults<HistoryEntry>> FindById(EntryId Id)
        {
            return FindByIdAux(Id, EntryErrors.UNKNOWN_ID);
        }

        public Task<PageQueryResults<HistoryEntry>> FindByPage(EntryQueryParams args)
        {
            return FindByPageAux(args, 
                args.tenant == ""? x => true : x => x.Tenant == null? false : x.Tenant.Value == args.tenant, 
                args.account == ""? x => true : x => x.Account == null? false : x.Account.Value == args.account);
        }
    }
}
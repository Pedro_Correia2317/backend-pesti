

using System;
using Backend.Utils;

namespace Backend.HistoryEntries
{
    public class HistoryEntrySortsAndFiltersConfig : BaseSortsAndFiltersConfig<HistoryEntry>
    {

        public HistoryEntrySortsAndFiltersConfig() : base(x => x.BusinessId, x => true) {}

        protected override void AddPossibleSorts()
        {
            this.Sorts.Add("date", x => x.Date.Date.GetValue());
        }

        protected override void AddPossibleFilters()
        {
            this.Filters.Add("dateBiggerThan2020", x => x.Date.Date.BiggerThan("01/01/2020"));
        }

        protected override void AddSearchFunction()
        {
            this.SearchQuery = (x, y) => x.Description.Description.Contains(y, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
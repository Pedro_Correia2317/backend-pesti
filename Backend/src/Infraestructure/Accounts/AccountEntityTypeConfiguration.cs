

using Backend.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Accounts
{
    public class AccountEntityTypeConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.ToTable("Contas");
            builder.Property(t => t.BusinessId).HasConversion(x => x.Value, x => new AccountId(x)).HasColumnName("Id");
            builder.HasKey(x => x.BusinessId);
            builder.OwnsOne(x => x.Name).Property(x => x.Name).HasColumnName("Nome");
            builder.Property(x => x.Email).HasColumnName("Email").HasConversion<string>(x => x.Email.GetValue(), x => new AccountEmail(x));
            builder.HasIndex(x => x.Email).IsUnique();
            builder.OwnsOne(x => x.Status).Property(x => x.Name).HasColumnName("Status");
            builder.OwnsOne(x => x.Type).Property(x => x.Name).HasColumnName("Tipo");
        }
    }
}
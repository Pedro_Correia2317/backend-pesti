

using System;
using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountSortsAndFiltersConfig : BaseSortsAndFiltersConfig<Account>
    {

        public AccountSortsAndFiltersConfig() : base(x => x.BusinessId, x => true) {}

        protected override void AddPossibleSorts()
        {
            this.Sorts.Add("name", x => x.Name.Name);
            this.Sorts.Add("email", x => x.Email.Email);
            this.Sorts.Add("status", x => x.Status.Name);
            this.Sorts.Add("type", x => x.Type.Name);
        }

        protected override void AddPossibleFilters()
        {
            this.Filters.Add("suspended", x => x.Status.Name == "Suspended");
            this.Filters.Add("active", x => x.Status.Name == "Active");
            this.Filters.Add("banned", x => x.Status.Name == "Banned");
            this.Filters.Add("clients", x => x.Type.Name == "Client");
            this.Filters.Add("admins", x => x.Type.Name == "Admin");
            this.Filters.Add("superAdmins", x => x.Type.Name == "SuperAdmin");
        }

        protected override void AddSearchFunction()
        {
            this.SearchQuery = (x, y) => x.Name.Name.Contains(y, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}


using System;
using System.Linq;
using System.Threading.Tasks;
using Backend.Utils;

namespace Backend.Accounts
{
    public class AccountRepositoryImpl : BaseRepository<Account, AccountId>, IAccountRepository
    {
        public AccountRepositoryImpl(BackendDBContext context)
            : base(context.Accounts, new AccountSortsAndFiltersConfig()) {}

        public override Task<QueryResults<Account>> FindById(AccountId Id)
        {
            return FindByIdAux(Id, AccountErrors.UNKNOWN_ID);
        }

        public Task<QueryResults<bool>> IsEmailUnique(AccountEmail email)
        {
            return Task.Run<QueryResults<bool>>(() =>
            {
                QueryResults<bool> results = new QueryResults<bool>();
                try
                {
                    int number = (from account in this.Objects where account.Email.Equals(email) select account).Count();
                    if (number == 0)
                    {
                        return results.withReturnedObject(true);
                    }
                    return results.withFailure(AccountErrors.REPEATED_EMAIL.Copy().WithParams(email.Email));
                }
                catch (Exception ex)
                {
                    return results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
            });
        }
        public Task<QueryResults<Account>> FindByEmail(AccountEmail email)
        {
            return Task.Run<QueryResults<Account>>(() =>
            {
                QueryResults<Account> results = new QueryResults<Account>();
                try
                {
                    Account account = (from acc in this.Objects where acc.Email.Equals(email) select acc).SingleOrDefault();
                    if (account == null)
                    {
                        return results.withFailure(AccountErrors.UNKNOWN_EMAIL.Copy().WithParams(email.Email));
                    }
                    return results.withReturnedObject(account);
                }
                catch (Exception ex)
                {
                    return results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
            });
        }
    }
}
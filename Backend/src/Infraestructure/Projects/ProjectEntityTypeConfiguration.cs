

using Backend.Tenants;
using Backend.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Projects
{
    public class ProjectEntityTypeConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Projetos");
            builder.Property(t => t.BusinessId).HasConversion(x => x.Value, x => new ProjectId(x)).HasColumnName("Id");
            builder.HasKey(x => x.BusinessId);
            builder.OwnsOne(x => x.Name).Property(x => x.Name).HasColumnName("Nome");
            builder.OwnsOne(x => x.Description).Property(x => x.Description).HasColumnName("Descrição");
            builder.Property(x => x.Tenant).HasConversion(x => x.Value, x => new TenantId(x)).HasColumnName("Tenant");
            //builder.HasOne(x => x.Tenant).WithMany().HasForeignKey();
        }
    }
}
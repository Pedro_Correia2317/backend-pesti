

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Tenants;
using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectRepositoryImpl : BaseRepository<Project, ProjectId>, IProjectRepository
    {
        public ProjectRepositoryImpl(BackendDBContext context)
            : base(context.Projects, new ProjectSortsAndFiltersConfig()) {}

        public override Task<QueryResults<Project>> FindById(ProjectId Id)
        {
            return FindByIdAux(Id, ProjectErrors.UNKNOWN_ID);
        }

        public Task<PageQueryResults<Project>> FindByPageWithTenant(QueryParams Params, Tenant tenant)
        {
            return FindByPageAux(Params, x => x.Tenant.Equals(tenant.BusinessId));
        }
    }
}
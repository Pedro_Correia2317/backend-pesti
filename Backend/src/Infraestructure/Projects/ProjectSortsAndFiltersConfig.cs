

using System;
using Backend.Utils;

namespace Backend.Projects
{
    public class ProjectSortsAndFiltersConfig : BaseSortsAndFiltersConfig<Project>
    {

        public ProjectSortsAndFiltersConfig() : base(x => x.BusinessId, x => true) {}

        protected override void AddPossibleSorts()
        {
            this.Sorts.Add("name", x => x.Name.Name);
            this.Sorts.Add("description", x => x.Description.Description);
            this.Sorts.Add("tenant", x => x.Tenant.Value);
        }

        protected override void AddPossibleFilters()
        {
        }

        protected override void AddSearchFunction()
        {
            this.SearchQuery = (x, y) => x.Description.Description.Contains(y, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}


using System;
using System.Threading.Tasks;

namespace Backend.Utils
{
    public class UnitOfWorkImpl : IUnitOfWork
    {
        private readonly BackendDBContext _context;

        public UnitOfWorkImpl(BackendDBContext context)
        {
            this._context = context;
        }

        public Task<QueryResults<bool>> Commit()
        {
            return Task.Run<QueryResults<bool>>(() => {
                QueryResults<bool> results;
                try {
                    this._context.SaveChanges();
                    results = new QueryResults<bool>().withReturnedObject(true);
                } catch (Exception ex){
                    results = new QueryResults<bool>().withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }
    }
}

using System;
using Backend.Accounts;
using Backend.HistoryEntries;
using Backend.Licenses;
using Backend.Meetings;
using Backend.Projects;
using Backend.Tenants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace Backend.Utils
{
    public class BackendDBContext : DbContext
    {
        public DbSet<License> Licenses { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<HistoryEntry> HistoryEntries { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Meeting> Meetings { get; set; }

        public BackendDBContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LicenseEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AccountEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TenantEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new HistoryEntryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MeetingEntityTypeConfiguration());
        }

    }
}
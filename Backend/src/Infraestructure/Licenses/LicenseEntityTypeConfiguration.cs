

using Backend.Accounts;
using Backend.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Licenses
{
    public class LicenseEntityTypeConfiguration : IEntityTypeConfiguration<License>
    {
        public void Configure(EntityTypeBuilder<License> builder)
        {
            builder.ToTable("Licenças");
            builder.Property(t => t.BusinessId).HasConversion(x => x.Value, x => new LicenseId(x)).HasColumnName("Id");
            builder.HasKey(x => x.BusinessId);
            builder.OwnsOne(x => x.Date, x => this.Configure(x));
            builder.OwnsOne(x => x.Duration, x => this.Configure(x));
            builder.OwnsOne(x => x.NumberMeetings).Property(x => x.NumberMeetings).HasColumnName("Reuniões_Maximas");
            builder.OwnsOne(x => x.RemainingMeetings).Property(x => x.RemainingMeetings).HasColumnName("Reuniões_Restantes");
            builder.OwnsOne(x => x.Account).Property(x => x.Value).HasColumnName("Conta");
            //builder.OwnsOne(x => x.Account).HasOne(x => x).WithOne().HasForeignKey<Account>(x => x.BusinessId.Value);
            builder.Property(x => x.Type).HasColumnName("Permissões").HasConversion<string>(x => x.Name, x => LicenseTypes.GetByName(x));
        }

        public void Configure<T>(OwnedNavigationBuilder<T, LicenseDate> rob) where T : class
        {
            rob.Property(x => x.Date).HasConversion(x => x.GetValue(), x => Date.Create(x)).HasColumnName("Data_Início");
        }

        public void Configure<T>(OwnedNavigationBuilder<T, LicenseDuration> rob) where T : class
        {
            rob.Property(x => x.Duration).HasConversion(x => x.GetValue(), x => Duration.Create(x)).HasColumnName("Duração");
        }
    }
}
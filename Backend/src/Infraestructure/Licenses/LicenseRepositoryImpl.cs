

using System;
using System.Linq;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseRepositoryImpl : BaseRepository<License, LicenseId>, ILicenseRepository
    {
        public LicenseRepositoryImpl(BackendDBContext context)
            : base(context.Licenses, new LicenseSortsAndFiltersConfig()) { }


        public override Task<QueryResults<License>> FindById(LicenseId Id)
        {
            return FindByIdAux(Id, LicenseErrors.UNKNOWN_ID);
        }

        public Task<QueryResults<bool>> HasNoLicense(AccountId account)
        {
            return Task.Run<QueryResults<bool>>(() =>
            {
                QueryResults<bool> results = new QueryResults<bool>();
                try
                {
                    int aux = (from entity in this.Objects where entity.Account.Value == account.Value select entity).Count();
                    if(aux > 0)
                    {
                        return results.withFailure(LicenseErrors.ACCOUNT_ALREADY_HAS_LICENSE.Copy().WithParams(account.Value));
                    }
                    results = results.withReturnedObject(true);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        public Task<QueryResults<License>> FindByAccount(AccountId account)
        {
            return Task.Run<QueryResults<License>>(() =>
            {
                QueryResults<License> results = new QueryResults<License>();
                try
                {
                    License license = (from entity in this.Objects where entity.Account.Value == account.Value select entity).SingleOrDefault();
                    if(license == null)
                    {
                        return results.withFailure(LicenseErrors.ACCOUNT_DOES_NOT_HAVE_LICENSE.Copy().WithParams(account.Value));
                    }
                    results = results.withReturnedObject(license);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }
    }
}
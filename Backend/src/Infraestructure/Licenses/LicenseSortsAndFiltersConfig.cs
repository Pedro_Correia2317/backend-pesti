

using Backend.Utils;

namespace Backend.Licenses
{
    public class LicenseSortsAndFiltersConfig : BaseSortsAndFiltersConfig<License>
    {

        public LicenseSortsAndFiltersConfig() : base(x => x.BusinessId, x => true) {}

        protected override void AddPossibleSorts()
        {
            this.Sorts.Add("date", x => x.Date.Date.GetValue());
            this.Sorts.Add("numberMeetings", x => x.NumberMeetings.NumberMeetings);
            this.Sorts.Add("remainingMeetings", x => x.RemainingMeetings.RemainingMeetings);
            this.Sorts.Add("type", x => x.Type.GetName());
            this.Sorts.Add("duration", x => x.Duration.Duration);
        }

        protected override void AddPossibleFilters()
        {
            this.Filters.Add("dateBiggerThan2020", x => x.Date.Date.BiggerThan("01/01/2020"));
            this.Filters.Add("typeBasic", x => x.Type.GetName() == "Basic");
            this.Filters.Add("durationBiggerThan1Year", x => x.Duration.Duration.BiggerThan("P1Y"));
        }

        protected override void AddSearchFunction()
        {
            this.SearchQuery = (x, y) => true;
        }
    }
}
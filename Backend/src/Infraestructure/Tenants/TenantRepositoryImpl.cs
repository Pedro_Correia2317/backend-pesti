

using System;
using System.Linq;
using System.Threading.Tasks;
using Backend.Accounts;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantRepositoryImpl : BaseRepository<Tenant, TenantId>, ITenantRepository
    {
        public TenantRepositoryImpl(BackendDBContext context)
            : base(context.Tenants, new TenantSortsAndFiltersConfig()) {}

        public override Task<QueryResults<Tenant>> FindById(TenantId Id)
        {
            return FindByIdAux(Id, TenantErrors.UNKNOWN_ID);
        }

        public Task<QueryResults<Tenant>> FindByMember(AccountId account)
        {
            return Task.Run<QueryResults<Tenant>>(() =>
            {
                QueryResults<Tenant> results = new QueryResults<Tenant>();
                try
                {
                    Tenant aux = (from entity in this.Objects where entity.Members.Members.Any(x => x.Account.Equals(account)) select entity).SingleOrDefault<Tenant>();
                    if (aux == null)
                    {
                        return results.withFailure(TenantErrors.ACCOUNT_HAS_NO_TENANT.Copy().WithParams(account.Value));
                    }
                    results = results.withReturnedObject(aux);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        public Task<QueryResults<bool>> DoesNotHaveMember(AccountId account)
        {
            return Task.Run<QueryResults<bool>>(() =>
            {
                QueryResults<bool> results = new QueryResults<bool>();
                try
                {
                    int count = (from entity in this.Objects where entity.Members.Members.Any(x => x.Account.Equals(account)) select entity).Count();
                    if (count > 0)
                    {
                        return results.withFailure(TenantErrors.ACCOUNT_ALREADY_BELONGS_TO_TENANT.Copy().WithParams(account.Value));
                    }
                    results = results.withReturnedObject(true);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }
    }
}
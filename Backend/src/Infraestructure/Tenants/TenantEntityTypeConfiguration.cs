

using Backend.Accounts;
using Backend.Licenses;
using Backend.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Tenants
{
    public class TenantEntityTypeConfiguration : IEntityTypeConfiguration<Tenant>
    {
        public void Configure(EntityTypeBuilder<Tenant> builder)
        {
            builder.ToTable("Tenants");
            builder.Property(t => t.BusinessId).HasConversion(x => x.Value, x => new TenantId(x)).HasColumnName("Id");
            builder.HasKey(x => x.BusinessId);
            builder.OwnsOne(x => x.Licenses, x => this.Configure(x));
            builder.OwnsOne(x => x.Members, x => this.Configure(x));
            builder.OwnsOne(x => x.Name).Property(x => x.Name).HasColumnName("Nome");
        }

        public void Configure<T>(OwnedNavigationBuilder<T, TenantMembers> rob) where T : class
        {
            var builder = rob.OwnsMany(x => x.Members).ToTable("Membros_Tenant");
            //builder.HasOne<Account>().WithMany().HasForeignKey(x => x.Account);
            builder.Property(x => x.Account).HasConversion(x => x.Value, x => new AccountId(x)).HasColumnName("Id_Conta");
            builder.HasKey(x => x.Account);
            builder.Property(x => x.Permissions).HasColumnName("Permissões")
                .HasConversion(x => x.Name, x => TenantPermissionList.GetByName(x));
        }

        public void Configure<T>(OwnedNavigationBuilder<T, TenantLicenses> rob) where T : class
        {
            var builder = rob.OwnsMany(x => x.Licenses).ToTable("Licenças_Tenant");
            //builder.HasOne<License>().WithMany().HasForeignKey(x => x);
            builder.Property(x => x.Value).HasColumnName("Id_Licença");
            //var builder = rob.ToTable("Licenças_Tenant");
            //builder.Property(x => x.Licenses).HasColumnName("Id_Licença");
            builder.HasKey(x => x.Value);
        }
    }
}


using System;
using Backend.Utils;

namespace Backend.Tenants
{
    public class TenantSortsAndFiltersConfig : BaseSortsAndFiltersConfig<Tenant>
    {

        public TenantSortsAndFiltersConfig() : base(x => x.BusinessId, x => true) {}

        protected override void AddPossibleSorts()
        {
            this.Sorts.Add("name", x => x.Name.Name);
        }

        protected override void AddPossibleFilters()
        {
        }

        protected override void AddSearchFunction()
        {
            this.SearchQuery = (x, y) => x.Name.Name.Contains(y, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}


using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Backend.Utils
{
    public abstract class BaseSortsAndFiltersConfig<T>
    {

        protected Dictionary<string, Func<T, IComparable>> Sorts;

        protected Func<T, IComparable> DefaultSortQuery;

        protected Dictionary<string, Func<T, bool>> Filters;

        public Func<T, bool> DefaultFilterQuery { get; private set; }

        public Func<T, string,  bool> SearchQuery { get; protected set; }

        protected BaseSortsAndFiltersConfig(Func<T, IComparable> sort, Func<T, bool> filter)
        {
            this.Sorts = new Dictionary<string, Func<T, IComparable>>();
            this.Filters = new Dictionary<string, Func<T, bool>>();
            AddPossibleSorts();
            AddPossibleFilters();
            AddSearchFunction();
            SetDefaultFilter(filter);
            SetDefaultSort(sort);
        }

        protected abstract void AddPossibleSorts();

        protected abstract void AddPossibleFilters();

        protected abstract void AddSearchFunction();

        protected void SetDefaultSort(Func<T, IComparable> sort)
        {
            this.DefaultSortQuery = sort;
        }

        protected void SetDefaultFilter(Func<T, bool> filter)
        {
            this.DefaultFilterQuery = filter;
        }

        public Func<T, IComparable> SortQueryOrDefault(string queryName)
        {
            Func<T, IComparable> query = this.Sorts.GetValueOrDefault(queryName);
            return query == null? this.DefaultSortQuery : query;
        }

        public Func<T, bool> FilterQueryOrDefault(string queryName)
        {
            Func<T, bool> query = this.Filters.GetValueOrDefault(queryName);
            return query == null? this.DefaultFilterQuery : query;
        }
    }
}
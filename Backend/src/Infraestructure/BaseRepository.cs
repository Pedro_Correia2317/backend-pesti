

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Backend.Utils
{
    public abstract class BaseRepository<TEntity, TEntityId> : IRepository<TEntity, TEntityId>
        where TEntity : Entity<TEntityId>
        where TEntityId : EntityId
    {

        protected readonly DbSet<TEntity> Objects;

        private BaseSortsAndFiltersConfig<TEntity> SortsAndFilters;

        public BaseRepository(DbSet<TEntity> Objects, BaseSortsAndFiltersConfig<TEntity> SortsAndFilters)
        {
            this.Objects = Objects;
            this.SortsAndFilters = SortsAndFilters;
        }

        public Task<QueryResults<bool>> Save(TEntity element)
        {
            return Task.Run<QueryResults<bool>>(() =>
            {
                QueryResults<bool> results = new QueryResults<bool>();
                try
                {
                    this.Objects.Add(element);
                    results = results.withReturnedObject(true);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        public Task<QueryResults<bool>> SaveAll(ICollection<TEntity> list)
        {
            return Task.Run<QueryResults<bool>>(() =>
            {
                QueryResults<bool> results = new QueryResults<bool>();
                try
                {
                    foreach (TEntity element in list)
                    {
                        this.Objects.Add(element);
                    }
                    results = results.withReturnedObject(true);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        public Task<QueryResults<bool>> Delete(TEntity element)
        {
            return Task.Run<QueryResults<bool>>(() =>
            {
                QueryResults<bool> results = new QueryResults<bool>();
                try
                {
                    this.Objects.Remove(element);
                    results = results.withReturnedObject(true);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        public Task<QueryResults<bool>> Update(TEntity element)
        {
            return Task.Run<QueryResults<bool>>(() =>
            {
                QueryResults<bool> results = new QueryResults<bool>();
                try
                {
                    this.Objects.Update(element);
                    results = results.withReturnedObject(true);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        protected Task<QueryResults<TEntity>> FindByIdAux(TEntityId Id, Error notFoundError)
        {
            return Task.Run<QueryResults<TEntity>>(() =>
            {
                QueryResults<TEntity> results = new QueryResults<TEntity>();
                try
                {
                    TEntity aux = (from entity in this.Objects where entity.BusinessId == Id select entity).SingleOrDefault<TEntity>();
                    if (aux == null)
                    {
                        return results.withFailure(notFoundError.Copy().WithParams(Id.Value));
                    }
                    results = results.withReturnedObject(aux);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        public virtual Task<QueryResults<TEntity>> FindById(TEntityId Id)
        {
            return FindByIdAux(Id, GeneralErrors.UNKNOWN_ID);
        }

        public async Task<QueryResults<bool>> AreIdsValid(List<TEntityId> ids)
        {
            QueryResults<bool> results = new QueryResults<bool>();
            try
            {
                foreach (TEntityId id in ids)
                {
                    int num = await (from entity in this.Objects where entity.BusinessId.Value == id.Value select entity).CountAsync<TEntity>();
                    if (num <= 0)
                    {
                        return results.withFailure(GeneralErrors.UNKNOWN_ID.Copy().WithParams(id.Value));
                    }
                }
                results = results.withReturnedObject(true);
            }
            catch (Exception ex)
            {
                results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
            }
            return results;
        }

        protected Task<PageQueryResults<TEntity>> FindByPageAux(QueryParams Params,
                params Func<TEntity, bool>[] extraFilterFunctions)
        {
            return Task.Run<PageQueryResults<TEntity>>(() =>
            {
                PageQueryResults<TEntity> results = new PageQueryResults<TEntity>();
                try
                {
                    IEnumerable<TEntity> query = BuildQuery(Params, extraFilterFunctions);
                    int count = query.Count();
                    results = results.withReturnedObject(ApplyCut(query, Params).ToList(), count);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        public Task<PageQueryResults<TEntity>> FindByPage(QueryParams Params)
        {
            return FindByPageAux(Params);
        }

        public Task<QueryResults<bool>> isIdUnique(TEntityId Id)
        {
            throw new System.NotImplementedException();
        }

        public Task<QueryResults<int>> CountElements(QueryParams Params)
        {
            return Task.Run<QueryResults<int>>(() =>
            {
                QueryResults<int> results = new QueryResults<int>();
                try
                {
                    int aux = BuildQuery(Params).Count<TEntity>();
                    results = results.withReturnedObject(aux);
                }
                catch (Exception ex)
                {
                    results = results.withFailure(GeneralErrors.DB_ERROR.Copy().WithParams(ex.Message));
                }
                return results;
            });
        }

        private IEnumerable<TEntity> BuildQuery(QueryParams param, params Func<TEntity, bool>[] extraFilterFunctions)
        {
            IEnumerable<TEntity> query = (from account in this.Objects select account);
            Func<TEntity, IComparable> sortQuery = this.SortsAndFilters.SortQueryOrDefault(param.sort);
            bool or = "or".Equals(param.condition, StringComparison.InvariantCultureIgnoreCase);
            Func<TEntity, bool> filters = or? BuildFiltersOr(param) : BuildFiltersAnd(param);
            query = query.Where(filters);
            foreach (var function in extraFilterFunctions)
            {
                query = query.Where(function);
            }
            query = query.Where(x => this.SortsAndFilters.SearchQuery(x, param.search));
            return param.order == "desc" ? query.OrderByDescending(sortQuery) : query.OrderBy(sortQuery);
        }
        
        private static Func<TEntity, bool> And(Func<TEntity, bool> left, Func<TEntity, bool> right)
        {
            return a => left(a) && right(a);
        }
        
        private static Func<TEntity, bool> Or(Func<TEntity, bool> left, Func<TEntity, bool> right)
        {
            return a => left(a) || right(a);
        }

        private Func<TEntity, bool> BuildFiltersAnd(QueryParams param)
        {
            Func<TEntity, bool> filterQuery = this.SortsAndFilters.DefaultFilterQuery;
            if (param.filters != null)
            {
                foreach (string filter in param.filters)
                {
                    filterQuery = And(filterQuery, this.SortsAndFilters.FilterQueryOrDefault(filter));
                }
            }
            return filterQuery;
        }

        private Func<TEntity, bool> BuildFiltersOr(QueryParams param)
        {
            Func<TEntity, bool> filterQuery = (x) => false;
            if (param.filters != null)
            {
                foreach (string filter in param.filters)
                {
                    filterQuery = Or(filterQuery, this.SortsAndFilters.FilterQueryOrDefault(filter));
                }
            }
            return (x) => filterQuery(x);
        }

        private IEnumerable<TEntity> ApplyCut(IEnumerable<TEntity> query, QueryParams param)
        {
            return query.Skip(param.size * (param.page - 1)).Take(param.size);
        }

    }
}